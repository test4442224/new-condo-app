# Mobile automation tests

> **NOTE:**
> This supports both Native and Hybrid for the iOS and Android

> This uses the Appium and the WebdriverIO

## Prerequiste

1. Setting up Appium See [Setting up Condo App Mobile E2E Test](https://mavericks-consulting.atlassian.net/wiki/spaces/CMA/pages/280887297/Setting+up+Condo+App+Mobile+E2E+Test)
2. Setup your local machine to use an Android emulator and an iOS simulator

## Quick start

Choose one of the following options:

1. Clone the git repo

2. Run `npm i` inside the repo

3. Modify the path of the `app` in the `wdio.conf.android` or `wdio.conf.ios` files

4. Start the appium server and start Metro if its not running anywhere in the machine

5. Run the tests for iOS with `npm run test-ios` and for Android with `npm run test-android`

Please refer to guide for more information:
[Running E2E for Condo Mobile Application](https://mavericks-consulting.atlassian.net/wiki/spaces/CMA/pages/304087042/Running+E2E+Test+for+Condo+Mobile+Application)
