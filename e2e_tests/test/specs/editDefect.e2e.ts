import LoginPage from '../pageobjects/login-page';
import editDefectPage from '../pageobjects/Defect/editDefect-page';

var loginCredentials = require('../../testdata/login.json');

describe('Edit Defect Workflow', () => {
    it('Should log in, navigate to the edit defect page and edit an existing defect', async () => {
        await LoginPage.login(
            loginCredentials.defectUserLoginEmail,
            loginCredentials.defectUserPassword,
        );
        await editDefectPage.navigateToEditDefectPageFromHomePage();
        await editDefectPage.submitDefectReport(
            ', Blk B',
            ' There is no power.',
        );
    });
});
