import managePayments from '../../pageObjects/Payments/managePayments';
import viewPaymentDetail from '../../pageObjects/Payments/viewPaymentDetail';
import loginPage from '../../pageObjects/login-page';
import ViewPaymentInstructionPage from '../../pageObjects/Payments/pendingCashPayment';

var loginCredentials = require('../../../testdata/login.json');

describe('Make cash payment', () => {
    it('should successfully make a cash payment', async () => {
        await loginPage.login(
            loginCredentials.loginEmail,
            loginCredentials.password,
        );

        await managePayments.navigateToManagePaymentsFromHomepage();
        await managePayments.navigateToViewPayment();
        await viewPaymentDetail.selectCashPayment();
        await ViewPaymentInstructionPage.confirmPaymentInstruction();
        await expect(managePayments.paymentPageHeader).toBeDisplayed();
    });
});
