import managePayments from '../../pageObjects/Payments/managePayments';
import viewPaymentDetail from '../../pageObjects/Payments/viewPaymentDetail';
import loginPage from '../../pageObjects/login-page';
import pendingPaynowPayment from '../../pageObjects/Payments/pendingPaynowPayment';
import pendingVerification from '../../pageObjects/Payments/pendingVerification';

var loginCredentials = require('../../../testdata/login.json');

describe('Make PayNow payment', () => {
    it('should successfully upload image', async () => {
        await loginPage.login(
            loginCredentials.loginEmail,
            loginCredentials.password,
        );

        await managePayments.navigateToManagePaymentsFromHomepage();
        await managePayments.navigateToViewPayment();
        await viewPaymentDetail.selectPaynowPayment();
        await pendingPaynowPayment.selectImageForUpload();
        await pendingVerification.verifyHeader();
    });
});
