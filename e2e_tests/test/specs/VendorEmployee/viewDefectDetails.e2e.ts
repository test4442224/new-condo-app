import defectDetailsPage from '../../pageobjects/Defect/defectDetails-page';
import LoginPage from '../../pageobjects/login-page';
import profilePage from '../../pageobjects/profile-page';
import VendorEmployeeHomePage from '../../pageobjects/VendorEmployee/VendorEmployeeHome-page';
var loginCredentials = require('../../../testdata/login.json');

describe('View Defect Details', () => {
    it('should log in, and view assigned defect details, then log out', async () => {
        await LoginPage.login(
            loginCredentials.vendorEmployeeLoginEmail,
            loginCredentials.vendorEmployeePassword,
        );
        // verify vendor employee is directed to manage defects page upon log in
        await VendorEmployeeHomePage.verifyInVendorHomePage();
        // //click into defect card
        await VendorEmployeeHomePage.selectDefectCard();
        // verify in defect details page
        await defectDetailsPage.verifyInDefectDetailsPage();
        // navigate to vendor home page from defect details page
        await defectDetailsPage.navigateToVendorHomePageFromDetailsPage();
        //navigate to profile page
        await VendorEmployeeHomePage.navigateToProfilePageFromHomePage();
        // verify in profile page
        await profilePage.verifyInProfilePage();
        //logout
        await profilePage.logout();
        //verify in log in page
        await LoginPage.verifyInLoginPage();
    });
});
