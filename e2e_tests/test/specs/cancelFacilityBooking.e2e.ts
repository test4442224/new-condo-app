import LoginPage from '../pageObjects/login-page';
import BookingDetailPage from '../pageObjects/FacilityBookings/bookingDetail';
import ManageBookingPage from '../pageObjects/FacilityBookings/manageBooking';
var loginCredentials = require('../../testdata/login.json');
describe('Cancel Facility Booking Flow', () => {
    it('should log in, navigate to manage bookings, delete an upcoming booking that is paid ', async () => {
        //login action
        await LoginPage.login(
            loginCredentials.loginEmail,
            loginCredentials.password,
        );
        //navigates to Manage Booking Page from Home Page
        await ManageBookingPage.navigateToManageBookingsFromHomePage();
        //Clicks on the booking that is paid and cancel it
        await BookingDetailPage.cancelPaidBooking();
        //navigates to Manage Booking Page from Details Page
        await BookingDetailPage.NavigateToManageBookingsPageFromDetailsPage();
        //verify that the booking is cancelled : Not in Upcoming Tab, and in Past tab
        await ManageBookingPage.verifyCancelled();
    });
});
