import loginPage from '../../pageobjects/login-page';
import manageBookingPage from '../../pageobjects/FacilityBookings/manageBooking';
import selectFacilityType from '../../pageobjects/FacilityBookings/selectFacilityType';
import selectDate from '../../pageobjects/FacilityBookings/selectDate';
import selectFacilityAndTime from '../../pageobjects/FacilityBookings/selectFacilityAndTime';
import bookingStatus from '../../pageobjects/FacilityBookings/bookingStatus';
import reviewBookingDetailsPaynow from '../../pageobjects/FacilityBookings/reviewBookingDetailsPaynow';

var loginCredentials = require('../../../testdata/login.json');

describe('Book a paid facility with paynow as payment method', () => {
    it('should successfully make a booking', async () => {
        await loginPage.login(
            loginCredentials.loginEmail,
            loginCredentials.password,
        );

        await manageBookingPage.navigateToManageBookingsFromHomePage();
        const numberOfBookingsBefore =
            await manageBookingPage.getNumberOfBookings();

        await manageBookingPage.makeNewBooking();

        await selectFacilityType.selectFunctionRoom();

        await selectDate.selectDate();

        await selectFacilityAndTime.selectFacilityAndTimeSlot();

        await reviewBookingDetailsPaynow.selectCashPayment();

        await reviewBookingDetailsPaynow.confirmBooking();

        await reviewBookingDetailsPaynow.confirmPaynow();

        const header = await bookingStatus.getHeader();
        await expect(header).toHaveText('Pending Payment');

        await bookingStatus.navigateBackToBookings();

        await manageBookingPage.verifyNewBookingAdded(numberOfBookingsBefore);
    });
});
