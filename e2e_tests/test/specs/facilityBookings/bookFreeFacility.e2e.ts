import loginPage from '../../pageObjects/login-page';
import manageBookingPage from '../../pageObjects/FacilityBookings/manageBooking';
import selectFacilityType from '../../pageObjects/FacilityBookings/selectFacilityType';
import selectDate from '../../pageObjects/FacilityBookings/selectDate';
import selectFacilityAndTime from '../../pageObjects/FacilityBookings/selectFacilityAndTime';
import reviewBookingDetails from '../../pageObjects/FacilityBookings/reviewBookingDetails';
import bookingStatus from '../../pageObjects/FacilityBookings/bookingStatus';

var loginCredentials = require('../../../testdata/login.json');

describe('Book a free facility', () => {
    it('should successfully make a booking', async () => {
        await loginPage.login(
            loginCredentials.loginEmail,
            loginCredentials.password,
        );

        await manageBookingPage.navigateToManageBookingsFromHomePage();
        const numberOfBookingsBefore =
            await manageBookingPage.getNumberOfBookings();

        await manageBookingPage.makeNewBooking();

        await selectFacilityType.selectBadmintonHall();

        await selectDate.selectDate();

        await selectFacilityAndTime.selectFacilityAndTimeSlot();

        await reviewBookingDetails.confirmBooking();

        const header = await bookingStatus.getHeader();
        await expect(header).toHaveText('Booking Confirmed');

        await bookingStatus.navigateBackToBookings();

        await manageBookingPage.verifyNewBookingAdded(numberOfBookingsBefore);
    });
});
