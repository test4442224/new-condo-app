import LoginPage from '../pageObjects/login-page';
import manageDefectPage from '../pageObjects/Defect/manageDefect-page';
import defectDetailsPage from '../pageObjects/Defect/defectDetails-page';

var loginCredentials = require('../../testdata/login.json');
describe('Cancel Defect Flow', () => {
    it('should log in, navigate to manage defect, delete an active defect', async () => {
        //login action
        await LoginPage.login(
            loginCredentials.defectUserLoginEmail,
            loginCredentials.defectUserPassword,
        );
        // navigates to Manage Defect Page from Home Page
        await manageDefectPage.navigateToManageDefectsFromHomePage();
        // clicks on the defect card and cancel it
        await defectDetailsPage.cancelDefect();
        // navigates to Manage Defect Page from Details Page
        await defectDetailsPage.navigateToManageDefectsPageFromDetailsPage();
        // verify that the defect is cancelled, not in active tab, and in past tab
        await manageDefectPage.verifyCancelled();
    });
});
