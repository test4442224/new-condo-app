import { expect } from '@wdio/globals';
import LoginPage from '../pageObjects/login-page';
import AboutUsPage from '../pageObjects/aboutUs-page';

var loginCredentials = require('../../testdata/login.json');

describe('About Us Flow', () => {
    it('Login workflow', async () => {
        await LoginPage.login(
            loginCredentials.loginEmail,
            loginCredentials.password,
        );
        const condoImage = await $('~condoImage');
        await expect(condoImage).toBeDisplayed();
        await condoImage.click();
        await expect(AboutUsPage.HeaderBar).toBeDisplayed();
    });
});
