import { expect } from '@wdio/globals';
import LoginPage from '../../pageObjects/login-page';
import HomePage from '../../pageObjects/home-page';
var loginCredentials = require('../../../testdata/login.json');

describe('Home Page as a Non-Defect Reporter', () => {
    it('Non-Defect Reporter should not see the defect report button', async () => {
        await LoginPage.login(
            loginCredentials.loginEmail,
            loginCredentials.password,
        );
        await expect(HomePage.HeaderBar).toBeDisplayed();
        await expect(HomePage.manageDefectsButton).not.toBeDisplayed();
    });
});
