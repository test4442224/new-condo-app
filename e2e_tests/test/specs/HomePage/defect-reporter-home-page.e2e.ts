import { expect } from '@wdio/globals';
import LoginPage from '../../pageObjects/login-page';
import HomePage from '../../pageObjects/home-page';
var loginCredentials = require('../../../testdata/login.json');

describe('Home Page as a Defect Reporter', () => {
    it('Defect Reporter should see the defect report button', async () => {
        await LoginPage.login(
            loginCredentials.defectUserLoginEmail,
            loginCredentials.defectUserPassword,
        );
        await expect(HomePage.HeaderBar).toBeDisplayed();
        await expect(HomePage.manageDefectsButton).toBeDisplayed();
    });
});
