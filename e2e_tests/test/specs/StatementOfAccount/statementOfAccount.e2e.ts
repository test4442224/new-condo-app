import loginPage from '../../pageobjects/login-page';
import StatementOfAccountPage from '../../pageobjects/StatementOfAccount/StatementOfAccountPage';

var loginCredentials = require('../../../testdata/login.json');

describe('View Statement of Account', () => {
    it('should successfully upload image', async () => {
        await loginPage.login(
            loginCredentials.loginEmail,
            loginCredentials.password,
        );

        await StatementOfAccountPage.navigateToStatementOfAccountFromHomepage();
        await StatementOfAccountPage.verifyStatementOfAccount();
    });
});
