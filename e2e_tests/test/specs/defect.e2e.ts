// import defectDetailsPage from '../pageobjects/Defect/defectDetails-page';
import LoginPage from '../pageobjects/login-page';
import manageDefectPage from '../pageobjects/Defect/manageDefect-page';
import defectDetailsPage from '../pageobjects/Defect/defectDetails-page';

var loginCredentials = require('../../testdata/login.json');

describe('DefectDetails Flow', () => {
    it('should visit the defect details page', async () => {
        await LoginPage.login(
            loginCredentials.defectUserLoginEmail,
            loginCredentials.defectUserPassword,
        );
        // navigates to Manage Defect Page from Home Page
        await manageDefectPage.navigateToManageDefectsFromHomePage();
        await defectDetailsPage.defectDetails();
        await defectDetailsPage.clickOnPopUpModal();
    });
});
