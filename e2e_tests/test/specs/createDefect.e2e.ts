import LoginPage from '../pageObjects/login-page';
import createDefectPage from '../pageObjects/Defect/createDefect-page';
var loginCredentials = require('../../testdata/login.json');
var defectInformation = require('../../testdata/defect.json');

describe('Create Defect Workflow', () => {
    it('Should log in, navigate to the create defect page and create a defect', async () => {
        await LoginPage.login(
            loginCredentials.defectUserLoginEmail,
            loginCredentials.defectUserPassword,
        );
        await createDefectPage.navigateToCreateDefectPageFromHomePage();
        await createDefectPage.submitDefectReport(
            defectInformation.defectName,
            defectInformation.defectDescription,
        );
    });
});
