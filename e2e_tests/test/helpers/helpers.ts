// create helper class for some resuable methods you will need. Like verifying texts, selecting dropdowns.

const WAIT_FOR_SWIPE_FINISHED = 200;

export async function getOneByTestId(testId: string) {
    if (driver.isIOS) {
        return await $(`~${testId}`);
    } else {
        return await $(`android=new UiSelector().resourceId("${testId}")`);
    }
}

export async function getOneByText(text: string) {
    if (driver.isIOS) {
        return await $(`-ios predicate string:value == ${text}`);
    } else {
        return await $(`android=new UiSelector().text("${text}")`);
    }
}

export async function getAllByTestId(testId: string) {
    if (driver.isIOS) {
        return await $$(`~${testId}`);
    } else {
        return await $$(`android=new UiSelector().resourceId("${testId}")`);
    }
}

export async function getAllByTestIdStartsWith(testIdSubstring: string) {
    if (driver.isIOS) {
        return await $$(
            `-ios predicate string:name BEGINSWITH "${testIdSubstring}"`,
        );
    } else {
        return await $$(
            `android=new UiSelector().resourceIdMatches("${testIdSubstring}.*")`,
        );
    }
}

export async function swipeAndGetByTestId(
    text: string,
    maxScrolls: number,
): Promise<WebdriverIO.Element> {
    if (driver.isAndroid) {
        return swipeIntoViewAndroid(text, maxScrolls);
    }
    return swipeIntoViewIOS(text, maxScrolls);
}

async function swipeIntoViewAndroid(
    testId: string,
    maxScrolls: number,
    amount = 0,
): Promise<WebdriverIO.Element> {
    const scrollForward = `android=new UiScrollable(new UiSelector().scrollable(true)).scrollForward()`;
    const search = `android=new UiSelector().resourceId("${testId}")`;
    const element = $(search);
    if (!(await element.isDisplayed()) && amount <= maxScrolls) {
        await $(scrollForward);
        await driver.pause(WAIT_FOR_SWIPE_FINISHED);
        await swipeIntoViewAndroid(testId, maxScrolls, amount + 1);
    } else if (amount > maxScrolls) {
        // If the element is still not visible after the max amount of scroll let it fail
        throw new Error(
            `The element with testID '${testId}' could not be found or is not visible.`,
        );
    }
    return element;
}

async function swipeIntoViewIOS(
    testId: string,
    maxScrolls: number,
    amount = 0,
): Promise<WebdriverIO.Element> {
    // If the element is not displayed and we haven't scrolled the max amount of scrolls
    // then scroll and execute the method again
    const element = $(`~${testId}`);
    if (!(await element.isDisplayed()) && amount <= maxScrolls) {
        await driver.execute('mobile:swipe', { direction: 'up' });
        await driver.pause(WAIT_FOR_SWIPE_FINISHED);
        await swipeIntoViewIOS(testId, maxScrolls, amount + 1);
    } else if (amount > maxScrolls) {
        // If the element is still not visible after the max amount of scroll let it fail
        throw new Error(
            `The element with testID '${testId}' could not be found or is not visible.`,
        );
    }
    return element;
}

export async function waitUntilLoaded(timeoutInMs: number) {
    await new Promise(resolve => {
        setTimeout(() => {
            resolve('');
        }, timeoutInMs); // 2 seconds delay (2000 milliseconds)
    });

    await browser.waitUntil(async () => {
        const loadingOverlay = await getOneByText('Loading...');
        const isExisting = await loadingOverlay.isExisting();

        return !isExisting;
    });
}
