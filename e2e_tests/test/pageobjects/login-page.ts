import { $ } from '@wdio/globals';

const LOGIN_SELECTORS = {
    EMAIL_INPUT: '~email-input',
    PASSWORD_INPUT: '~password-input',
    LOGIN_BUTTON: '~login-button',
};

class LoginPage {
    get inputUsername() {
        return $(LOGIN_SELECTORS.EMAIL_INPUT);
    }

    get inputPassword() {
        return $(LOGIN_SELECTORS.PASSWORD_INPUT);
    }

    get btnSubmit() {
        return $(LOGIN_SELECTORS.LOGIN_BUTTON);
    }

    public async verifyInLoginPage() {
        await expect(this.inputUsername).toBeDisplayed();
        await expect(this.inputPassword).toBeDisplayed();
    }
    public async login(username: string, password: string) {
        await this.inputUsername.setValue(username);
        await this.inputPassword.setValue(password);
        if (driver.isIOS) {
            const iOSKeyboardReturnButton = await driver.$(
                '//XCUIElementTypeButton[@name="Return"]',
            );
            await iOSKeyboardReturnButton.click();
        }
        await this.btnSubmit?.click();
    }
}

export default new LoginPage();
