import { $ } from '@wdio/globals';
import defectDetailsPage from '../Defect/defectDetails-page';

class VendorEmployeeHomePage {
    get defectPageHeader() {
        return $('~vendor-manage-defects.header-bar-text');
    }
    get activeTabButton() {
        return $(`~vendor-manage-defects.active-defects-tab`);
    }
    get pastTabButton() {
        return $(`~vendor-manage-defects.past-defects-tab`);
    }
    get profileIcon() {
        return $(`~profile-icon`);
    }

    public async verifyInVendorHomePage() {
        await expect(this.defectPageHeader).toBeDisplayed();
    }

    public async selectDefectCard() {
        if (driver.isIOS) {
            await defectDetailsPage.iosDefectCard.click();
        } else {
            await defectDetailsPage.androidDefectCard.click();
        }
    }

    public async navigateToProfilePageFromHomePage() {
        (await this.profileIcon).click();
    }
}

export default new VendorEmployeeHomePage();
