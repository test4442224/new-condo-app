import { $ } from '@wdio/globals';

const ABOUTUSPAGE_SELECTORS = {
    ABOUTUS_HEADER: '~about-us.header-bar-text',
};

class AboutUsPage {
    get HeaderBar() {
        return $(ABOUTUSPAGE_SELECTORS.ABOUTUS_HEADER);
    }
}

export default new AboutUsPage();
