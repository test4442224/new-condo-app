import { $ } from '@wdio/globals';

class ProfilePage {
    get headerBar() {
        return $(`~profile-page.header-bar-text`);
    }
    get logoutButton() {
        return $(`~logout-button`);
    }
    get logoutModal() {
        return $(`~logout-modal`);
    }
    get modalConfirm() {
        return $('~PopModal-Confirm');
    }

    public async verifyInProfilePage() {
        await expect(this.headerBar).toBeDisplayed();
    }

    public async logout() {
        (await this.logoutButton).click();
        expect(this.logoutModal).toBeDisplayed();
        (await this.modalConfirm).click();
    }
}

export default new ProfilePage();
