import homePage from '../home-page';

class StatementOfAccountPage {
    get manageStatementOfAccountButton() {
        return homePage.manageStatementOfAccountButton;
    }

    get StatementOfAccountPageHeader() {
        return $('~statment-of-account.header-bar-text');
    }

    get StatementOfAccountCard() {
        return $('~statement-of-account-SoA-001');
    }

    public async navigateToStatementOfAccountFromHomepage() {
        await expect(this.manageStatementOfAccountButton).toBeDisplayed();
        await this.manageStatementOfAccountButton.click();
        await expect(this.StatementOfAccountPageHeader).toBeDisplayed();
    }

    public async verifyStatementOfAccount() {
        await expect(this.StatementOfAccountCard).toBeDisplayed();
    }
}

export default new StatementOfAccountPage();
