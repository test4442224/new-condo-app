import { getOneByTestId } from '../../helpers/helpers';

class ViewPaymentPage {
    get viewPaymentDetailPageHeader() {
        return $('~payment-detail.header-bar-text');
    }

    public async selectCashPayment() {
        await expect(this.viewPaymentDetailPageHeader).toBeDisplayed();
        const button = await getOneByTestId('payment-method-select.cash');
        await button.click();
    }

    public async selectPaynowPayment() {
        const button = await getOneByTestId('payment-method-select.paynow');
        await button.click();
    }
}

export default new ViewPaymentPage();
