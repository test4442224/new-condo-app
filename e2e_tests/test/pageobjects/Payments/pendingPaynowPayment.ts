import { swipeAndGetByTestId } from '../../helpers/helpers';

class PendingPaynowPaymentPage {
    public async selectImageForUpload() {
        const uploadBtn = await swipeAndGetByTestId(
            'upload-paynow-receipt-btn',
            5,
        );
        await uploadBtn.click();

        if (driver.isIOS) {
            await $(`-ios predicate string:name CONTAINS 'March'`).click();
        } else {
            await $(
                '(//android.widget.ImageView[@resource-id="com.google.android.providers.media.module:id/icon_thumbnail"])[1]',
            ).click();
        }

        const proceedBtn = await swipeAndGetByTestId('proceed-btn', 5);
        await proceedBtn.click();
    }
}

export default new PendingPaynowPaymentPage();
