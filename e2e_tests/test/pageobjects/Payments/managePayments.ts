import { getOneByTestId } from '../../helpers/helpers';
import homePage from '../home-page';

class ManagePaymentPage {
    get managePaymentsButton() {
        return homePage.managePaymentButton;
    }

    get paymentPageHeader() {
        return $('~manage-payment.header-bar-text');
    }

    get paymentCard() {
        return $('~payment-card-TRE-20240304-abcd');
    }

    public async navigateToManagePaymentsFromHomepage() {
        await expect(this.managePaymentsButton).toBeDisplayed();
        await this.managePaymentsButton.click();
        await expect(this.paymentPageHeader).toBeDisplayed();
    }

    public async navigateToViewPayment() {
        // await expect(this.paymentCard).toBeDisplayed();
        const button = await getOneByTestId('payment-card-3-TRE-20240304-abce');
        await button.click();
    }
}

export default new ManagePaymentPage();
