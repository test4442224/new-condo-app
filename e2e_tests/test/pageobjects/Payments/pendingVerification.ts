import { getOneByTestId } from '../../helpers/helpers';

class PendingVerificationPage {
    public async verifyHeader() {
        const header = await getOneByTestId('header');
        expect(header).toHaveText('Pending Verification');
    }
}

export default new PendingVerificationPage();
