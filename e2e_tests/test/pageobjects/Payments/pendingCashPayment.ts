import { getOneByTestId } from '../../helpers/helpers';
import homePage from '../home-page';

class PendingCashPaymentPage {
    get managePaymentsButton() {
        return homePage.managePaymentButton;
    }

    get viewPaymentDetailPageHeader() {
        return $('~payment-type-header');
    }

    public async confirmPaymentInstruction() {
        await expect(getOneByTestId('payment-type-header')).toBeDisplayed();
        const button = await getOneByTestId('payment-pending.view-payment-btn');
        await button.click();
    }
}

export default new PendingCashPaymentPage();
