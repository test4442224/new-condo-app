import { getAllByTestId, getOneByTestId } from '../../helpers/helpers';

class SelectFacilityAndTimePage {
    public async selectFacilityAndTimeSlot() {
        const facilityButton = await browser.waitUntil(async () => {
            const facilityButtons = await getAllByTestId('select-facility');
            if (facilityButtons.length === 0) return false;

            return facilityButtons[0];
        });

        await facilityButton.click();

        const timeSlotButton = await browser.waitUntil(async () => {
            const timeSlotButtons = await getAllByTestId('select-time-slot');
            if (timeSlotButtons.length === 0) return false;

            return timeSlotButtons[0];
        });

        await timeSlotButton.click();

        const proceedButton = await getOneByTestId('proceed');
        await proceedButton.click();
    }
}

export default new SelectFacilityAndTimePage();
