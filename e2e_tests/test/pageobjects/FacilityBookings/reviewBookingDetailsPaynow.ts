import {
    getOneByTestId,
    swipeAndGetByTestId,
    waitUntilLoaded,
} from '../../helpers/helpers';

class ReviewBookingDetailsPaynowPage {
    public async selectCashPayment() {
        const paynowButton = await swipeAndGetByTestId(
            'payment-method-select.paynow',
            5,
        );
        await paynowButton.click();
    }

    public async confirmBooking() {
        await waitUntilLoaded(500);

        const button = await getOneByTestId('confirm');
        await button.click();
    }

    public async confirmPaynow() {
        await waitUntilLoaded(500);

        const confirmButton = await getOneByTestId('PopModal-Confirm');
        await confirmButton.click();
    }
}

export default new ReviewBookingDetailsPaynowPage();
