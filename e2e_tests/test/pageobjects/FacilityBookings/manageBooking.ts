import {
    getAllByTestIdStartsWith,
    getOneByTestId,
    waitUntilLoaded,
} from '../../helpers/helpers';
import homePage from '../home-page';

class ManageBookingPage {
    get manageBookingsButton() {
        return homePage.manageBookingsButton;
    }

    get bookingPageHeader() {
        return $('~manage-bookings.header-bar-text');
    }

    get bookingCard() {
        return $(`~booking-card-TRE-20240131-c995`);
    }

    get pastTabButton() {
        return $(`~manage-bookings.past-booking-tab`);
    }

    public async navigateToManageBookingsFromHomePage() {
        await waitUntilLoaded(1000);
        //assert that user is in Home Page
        await expect(this.manageBookingsButton).toBeDisplayed();
        //navigate to Manage Bookings Page
        await this.manageBookingsButton.click();
        //assert that user is in Manage Bookings Page
        await expect(this.bookingPageHeader).toBeDisplayed();
    }

    public async verifyCancelled() {
        //assert that user is in Manage Bookings Page
        await expect(this.bookingPageHeader).toBeDisplayed();
        //assert that BBQ Pit 1 is in past tab
        await expect(this.bookingCard).toBeDisplayed();
    }

    public async makeNewBooking() {
        const button = await getOneByTestId('make-new-booking');
        await button.click();
    }

    public async getNumberOfBookings() {
        let bookings: WebdriverIO.ElementArray;
        let bookingLength = 0;

        bookingLength = await browser.waitUntil(async () => {
            bookings = await getAllByTestIdStartsWith('booking-card');
            if (bookings.length === 0) return false;
            return bookings.length;
        });

        return bookingLength;
    }

    public async verifyNewBookingAdded(prevNoOfBookings: number) {
        await browser.waitUntil(async () => {
            const numberOfBookingsAfter = await this.getNumberOfBookings();

            return numberOfBookingsAfter > prevNoOfBookings;
        });
    }
}

export default new ManageBookingPage();
