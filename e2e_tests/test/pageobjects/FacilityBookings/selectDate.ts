import { addWeeks, format, isAfter, lastDayOfMonth } from 'date-fns';
import { getOneByTestId, waitUntilLoaded } from '../../helpers/helpers';

class SelectDatePage {
    public async selectDate() {
        await waitUntilLoaded(500);

        const today = new Date();
        const oneWeekLater = addWeeks(today, 1);
        const lastDayOfCurrentMonth = lastDayOfMonth(today);
        if (isAfter(oneWeekLater, lastDayOfCurrentMonth)) {
            if (driver.isIOS) {
                const month = format(today, 'MMMM yyyy');
                const nextMonthButton = await $(
                    `-ios class chain:**/XCUIElementTypeOther[\`name == "${month}"\`][1]/XCUIElementTypeOther[3]`,
                );
                await nextMonthButton.click();
            } else {
                const nextMonthButton =
                    await getOneByTestId('go-to-next-month');
                await nextMonthButton.click();
            }
        }

        const dayToSelect = oneWeekLater.getDate();
        let selector: string;
        let dateButton: WebdriverIO.Element;
        if (driver.isIOS) {
            selector = `label == '${dayToSelect}'`;
            dateButton = await $(`-ios predicate string:${selector}`);
        } else {
            selector = `new UiSelector().description("${dayToSelect}")`;
            dateButton = await $(`android=${selector}`);
        }
        await dateButton.click();
    }
}

export default new SelectDatePage();
