import { getOneByTestId } from '../../helpers/helpers';

class SelectFacilityTypePage {
    public async selectBadmintonHall() {
        const button = await getOneByTestId(
            'facility-type-list-Badminton Hall',
        );
        await button.click();
    }

    public async selectFunctionRoom() {
        const button = await getOneByTestId('facility-type-list-Function Room');
        await button.click();
    }
}

export default new SelectFacilityTypePage();
