class BookingDetailPage {
    get bookingCard() {
        return $(`~booking-card-TRE-20240131-c995`);
    }
    get iosBadgeStatus() {
        return $(`~ios-booking-status-badge`);
    }
    get androidBadgeStatus() {
        return $(`~android-booking-status-badge`);
    }
    get iosCancelBookingButton() {
        return $(`~ios-booking-details.cancel-btn`);
    }
    get androidCancelBookingButton() {
        return $(`~android-booking-details.cancel-btn`);
    }
    get cancelBookingModal() {
        return $(`~cancel-confirmation`);
    }
    get confirmCancelButton() {
        return $(`~PopModal-Confirm`);
    }
    get cancelText() {
        return $(`~booking-details.booking-cancelled-text`);
    }
    public async cancelPaidBooking() {
        //select BBQ pit 1
        await this.bookingCard.click();
        //assert that user is in the 'Paid' booking page and the badge status is Paid
        //click on the cancel button, different selectors for IOS and android
        //If condition true : for IOS, else for Android
        if (driver.isIOS) {
            expect(this.iosBadgeStatus).toHaveText('PAID');
            await this.iosCancelBookingButton.click();
        } else {
            expect(this.androidBadgeStatus).toHaveText('PAID');
            await this.androidCancelBookingButton.click();
        }
        // assert that the modal is popped up
        await expect(this.confirmCancelButton).toBeDisplayed();
        //click on confirm cancel button
        await this.confirmCancelButton.click();
        //assert that the modal for Successfully Cancelled Booking pops up and "Okay" button appears
        await expect(this.confirmCancelButton).toBeDisplayed();
        //click on Okay button
        await new Promise(resolve => setTimeout(resolve, 1000));
        await this.confirmCancelButton.click();
        //assert that badge status has changed to PENDING REFUND and cancel text is present
        if (driver.isIOS) {
            expect(this.iosBadgeStatus).toHaveText('PENDING REFUND');
        } else {
            expect(this.androidBadgeStatus).toHaveText('PENDING REFUND');
        }
    }

    public async NavigateToManageBookingsPageFromDetailsPage() {
        //go back to Manage Booking page
        const backButton = await $(`~header-bar-back-button`);
        await backButton.click();
    }
}

export default new BookingDetailPage();
