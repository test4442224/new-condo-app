import {
    getOneByTestId,
    swipeAndGetByTestId,
    waitUntilLoaded,
} from '../../helpers/helpers';

class ReviewBookingDetailsPage {
    get confirmBookingButton() {
        return $('~confirm');
    }

    public async selectCashPayment() {
        const cashButton = await swipeAndGetByTestId(
            'payment-method-select.cash',
            5,
        );
        await cashButton.click();
    }

    public async confirmBooking() {
        await waitUntilLoaded(500);

        const button = await getOneByTestId('confirm');
        await button.click();
    }
}

export default new ReviewBookingDetailsPage();
