import { getOneByTestId, swipeAndGetByTestId } from '../../helpers/helpers';

class BookingStatusPage {
    public async getHeader() {
        return await getOneByTestId('header-text');
    }

    public async navigateBackToBookings() {
        const button = await swipeAndGetByTestId('back-to-my-bookings', 3);
        await button.click();
    }
}

export default new BookingStatusPage();
