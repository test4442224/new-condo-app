import { $ } from '@wdio/globals';

const HOMEPAGE_SELECTORS = {
    HEADER_BAR: '~header-bar',
    MANAGE_DEFECTS_BUTTON: '~manage-defects-btn',
    MANAGE_BOOKINGS_BUTTON: '~manage-booking-btn',
    MANAGE_PAYMENTS_BUTTON: '~payments-btn',
    MANAGE_STATEMENT_OF_ACCOUNT_BUTTON: '~statement-of-account-btn',
};

class HomePage {
    get HeaderBar() {
        return $(HOMEPAGE_SELECTORS.HEADER_BAR);
    }
    get manageDefectsButton() {
        return $(HOMEPAGE_SELECTORS.MANAGE_DEFECTS_BUTTON);
    }

    get manageBookingsButton() {
        return $(HOMEPAGE_SELECTORS.MANAGE_BOOKINGS_BUTTON);
    }

    get managePaymentButton() {
        return $(HOMEPAGE_SELECTORS.MANAGE_PAYMENTS_BUTTON);
    }

    get manageStatementOfAccountButton() {
        return $(HOMEPAGE_SELECTORS.MANAGE_STATEMENT_OF_ACCOUNT_BUTTON);
    }
}

export default new HomePage();
