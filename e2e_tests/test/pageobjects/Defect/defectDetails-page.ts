import { $ } from '@wdio/globals';
import homePage from '../home-page';
import { getOneByTestId, swipeAndGetByTestId } from '../../helpers/helpers';

class DefectDetailsPage {
    // TO DO: REQUIRE REFACTORING
    public async navigateToDefectDetailsPageFromHomePage() {
        //assert that user is in Home Page
        const manageDefectsButton = await homePage.manageDefectsButton;
        await expect(manageDefectsButton).toBeDisplayed();
        //navigate to Manage Defects Page
        await manageDefectsButton.click();
        //check for the past button
        const pastDefect = await $('~manage-defects.past-defects-tab');
        await expect(pastDefect).toBeDisplayed();

        //check for the active button
        const activeDefect = await $('~manage-defects.active-defects-tab');
        await expect(activeDefect).toBeDisplayed();
        //Ensure that the list is displayed
        const defectList = await $('~manage-defect.flat-list');
        await expect(defectList).toBeDisplayed();

        await expect(this.iosDefectCard).toBeDisplayed();
        //click the cards button
        await this.iosDefectCard.click();

        //assert that the user is in defect details page
        const defectDetails = await $('~defect-details.header-bar-text');
        await expect(defectDetails).toBeDisplayed();

        //assert that the image for defect is displayed in the page
        const defectImage = await $('~defect-details.image');
        await expect(defectImage).toBeDisplayed();

        //assert that the name of the defect is displayed in the page
        const defectname = await $('~defect-details.view-name');
        await expect(defectname).toBeDisplayed();

        //assert that the reference no. of the defect is displayed in the page
        const defectRefNo = await $('~defect-details.referenceNo');
        await expect(defectRefNo).toBeDisplayed();

        //assert that the date of the defect is displayed in the page
        const defectDate = await $('~defect-details.date');
        await expect(defectDate).toBeDisplayed();

        //assert that the description of the defect is displayed in the page
        const defectDescription = await $('~defect-details.description');
        await expect(defectDescription).toBeDisplayed();

        //assert that the status history of the defect is displayed in the page
        const defectStatusHistory = await $(
            '~defect-details.status-history-status-text-label',
        );
        await expect(defectStatusHistory).toBeDisplayed();

        //assert that the notes of the status history of the defect is displayed in the page
        const defectStatusHistoryNotes = await $(
            '~defect-details.status-history-note-label',
        );
        await expect(defectStatusHistoryNotes).toBeDisplayed();
    }
    get iosDefectCard() {
        return $(`~ios-defect-card-D-240131-v3`);
    }
    get androidDefectCard() {
        return $(`~android-defect-card-D-240131-v3`);
    }
    get iosBadgeStatus() {
        return $(`~ios-defect-status-badge`);
    }
    get androidBadgeStatus() {
        return $(`~android-defect-status-badge`);
    }
    get iosCancelDefectButton() {
        return $(`~ios-defect-details.cancel-defect-btn`);
    }
    get androidCancelDefectButton() {
        return $(`~android-defect-details.cancel-defect-btn`);
    }
    get cancelDefectModal() {
        return $(`~cancel-confirmation`);
    }
    get confirmCancelButton() {
        return $(`~PopModal-Confirm`);
    }
    get toEditDefectPageButton() {
        return $(`~edit-defect-page-button`);
    }

    get closeImageModal() {
        return $(`~image-popup-close-button`);
    }

    public async verifyInDefectDetailsPage() {
        const defectImage = await $('~defect-details.image');
        await expect(defectImage).toBeDisplayed();

        const defectname = await $('~defect-details.view-name');
        await expect(defectname).toBeDisplayed();

        const defectRefNo = await $('~defect-details.referenceNo');
        await expect(defectRefNo).toBeDisplayed();

        const defectDate = await $('~defect-details.date');
        await expect(defectDate).toBeDisplayed();

        const defectDescription = await $('~defect-details.description');
        await expect(defectDescription).toBeDisplayed();

        const defectStatusHistory = await $(
            '~defect-details.status-history-status-text-label',
        );
        await expect(defectStatusHistory).toBeDisplayed();

        const defectStatusHistoryNotes = await $(
            '~defect-details.status-history-note-label',
        );
        await expect(defectStatusHistoryNotes).toBeDisplayed();
    }
    public async cancelDefect() {
        // select defect
        if (driver.isIOS) {
            await this.iosDefectCard.click();
        } else {
            await this.androidDefectCard.click();
        }
        // assert that user is in the defect page and the badge status is IN PROGRESS
        // click on the cancel button, different selectors for IOS and android
        if (driver.isIOS) {
            expect(this.iosBadgeStatus).toHaveText('IN PROGRESS');
            await this.iosCancelDefectButton.click();
        } else {
            expect(this.androidBadgeStatus).toHaveText('IN PROGRESS');
            const proceedBtn = await swipeAndGetByTestId(
                'ios-defect-details.cancel-defect-btn',
                5,
            );

            await proceedBtn.click();
        }
        // assert that the modal is popped up
        await expect(this.confirmCancelButton).toBeDisplayed();
        // click on confirm cancel button
        await this.confirmCancelButton.click();
        // assert that the modal for successfully cancelled defect pops up and "Okay" button appears
        await expect(this.confirmCancelButton).toBeDisplayed();
        // click on Okay button
        await new Promise(resolve => setTimeout(resolve, 1000));
        await this.confirmCancelButton.click();
        // assert that badge status has changed to CANCELLED
        if (driver.isIOS) {
            expect(this.iosBadgeStatus).toHaveText('CANCELLED');
        } else {
            expect(this.androidBadgeStatus).toHaveText('CANCELLED');
        }
    }

    public async navigateToManageDefectsPageFromDetailsPage() {
        //go back to Manage Defect page
        const backButton = await $(`~header-bar-back-button`);
        await backButton.click();
    }

    public async navigateToVendorHomePageFromDetailsPage() {
        const backButton = await $(`~header-bar-back-button`);
        await backButton.click();
    }

    public async defectDetails() {
        // select defect
        if (driver.isIOS) {
            await this.iosDefectCard.click();
        } else {
            await this.androidDefectCard.click();
        }
    }

    public async clickOnPopUpModal() {
        //click on popup page

        const imageModal = await getOneByTestId('carousel-image-swiper');
        await imageModal.click();

        //close
        const closeImageModal = await this.closeImageModal;
        await closeImageModal.click();
    }
}

export default new DefectDetailsPage();
