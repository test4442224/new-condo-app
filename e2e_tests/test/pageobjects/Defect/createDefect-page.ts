import { $ } from '@wdio/globals';
import homePage from '../home-page';
import manageDefectPage from './manageDefect-page';
const CREATE_DEFECT_SELECTORS = {
    INPUT_ERROR_MODAL: '~input-error-modal',
    CANCEL_REPORT_MODAL: '~cancel-report-modal',
    REPORT_SUBMITTED_MODAL: '~report-submitted-modal',
    SUBMIT_ERROR_MODAL: '~submit-error-modal',
    HEADER: '~report-defect.header-bar-text',
    NAME_INPUT_BOX: '~name-input-box',
    DESCRIPTION_INPUT_BOX: '~description-input-box',
    SUBMIT_DEFECT_BUTTON: '~submit-defect-button',
    MODAL_CONFIRM_BUTTON: '~PopModal-Confirm',
    UPLOAD_ATTACHMENT_BUTTON: '~upload-media-button',
    ADD_IMAGE: '~add-image',
};

const CREATE_DEFECT_SELECTORS_IOS = {
    IOS_IMAGE_ONE: `//XCUIElementTypeImage[contains(@name,'August')]`,
    IOS_IMAGE_TWO: `//XCUIElementTypeImage[contains(@name,'March')]`,
    TAP_FIRST_IOS: '//XCUIElementTypeCollectionView/XCUIElementTypeCell[2]',
    TAP_SECOND_IOS:
        '//XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther[1]',
    IOS_UPLOADIMAGE_DONE: '~Done',
};

const CREATE_DEFECT_SELECTORS_ANDROID = {
    ANDROID_CAMERA:
        '//android.widget.TextView[@resource-id="com.condoapp.app:id/tvCamera"]',
    ANDROID_CAMERA_CANCEL:
        '//android.widget.TextView[@resource-id="com.condoapp.app:id/ps_tv_cancel"]',
    ANDROID_IMAGE_UPLOAD_DONE: '//*[@text="Done"]',
};
class CreateDefectPage {
    get inputErrorModal() {
        return $(CREATE_DEFECT_SELECTORS.INPUT_ERROR_MODAL);
    }
    get cancelReportModal() {
        return $(CREATE_DEFECT_SELECTORS.CANCEL_REPORT_MODAL);
    }
    get reportSubmittedModal() {
        return $(CREATE_DEFECT_SELECTORS.REPORT_SUBMITTED_MODAL);
    }
    get submitErorrModal() {
        return $(CREATE_DEFECT_SELECTORS.SUBMIT_ERROR_MODAL);
    }
    get header() {
        return $(CREATE_DEFECT_SELECTORS.HEADER);
    }
    get nameInputBox() {
        return $(CREATE_DEFECT_SELECTORS.NAME_INPUT_BOX);
    }
    get descriptionInputBox() {
        return $(CREATE_DEFECT_SELECTORS.DESCRIPTION_INPUT_BOX);
    }
    get submitDefectButton() {
        return $(CREATE_DEFECT_SELECTORS.SUBMIT_DEFECT_BUTTON);
    }
    get modalConfirmButton() {
        return $(CREATE_DEFECT_SELECTORS.MODAL_CONFIRM_BUTTON);
    }
    get uploadMediaButton() {
        return $(CREATE_DEFECT_SELECTORS.UPLOAD_ATTACHMENT_BUTTON);
    }

    get uploadIOSPhoto() {
        return $(CREATE_DEFECT_SELECTORS_IOS.IOS_IMAGE_ONE);
    }
    get uploadIOSPhoto2() {
        return $(CREATE_DEFECT_SELECTORS_IOS.IOS_IMAGE_TWO);
    }

    get uploadIOSDone() {
        return $(CREATE_DEFECT_SELECTORS_IOS.IOS_UPLOADIMAGE_DONE);
    }
    get selectedIOSImage() {
        return $(CREATE_DEFECT_SELECTORS_IOS.TAP_FIRST_IOS);
    }
    get selectedIOSImage2() {
        return $(CREATE_DEFECT_SELECTORS_IOS.TAP_SECOND_IOS);
    }
    get androidCamera() {
        return $(CREATE_DEFECT_SELECTORS_ANDROID.ANDROID_CAMERA);
    }
    get cancelAndroidCamera() {
        return $(CREATE_DEFECT_SELECTORS_ANDROID.ANDROID_CAMERA_CANCEL);
    }
    get androidImageUploadDone() {
        return $(CREATE_DEFECT_SELECTORS_ANDROID.ANDROID_IMAGE_UPLOAD_DONE);
    }
    public async navigateToCreateDefectPageFromHomePage() {
        //assert that user is in Home Page
        const manageDefectsButton = await homePage.manageDefectsButton;
        await expect(manageDefectsButton).toBeDisplayed();
        //navigate to Manage Defects Page
        await manageDefectsButton.click();
        //assert that user is in Manage Defects Page
        const addDefectButton = await manageDefectPage.addDefectsButton;
        await expect(addDefectButton).toBeDisplayed();
        //navigate to Create Defect Page
        await addDefectButton.click();
        //assert that user is in Create Defect Page
        const reportDefectHeader = this.header;
        await expect(reportDefectHeader).toBeDisplayed();
    }
    public async submitDefectReport(
        defectName: string,
        defectDescription: string,
    ) {
        await this.nameInputBox.setValue(defectName);
        await this.descriptionInputBox.setValue(defectDescription);
        //first click is to close the ios keyboard
        if (driver.isIOS) {
            await this.submitDefectButton.click();
        }
        await this.uploadMediaButton.click();
        if (driver.isIOS) {
            await this.uploadIOSPhoto.click();
            await this.uploadIOSPhoto2.click();
            await this.uploadIOSDone.click();
            await this.selectedIOSImage2.click();
            await this.selectedIOSImage.click();
            await this.uploadIOSDone.click();
        } else {
            await this.androidCamera.click();
            await this.cancelAndroidCamera.click();

            await $(
                `//android.widget.RelativeLayout[3]/android.widget.ImageView`,
            ).click();
            $(
                `//android.widget.RelativeLayout[2]/android.widget.ImageView`,
            ).click();

            await this.androidImageUploadDone.click();
        }

        //second click is to submit the defect
        await this.submitDefectButton.click();
        await expect(this.reportSubmittedModal).toBeDisplayed();
        await this.modalConfirmButton.click();
        //assert user is navigated back to manage defect page
        const addDefectButton = await manageDefectPage.addDefectsButton;
        await expect(addDefectButton).toBeDisplayed();
    }
}
export default new CreateDefectPage();
