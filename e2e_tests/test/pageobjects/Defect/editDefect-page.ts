import { $ } from '@wdio/globals';
import manageDefectPage from './manageDefect-page';
import defectDetailsPage from './defectDetails-page';

const EDIT_DEFECT_SELECTORS = {
    INPUT_ERROR_MODAL: '~edit-defect-input-error-modal',
    CANCEL_REPORT_MODAL: '~cancel-edit-report-modal',
    REPORT_SUBMITTED_MODAL: '~defect-edit-successful-modal',
    SUBMIT_ERROR_MODAL: '~submit-error-modal',
    HEADER: '~edit-defect.header-bar-text',
    NAME_INPUT_BOX: '~name-input-box',
    DESCRIPTION_INPUT_BOX: '~description-input-box',
    SUBMIT_DEFECT_BUTTON: '~submit-edit-defect-button',
    MODAL_CONFIRM_BUTTON: '~PopModal-Confirm',
    UPLOAD_ATTACHMENT_BUTTON: '~upload-media-button',
    ADD_IMAGE: '~add-image',
};

const EDIT_DEFECT_SELECTORS_IOS = {
    IOS_REMOVE_ATTACHMENT_BUTTON: `(//XCUIElementTypeOther[@name="delete-media-button"])[1]`,
    IOS_IMAGE_ONE: `//XCUIElementTypeImage[contains(@name,'August')]`,
    IOS_IMAGE_TWO: `//XCUIElementTypeImage[contains(@name,'March')]`,
    TAP_FIRST_IOS: '//XCUIElementTypeCollectionView/XCUIElementTypeCell[2]',
    TAP_SECOND_IOS:
        '//XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther[1]',
    IOS_UPLOADIMAGE_DONE: '~Done',
};

const EDIT_DEFECT_SELECTORS_ANDROID = {
    ANDROID_REMOVE_ATTACHMENT_BUTTON: `~delete-media-button`,
    ANDROID_CAMERA:
        '//android.widget.TextView[@resource-id="com.condoapp.app:id/tvCamera"]',
    ANDROID_CAMERA_CANCEL:
        '//android.widget.TextView[@resource-id="com.condoapp.app:id/ps_tv_cancel"]',
    ANDROID_IMAGE_UPLOAD_DONE: '//*[@text="Done"]',
};

class EditDefectPage {
    get inputErrorModal() {
        return $(EDIT_DEFECT_SELECTORS.INPUT_ERROR_MODAL);
    }
    get cancelReportModal() {
        return $(EDIT_DEFECT_SELECTORS.CANCEL_REPORT_MODAL);
    }
    get reportSubmittedModal() {
        return $(EDIT_DEFECT_SELECTORS.REPORT_SUBMITTED_MODAL);
    }
    get submitErorrModal() {
        return $(EDIT_DEFECT_SELECTORS.SUBMIT_ERROR_MODAL);
    }
    get header() {
        return $(EDIT_DEFECT_SELECTORS.HEADER);
    }
    get nameInputBox() {
        return $(EDIT_DEFECT_SELECTORS.NAME_INPUT_BOX);
    }
    get descriptionInputBox() {
        return $(EDIT_DEFECT_SELECTORS.DESCRIPTION_INPUT_BOX);
    }
    get submitDefectButton() {
        return $(EDIT_DEFECT_SELECTORS.SUBMIT_DEFECT_BUTTON);
    }
    get modalConfirmButton() {
        return $(EDIT_DEFECT_SELECTORS.MODAL_CONFIRM_BUTTON);
    }
    get uploadMediaButton() {
        return $(EDIT_DEFECT_SELECTORS.UPLOAD_ATTACHMENT_BUTTON);
    }

    //IOS Commands
    get removeIOSAttachmentButton() {
        return $(EDIT_DEFECT_SELECTORS_IOS.IOS_REMOVE_ATTACHMENT_BUTTON);
    }
    get uploadIOSPhoto() {
        return $(EDIT_DEFECT_SELECTORS_IOS.IOS_IMAGE_ONE);
    }
    get uploadIOSPhoto2() {
        return $(EDIT_DEFECT_SELECTORS_IOS.IOS_IMAGE_TWO);
    }
    get uploadIOSDone() {
        return $(EDIT_DEFECT_SELECTORS_IOS.IOS_UPLOADIMAGE_DONE);
    }
    get selectedIOSImage() {
        return $(EDIT_DEFECT_SELECTORS_IOS.TAP_FIRST_IOS);
    }
    get selectedIOSImage2() {
        return $(EDIT_DEFECT_SELECTORS_IOS.TAP_SECOND_IOS);
    }

    //Android Commands
    get removeAndroidAttachmentButton() {
        return $(
            EDIT_DEFECT_SELECTORS_ANDROID.ANDROID_REMOVE_ATTACHMENT_BUTTON,
        );
    }
    get androidCamera() {
        return $(EDIT_DEFECT_SELECTORS_ANDROID.ANDROID_CAMERA);
    }
    get cancelAndroidCamera() {
        return $(EDIT_DEFECT_SELECTORS_ANDROID.ANDROID_CAMERA_CANCEL);
    }
    get androidImageUploadDone() {
        return $(EDIT_DEFECT_SELECTORS_ANDROID.ANDROID_IMAGE_UPLOAD_DONE);
    }

    public async navigateToEditDefectPageFromHomePage() {
        await manageDefectPage.navigateToManageDefectsFromHomePage();
        if (driver.isIOS) {
            await defectDetailsPage.iosDefectCard.click();
        } else {
            await defectDetailsPage.androidDefectCard.click();
        }
        const toEditDefectPage = await defectDetailsPage.toEditDefectPageButton;
        await toEditDefectPage.click();
        await expect(this.header).toBeDisplayed();
    }
    public async submitDefectReport(
        defectName: string,
        defectDescription: string,
    ) {
        const nameInput = await this.nameInputBox;
        await nameInput.setValue(defectName);
        await this.descriptionInputBox.setValue(defectDescription);
        //first click is to close the ios keyboard
        if (driver.isIOS) {
            await this.submitDefectButton.click();
        }

        // remove attachment
        if (driver.isIOS) {
            await this.removeIOSAttachmentButton.click();
        } else {
            await this.removeAndroidAttachmentButton.click();
        }

        // upload attachment
        await this.uploadMediaButton.click();
        if (driver.isIOS) {
            await this.uploadIOSPhoto.click();
            await this.uploadIOSPhoto2.click();
            await this.uploadIOSDone.click();
            await this.selectedIOSImage2.click();
            await this.selectedIOSImage.click();
            await this.uploadIOSDone.click();
        } else {
            await this.androidCamera.click();
            await this.cancelAndroidCamera.click();

            await $(
                `//android.widget.RelativeLayout[3]/android.widget.ImageView`,
            ).click();
            $(
                `//android.widget.RelativeLayout[2]/android.widget.ImageView`,
            ).click();

            await this.androidImageUploadDone.click();
        }

        //second click is to submit the updated defect
        await this.submitDefectButton.click();
        await expect(this.reportSubmittedModal).toBeDisplayed();
        await this.modalConfirmButton.click();
        //assert user is navigated back to defect details page
        const editDefectButton = await defectDetailsPage.toEditDefectPageButton;
        await expect(editDefectButton).toBeDisplayed();
    }
}

export default new EditDefectPage();
