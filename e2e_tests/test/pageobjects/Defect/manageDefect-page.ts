import { $ } from '@wdio/globals';
import homePage from '../home-page';

const MANAGE_DEFECT_PAGE__SELECTORS = {
    ADD_DEFECT_BUTTON: '~manage-defects.add-btn',
};

class ManageDefectPage {
    get addDefectsButton() {
        return $(MANAGE_DEFECT_PAGE__SELECTORS.ADD_DEFECT_BUTTON);
    }
    get manageDefectButton() {
        return homePage.manageDefectsButton;
    }
    get defectPageHeader() {
        return $('~manage-defects.header-bar-text');
    }
    get iosDefectCard() {
        return $(`~ios-defect-card-D-240131-v3`);
    }
    get androidDefectCard() {
        return $(`~android-defect-card-D-240131-v3`);
    }
    get pastTabButton() {
        return $(`~manage-defects.past-defects-tab`);
    }
    public async navigateToManageDefectsFromHomePage() {
        // assert that user is in homepage
        await expect(this.manageDefectButton).toBeDisplayed();
        // navigate to Manage Defect Page
        await this.manageDefectButton.click();
        // assert that user is in Manage Defect Page
        await expect(this.defectPageHeader).toBeDisplayed();
    }
    public async verifyCancelled() {
        //assert that user is in Manage Defect Page
        await expect(this.defectPageHeader).toBeDisplayed();

        // assert that cancelled defect is in past tab
        if (driver.isIOS) {
            await expect(this.iosDefectCard).toBeDisplayed();
        } else {
            await expect(this.androidDefectCard).toBeDisplayed();
        }
    }
}

export default new ManageDefectPage();
