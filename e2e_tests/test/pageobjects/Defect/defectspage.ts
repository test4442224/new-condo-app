import { $ } from '@wdio/globals';
import homePage from '../home-page';

const DEFECTSPAGE_SELECTORS = {
    DEFECTS_HEADER: '~manage-defects.header-bar-text',
};

class DefectsPage {
    get HeaderBar() {
        return $(DEFECTSPAGE_SELECTORS.DEFECTS_HEADER);
    }
    public async navigateToManageDefectPageFromHomePage() {
        //assert that user is in Home Page
        const manageDefectsButton = await homePage.manageDefectsButton;
        await expect(manageDefectsButton).toBeDisplayed();
        //navigate to Manage Defects Page
        await manageDefectsButton.click();
    }
}

export default new DefectsPage();
