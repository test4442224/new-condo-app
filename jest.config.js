module.exports = {
    preset: 'react-native',
    transformIgnorePatterns: ['node_modules/(?!@react-native|react-native)'],
    verbose: true,
    collectCoverage: true,
    setupFilesAfterEnv: ['./jest-setup.ts'],
};
