# New Condo App

## Dev Environment Requirement

-   Node >= 18
-   NPM >= 9
-   Watchman installed
-   Xcode installed
-   CocoaPods installed
-   Android Studio installed

To setup Xcode or Android studio, Follow this guide:

-   [Xcode & Android Studio set up](https://mavericks-consulting.atlassian.net/wiki/spaces/MKB/pages/274726913/Create+React+Native+App+using+Bare+React+Native+version+0.72.3)

# Setup

# .Env

```bash
# ENV=development
# API_URL="http://localhost:3001"
# ANDROID_URL="http://10.0.2.2:3001"

#ENV=production
#API_URL=<BACKEND_API_URL>
#ANDROID_URL=<BACKEND_API_URL_FOR_ANDROID>
```

# Deployment

-   [Fastlane Deployment](https://mavericks-consulting.atlassian.net/wiki/spaces/CMA/pages/280887341/Deployment+process+Condo+Mobile+Application.)

# Getting Started

## Step 1: Start the Metro Server

First, you will need to start **Metro**, the JavaScript _bundler_ that ships _with_ React Native.

To start Metro, run the following command from the _root_ of your React Native project:

```bash
# using npm
npm start
```

## Step 2: Start your Application

Let Metro Bundler run in its _own_ terminal. Open a _new_ terminal from the _root_ of your React Native project. Run the following command to start your _Android_ or _iOS_ app:

### For Android

```bash
npm run android
```

### For iOS

```bash
npm run ios
```

If everything is set up _correctly_, you should see your new app running in your _Android Emulator_ or _iOS Simulator_ shortly provided you have set up your emulator/simulator correctly.

This is one way to run your app — you can also run it directly from within Android Studio and Xcode respectively.

## Code Conventions

-   [UI Guidelines](https://mavericks-consulting.atlassian.net/wiki/spaces/CMA/pages/281968641/UI+Guidelines)

### Border Radius

Use preset instead of hard coded variables.

```tsx
//Don't
{
    borderRadius: 8;
}
```

```tsx!
//Do
{
    borderRadius: BorderRadiuses.br30,
}
```

To create a circle, use `br100`.

```tsx
//Don't
{
    borderRadius: 999;
}
```

```tsx!
//Do
{
    borderRadius: BorderRadiuses.br100,
}
```

### Spacing

Use preset instead of hard coded variables.

```tsx
//Don't
{
    margin: 8,
}
```

```tsx!
//Do
{
    margin: Spacings.s2
}
```

If the presets aren't enough, multiply with s1.

```tsx
{
    margin: 88,
}

```

```tsx
{
    margin: Spacings.s1 * 11,
}
```

### Color

Use design token instead of color code.

```tsx
//Don't
{
    backgroundColor: '#234BA5';
}
```

```tsx
//Do
{
    backgroundColor: Colors.$backgroundPrimaryHeavy,
}
```

### Button

Used size prop instead of hard coded height. Width is flexible to the parent.

```tsx
//Don't
<Button style={{ height: 48 }} />
```

```tsx
//Do
<Button size="large" />
```

### Font

Used size prop instead of hard coded height. Width is flexible to the parent.

```tsx
//Don't
{
  fontSize: 12,
  fontWeight: 'bold'
}
```

```tsx
//Do
{
  ...Typography.text90,
}

```

## Presets

### Border Radius

```tsx
{
  br0: Constants.isIOS ? 0 : 0,
  br10: Constants.isIOS ? 3 : 2,
  br20: 6,
  br30: Constants.isIOS ? 9 : 8,
  br40: 12,
  br50: Constants.isIOS ? 15 : 16,
  br60: 20,
  br100: 999
};
```

### Spacing

```tsx
{
  s1: 4,
  s2: 8,
  s3: 12,
  s4: 16,
  s5: 20,
  s6: 24,
  s7: 28,
  s8: 32,
  s9: 36,
  s10: 40
}
```

### Shadow

```tsx
{
  sh10: {
    top: {
      shadowColor: isDark ? 'transparent' : Colors.grey40,
      shadowOpacity: 0.18,
      shadowRadius: 5,
      shadowOffset: {height: -1, width: 0},
      elevation: isDark ? 0 : 2
    },
    bottom: {
      shadowColor: isDark ? 'transparent' : Colors.grey40,
      shadowOpacity: 0.18,
      shadowRadius: 5,
      shadowOffset: {height: 1, width: 0},
      elevation: isDark ? 0 : 2
    }
  },
  sh20: {
    top: {
      shadowColor: isDark ? 'transparent' : Colors.grey30,
      shadowOpacity: 0.2,
      shadowRadius: 10,
      shadowOffset: {height: -2, width: 0},
      elevation: isDark ? 0 : 3
    },
    bottom: {
      shadowColor: isDark ? 'transparent' : Colors.grey30,
      shadowOpacity: 0.2,
      shadowRadius: 10,
      shadowOffset: {height: 2, width: 0},
      elevation: isDark ? 0 : 3
    }
  },
  sh30: {
    top: {
      shadowColor: isDark ? 'transparent' : Colors.grey30,
      shadowOpacity: 0.2,
      shadowRadius: 12,
      shadowOffset: {height: -5, width: 0},
      elevation: isDark ? 0 : 4
    },
    bottom: {
      shadowColor: isDark ? 'transparent' : Colors.grey30,
      shadowOpacity: 0.2,
      shadowRadius: 12,
      shadowOffset: {height: 5, width: 0},
      elevation: isDark ? 0 : 4
    }
  },
 }
```

### Typography

```tsx
export const WEIGHT_TYPES: { [key: string]: TextStyle['fontWeight'] } = {
    THIN: '200' as const,
    LIGHT: '300' as const,
    REGULAR: '400' as const,
    MEDIUM:
        parseFloat(Platform.Version as string) >= 11.2
            ? '600'
            : ('500' as '500' | '600'),
    BOLD: '700' as const,
    HEAVY: '800' as const,
    BLACK: '900' as const,
};

// text10
const text10: TextStyle = {
    fontSize: 64,
    fontWeight: Constants.isIOS ? WEIGHT_TYPES.THIN : undefined,
    lineHeight: 76,
    fontFamily: 'System',
};

// text20
const text20: TextStyle = {
    fontSize: 48,
    fontWeight: Constants.isIOS ? WEIGHT_TYPES.REGULAR : undefined,
    lineHeight: Constants.isIOS ? 60 : 62,
    fontFamily: 'System',
};

// text30
const text30: TextStyle = {
    fontSize: 36,
    fontWeight: Constants.isIOS ? WEIGHT_TYPES.REGULAR : undefined,
    lineHeight: Constants.isIOS ? 43 : 46,
    fontFamily: 'System',
};

// text40
const text40: TextStyle = {
    fontSize: 28,
    fontWeight: Constants.isIOS ? WEIGHT_TYPES.HEAVY : 'bold',
    lineHeight: 32,
    fontFamily: 'System',
};

// text50
const text50: TextStyle = {
    fontSize: 24,
    fontWeight: Constants.isIOS ? WEIGHT_TYPES.HEAVY : 'bold',
    lineHeight: 28,
    fontFamily: 'System',
};

// text60
const text60: TextStyle = {
    fontSize: 20,
    fontWeight: Constants.isIOS ? WEIGHT_TYPES.HEAVY : 'bold',
    lineHeight: 24,
    fontFamily: 'System',
};

// text65
const text65: TextStyle = {
    fontSize: 18,
    fontWeight: Constants.isIOS ? WEIGHT_TYPES.MEDIUM : undefined,
    lineHeight: 24,
    fontFamily: 'System',
};

// text70
const text70: TextStyle = {
    fontSize: 16,
    fontWeight: Constants.isIOS ? WEIGHT_TYPES.REGULAR : undefined,
    lineHeight: 24,
    fontFamily: 'System',
};

// text80
const text80: TextStyle = {
    fontSize: 14,
    fontWeight: Constants.isIOS ? WEIGHT_TYPES.REGULAR : undefined,
    lineHeight: 20,
    fontFamily: 'System',
};

// text90
const text90: TextStyle = {
    fontSize: 12,
    fontWeight: Constants.isIOS ? WEIGHT_TYPES.BOLD : 'bold',
    lineHeight: 16,
    fontFamily: 'System',
};

// text100
const text100: TextStyle = {
    fontSize: 10,
    fontWeight: Constants.isIOS ? WEIGHT_TYPES.BOLD : 'bold',
    lineHeight: 16,
    fontFamily: 'System',
};
```

## Releases

Releases follow [CalVer](https://calver.org/) using `YYYYMM.MICRO` format, starting with release `202311.04`. Releases are tagged on the `release` branch, normally merged from the develop branch and merged into `main` after a successful release.
