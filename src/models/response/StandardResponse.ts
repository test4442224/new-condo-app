export interface IStandardResponse<T> {
    response: T | null;
    statusCode: number;
    error?: string;
}
