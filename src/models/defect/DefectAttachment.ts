export interface DefectAttachment {
    id: number;
    thumbnailPath: string;
    attachmentPath: string;
    attachmentType: string;
    createdAt: Date;
}
