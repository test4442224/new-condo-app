export interface IDefectPriority {
    id: number;
    name: string;
    dueAt: number;
    dueAtUnit: string;
    condoId: number;
}
