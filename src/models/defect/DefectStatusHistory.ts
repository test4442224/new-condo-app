import { DefectAttachment } from './DefectAttachment';

export enum DefectStatus {
    IN_PROGRESS = 'IN_PROGRESS',
    UNASSIGNED = 'UNASSIGNED',
    COMPLETED = 'COMPLETED',
    CANCELLED = 'CANCELLED',
}

export interface IDefectStatusHistory {
    id: number;
    defectId: number;
    status: DefectStatus;
    notes: string;
    createdBy: number; // userId
    attachments: DefectAttachment[];
    createdAt: Date;
}
