export interface IGetFacilityAvailableTimeRequest {
    facilityTypeId: number;
    dateString: string;
}
