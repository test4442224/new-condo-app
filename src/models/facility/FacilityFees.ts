import { PriceType } from '../booking/PriceBreakdown';

export interface IFacilityFees {
    facilityId: number;
    feeDescription: string;
    feeType: PriceType;
    feeAmount: number;
}
