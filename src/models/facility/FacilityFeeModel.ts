import { PriceType } from '../booking/PriceBreakdown';

export interface IFacilityFeeModel {
    id: number;
    facilityId: number;
    feeDescription: string;
    feeType: PriceType;
    feeAmount: number;
    readonly createdAt?: Date;
    readonly updatedAt?: Date;
}
