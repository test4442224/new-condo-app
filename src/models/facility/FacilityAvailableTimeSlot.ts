import { ITimeSlot } from '../booking/TimeSlotModel';
import { IFacility } from './FacilityModel';

export default interface IFacilityAvailableTimeSlot {
    facility: IFacility;
    availableTimeSlots: ITimeSlot[];
}
