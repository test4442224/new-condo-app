import { IFacility } from './FacilityModel';

export interface IFacilityType {
    id: number;
    condoId: number;
    name: string;
    imageUrl: string;
    readonly createdAt?: Date;
    readonly updatedAt?: Date;
    facilities?: IFacility[];
}
