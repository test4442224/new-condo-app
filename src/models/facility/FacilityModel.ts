import { IFacilityFeeModel } from './FacilityFeeModel';

export interface IFacility {
    readonly id: number;
    name: string;
    facilityTypeId: number;
    pax: number;
    location: string;
    tnc: string;
    minimumBookingDurationInMinutes: number;
    depositPercentage: number;
    openingTime: Date;
    closingTime: Date;
    isArchived: boolean;
    description: string;
    readonly createdAt?: Date;
    readonly updatedAt?: Date;
    facilityImageUrls?: string[];
    primaryFacilityImageUrl?: string | null;
    facilityFees: IFacilityFeeModel[] | null;
}
