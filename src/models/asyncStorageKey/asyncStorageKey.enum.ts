export enum AsyncStorageKey {
    TOKEN = 'token',
    CONDO_UEN = 'condo_uen',
    CONDO_NAME = 'condo_name',
    AGREED_TOS_AT = 'agreedToSAt',
}
