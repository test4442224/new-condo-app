import { RoleType } from './RoleType';

export default interface UserRole {
    id: number;
    roleType: RoleType;
    condoId: number;
    userId: number;
    createdAt?: Date;
    updatedAt?: Date;
}
