export interface ICondo {
    name: string;
    uen: string;
    aboutUs: string | null;
    aboutUsImagePath: string | null;
    paynowQrCodeImagePath: string;
}

export interface IGetCondoResponse extends ICondo {
    createdAt: string;
    updatedAt: string;
}
