import { ManageBookingAttributes } from '../../store/manageBooking/slice';

export interface ManageBookingsResponse
    extends Omit<
        ManageBookingAttributes,
        'bookingDate' | 'startTime' | 'endTime' | 'createdAt' | 'updatedAt'
    > {
    bookingDate: string;
    startTime: string;
    endTime: string;
    createdAt: string;
    updatedAt: string;
}
