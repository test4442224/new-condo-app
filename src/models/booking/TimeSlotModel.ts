export interface ITimeSlot {
    start: string;
    end: string;
    slotNumber?: number;
}
