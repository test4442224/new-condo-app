export enum PriceType {
    PER_BOOKING = 'PER_BOOKING',
    PER_HOUR = 'PER_HOUR',
}

export interface IPriceBreakdown {
    id: number;
    bookingId: number;
    description: string;
    unitPriceAmount: number;
    priceType: PriceType;
    subtotalPriceAmount: number;
}

export interface IPriceSummary {
    id?: number;
    description: string;
    unitPriceAmount: number;
    priceType: PriceType;
    subtotalPriceAmount?: number;
}
