export interface IUnavailableDatesResponse {
    period: {
        startDate: string;
        endDate: string;
    };
    unavailableDates: string[];
}

export interface IUnavailableDatesRequest {
    totalDays: number;
    facilityTypeId: number;
}
