export interface IBookingLockResponse {
    lockDurationInMinutes: number;
    lockDetail: {
        id: number;
        facilityId: number;
        userId: number;
        bookingDate: string; // DateOnly String
        startTime: string; // TimeOnly String
        endTime: string; // TimeOnly String
        updatedAt: string; // Timestamp String;
        createdAt: string; // Timestamp String;
    };
}
