export enum NewBookingError {
    GENERAL = 'Failed to Proceed',
    DATE_NOT_AVAILABLE = 'Failed to Fetch Data.',
    TIMESLOT_NOT_AVAILABLE = 'Failed to Fetch Facility Available Time Slots',
    FACILITY_TYPES_NOT_AVAILABLE = 'Failed to Get Facility Types',
}

export interface ISetErrorDialog {
    title: NewBookingError;
    message: string;
}
