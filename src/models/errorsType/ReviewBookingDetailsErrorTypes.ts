import PageName from '../../navigation/PageNameEnum';

export enum ReviewBookingDetailError {
    GENERAL = 'Error',
    CREATION_FAILED = 'Failed to Create Booking',
    BOOKING_TIME_OUT = 'Booking has timed out',
    SLOT_NOT_AVAILABLE = 'Selected slot is not available',
}

export interface ISetErrorDialog {
    title: ReviewBookingDetailError;
    message: string;
    backToPage?: PageName;
}
