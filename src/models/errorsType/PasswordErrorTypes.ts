export enum PasswordError {
    GENERAL = 'Failed to Proceed',
    RESET_PASSWORD = 'Failed to Reset Password',
    CHANGE_PASSWORD = 'Failed to Change Password',
    FORGET_PASSWORD = 'Failed to Forget Password',
}

export interface ISetErrorDialog {
    title: PasswordError;
    message: string;
}
