export interface ILegalDocument {
    id: number;
    name: string;
    content: string;
}

export interface IGetLegalDocumentResponse extends ILegalDocument {
    createdAt: string;
    updatedAt: string;
}
