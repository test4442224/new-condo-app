export enum LegalDocumentName {
    TERMS_OF_SERVICE = 'terms-of-service',
    PRIVACY_POLICY = 'privacy-policy',
}
