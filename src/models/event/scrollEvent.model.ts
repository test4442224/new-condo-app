export type ScrollEvent = {
    layoutMeasurement: { height: number; width: number };
    contentOffset: { x: number; y: number };
    contentSize: { height: number; width: number };
};
