import { ResidentStatus } from './residentStatus.enum';
import { IUnit } from './unit.model';

export interface IResident {
    id: number;
    email: string;
    phoneNumber: number;
    firstName: string;
    lastName: string;
    isAgreedToS: boolean;
    agreedToSAt: string;
    isPasswordReset: boolean;
    status: ResidentStatus;
    createdAt: string;
    updatedAt: string;
    isPrimaryUser: boolean;
}

export interface IGetResidentDetailResponse
    extends Omit<IResident, 'createdAt' | 'updatedAt'> {
    units: IUnit[];
}
