export interface IUnit {
    id: number;
    blockNumber: string;
    unitNumber: string;
    condoId: number;
    createdAt: string;
    updatedAt: string;
}
