export interface IPaginationResponse<T> {
    data: T;
    pagination: {
        totalRecords: number;
        recordsPerPage: number;
        currentPage: number;
        totalPages: number;
        nextPage: number | null;
        prevPage: number | null;
    } | null;
}
