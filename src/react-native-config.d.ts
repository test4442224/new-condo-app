declare module 'react-native-config' {
    export interface NativeConfig {
        ENV: 'development' | 'production' | 'test';
        ANDROID_URL?: string;
        API_URL: string;
    }

    export const Config: NativeConfig;
    export default Config;
}
