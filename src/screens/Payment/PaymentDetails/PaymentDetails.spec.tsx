import { cleanup } from '@testing-library/react-native';
import PaymentDetails from './PaymentDetails';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import PageName from '../../../navigation/PageNameEnum';
import NavParamList from '../../../navigation/NavParmList';
import { RouteProp } from '@react-navigation/native';
import React from 'react';
import { renderWithProviders } from '../../../test/utils';
import {
    IConservancyFeesAttribute,
    PaymentMethod,
    PaymentPurpose,
} from '../../../store/payment/slice';
import { PaymentStatus } from '../../../store/manageBooking/slice';

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.PAYMENT_DETAILS_SCREEN>
> = {
    navigate: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.PAYMENT_DETAILS_SCREEN> = {
    key: 'PaymentDetailsKey',
    name: PageName.PAYMENT_DETAILS_SCREEN,
};

const mockPayment: IConservancyFeesAttribute = {
    id: 1,
    referenceId: 'TRE-20240308-1a82',
    conservancyPaymentId: 1,
    unitId: 1,
    totalAmount: 5094,
    createdAt: '2024-03-08T04:28:46.885Z',
    updatedAt: '2024-03-08T04:28:46.885Z',
    payments: [
        {
            id: 1,
            paymentPurpose: PaymentPurpose.CONSERVANCY,
            paymentMethod: PaymentMethod.CASH,
            bankTransactionNo: null,
            referenceId: 'TRE-20240308-1a82',
            status: PaymentStatus.PENDING_PAYMENT,
            userId: 3,
            paynowReceiptImageUrl: null,
            amount: '5094',
            createdAt: '2024-03-08T07:53:13.769Z',
            updatedAt: '2024-03-08T07:53:13.769Z',
        },
    ],
    conservancyPayment: {
        id: 1,
        name: 'Conservancy Fee',
        billingPeriod: 'Quarter 1 of 2024',
        dueDate: '2024-03-03T00:00:00.000Z',
        condoId: 1,
        createdAt: '2024-03-08T04:28:46.861Z',
        updatedAt: '2024-03-08T04:28:46.861Z',
    },
    conservancyPaymentBreakdowns: [
        {
            id: 3,
            conservancyUnitPaymentId: 1,
            feeName: 'Security Fee',
            amount: '150',
            createdAt: '2024-03-08T04:28:46.894Z',
            updatedAt: '2024-03-08T04:28:46.894Z',
        },
        {
            id: 2,
            conservancyUnitPaymentId: 1,
            feeName: 'Rubbish Disposal',
            amount: '60',
            createdAt: '2024-03-08T04:28:46.893Z',
            updatedAt: '2024-03-08T04:28:46.893Z',
        },
        {
            id: 1,
            conservancyUnitPaymentId: 1,
            feeName: 'Conservancy Fee',
            amount: '4884',
            createdAt: '2024-03-08T04:28:46.893Z',
            updatedAt: '2024-03-08T04:28:46.893Z',
        },
    ],
};

const component = (
    <PaymentDetails
        navigation={
            mockNavigation as NativeStackNavigationProp<
                NavParamList,
                PageName.PAYMENT_DETAILS_SCREEN
            >
        }
        route={mockRoute}
    />
);

describe('Payment Details components', () => {
    beforeEach(() => {
        cleanup();
    });

    it('should render payment details correctly', async () => {
        const { getByTestId, getByText } = renderWithProviders(component, {
            preloadedState: {
                payment: {
                    selectedPayment: mockPayment,
                    paymentsArray: [],
                    isLoading: false,
                    upcomingPayments: [],
                },
            },
        });

        expect(getByTestId('payment-detail.header-bar-text')).toBeOnTheScreen();
        expect(
            getByText('Conservancy Fee\nQuarter 1 of 2024'),
        ).toBeOnTheScreen();
        expect(getByText('Due on 3 Mar 2024')).toBeOnTheScreen();
        expect(getByTestId('payment-status-badge')).toBeOnTheScreen();

        expect(getByText('Fee Breakdown:')).toBeOnTheScreen();
        expect(getByText('Item Description')).toBeOnTheScreen();
        expect(getByText('Item Total')).toBeOnTheScreen();
        expect(getByText('Security Fee')).toBeOnTheScreen();
        expect(getByText('$150')).toBeOnTheScreen();
        expect(getByText('Rubbish Disposal')).toBeOnTheScreen();
        expect(getByText('$60')).toBeOnTheScreen();
        expect(getByText('Conservancy Fee')).toBeOnTheScreen();
        expect(getByText('$4884')).toBeOnTheScreen();
        expect(getByText('Total Amount')).toBeOnTheScreen();
        expect(getByText('$5094')).toBeOnTheScreen();
    });

    it('should render payment method selection if unpaid', async () => {
        const mockUnpaidPayment = {
            ...mockPayment,
            payments: [],
        };

        const { getByTestId } = renderWithProviders(component, {
            preloadedState: {
                payment: {
                    selectedPayment: mockUnpaidPayment,
                    paymentsArray: [],
                    isLoading: false,
                    upcomingPayments: [],
                },
            },
        });

        expect(getByTestId('payment-method-select')).toBeOnTheScreen();
    });

    it('should not render payment method selection if paid', async () => {
        const mockPaidPayment = {
            ...mockPayment,
            payments: [
                {
                    ...mockPayment.payments[0],
                    status: PaymentStatus.PAID,
                },
            ],
        };

        const { queryByTestId } = renderWithProviders(component, {
            preloadedState: {
                payment: {
                    selectedPayment: mockPaidPayment,
                    paymentsArray: [],
                    isLoading: false,
                    upcomingPayments: [],
                },
            },
        });

        expect(queryByTestId('payment-method-select')).not.toBeOnTheScreen();
    });

    it('should not render payment method selection if pending verification', async () => {
        const mockPaidPayment = {
            ...mockPayment,
            payments: [
                {
                    ...mockPayment.payments[0],
                    status: PaymentStatus.PENDING_VERIFICATION,
                },
            ],
        };

        const { queryByTestId } = renderWithProviders(component, {
            preloadedState: {
                payment: {
                    selectedPayment: mockPaidPayment,
                    paymentsArray: [],
                    isLoading: false,
                    upcomingPayments: [],
                },
            },
        });

        expect(queryByTestId('payment-method-select')).not.toBeOnTheScreen();
    });

    it('should not render payment method selection if cancelled', async () => {
        const mockPaidPayment = {
            ...mockPayment,
            payments: [
                {
                    ...mockPayment.payments[0],
                    status: PaymentStatus.CANCELLED,
                },
            ],
        };

        const { queryByTestId } = renderWithProviders(component, {
            preloadedState: {
                payment: {
                    selectedPayment: mockPaidPayment,
                    paymentsArray: [],
                    isLoading: false,
                    upcomingPayments: [],
                },
            },
        });

        expect(queryByTestId('payment-method-select')).not.toBeOnTheScreen();
    });

    it('should not render payment method selection if pending refund', async () => {
        const mockPaidPayment = {
            ...mockPayment,
            payments: [
                {
                    ...mockPayment.payments[0],
                    status: PaymentStatus.PENDING_REFUND,
                },
            ],
        };

        const { queryByTestId } = renderWithProviders(component, {
            preloadedState: {
                payment: {
                    selectedPayment: mockPaidPayment,
                    paymentsArray: [],
                    isLoading: false,
                    upcomingPayments: [],
                },
            },
        });

        expect(queryByTestId('payment-method-select')).not.toBeOnTheScreen();
    });

    it('should not render payment method selection if refunded', async () => {
        const mockPaidPayment = {
            ...mockPayment,
            payments: [
                {
                    ...mockPayment.payments[0],
                    status: PaymentStatus.REFUNDED,
                },
            ],
        };

        const { queryByTestId } = renderWithProviders(component, {
            preloadedState: {
                payment: {
                    selectedPayment: mockPaidPayment,
                    paymentsArray: [],
                    isLoading: false,
                    upcomingPayments: [],
                },
            },
        });

        expect(queryByTestId('payment-method-select')).not.toBeOnTheScreen();
    });
});
