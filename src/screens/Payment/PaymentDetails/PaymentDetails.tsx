import React, { FC } from 'react';
import { PaymentDetailsScreenNavProps } from '../../../navigation/NavProps';
import { View } from 'react-native';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import { useAppSelector } from '../../../store/hook';
import SubHeaderText from '../../../components/ui/titles/SubHeaderText';
import Badge from '../../../components/ui/badge/Badge';
import { generatePaymentStatusBadge } from '../../../components/ui/buttons/PaymentCard/utils/GeneratePaymentStatusBadge';
import styles from './PaymentDetailsStyles';
import NormalText from '../../../components/ui/titles/NormalText';
import { toDateFormat } from '../../../utils/dateTimeUtil';
import PageName from '../../../navigation/PageNameEnum';
import { IPaymentPendingProps } from '../PaymentStatus/PendingCashPayment/PendingCashPayment';
import PaymentMethodSelect from '../../../components/PaymentMethodSelect';
import { PaymentMethod } from '../../../store/manageBooking/slice';

const PaymentDetails: FC<PaymentDetailsScreenNavProps> = ({ navigation }) => {
    const { selectedPayment } = useAppSelector(state => state.payment);

    const onPressPayNowHandler = () => {
        let paymentDetails: Omit<IPaymentPendingProps, 'paymentMethod'> = {
            name: selectedPayment!.conservancyPayment.name,
            billingPeriod: selectedPayment!.conservancyPayment.billingPeriod,
            dueDate: selectedPayment!.conservancyPayment.dueDate,
            referenceId: selectedPayment!.referenceId,
            totalAmount: selectedPayment!.totalAmount,
        };

        navigation.navigate(PageName.PENDING_PAYNOW_PAYMENT_SCREEN, {
            paymentDetails: {
                ...paymentDetails,
                paymentMethod: PaymentMethod.PAYNOW,
            },
        });
    };
    const onPressCashHandler = () => {
        let paymentDetails: Omit<IPaymentPendingProps, 'paymentMethod'> = {
            name: selectedPayment!.conservancyPayment.name,
            billingPeriod: selectedPayment!.conservancyPayment.billingPeriod,
            dueDate: selectedPayment!.conservancyPayment.dueDate,
            referenceId: selectedPayment!.referenceId,
            totalAmount: selectedPayment!.totalAmount,
        };

        navigation.navigate(PageName.PENDING_CASH_PAYMENT_SCREEN, {
            paymentDetails: {
                ...paymentDetails,
                paymentMethod: PaymentMethod.CASH,
            },
        });
    };

    return (
        <>
            <View style={styles.rootContainer}>
                <HeaderBar
                    title="View Payment"
                    onBackButtonPress={() => navigation.goBack()}
                    testID="payment-detail.header-bar-text"
                    showBackButton={true}
                    size="xSmall"
                />
                <View style={styles.generalContainer}>
                    <View>
                        <View
                            style={[
                                styles.rowContainer,
                                styles.badgeContainer,
                            ]}>
                            <SubHeaderText underline={false}>
                                {`${selectedPayment?.conservancyPayment.name}\n${selectedPayment?.conservancyPayment.billingPeriod}`}
                            </SubHeaderText>
                            <Badge
                                details={generatePaymentStatusBadge(
                                    selectedPayment!,
                                )}
                                textStyles={styles.badge}
                                testID="payment-status-badge"
                            />
                        </View>

                        <View>
                            <NormalText>
                                {`Due on ${toDateFormat(
                                    selectedPayment?.conservancyPayment
                                        .dueDate!,
                                )}`}
                            </NormalText>
                        </View>

                        <View style={styles.feeBreakdownContainer}>
                            <SubHeaderText>Fee Breakdown:</SubHeaderText>
                        </View>

                        <View>
                            <View style={styles.paymentRowContainer}>
                                <NormalText
                                    style={[
                                        styles.cellLeftContainer,
                                        styles.italicText,
                                    ]}>
                                    {'Item Description'}
                                </NormalText>
                                <NormalText
                                    style={[
                                        styles.cellRightContainer,
                                        styles.italicText,
                                    ]}>
                                    {'Item Total'}
                                </NormalText>
                            </View>

                            <View>
                                {selectedPayment?.conservancyPaymentBreakdowns.map(
                                    details => (
                                        <View
                                            style={styles.paymentRowContainer}
                                            key={details.feeName}>
                                            <NormalText
                                                style={
                                                    styles.cellLeftContainer
                                                }>
                                                {details.feeName}
                                            </NormalText>
                                            <NormalText
                                                style={[
                                                    styles.cellRightContainer,
                                                    styles.boldText,
                                                ]}>
                                                {`$${details.amount}`}
                                            </NormalText>
                                        </View>
                                    ),
                                )}
                            </View>

                            <View style={styles.horizontalLine} />

                            <View style={styles.paymentRowContainer}>
                                <NormalText style={[styles.cellLeftContainer]}>
                                    {'Total Amount'}
                                </NormalText>
                                <NormalText
                                    style={[
                                        styles.cellRightContainer,
                                        styles.boldText,
                                    ]}>
                                    {`$${selectedPayment?.totalAmount}`}
                                </NormalText>
                            </View>
                        </View>
                    </View>
                    {!selectedPayment!.payments[0] && (
                        <View>
                            <PaymentMethodSelect
                                onPressPayNowHandler={onPressPayNowHandler}
                                onPressCashHandler={onPressCashHandler}
                                isViaUen={false}
                                testID="payment-method-select"
                            />
                        </View>
                    )}
                </View>
            </View>
        </>
    );
};

export default PaymentDetails;
