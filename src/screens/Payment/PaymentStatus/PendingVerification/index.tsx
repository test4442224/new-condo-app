import React, { FC } from 'react';

import ScrollableContainer from '../../../../components/container/ScrollableContainer';
import { View } from 'react-native-ui-lib';
import Header from '../components/Header';
import PrimaryButton from '../../../../components/ui/buttons/PrimaryButton';
import PageName from '../../../../navigation/PageNameEnum';
import { PendingVerificationScreenNavProps } from '../../../../navigation/NavProps';
import styles from '../PaymentStatusStyle';

export const headerText =
    'Thank you for your payment.\n' +
    'A confirmation email will be sent to you shortly.\n' +
    'Admin will verify your payment and payment status will be reflected accordingly.';

const PendingVerification: FC<PendingVerificationScreenNavProps> = ({
    navigation,
}) => {
    return (
        <>
            <ScrollableContainer
                style={[styles.scrollableContainer, styles.successBg]}>
                <View
                    style={[styles.rootContainer, styles.justifySpaceBetween]}>
                    <Header
                        testId="payment-type-header"
                        type="success"
                        title="Pending Verification"
                        text={headerText}
                    />

                    <View style={styles.buttonContainer}>
                        <PrimaryButton
                            label="Back to Home Page"
                            testID="payment-pending.view-payment-btn"
                            onPress={() => {
                                navigation.navigate(PageName.HOME_PAGE_SCREEN);
                            }}
                        />
                    </View>
                </View>
            </ScrollableContainer>
        </>
    );
};

export default PendingVerification;
