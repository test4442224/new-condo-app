import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { cleanup, render, userEvent } from '@testing-library/react-native';
import NavParamList from '../../../../navigation/NavParmList';
import PageName from '../../../../navigation/PageNameEnum';
import { RouteProp } from '@react-navigation/native';
import PendingVerification, { headerText } from '.';
import React from 'react';

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

const mockNavigation: Partial<
    NativeStackNavigationProp<
        NavParamList,
        PageName.PENDING_VERIFICATION_SCREEN
    >
> = {
    navigate: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.PENDING_VERIFICATION_SCREEN> = {
    key: 'PendingVerification',
    name: PageName.PENDING_VERIFICATION_SCREEN,
};

describe('PendingVerification screen', () => {
    beforeEach(() => {
        cleanup();
    });

    it('should render correctly', () => {
        const { getByRole, getByText } = render(
            <PendingVerification
                navigation={
                    mockNavigation as NativeStackNavigationProp<
                        NavParamList,
                        PageName.PENDING_VERIFICATION_SCREEN
                    >
                }
                route={mockRoute}
            />,
        );

        expect(getByText(headerText)).toBeOnTheScreen();
        expect(
            getByRole('button', { name: 'Back to Home Page' }),
        ).toBeOnTheScreen();
    });

    it('should navigate to home page on pressing button', async () => {
        const user = userEvent.setup();

        const { getByRole } = render(
            <PendingVerification
                navigation={
                    mockNavigation as NativeStackNavigationProp<
                        NavParamList,
                        PageName.PENDING_VERIFICATION_SCREEN
                    >
                }
                route={mockRoute}
            />,
        );

        await user.press(getByRole('button', { name: 'Back to Home Page' }));
        expect(mockNavigation.navigate).toHaveBeenCalledWith(
            PageName.HOME_PAGE_SCREEN,
        );
    });
});
