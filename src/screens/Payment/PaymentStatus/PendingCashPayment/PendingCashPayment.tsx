import React, { FC } from 'react';
import styles from '../PaymentStatusStyle';
import { View } from 'react-native';

import { PendingCashPaymentScreenNavProps } from '../../../../navigation/NavProps';

import ScrollableContainer from '../../../../components/container/ScrollableContainer';

import { PaymentMethod } from '../../../../store/manageBooking/slice';
import Loading from '../../../../components/ui/loading/Loading';
import Header from '../components/Header';
import {
    isAfterCurrentTime,
    toDateFormat,
} from '../../../../utils/dateTimeUtil';
import RoundContainer from '../../../../components/container/RoundContainer';
import Field from '../components/Field';
import NormalText from '../../../../components/ui/titles/NormalText';
import PrimaryButton from '../../../../components/ui/buttons/PrimaryButton';
import PageName from '../../../../navigation/PageNameEnum';

export interface IPaymentPendingProps {
    referenceId: string;
    name: string;
    billingPeriod: string;
    paymentMethod: PaymentMethod;
    totalAmount: number;
    dueDate: string;
}

export type Error = {
    hasError: boolean;
    title: string;
    message: string;
};

const PendingCashPayment: FC<PendingCashPaymentScreenNavProps> = ({
    route,
    navigation,
}) => {
    const { paymentDetails } = route.params;

    let headerText: React.JSX.Element;
    const isNotOverdue = isAfterCurrentTime(paymentDetails.dueDate);
    headerText = isNotOverdue ? (
        <>
            Please make payment at the management office by{' '}
            <NormalText style={styles.boldText}>
                {toDateFormat(paymentDetails.dueDate)}
            </NormalText>
            .
        </>
    ) : (
        <>
            Please make the payment at the management office.{`\n`}The payment
            is overdue.
        </>
    );

    return (
        <View style={[styles.rootContainer, styles.pendingBg]}>
            <ScrollableContainer
                style={[styles.scrollableContainer, styles.pendingBg]}>
                <Header
                    testId="payment-type-header"
                    type="pending"
                    title="Pending Payment"
                    text={headerText}
                />

                <View style={styles.cardContainer}>
                    <RoundContainer style={styles.roundContainer}>
                        <View style={styles.rowContainer}>
                            <Field
                                label="Booking Reference No."
                                value={paymentDetails.referenceId}
                            />
                        </View>

                        <View style={styles.textCardContainer}>
                            <NormalText
                                center={true}
                                style={styles.labelDotText}>
                                ................................................
                            </NormalText>
                        </View>

                        <Field label="Name" value={paymentDetails.name} />
                        <Field
                            label="Billing Period"
                            value={paymentDetails.billingPeriod}
                        />
                        <Field
                            label="Amount Payable"
                            value={`$${String(paymentDetails.totalAmount)}`}
                        />
                        <Field
                            label="Payment Method"
                            value={paymentDetails.paymentMethod}
                        />
                    </RoundContainer>
                </View>
                <View style={styles.buttonContainer}>
                    <PrimaryButton
                        label="Back to My Payments"
                        testID="payment-pending.view-payment-btn"
                        onPress={() => {
                            navigation.navigate(
                                PageName.MANAGE_PAYMENTS_SCREEN,
                            );
                        }}
                    />
                </View>
            </ScrollableContainer>
            <Loading />
        </View>
    );
};

export default PendingCashPayment;
