import { cleanup } from '@testing-library/react-native';
import { setupServer } from 'msw/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import PageName from '../../../../navigation/PageNameEnum';
import NavParamList from '../../../../navigation/NavParmList';
import { RouteProp } from '@react-navigation/native';
import React from 'react';
import { renderWithProviders } from '../../../../test/utils';
import { HttpResponse, http } from 'msw';
import PaymentPending from './PendingCashPayment';
import { PaymentMethod } from '../../../../store/manageBooking/slice';

jest.mock('@react-navigation/native', () => {
    return {
        useNavigation: jest.fn(),
    };
});

jest.mock('../../../../navigation/NavProps.ts', () => ({}));

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

const mockNavigation: Partial<
    NativeStackNavigationProp<
        NavParamList,
        PageName.PENDING_CASH_PAYMENT_SCREEN
    >
> = {
    navigate: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.PENDING_CASH_PAYMENT_SCREEN> = {
    key: 'PendingPaymentKey',
    name: PageName.PENDING_CASH_PAYMENT_SCREEN,
    params: {
        paymentDetails: {
            name: '',
            billingPeriod: '',
            dueDate: '2029-05-05',
            paymentMethod: PaymentMethod.CASH,
            referenceId: '',
            totalAmount: 5094,
        },
    },
};

let overdueMockRoute: RouteProp<
    NavParamList,
    PageName.PENDING_CASH_PAYMENT_SCREEN
> = {
    key: 'PendingPaymentKey',
    name: PageName.PENDING_CASH_PAYMENT_SCREEN,
    params: {
        paymentDetails: {
            name: '',
            billingPeriod: '',
            dueDate: '2021-01-01',
            paymentMethod: PaymentMethod.CASH,
            referenceId: '',
            totalAmount: 5094,
        },
    },
};

jest.mock('../../../../utils/hooks/useRtkResponse.ts', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        showToast: false,
    })),
}));

const component = (
    <PaymentPending
        navigation={
            mockNavigation as NativeStackNavigationProp<
                NavParamList,
                PageName.PENDING_CASH_PAYMENT_SCREEN
            >
        }
        route={mockRoute}
    />
);

const overDueComponent = (
    <PaymentPending
        navigation={
            mockNavigation as NativeStackNavigationProp<
                NavParamList,
                PageName.PENDING_CASH_PAYMENT_SCREEN
            >
        }
        route={overdueMockRoute}
    />
);

const handlers = [
    http.get('http://localhost:3001/conservancy-fees', async () => {
        return HttpResponse.json({});
    }),
];

const server = setupServer(...handlers);

describe('Payment Details components', () => {
    beforeAll(() => {
        server.listen();
        server.events.on('request:start', ({ request }) => {
            console.log('MSW intercepted:', request.method, request.url);
        });
    });

    beforeEach(() => {
        cleanup();
    });

    afterEach(() => {
        server.resetHandlers();
    });

    it('should render correctly', async () => {
        const { getByTestId, getByText } = renderWithProviders(component);

        expect(getByTestId('payment-type-header')).toBeTruthy();
        expect(getByText('Booking Reference No.')).toBeTruthy();
        expect(getByText('Name')).toBeTruthy();
        expect(getByText('Billing Period')).toBeTruthy();
        expect(getByText('Amount Payable')).toBeTruthy();
        expect(getByText('Payment Method')).toBeTruthy();
        expect(getByTestId('payment-pending.view-payment-btn')).toBeTruthy();
    });

    it('should show payment is overdue if date is past', () => {
        const { getByText } = renderWithProviders(overDueComponent);

        expect(
            getByText(
                'Please make the payment at the management office. The payment is overdue.',
            ),
        ).toBeTruthy();
    });
});
