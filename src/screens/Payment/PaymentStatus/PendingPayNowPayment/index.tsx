import React, { FC, useState } from 'react';

import styles from '../PaymentStatusStyle';
import { View } from 'react-native';

import { PendingPayNowPaymentScreenNavProps } from '../../../../navigation/NavProps';

import ScrollableContainer from '../../../../components/container/ScrollableContainer';

import Loading from '../../../../components/ui/loading/Loading';
import Header from '../components/Header';
import { toDateFormat } from '../../../../utils/dateTimeUtil';
import RoundContainer from '../../../../components/container/RoundContainer';
import Field from '../components/Field';
import NormalText from '../../../../components/ui/titles/NormalText';
import PrimaryButton from '../../../../components/ui/buttons/PrimaryButton';
import { ImageData } from '../../../../store/manageBooking/api';
import ImageUploadField from '../../../../components/statusScreen/ImageUploadField';
import PopModal from '../../../../components/modal/PopModal';
import { Image } from 'react-native-ui-lib';
import { Dimensions } from 'react-native';
import { useCreatePaymentMutation } from '../../../../store/payment/api';
import { useAppDispatch } from '../../../../store/hook';
import { loadingActions } from '../../../../store/loading/slice';
import { PaymentMethod, PaymentPurpose } from '../../../../store/payment/slice';
import { RtkErrorHandler } from '../../../../utils/error/RtkErrorHandler.util';
import PageName from '../../../../navigation/PageNameEnum';
import CopyButton from '../../../FacilityBooking/BookingComplete/components/CopyButton';
import { CopiedFeedback } from '../../../FacilityBooking/BookingComplete/BookingCompletePending';
import Toast from 'react-native-simple-toast';
import { useGetCondoDetailsQuery } from '../../../../store/condo/api';
import useRtkResponse from '../../../../utils/hooks/useRtkResponse';
import { getImageUrl } from '../../../../Constants/api';

export type Error = {
    hasError: boolean;
    title: string;
    message: string;
};

const PendingPayNowPayment: FC<PendingPayNowPaymentScreenNavProps> = ({
    route,
    navigation,
}) => {
    const dispatch = useAppDispatch();
    const { paymentDetails } = route.params;
    const { setLoading } = loadingActions;

    const [selectedPhoto, setSelectedPhoto] = useState<ImageData | null>(null);
    const [error, setError] = useState<Error>({
        hasError: false,
        title: '',
        message: '',
    });
    const [isRefNoCopiedShown, setIsRefNoCopiedShown] =
        useState<boolean>(false);
    const [isUENCopiedShown, setIsUENCopiedShown] = useState<boolean>(false);

    const [createPayment] = useCreatePaymentMutation();
    const {
        data: condo,
        isFetching: isFetchingCondo,
        error: getCondoError,
    } = useGetCondoDetailsQuery();

    const headerText = (
        <>
            Please make payment to the UEN provided{' '}
            <NormalText style={styles.boldText}>
                {toDateFormat(paymentDetails.dueDate)}
            </NormalText>
            . Kindly include the payment reference no. during payment and upload
            the payment receipt for verification purposes.
        </>
    );
    const screenWidth = Dimensions.get('window').width;
    const imageWidth = screenWidth * 0.6;

    useRtkResponse(isFetchingCondo, getCondoError);

    const handleSubmit = async () => {
        if (!selectedPhoto) return;

        dispatch(setLoading(true));
        try {
            await createPayment({
                paymentMethod: PaymentMethod.PAYNOW,
                paymentPurpose: PaymentPurpose.CONSERVANCY,
                referenceId: paymentDetails.referenceId,
                image: selectedPhoto,
            });

            navigation.replace(PageName.PENDING_VERIFICATION_SCREEN);
        } catch (err) {
            setError({
                hasError: true,
                title: 'An Error Occurred',
                message: RtkErrorHandler.getMessage(err),
            });
        } finally {
            dispatch(setLoading(false));
        }
    };

    const showCopiedFeedback = (feedback: CopiedFeedback) => {
        Toast.showWithGravityAndOffset(
            'Copied to clipboard!',
            Toast.SHORT,
            Toast.BOTTOM,
            0,
            20,
        );
        if (feedback === CopiedFeedback.RefNo) {
            setIsRefNoCopiedShown(true);
        } else if (feedback === CopiedFeedback.UEN) {
            setIsUENCopiedShown(true);
        }
    };

    return (
        <>
            <ScrollableContainer
                style={[styles.scrollableContainer, styles.pendingBg]}>
                <View style={[styles.rootContainer]}>
                    <Header
                        testId="payment-type-header"
                        type="pending"
                        title="Pending Payment"
                        text={headerText}
                    />

                    <View style={styles.cardContainer}>
                        <RoundContainer style={styles.roundContainer}>
                            <View style={styles.rowContainer}>
                                <Field
                                    label="Payment Reference No."
                                    value={paymentDetails.referenceId}
                                />
                                <CopyButton
                                    testID="copyRefId"
                                    valueToCopy={paymentDetails.referenceId}
                                    isValueCopiedShown={isRefNoCopiedShown}
                                    setIsValueCopiedShown={
                                        setIsRefNoCopiedShown
                                    }
                                    showCopiedFeedback={showCopiedFeedback.bind(
                                        null,
                                        CopiedFeedback.RefNo,
                                    )}
                                />
                            </View>
                            <Field
                                label="Amount Payable"
                                value={`$${String(paymentDetails.totalAmount)}`}
                            />

                            <View style={styles.textCardContainer}>
                                <NormalText
                                    center={true}
                                    style={styles.labelDotText}>
                                    ................................................
                                </NormalText>
                            </View>

                            <View style={styles.imageContainer}>
                                <Image
                                    testID="qrCode"
                                    src={
                                        condo &&
                                        getImageUrl(condo.paynowQrCodeImagePath)
                                    }
                                    style={{
                                        width: imageWidth,
                                        height: imageWidth,
                                    }}
                                />
                            </View>

                            <View style={styles.rowContainer}>
                                <Field label="UEN" value={condo?.uen} />
                                {condo && condo.uen && (
                                    <CopyButton
                                        testID="copyUen"
                                        valueToCopy={condo.uen}
                                        isValueCopiedShown={isUENCopiedShown}
                                        setIsValueCopiedShown={
                                            setIsUENCopiedShown
                                        }
                                        showCopiedFeedback={showCopiedFeedback.bind(
                                            null,
                                            CopiedFeedback.UEN,
                                        )}
                                    />
                                )}
                            </View>
                            <ImageUploadField
                                selectedPhoto={selectedPhoto}
                                setSelectedPhoto={setSelectedPhoto}
                                setError={setError}
                            />
                        </RoundContainer>
                    </View>
                    <View style={styles.buttonContainer}>
                        <PrimaryButton
                            disabled={selectedPhoto === null}
                            label="Proceed"
                            testID="proceed-btn"
                            onPress={handleSubmit}
                            style={[styles.buttonMarginBottom]}
                        />
                        <PrimaryButton
                            label="Back to My Payments"
                            testID="payment-pending.view-payment-btn"
                            onPress={() => {
                                navigation.navigate(
                                    PageName.MANAGE_PAYMENTS_SCREEN,
                                );
                            }}
                        />
                    </View>
                </View>
            </ScrollableContainer>
            <PopModal
                visibility={error.hasError}
                modalTitle={error.title}
                modalBody={error.message}
                confirmText="OK"
                onConfirm={() => {
                    setError({
                        hasError: false,
                        title: '',
                        message: '',
                    });
                }}
            />
            <Loading />
        </>
    );
};

export default PendingPayNowPayment;
