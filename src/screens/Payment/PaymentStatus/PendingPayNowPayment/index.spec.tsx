import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import {
    cleanup,
    screen,
    userEvent,
    waitFor,
} from '@testing-library/react-native';
import NavParamList from '../../../../navigation/NavParmList';
import PageName from '../../../../navigation/PageNameEnum';
import { RouteProp } from '@react-navigation/native';
import React from 'react';
import { renderWithProviders } from '../../../../test/utils';
import PendingPayNowPayment from '.';
import { PaymentMethod } from '../../../../store/payment/slice';
import { HttpResponse, http } from 'msw';
import { setupServer } from 'msw/native';

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

jest.mock('@react-native-clipboard/clipboard', () => ({
    setString: jest.fn(),
}));

jest.mock('react-native-simple-toast', () => ({
    showWithGravityAndOffset: jest.fn(),
    SHORT: '',
    BOTTOM: '',
}));

jest.mock('react-native-image-picker', () => ({
    launchImageLibrary: jest.fn(),
}));

jest.mock('../../../../utils/hooks/useRtkResponse.ts', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        showToast: false,
    })),
}));

const clipboardMock = require('@react-native-clipboard/clipboard');
const toastMock = require('react-native-simple-toast');
const launchImageLibraryMock =
    require('react-native-image-picker').launchImageLibrary;

const mockNavigation: Partial<
    NativeStackNavigationProp<
        NavParamList,
        PageName.PENDING_PAYNOW_PAYMENT_SCREEN
    >
> = {
    replace: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.PENDING_PAYNOW_PAYMENT_SCREEN> =
    {
        key: 'PendingPayNowPayment',
        name: PageName.PENDING_PAYNOW_PAYMENT_SCREEN,
        params: {
            paymentDetails: {
                name: '',
                billingPeriod: '',
                dueDate: '2024-03-31T00:00:00.000Z',
                paymentMethod: PaymentMethod.PAYNOW,
                referenceId: 'testReferenceId',
                totalAmount: 5094,
            },
        },
    };

const componentUnderTest = (
    <PendingPayNowPayment
        navigation={
            mockNavigation as NativeStackNavigationProp<
                NavParamList,
                PageName.PENDING_PAYNOW_PAYMENT_SCREEN
            >
        }
        route={mockRoute}
    />
);

const handlers = [
    http.get('http://localhost:3001/condo', async () => {
        return HttpResponse.json({
            paynowQrCodeImagePath: 'qrCode.jpeg',
            uen: 'testUen',
        });
    }),
    http.post('http://localhost:3001/payment', async () => {
        return HttpResponse.json({});
    }),
];

const server = setupServer(...handlers);

describe('PendingPayNowPayment screen', () => {
    beforeAll(() => {
        server.listen();
        jest.useFakeTimers({
            doNotFake: ['queueMicrotask'],
        });
    });

    afterEach(() => {
        cleanup();
        server.resetHandlers();
        jest.clearAllMocks();
    });

    afterAll(() => {
        server.close();
        jest.useRealTimers();
    });

    it('should render correctly', async () => {
        const { getByText, findByText, getByTestId, getByRole } =
            renderWithProviders(componentUnderTest);

        expect(getByText('31 Mar 2024', { exact: false })).toBeOnTheScreen();
        expect(getByText('Payment Reference No.')).toBeOnTheScreen();
        expect(getByText('testReferenceId')).toBeOnTheScreen();
        expect(getByText('Amount Payable')).toBeOnTheScreen();
        expect(getByText('$5094')).toBeOnTheScreen();
        expect(getByText('UEN')).toBeOnTheScreen();
        expect(await findByText('testUen')).toBeOnTheScreen();
        expect(getByTestId('qrCode')).toHaveProp('src', 'qrCode.jpeg');
        expect(getByRole('button', { name: 'Proceed' })).toBeOnTheScreen();
        expect(
            getByRole('button', { name: 'Back to My Payments' }),
        ).toBeOnTheScreen();
    });

    it('should copy reference ID and show Toast message when copy button is pressed', async () => {
        const user = userEvent.setup();

        const { getByTestId } = renderWithProviders(componentUnderTest);

        await user.press(getByTestId('copyRefId'));

        expect(clipboardMock.setString).toHaveBeenCalledWith('testReferenceId');
        expect(toastMock.showWithGravityAndOffset).toHaveBeenCalledWith(
            'Copied to clipboard!',
            expect.anything(),
            expect.anything(),
            expect.anything(),
            expect.anything(),
        );
    });

    it('should copy UEN and show Toast message when copy button is pressed', async () => {
        const user = userEvent.setup();

        const { findByTestId } = renderWithProviders(componentUnderTest);

        const copyBtn = await findByTestId('copyUen');
        await user.press(copyBtn);

        expect(clipboardMock.setString).toHaveBeenCalledWith('testUen');
        expect(toastMock.showWithGravityAndOffset).toHaveBeenCalledWith(
            'Copied to clipboard!',
            expect.anything(),
            expect.anything(),
            expect.anything(),
            expect.anything(),
        );
    });

    it('should enable Proceed button after image successfully selected', async () => {
        const user = userEvent.setup();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: false,
            errorCode: null,
            assets: [
                {
                    fileName: 'imageName',
                    type: 'image/jpg',
                    uri: 'imageUri',
                    fileSize: 1000000,
                },
            ],
        });

        const { getByRole } = renderWithProviders(componentUnderTest);

        const proceedButton = getByRole('button', {
            name: 'Proceed',
        });
        expect(proceedButton).toBeDisabled();
        await user.press(
            getByRole('button', { name: 'Upload PayNow Receipt' }),
        );
        await waitFor(() => expect(proceedButton).toBeEnabled());
    });

    // Skipped the below test as it seems that the createPayment mutation is preventing Jest from
    // exiting. Nevertheless this scenario is covered in the E2E test.
    xit('should navigate to Pending Verification screen if upload is successful', async () => {
        const user = userEvent.setup();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: false,
            errorCode: null,
            assets: [
                {
                    fileName: 'imageName',
                    type: 'image/jpg',
                    uri: 'imageUri',
                    fileSize: 1000000,
                },
            ],
        });

        const { getByRole } = renderWithProviders(componentUnderTest);

        const proceedButton = getByRole('button', { name: 'Proceed' });
        await user.press(
            getByRole('button', { name: 'Upload PayNow Receipt' }),
        );
        await user.press(proceedButton);
        expect(mockNavigation.replace).toHaveBeenCalledWith(
            PageName.PENDING_VERIFICATION_SCREEN,
        );
    });

    it('should show error message if camera not available', async () => {
        const user = userEvent.setup();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: false,
            errorCode: 'camera_unavailable',
        });

        const { getByRole } = renderWithProviders(componentUnderTest);

        const uploadButton = getByRole('button', {
            name: 'Upload PayNow Receipt',
        });
        await user.press(uploadButton);

        await waitFor(() =>
            expect(screen.getByText('Camera Unavailable')).toBeOnTheScreen(),
        );
        await waitFor(() =>
            expect(
                screen.getByText('Camera not available on device.'),
            ).toBeOnTheScreen(),
        );
    });

    it('should show error message if camera permission not granted', async () => {
        const user = userEvent.setup();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: false,
            errorCode: 'permission',
        });

        const { getByRole } = renderWithProviders(componentUnderTest);

        const uploadButton = getByRole('button', {
            name: 'Upload PayNow Receipt',
        });
        await user.press(uploadButton);

        await waitFor(() =>
            expect(
                screen.getByText('Camera Access Not Granted'),
            ).toBeOnTheScreen(),
        );
        await waitFor(() =>
            expect(
                screen.getByText('Permission to access camera not granted.'),
            ).toBeOnTheScreen(),
        );
    });

    it('should show generic error message', async () => {
        const user = userEvent.setup();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: false,
            errorCode: 'others',
        });

        const { getByRole } = renderWithProviders(componentUnderTest);

        const uploadButton = getByRole('button', {
            name: 'Upload PayNow Receipt',
        });
        await user.press(uploadButton);

        await waitFor(() =>
            expect(screen.getByText('An Error Occurred')).toBeOnTheScreen(),
        );
        await waitFor(() =>
            expect(
                screen.getByText(
                    'Please contact condo management for assistance.',
                ),
            ).toBeOnTheScreen(),
        );
    });

    it('should show error message if invalid file format uploaded', async () => {
        const user = userEvent.setup();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: false,
            errorCode: null,
            assets: [
                {
                    fileName: 'fileName',
                    type: 'application/pdf',
                    uri: 'fileUri',
                    fileSize: 1000000,
                },
            ],
        });

        const { getByRole } = renderWithProviders(componentUnderTest);

        const uploadButton = getByRole('button', {
            name: 'Upload PayNow Receipt',
        });
        await user.press(uploadButton);

        await waitFor(() =>
            expect(screen.getByText('Invalid File Format')).toBeOnTheScreen(),
        );
        await waitFor(() =>
            expect(screen.getByTestId('modal-body')).toBeOnTheScreen(),
        );
        expect(screen.getByTestId('modal-body')).toHaveTextContent(
            'Only .jpeg, .jpg or .png file allowed.',
        );
    });

    it('should show error message if image size too large', async () => {
        const user = userEvent.setup();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: false,
            errorCode: null,
            assets: [
                {
                    fileName: 'imageName',
                    type: 'image/jpg',
                    uri: 'imageUri',
                    fileSize: 4000000,
                },
            ],
        });

        const { getByRole } = renderWithProviders(componentUnderTest);

        const uploadButton = getByRole('button', {
            name: 'Upload PayNow Receipt',
        });
        await user.press(uploadButton);

        await waitFor(() =>
            expect(screen.getByText('Invalid File Size')).toBeOnTheScreen(),
        );
        await waitFor(() =>
            expect(screen.getByTestId('modal-body')).toBeOnTheScreen(),
        );
        expect(screen.getByTestId('modal-body')).toHaveTextContent(
            'Maximum file size is 2MB.',
        );
    });
});
