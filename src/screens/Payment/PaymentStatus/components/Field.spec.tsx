import { cleanup, render } from '@testing-library/react-native';
import Field from './Field';
import React from 'react';

describe('Field component', () => {
    beforeEach(() => {
        cleanup();
    });

    it('should render text correctly', () => {
        const { getByTestId } = render(
            <Field label="Test Label" value="Test Value" />,
        );

        expect(getByTestId('label')).toHaveTextContent('Test Label');
        expect(getByTestId('value')).toHaveTextContent('Test Value');
    });

    it('should not render the component with testID "value" if the value prop is undefined', () => {
        const { getByTestId, queryByTestId } = render(
            <Field label="Test Label" value={undefined} />,
        );

        expect(getByTestId('label')).toBeOnTheScreen();
        expect(queryByTestId('value')).not.toBeOnTheScreen();
    });
});
