import { cleanup, render } from '@testing-library/react-native';
import React from 'react';
import Header from './Header';

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: () => '',
}));

describe('Header component', () => {
    beforeEach(() => {
        cleanup();
    });

    it('should render text correctly', () => {
        const header = 'Test Pending';
        const subheader = 'Test text';
        const { getByTestId } = render(
            <Header type="pending" title={header} text={subheader} />,
        );

        expect(getByTestId('header')).toHaveTextContent(header);
        expect(getByTestId('subheader-text')).toHaveTextContent(subheader);
    });
});
