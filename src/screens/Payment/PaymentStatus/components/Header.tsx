import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React, { FC } from 'react';
import { View } from 'react-native';
import {
    faCircleCheck,
    faExclamationCircle,
} from '@fortawesome/free-solid-svg-icons';
import styles from '../PaymentStatusStyle';
import HeaderText from '../../../../components/ui/titles/HeaderText';
import NormalText from '../../../../components/ui/titles/NormalText';
import Colors from '../../../../Constants/Colors';

type Props = {
    type: 'pending' | 'success';
    title: string;
    text: string | React.JSX.Element;
    testId?: string;
};

const Header: FC<Props> = ({ type, title, text, testId }) => {
    return (
        <View testID={testId}>
            <View style={styles.iconContainer}>
                <FontAwesomeIcon
                    icon={
                        type === 'pending' ? faExclamationCircle : faCircleCheck
                    }
                    style={
                        type === 'pending'
                            ? styles.infoIcon
                            : styles.successIcon
                    }
                    size={40}
                    testID="icon"
                />
            </View>

            <View style={styles.generalContainer}>
                <View style={styles.textContainer}>
                    <HeaderText
                        testID="header"
                        style={{ color: Colors.$textDefault }}>
                        {title}
                    </HeaderText>
                </View>
                <View style={styles.textContainer} testID="subheader-text">
                    <NormalText center={true}>{text}</NormalText>
                </View>
            </View>
        </View>
    );
};

export default Header;
