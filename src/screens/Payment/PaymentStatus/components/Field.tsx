import React, { FC } from 'react';
import { View } from 'react-native';
import NormalText from '../../../../components/ui/titles/NormalText';
import styles from '../PaymentStatusStyle';

type Props = {
    label: string;
    value: string | undefined;
};

const Field: FC<Props> = ({ label, value }) => {
    return (
        <View style={styles.textCardContainer}>
            <NormalText testID="label" style={styles.labelText}>
                {label}
            </NormalText>
            {value && <NormalText testID="value">{value}</NormalText>}
        </View>
    );
};

export default Field;
