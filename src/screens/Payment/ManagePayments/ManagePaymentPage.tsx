import React, { FC, useState } from 'react';
import { View } from 'react-native';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import { ManagePaymentsScreenNavProps } from '../../../navigation/NavProps';
import PageName from '../../../navigation/PageNameEnum';
import styles from './ManagePaymentPageStyles';
import TabButton from '../../../components/ui/buttons/TabButton';
import PaymentList from '../../../components/lists/paymentList/PaymentList';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import {
    IConservancyFeesAttribute,
    paymentAction,
} from '../../../store/payment/slice';
import { PaymentStatus } from '../../../store/manageBooking/slice';

const ManagePaymentsPage: FC<ManagePaymentsScreenNavProps> = ({
    navigation,
}) => {
    const dispatch = useAppDispatch();
    const [isPendingTabActive, setPendingTabActive] = useState<boolean>(true);
    const isPending = (payment: IConservancyFeesAttribute) => {
        const paymentStatus = payment.payments[0]?.status;
        return ![
            PaymentStatus.PAID,
            PaymentStatus.CANCELLED,
            PaymentStatus.REFUNDED,
            PaymentStatus.PENDING_REFUND,
        ].includes(paymentStatus);
    };

    let fetchedPayments = useAppSelector(state => state.payment.paymentsArray);
    const pendingPayments = fetchedPayments.filter(isPending);
    const historyPayments = fetchedPayments.filter(
        payment => !isPending(payment),
    );

    const onBackButtonPressHandler = () => {
        navigation.navigate(PageName.HOME_PAGE_SCREEN);
        setPendingTabActive(true);
    };

    const onPressPendingTabHandler = async () => {
        if (isPendingTabActive) return;
        setPendingTabActive(true);
    };

    const onPressHistoryTabHandler = async () => {
        if (!isPendingTabActive) return;
        setPendingTabActive(false);
    };

    const onPaymentCardPressHandler = (payment: IConservancyFeesAttribute) => {
        dispatch(paymentAction.selectPaymentForDetail(payment));
        navigation.navigate(PageName.PAYMENT_DETAILS_SCREEN);
    };

    return (
        <View style={styles.rootContainer}>
            <View style={styles.headersContainer}>
                <View style={styles.headerBarContainer}>
                    <HeaderBar
                        testID="manage-payment.header-bar-text"
                        title="View Payments"
                        onBackButtonPress={onBackButtonPressHandler}
                        showBackButton={true}
                    />
                </View>
            </View>
            <View style={styles.buttonsContainer}>
                <View
                    style={
                        isPendingTabActive
                            ? styles.pressed
                            : styles.buttonContainer
                    }>
                    <TabButton
                        testID="manage-payments.pending-booking-tab"
                        onPress={onPressPendingTabHandler}
                        isActive={isPendingTabActive}>
                        Pending
                    </TabButton>
                </View>
                <View
                    style={
                        !isPendingTabActive
                            ? styles.pressed
                            : styles.buttonContainer
                    }>
                    <TabButton
                        testID="manage-payments.history-booking-tab"
                        onPress={onPressHistoryTabHandler}
                        isActive={!isPendingTabActive}>
                        History
                    </TabButton>
                </View>
            </View>
            {isPendingTabActive ? (
                <PaymentList
                    payments={pendingPayments}
                    noPaymentLabel="You have no pending payments at the moment"
                    onPressed={onPaymentCardPressHandler}
                />
            ) : (
                <PaymentList
                    payments={historyPayments}
                    noPaymentLabel="You have no history payments at the moment"
                    onPressed={onPaymentCardPressHandler}
                />
            )}
        </View>
    );
};

export default ManagePaymentsPage;
