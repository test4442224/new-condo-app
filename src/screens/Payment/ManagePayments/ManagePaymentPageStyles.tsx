import { StyleSheet } from 'react-native';
import { Shadows, Spacings } from 'react-native-ui-lib';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
    },
    headersContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerBarContainer: {
        flex: 1,
    },
    buttonsContainer: {
        flexDirection: 'row',
        marginTop: Spacings.s1,
        marginBottom: Spacings.s4,
        marginHorizontal: Spacings.s2,
    },
    buttonContainer: {
        flex: 1,
        marginHorizontal: Spacings.s2,
        opacity: 0.8,
    },
    pressed: {
        flex: 1,
        marginHorizontal: Spacings.s2,
        ...Shadows.sh30.bottom,
    },
    addButton: {
        color: Colors.$iconPrimary,
    },
    flatList: {
        paddingBottom: Spacings.s1 * 40,
    },
});
