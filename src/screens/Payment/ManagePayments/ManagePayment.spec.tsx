import { cleanup, fireEvent } from '@testing-library/react-native';
import { setupServer } from 'msw/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import PageName from '../../../navigation/PageNameEnum';
import NavParamList from '../../../navigation/NavParmList';
import { RouteProp } from '@react-navigation/native';
import React from 'react';
import { renderWithProviders } from '../../../test/utils';
import ManagePaymentsPage from './ManagePaymentPage';
import { HttpResponse, http } from 'msw';

jest.mock('@react-navigation/native', () => {
    return {
        useNavigation: jest.fn(),
    };
});

jest.mock('../../../navigation/NavProps.ts', () => ({}));

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.MANAGE_PAYMENTS_SCREEN>
> = {
    navigate: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.MANAGE_PAYMENTS_SCREEN> = {
    key: 'ManagePaymentsKey',
    name: PageName.MANAGE_PAYMENTS_SCREEN,
};

jest.mock('../../../utils/hooks/useRtkResponse.ts', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        showToast: false,
    })),
}));

const component = (
    <ManagePaymentsPage
        navigation={
            mockNavigation as NativeStackNavigationProp<
                NavParamList,
                PageName.MANAGE_PAYMENTS_SCREEN
            >
        }
        route={mockRoute}
    />
);

const handlers = [
    http.get('http://localhost:3001/conservancy-fees', async () => {
        return HttpResponse.json({});
    }),
];

const server = setupServer(...handlers);

describe('Payment Details components', () => {
    beforeAll(() => {
        server.listen();
        server.events.on('request:start', ({ request }) => {
            console.log('MSW intercepted:', request.method, request.url);
        });
    });

    beforeEach(() => {
        cleanup();
    });

    afterEach(() => {
        server.resetHandlers();
    });

    it('should render correctly', async () => {
        const { getByTestId, getByLabelText } = renderWithProviders(component);

        expect(getByTestId('manage-payment.header-bar-text')).toBeTruthy();
        expect(
            getByLabelText('manage-payments.pending-booking-tab'),
        ).toBeTruthy();
        expect(
            getByLabelText('manage-payments.history-booking-tab'),
        ).toBeTruthy();
        expect(getByTestId('data-display')).toBeTruthy();
    });

    it('should display no payment for pending and history if there is no data', async () => {
        const { getByTestId, getByLabelText } = renderWithProviders(component);

        expect(getByTestId('payment.no-payment')).toBeTruthy();

        const primaryButton = getByLabelText(
            'manage-payments.history-booking-tab',
        );
        fireEvent.press(primaryButton);

        expect(getByTestId('payment.no-payment')).toBeTruthy();
    });
});
