import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { renderWithProviders } from '../../test/utils';
import NavParamList from '../../navigation/NavParmList';
import PageName from '../../navigation/PageNameEnum';
import StatementOfAccountPage from './StatementOfAccountPage';
import React from 'react';
import { RouteProp } from '@react-navigation/native';
import { waitFor } from '@testing-library/react-native';

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

jest.mock('react-native-blob-util', () => ({
    ReactNativeBlobUtil: { config: jest.fn() },
}));

jest.mock('react-native-share', () => ({
    Share: { open: jest.fn() },
}));

jest.mock('react-native-simple-toast', () => {
    return {
        show: jest.fn(),
    };
});

let mockRoute: RouteProp<NavParamList, PageName.STATEMENT_OF_ACCOUNT> = {
    key: 'StatementOfAccountTestKey',
    name: PageName.STATEMENT_OF_ACCOUNT,
};

const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.STATEMENT_OF_ACCOUNT>
> = {
    navigate: jest.fn(),
};

describe('StatementOfAccountPage component', () => {
    const getComponent = (toggle: boolean) =>
        renderWithProviders(
            <StatementOfAccountPage
                navigation={
                    mockNavigation as NativeStackNavigationProp<
                        NavParamList,
                        PageName.STATEMENT_OF_ACCOUNT
                    >
                }
                route={mockRoute}
            />,
            {
                preloadedState: {
                    statementOfAccount: {
                        statementOfAccountArray: toggle
                            ? [
                                  {
                                      balanceAmount: 1000,
                                      createdAt: new Date('2024-04-16'),
                                      endDate: new Date('2024-04-16'),
                                      id: 1,
                                      referenceId: 'SOA-001',
                                      startDate: new Date('2024-04-16'),
                                      statementFileLink:
                                          'http://localhost:3001/image/test-data-attachments/00B2642A-E73C-11EE-A52E-A9E059CA23EA.pdf',
                                      updatedAt: new Date('2024-04-16'),
                                      userId: 3,
                                  },
                              ]
                            : [],
                        isLoading: false,
                    },
                },
            },
        );
    it('renders correctly for Statement of Accounts with data', async () => {
        const { getByTestId, getByLabelText, getByText } = getComponent(true);
        await waitFor(() => {});
        expect(
            getByLabelText('statment-of-account.header-bar-text'),
        ).toBeTruthy();
        expect(getByTestId('statement-of-account-SOA-001')).toBeTruthy();
        expect(getByText('16 Apr 24 - 16 Apr 24')).toBeTruthy();
    });

    it('renders correctly for Statement of Accounts without data', async () => {
        const { queryByTestId, getByLabelText, queryByText } =
            getComponent(false);
        await waitFor(() => {});
        expect(
            getByLabelText('statment-of-account.header-bar-text'),
        ).toBeTruthy();
        expect(queryByTestId('statement-of-account-SOA-001')).toBeNull();
        expect(queryByText('16 Apr 24 - 16 Apr 24')).toBeNull();
    });
});
