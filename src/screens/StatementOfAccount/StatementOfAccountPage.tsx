import { FC } from 'react';
import { FlatList, View } from 'react-native';
import { StatementOfAccountNavProps } from '../../navigation/NavProps';
import styles from './StatementOfAccountPageStyles';
import React from 'react';
import HeaderBar from '../../components/headerbar/HeaderBar';
import PageName from '../../navigation/PageNameEnum';
import { useAppSelector } from '../../store/hook';
import NormalText from '../../components/ui/titles/NormalText';
import StatementOfAccountCard from '../../components/ui/buttons/StatementOfAccountCard/StatementOfAccountCard';
import downloadFile from '../../utils/DownloadFile';
import { toDateFormat } from '../../utils/dateTimeUtil';

const StatementOfAccountPage: FC<StatementOfAccountNavProps> = ({
    navigation,
}) => {
    let fetchedStatementOfAccounts = useAppSelector(
        state => state.statementOfAccount.statementOfAccountArray,
    );

    const onBackButtonPressHandler = () => {
        navigation.navigate(PageName.HOME_PAGE_SCREEN);
    };

    return (
        <View style={styles.rootContainer}>
            <View style={styles.headersContainer}>
                <View style={styles.headerBarContainer}>
                    <HeaderBar
                        testID="statment-of-account.header-bar-text"
                        title="Statement of Account"
                        onBackButtonPress={onBackButtonPressHandler}
                        showBackButton={true}
                    />
                </View>
            </View>
            <View testID="data-display">
                {fetchedStatementOfAccounts &&
                fetchedStatementOfAccounts.length ? (
                    <FlatList
                        data={fetchedStatementOfAccounts}
                        renderItem={({ item: soa }) => {
                            const temp = soa.statementFileLink.split('/');
                            const fileName = temp[temp.length - 1].slice(0, -4);
                            return (
                                <StatementOfAccountCard
                                    data={{
                                        fileName: `${toDateFormat(
                                            soa.startDate,
                                            true,
                                        )} - ${toDateFormat(
                                            soa.endDate,
                                            true,
                                        )}`,
                                        createdAt: soa.createdAt,
                                    }}
                                    onPress={() =>
                                        downloadFile(
                                            fileName,
                                            soa.statementFileLink,
                                        )
                                    }
                                    testID={`statement-of-account-${soa.referenceId}`}
                                />
                            );
                        }}
                        testID="statement-of-account.flat-list"
                        keyExtractor={(_, i) => `${i}`}
                        alwaysBounceVertical={false}
                        contentContainerStyle={styles.flatList}
                    />
                ) : (
                    <NormalText
                        center={true}
                        testID="statement-of-account.no-SOA">
                        {'You have no Statement of Account at the moment'}
                    </NormalText>
                )}
            </View>
        </View>
    );
};

export default StatementOfAccountPage;
