import { Platform, Pressable, View } from 'react-native';
import React, { FC, useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../store/hook';
import { userActions } from '../../store/user/UserActions';
import HeaderBar from '../../components/headerbar/HeaderBar';
import { ProfilePageScreenNavProps } from '../../navigation/NavProps';
import PageName from '../../navigation/PageNameEnum';
import { SafeAreaView } from 'react-native-safe-area-context';
import styles from './ProfilePageStyles';
import DefaultProfilePicture from '../../components/defaultProfilePicture/DefaultProfilePicture';
import Divider from '../../components/ui/divider/Divider';
import SubHeaderText from '../../components/ui/titles/SubHeaderText';
import SecondaryButton from '../../components/ui/buttons/SecondaryButton';
import PrimaryButton from '../../components/ui/buttons/PrimaryButton';
import PopModal from '../../components/modal/PopModal';
import HeaderText from '../../components/ui/titles/HeaderText';
import NormalText from '../../components/ui/titles/NormalText';
import ScrollableContainer from '../../components/container/ScrollableContainer';
import { formatUnitNumber } from '../../utils/FormatUnitNumber';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCog } from '@fortawesome/free-solid-svg-icons';
import {
    useGetUserDetailsQuery,
    useLogoutMutation,
} from '../../store/user/userApi';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { userRoleActions } from '../../store/userRole/slice';
import messaging from '@react-native-firebase/messaging';
import { RoleType } from '../../models/userRole/RoleType';
import Config from 'react-native-config';

// SCREEN FOR PROFILE PAGE WITHOUT EDIT PROFILE IMPLEMENTATION.
// IF REQUIRE EDIT PROFILE IMPLEMENTATION, LOOK AT DeprecatedProfilePage.tsx
const ProfilePage: FC<ProfilePageScreenNavProps> = ({ navigation }) => {
    const [logoutModalVisibility, setLogoutModalVisibility] =
        useState<boolean>(false);
    const [isVendorEmployee, setIsVendorEmployee] = useState<boolean>(false);
    const dispatch = useAppDispatch();
    const profileDetails = useAppSelector(state => state.user.profileDetails);
    const userRoles = useAppSelector(
        state => state.userRole.allAvailableUseRoles,
    );
    const { setIsDefectReporter } = userRoleActions;

    useGetUserDetailsQuery(undefined);

    const firstName = profileDetails.firstName;
    const lastName = profileDetails.lastName;
    const email = profileDetails.email;
    const phoneNumber = profileDetails.phoneNumber;
    const blockNumber =
        profileDetails.units.length > 0
            ? profileDetails.units[0].blockNumber
            : '';
    let unitNumber =
        profileDetails.units.length > 0
            ? profileDetails.units[0].unitNumber
            : '';
    unitNumber = unitNumber ? formatUnitNumber(unitNumber!) : '';

    const onBackButtonPressHandler = () => {
        navigation.goBack();
    };
    const onSettingsButtonPressHandler = () => {
        navigation.navigate(PageName.SETTINGS_SCREEN);
    };

    const [logout] = useLogoutMutation();

    const onLogoutButtonPressHandler = async () => {
        let deviceToken;
        if (Platform.OS === 'ios') {
            await messaging().getAPNSToken();
        }
        if (Config.ENV === 'test') {
            deviceToken = 'string';
        } else {
            deviceToken = await messaging().getToken();
        }

        logout({ deviceToken: deviceToken });
        dispatch(userActions.logout());
        dispatch(setIsDefectReporter(false));
        await AsyncStorage.clear();
    };
    const onChangePasswordButtonPressHandler = () => {
        navigation.navigate(PageName.CHANGE_PASSWORD_SCREEN);
    };

    useEffect(() => {
        const hasVendorEmployeeRole = userRoles.find(
            role => role.roleType === RoleType.VENDOR_EMPLOYEE,
        );
        if (hasVendorEmployeeRole) {
            setIsVendorEmployee(true);
        } else {
            setIsVendorEmployee(false);
        }
    }, [userRoles]);

    return (
        <ScrollableContainer>
            <SafeAreaView style={styles.rootContainer}>
                <HeaderBar
                    title="My Profile"
                    onBackButtonPress={onBackButtonPressHandler}
                    testID="profile-page.header-bar-text"
                    iconStyle={styles.iconButton}
                    labelStyle={styles.headerTitle}
                    additionalIcon={
                        <View>
                            <Pressable onPress={onSettingsButtonPressHandler}>
                                <FontAwesomeIcon
                                    icon={faCog}
                                    style={styles.iconButton}
                                    size={28}
                                    testID="profile-page.settings-btn"
                                />
                            </Pressable>
                        </View>
                    }
                />
                <View style={styles.whiteOverlayContainer}>
                    <View style={styles.profilePictureContainer}>
                        <DefaultProfilePicture
                            onPress={() => {}}
                            size="medium"
                            buttonStyle={styles.profilePicture}
                            textStyle={styles.profilePictureText}
                        />
                    </View>
                    <View style={styles.profileDetailsContainer}>
                        <HeaderText style={styles.nameText}>
                            {firstName} {lastName}
                        </HeaderText>
                        {!isVendorEmployee ? (
                            <>
                                <SubHeaderText
                                    style={styles.profileDetailsText}
                                    underline={false}>
                                    Residentia
                                </SubHeaderText>
                                <SubHeaderText
                                    style={styles.profileDetailsText}
                                    underline={false}>
                                    Block {blockNumber}, #{unitNumber}
                                </SubHeaderText>
                            </>
                        ) : null}
                    </View>
                    <Divider />

                    <View style={styles.rowDetailsContainer}>
                        <View style={styles.rowContainer}>
                            <View style={styles.rowBoldTextContainer}>
                                <SubHeaderText underline={false}>
                                    Contact No.
                                </SubHeaderText>
                            </View>
                            <View style={styles.rowTextContainer}>
                                <SubHeaderText
                                    style={styles.rowText}
                                    underline={false}>
                                    {phoneNumber}
                                </SubHeaderText>
                            </View>
                        </View>
                        <View style={styles.rowContainer}>
                            <View style={styles.rowBoldTextContainer}>
                                <SubHeaderText underline={false}>
                                    Email Address
                                </SubHeaderText>
                            </View>
                            <View style={styles.rowTextContainer}>
                                <SubHeaderText
                                    style={styles.rowText}
                                    underline={false}>
                                    {email}
                                </SubHeaderText>
                            </View>
                        </View>
                    </View>

                    <Divider />

                    <View style={styles.buttonsViewProfileContainer}>
                        <SecondaryButton
                            label="Change Password"
                            onPress={onChangePasswordButtonPressHandler}
                            style={styles.button}
                        />
                        <PrimaryButton
                            label="Log out"
                            onPress={() => setLogoutModalVisibility(true)}
                            testID="logout-button"
                            accessibilityLabel="logout-button"
                            style={styles.button}
                        />
                    </View>
                </View>
                <PopModal
                    modalTitle={`Log out of Residentia?`}
                    modalHeaderBody={`Are you sure you want to log out?`}
                    modalBody={
                        <NormalText>
                            You can always log back in anytime.
                        </NormalText>
                    }
                    testID="logout-modal"
                    visibility={logoutModalVisibility}
                    confirmText="Log out"
                    onCancel={() => setLogoutModalVisibility(false)}
                    onConfirm={() => {
                        setLogoutModalVisibility(false);
                        onLogoutButtonPressHandler();
                    }}
                />
            </SafeAreaView>
        </ScrollableContainer>
    );
};

export default ProfilePage;
