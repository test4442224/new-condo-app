import { Platform, Pressable, TextInput, View } from 'react-native';
import React, {
    FC,
    // useEffect,
    useState,
} from 'react';
import { useAppDispatch, useAppSelector } from '../../store/hook';
import { userActions } from '../../store/user/UserActions';
import HeaderBar from '../../components/headerbar/HeaderBar';
import { ProfilePageScreenNavProps } from '../../navigation/NavProps';
// import PageName from '../../navigation/PageNameEnum';
import { SafeAreaView } from 'react-native-safe-area-context';
import styles from './ProfilePageStyles';
import DefaultProfilePicture from '../../components/defaultProfilePicture/DefaultProfilePicture';
import Divider from '../../components/ui/divider/Divider';
import SubHeaderText from '../../components/ui/titles/SubHeaderText';
import SecondaryButton from '../../components/ui/buttons/SecondaryButton';
import PrimaryButton from '../../components/ui/buttons/PrimaryButton';
import PopModal from '../../components/modal/PopModal';
import HeaderText from '../../components/ui/titles/HeaderText';
// import Toast from 'react-native-simple-toast';
import NormalText from '../../components/ui/titles/NormalText';
import { InputValidation } from '../../utils/InputValidation';
import ScrollableContainer from '../../components/container/ScrollableContainer';
import { formatUnitNumber } from '../../utils/FormatUnitNumber';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCog } from '@fortawesome/free-solid-svg-icons';
import {
    useGetUserDetailsQuery,
    useLogoutMutation,
    // useUpdateUserDetailsMutation,
} from '../../store/user/userApi';
// import { QueryStatus } from '@reduxjs/toolkit/dist/query';
// import { RtkErrorHandler } from '../../utils/error/RtkErrorHandler.util';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { userRoleActions } from '../../store/userRole/slice';
import messaging from '@react-native-firebase/messaging';

// SCREEN FOR PROFILE PAGE WITH EDIT PROFILE IMPLEMENTATION. COMMENTED FOR FUTURE USE
const DeprecatedProfilePage: FC<ProfilePageScreenNavProps> = () => {
    const dispatch = useAppDispatch();
    const [logoutModalVisibility, setLogoutModalVisibility] =
        useState<boolean>(false);
    const [firstNameState, setFirstNameState] = useState<string>('');
    const [lastNameState, setLastNameState] = useState<string>('');
    const [emailState, setEmailState] = useState<string>('');
    const [phoneNumberState, setPhoneNumberState] = useState<string>('');
    const [isEditProfileScreenState, setIsEditProfileScreenState] =
        useState<boolean>(false);
    const [firstNameError, setFirstNameError] = useState<string>('');
    const [lastNameError, setLastNameError] = useState<string>('');
    const [emailError, setEmailError] = useState<string>('');
    const [phoneNumberError, setPhoneNumberError] = useState<string>('');

    const profileDetails = useAppSelector(state => state.user.profileDetails);
    const { setIsDefectReporter } = userRoleActions;
    // const [
    //     updateUserDetails,
    //     {
    //         status: updateUserDetailsStatus,
    //         data: updateUserDetailsData,
    //         error: updateUserDetailsError,
    //         reset: resetUpdateUserdetailsStatus,
    //     },
    // ] = useUpdateUserDetailsMutation();
    useGetUserDetailsQuery(undefined);

    const firstName = profileDetails.firstName;
    const lastName = profileDetails.lastName;
    const email = profileDetails.email;
    const phoneNumber = profileDetails.phoneNumber;
    const blockNumber = profileDetails.units[0].blockNumber;
    let unitNumber = profileDetails.units[0].unitNumber;
    unitNumber = formatUnitNumber(unitNumber!);

    const onEditProfileButtonPressHandler = () => {
        setIsEditProfileScreenState(true);
        setFirstNameState(firstName!);
        setLastNameState(lastName!);
        setEmailState(email!);
        setPhoneNumberState(phoneNumber!.toString());
    };

    const onSaveChangesButtonPressHandler = () => {
        // Stop process if there is error
        if (
            firstNameError !== '' ||
            lastNameError !== '' ||
            emailError !== '' ||
            phoneNumberError !== ''
        ) {
            return;
        }

        // updateUserDetails({
        //     firstName: firstNameState,
        //     lastName: lastNameState,
        //     email: emailState,
        //     phoneNumber: Number(phoneNumberState),
        // });
    };
    const onCancelButtonPressHandler = () => {
        setIsEditProfileScreenState(false);
        setFirstNameError('');
        setLastNameError('');
        setEmailError('');
        setPhoneNumberError('');
    };
    const onBackButtonPressHandler = () => {
        // navigation.navigate(PageName.HOME_PAGE_SCREEN);
    };
    const onSettingsButtonPressHandler = () => {
        // navigation.navigate(PageName.SETTINGS_SCREEN);
    };

    const [logout] = useLogoutMutation();

    const onLogoutButtonPressHandler = async () => {
        if (Platform.OS === 'ios') {
            await messaging().getAPNSToken();
        }
        const deviceToken = await messaging().getToken();

        logout({ deviceToken: deviceToken });
        dispatch(userActions.logout());
        dispatch(setIsDefectReporter(false));
        await AsyncStorage.clear();
    };
    const onChangePasswordButtonPressHandler = () => {
        // navigation.navigate(PageName.CHANGE_PASSWORD_SCREEN);
    };

    // Handle updateUserDetails data and error
    // useEffect(() => {
    //     if (
    //         updateUserDetailsStatus === QueryStatus.fulfilled &&
    //         updateUserDetailsData
    //     ) {
    //         Toast.showWithGravity(
    //             'Profile updated successfully!',
    //             Toast.SHORT,
    //             Toast.CENTER,
    //         );

    //         setIsEditProfileScreenState(false);

    //         resetUpdateUserdetailsStatus();
    //         return;
    //     }

    //     if (
    //         updateUserDetailsStatus === QueryStatus.rejected &&
    //         updateUserDetailsError
    //     ) {
    //         const message = RtkErrorHandler.getMessage(updateUserDetailsError);

    //         Toast.show(message, Toast.LONG);

    //         resetUpdateUserdetailsStatus();
    //         return;
    //     }
    // }, [
    //     updateUserDetailsStatus,
    //     updateUserDetailsData,
    //     updateUserDetailsError,
    //     resetUpdateUserdetailsStatus,
    // ]);

    return (
        <ScrollableContainer>
            <SafeAreaView style={styles.rootContainer}>
                <HeaderBar
                    title="My Profile"
                    onBackButtonPress={onBackButtonPressHandler}
                    testID="profile-page.header-bar-text"
                    iconStyle={styles.iconButton}
                    labelStyle={styles.headerTitle}
                    additionalIcon={
                        <View>
                            <Pressable onPress={onSettingsButtonPressHandler}>
                                <FontAwesomeIcon
                                    icon={faCog}
                                    style={styles.iconButton}
                                    size={28}
                                    testID="profile-page.settings-btn"
                                />
                            </Pressable>
                        </View>
                    }
                />
                <View style={styles.whiteOverlayContainer}>
                    <View style={styles.profilePictureContainer}>
                        <DefaultProfilePicture
                            onPress={() => {}}
                            size="medium"
                            buttonStyle={styles.profilePicture}
                            textStyle={styles.profilePictureText}
                        />
                    </View>
                    <View style={styles.profileDetailsContainer}>
                        <HeaderText style={styles.nameText}>
                            {firstName} {lastName}
                        </HeaderText>
                        <SubHeaderText
                            style={styles.profileDetailsText}
                            underline={false}>
                            Residentia
                        </SubHeaderText>
                        <SubHeaderText
                            style={styles.profileDetailsText}
                            underline={false}>
                            Block {blockNumber}, #{unitNumber}
                        </SubHeaderText>
                    </View>
                    <Divider />
                    {isEditProfileScreenState ? (
                        <View style={styles.rowDetailsContainer}>
                            <View style={styles.inputRowContainer}>
                                <View style={styles.rowBoldTextInputContainer}>
                                    <SubHeaderText underline={false}>
                                        First Name
                                    </SubHeaderText>
                                </View>
                                <View style={styles.rowTextContainer}>
                                    <TextInput
                                        testID="first-name-input"
                                        value={firstNameState}
                                        onChangeText={text => {
                                            setFirstNameError(
                                                new InputValidation().validateNameInputs(
                                                    text,
                                                ),
                                            );
                                            setFirstNameState(text);
                                        }}
                                        style={styles.inputBox}
                                        inputMode="text"
                                    />
                                    {firstNameError ? (
                                        <View style={styles.errorTextContainer}>
                                            <NormalText
                                                style={styles.errorText}>
                                                {firstNameError}
                                            </NormalText>
                                        </View>
                                    ) : null}
                                </View>
                            </View>
                            <View style={styles.inputRowContainer}>
                                <View style={styles.rowBoldTextInputContainer}>
                                    <SubHeaderText underline={false}>
                                        Last Name
                                    </SubHeaderText>
                                </View>
                                <View style={styles.rowTextContainer}>
                                    <TextInput
                                        testID="last-name-input"
                                        value={lastNameState}
                                        onChangeText={text => {
                                            setLastNameError(
                                                new InputValidation().validateNameInputs(
                                                    text,
                                                ),
                                            );
                                            setLastNameState(text);
                                        }}
                                        style={styles.inputBox}
                                        inputMode="text"
                                    />
                                    {lastNameError ? (
                                        <View style={styles.errorTextContainer}>
                                            <NormalText
                                                style={styles.errorText}>
                                                {lastNameError}
                                            </NormalText>
                                        </View>
                                    ) : null}
                                </View>
                            </View>
                            <View style={styles.inputRowContainer}>
                                <View style={styles.rowBoldTextInputContainer}>
                                    <SubHeaderText underline={false}>
                                        Contact No.
                                    </SubHeaderText>
                                </View>
                                <View style={styles.rowTextContainer}>
                                    <TextInput
                                        testID="contact-no-input"
                                        value={phoneNumberState.toString()}
                                        onChangeText={text => {
                                            setPhoneNumberError(
                                                new InputValidation().validatePhoneNumberInput(
                                                    text,
                                                ),
                                            );
                                            setPhoneNumberState(text);
                                        }}
                                        style={styles.inputBox}
                                        inputMode="numeric"
                                    />
                                    {phoneNumberError ? (
                                        <View style={styles.errorTextContainer}>
                                            <NormalText
                                                style={styles.errorText}>
                                                {phoneNumberError}
                                            </NormalText>
                                        </View>
                                    ) : null}
                                </View>
                            </View>
                            <View style={styles.inputRowContainer}>
                                <View style={styles.rowBoldTextInputContainer}>
                                    <SubHeaderText underline={false}>
                                        Email Address
                                    </SubHeaderText>
                                </View>
                                <View style={styles.rowTextContainer}>
                                    <TextInput
                                        testID="email-address-input"
                                        value={emailState}
                                        onChangeText={text => {
                                            setEmailError(
                                                new InputValidation().validateEmailInput(
                                                    text,
                                                ),
                                            );
                                            setEmailState(text);
                                        }}
                                        style={styles.inputBox}
                                        inputMode="email"
                                    />

                                    {emailError ? (
                                        <View style={styles.errorTextContainer}>
                                            <NormalText
                                                style={styles.errorText}>
                                                {emailError}
                                            </NormalText>
                                        </View>
                                    ) : null}
                                </View>
                            </View>
                        </View>
                    ) : (
                        <View style={styles.rowDetailsContainer}>
                            <View style={styles.rowContainer}>
                                <View style={styles.rowBoldTextContainer}>
                                    <SubHeaderText underline={false}>
                                        Contact No.
                                    </SubHeaderText>
                                </View>
                                <View style={styles.rowTextContainer}>
                                    <SubHeaderText
                                        style={styles.rowText}
                                        underline={false}>
                                        {phoneNumber}
                                    </SubHeaderText>
                                </View>
                            </View>
                            <View style={styles.rowContainer}>
                                <View style={styles.rowBoldTextContainer}>
                                    <SubHeaderText underline={false}>
                                        Email Address
                                    </SubHeaderText>
                                </View>
                                <View style={styles.rowTextContainer}>
                                    <SubHeaderText
                                        style={styles.rowText}
                                        underline={false}>
                                        {email}
                                    </SubHeaderText>
                                </View>
                            </View>
                        </View>
                    )}
                    <Divider />
                    {isEditProfileScreenState ? (
                        <View style={styles.buttonOuterContainer}>
                            <View style={styles.editButtonsContainer}>
                                <SecondaryButton
                                    label="Cancel"
                                    onPress={onCancelButtonPressHandler}
                                />
                            </View>
                            <View style={styles.editButtonsContainer}>
                                <PrimaryButton
                                    label="Save Changes"
                                    onPress={onSaveChangesButtonPressHandler}
                                />
                            </View>
                        </View>
                    ) : (
                        <View style={styles.buttonsViewProfileContainer}>
                            <SecondaryButton
                                label="Edit Profile"
                                onPress={onEditProfileButtonPressHandler}
                                style={styles.button}
                            />
                            <SecondaryButton
                                label="Change Password"
                                onPress={onChangePasswordButtonPressHandler}
                                style={styles.button}
                            />
                            <PrimaryButton
                                label="Log out"
                                onPress={() => setLogoutModalVisibility(true)}
                                style={styles.button}
                            />
                        </View>
                    )}
                </View>
                <PopModal
                    modalTitle={`Log out of Residentia?`}
                    modalHeaderBody={`Are you sure you want to log out?`}
                    modalBody={
                        <NormalText>
                            You can always log back in anytime.
                        </NormalText>
                    }
                    visibility={logoutModalVisibility}
                    confirmText="Log out"
                    onCancel={() => setLogoutModalVisibility(false)}
                    onConfirm={() => {
                        setLogoutModalVisibility(false);
                        onLogoutButtonPressHandler();
                    }}
                />
            </SafeAreaView>
        </ScrollableContainer>
    );
};

export default DeprecatedProfilePage;
