import { StyleSheet } from 'react-native';
import Colors from '../../Constants/Colors';
import { BorderRadiuses, Spacings, Typography } from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
export default StyleSheet.create({
    rootContainer: {
        backgroundColor: Colors.$backgroundPrimary,
        flex: 1,
    },
    iconButton: {
        color: Colors.$iconDefault,
    },
    headerTitle: {
        color: Colors.$iconDefault,
    },
    whiteOverlayContainer: {
        marginTop: Spacings.s10,
        backgroundColor: Colors.$backgroundDefault,
        flex: 1,
        borderTopLeftRadius: BorderRadiuses.br10 * 35,
        borderTopRightRadius: BorderRadiuses.br10 * 35,
    },
    profilePictureContainer: {
        alignItems: 'center',
        marginTop: -Spacings.s8,
        marginBottom: Spacings.s6,
    },
    profilePicture: {
        height: 84,
        width: 84,
    },
    profilePictureText: {
        ...Typography.text30M,
    },
    profileDetailsContainer: {
        alignItems: 'center',
    },
    nameText: {
        color: Colors.$textDefault,
        marginBottom: Spacings.s4,
    },
    profileDetailsText: {
        ...Typography.text60R,
        marginBottom: Spacings.s2,
    },
    rowDetailsContainer: {
        marginTop: Spacings.s1,
    },
    rowContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: Spacings.s3,
    },
    rowBoldTextContainer: {
        flex: 1,
        marginHorizontal: Spacings.s5,
    },
    rowTextContainer: {
        flex: 1,
        marginHorizontal: Spacings.s4,
    },
    rowText: {
        fontWeight: WEIGHT_TYPES.REGULAR,
    },
    buttonOuterContainer: {
        flexDirection: 'row',
        gap: Spacings.s3,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: Spacings.s3,
        paddingHorizontal: Spacings.s4,
    },
    buttonsViewProfileContainer: {
        marginHorizontal: Spacings.s4,
    },
    button: {
        marginVertical: Spacings.s2,
    },
    // CSS FOR PROFILE PAGE WITH EDIT PROFILE IMPLEMENTATION. COMMENTED FOR FUTURE USE
    // inputRowContainer: {
    //     flexDirection: 'row',
    //     marginTop: Spacings.s3,
    // },
    // CSS FOR PROFILE PAGE WITH EDIT PROFILE IMPLEMENTATION. COMMENTED FOR FUTURE USE
    // inputBox: {
    //     borderWidth: 1,
    //     borderColor: Colors.$outlinePrimary,
    //     borderRadius: BorderRadiuses.br30,
    //     padding: Spacings.s2,
    // },
    // rowBoldTextInputContainer: {
    //     paddingTop: Spacings.s1,
    //     width: '35%',
    //     marginLeft: Spacings.s8,
    // },
    editButtonsContainer: {
        flex: 1,
    },
    // CSS FOR PROFILE PAGE WITH EDIT PROFILE IMPLEMENTATION. COMMENTED FOR FUTURE USE
    // errorTextContainer: {
    //     width: '100%',
    // },
    // errorText: {
    //     color: Colors.$textError,
    // },
});
