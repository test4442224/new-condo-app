import { StyleSheet } from 'react-native';
import Colors from '../../Constants/Colors';
import { Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    rootContainer: {
        backgroundColor: Colors.$backgroundDefault,
        flex: 1,
    },
    backButton: {
        color: Colors.$iconPrimary,
    },
    headerTitle: {
        color: Colors.$textPrimary,
    },
    generalSectionContainer: {
        marginHorizontal: Spacings.s4,
        marginBottom: Spacings.s7,
    },
    privacySectionContainer: {
        marginHorizontal: Spacings.s4,
    },
    titleText: {
        color: Colors.$textPrimary,
    },
});
