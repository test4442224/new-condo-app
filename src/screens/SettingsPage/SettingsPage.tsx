import { View } from 'react-native';
import React, { FC } from 'react';
import HeaderBar from '../../components/headerbar/HeaderBar';
import styles from './SettingsPageStyles';
import { SettingsScreenNavProps } from '../../navigation/NavProps';
import PageName from '../../navigation/PageNameEnum';
import SettingsCard from '../../components/ui/buttons/SettingCard/SettingsCard';
import SubHeaderText from '../../components/ui/titles/SubHeaderText';

const SettingsPage: FC<SettingsScreenNavProps> = ({ navigation }) => {
    const onBackButtonPressHandler = () => {
        navigation.navigate(PageName.PROFILE_PAGE_SCREEN);
    };

    const onPrivacyPolicyButtonPressHandler = () => {
        navigation.navigate(PageName.PRIVACY_POLICY_SCREEN);
    };

    const onTermsOfServiceButtonPressHandler = () => {
        navigation.navigate(PageName.TERMS_OF_SERVICE_SCREEN);
    };
    return (
        <View style={styles.rootContainer}>
            <HeaderBar
                title="Settings"
                onBackButtonPress={onBackButtonPressHandler}
                testID="settings-page.header-bar-text"
                iconStyle={styles.backButton}
                labelStyle={styles.headerTitle}
            />
            <View style={styles.privacySectionContainer}>
                <SubHeaderText
                    underline={false}
                    style={styles.titleText}>{`Privacy`}</SubHeaderText>
                <SettingsCard
                    onPress={onTermsOfServiceButtonPressHandler}
                    label={`SettingsCardTermsOfService`}
                    title={`Terms of Service`}
                />
                <SettingsCard
                    onPress={onPrivacyPolicyButtonPressHandler}
                    label={`SettingsCardPrivacyPolicy`}
                    title={`Privacy Policy`}
                />
            </View>
        </View>
    );
};

export default SettingsPage;
