import { FC, useEffect, useState, useCallback } from 'react';
import { EditDefectScreenNavProps } from '../../../navigation/NavProps';
import { Text, View } from 'react-native';
import React from 'react';
import ScrollableContainer from '../../../components/container/ScrollableContainer';
import styles from './EditDefectStyles';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import DefectForm from '../../../components/form/DefectForm/DefectForm';
import PrimaryButton from '../../../components/ui/buttons/PrimaryButton';
import Loading from '../../../components/ui/loading/Loading';
import { useAppSelector } from '../../../store/hook';
import { useUpdateDefectMutation } from '../../../store/defect/api';
import useRtkResponse from '../../../utils/hooks/useRtkResponse';
import PopModal from '../../../components/modal/PopModal';
import { InputValidation } from '../../../utils/InputValidation';
import { ImageData } from '../../../store/manageBooking/api';
import { useRemoveAttachmentMutation } from '../../../store/defect/api';
import ImageUpload from '../../../components/ui/attachmentUpload/ImageUpload';
import { DefectAttachment } from '../../../models/defect/DefectAttachment';
import { useCreateDefectAttachmentMutation } from '../../../store/defect/attachment/api';

const EditDefectPage: FC<EditDefectScreenNavProps> = ({
    navigation,
    route,
}) => {
    const { defectData, onEditSuccess } = route.params;
    const [inputError, setInputError] = useState<boolean>(false);
    const [uploadError, setUploadError] = useState<boolean>(false);
    const [cancel, setCancel] = useState<boolean>(false);
    const [name, setName] = useState<string>(defectData?.name ?? '');
    const token = useAppSelector(state => state.user.token);
    const [isLoading, setLoading] = useState<boolean>(true);
    const [existingAttachments] = useState<DefectAttachment[]>(
        defectData?.attachments ?? [],
    );
    const [selectedAttachment, setSelectedAttachment] = useState<ImageData[]>(
        [],
    );
    const [removeAttachmentId, setRemoveAttachmentId] = useState<number[]>([]);
    const [description, setDescription] = useState<string>(
        defectData?.description ?? '',
    );
    const handleNameChange = (defectName: string) => setName(defectName);
    const handleDescriptionChange = (defectDescription: string) =>
        setDescription(defectDescription);

    const [
        updateDefect,
        { isLoading: editLoading, error: editError, isSuccess },
    ] = useUpdateDefectMutation();
    const [
        createDefectAttachment,
        { isLoading: isLoadingAttachment, error: isLoadingAttachmenError },
    ] = useCreateDefectAttachmentMutation();

    const [removeAttachment] = useRemoveAttachmentMutation();

    useRtkResponse(isLoadingAttachment, isLoadingAttachmenError, false);
    useRtkResponse(editLoading, editError);

    const backButtonOnPressHandler = () => {
        setCancel(true);
    };

    const removeAttachmentHandler = async (attachmentId: number) => {
        const defectId = defectData!.id;
        try {
            await removeAttachment({
                defectId,
                attachmentId,
            });
        } catch (error) {
            console.error('Error removing attachment: ', error);
        }
    };

    const handleSubmit = async () => {
        const validInputs = InputValidation.checkInputsNotEmpty([
            name,
            description,
        ]);
        setInputError(!validInputs);

        const id = defectData?.id;
        if (!id) {
            navigation.goBack();
            return;
        }

        if (removeAttachmentId) {
            for (const index of removeAttachmentId) {
                removeAttachmentHandler(index);
            }
        }

        if (validInputs) {
            const updatedBody = { name, description };
            await updateDefect({
                defectId: id,
                updatedDefectBody: updatedBody,
            });
        }
    };

    const uploadImages = useCallback(async () => {
        if (selectedAttachment && token && isSuccess && defectData) {
            await Promise.all(
                selectedAttachment.map(attachment => {
                    const attachmentExist = existingAttachments.some(
                        existingAttachment =>
                            existingAttachment.attachmentPath.endsWith(
                                attachment.name,
                            ),
                    );
                    if (!attachmentExist) {
                        return createDefectAttachment({
                            defectId: defectData?.id,
                            attachment,
                        });
                    }
                }),
            );
        }
    }, [
        selectedAttachment,
        token,
        isSuccess,
        defectData,
        existingAttachments,
        createDefectAttachment,
    ]);

    useEffect(() => {
        uploadImages();
    }, [uploadImages, isSuccess, defectData]);

    useEffect(() => {
        if (editError) {
            setUploadError(true);
        }
    }, [editError]);

    useEffect(() => {
        if (editLoading && isLoadingAttachment) {
            setLoading(false);
        }
    }, [editLoading, isLoadingAttachment]);

    useEffect(() => {
        if (!defectData) {
            navigation.goBack();
        }
    }, [defectData, navigation]);

    return (
        <ScrollableContainer style={styles.rootContainer}>
            <PopModal
                modalTitle={'Input Error'}
                modalBody={<Text>{'Both fields must be filled!' || ''}</Text>}
                visibility={inputError}
                confirmText="Okay"
                onConfirm={() => {
                    setInputError(!inputError);
                }}
                testID="edit-defect-input-error-modal"
            />
            <PopModal
                modalTitle={'Cancel Report'}
                modalBody={
                    <Text>
                        {'Are you sure? All changes will be lost.' || ''}
                    </Text>
                }
                visibility={cancel}
                confirmText="Confirm"
                cancelText="Cancel"
                onConfirm={() => {
                    navigation.goBack();
                }}
                onCancel={() => {
                    setCancel(false);
                }}
                testID="cancel-edit-report-modal"
            />
            <PopModal
                modalTitle={'Error Editing Defect'}
                modalBody={
                    <Text>
                        {'Oops! Something went wrong. Please try again.' || ''}
                    </Text>
                }
                visibility={uploadError}
                confirmText="Okay"
                onConfirm={() => {
                    setUploadError(false);
                }}
                testID="submit-error-modal"
            />
            <PopModal
                modalTitle={'Defect Edit Successful'}
                modalBody={
                    <Text>
                        {'Your defect has been edited successfully' || ''}
                    </Text>
                }
                visibility={isSuccess}
                confirmText="Okay"
                onConfirm={() => {
                    onEditSuccess();
                    navigation.goBack();
                }}
                testID="defect-edit-successful-modal"
            />
            <HeaderBar
                title="Edit Defect"
                onBackButtonPress={backButtonOnPressHandler}
                testID="edit-defect.header-bar-text"
                size="medium"
                showBackButton
            />
            <View style={styles.formContainer}>
                <DefectForm
                    defectName={name}
                    defectDescription={description}
                    onHandleDescriptionChange={handleDescriptionChange}
                    onHandleNameChange={handleNameChange}
                />
                <ImageUpload
                    existingAttachment={existingAttachments}
                    setSelectedAttachments={setSelectedAttachment}
                    setRemoveAttachmentIds={setRemoveAttachmentId}
                />
                <View style={styles.buttonContainer}>
                    <PrimaryButton
                        testID="submit-edit-defect-button"
                        accessibilityLabel="submit-edit-defect-button"
                        label="Submit"
                        onPress={handleSubmit}
                    />
                    <Text style={styles.warningText}>* mandatory fields</Text>
                </View>
            </View>
            {isLoading && <Loading />}
        </ScrollableContainer>
    );
};

export default EditDefectPage;
