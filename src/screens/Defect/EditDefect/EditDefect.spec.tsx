import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import NavParamList from '../../../navigation/NavParmList';
import PageName from '../../../navigation/PageNameEnum';
import { RouteProp } from '@react-navigation/native';
import {
    cleanup,
    fireEvent,
    screen,
    userEvent,
} from '@testing-library/react-native';
import EditDefectPage from './EditDefect';
import React from 'react';
import { http, HttpResponse } from 'msw';
import { setupServer } from 'msw/native';
import { renderWithProviders } from '../../../test/utils';
import { DefectStatus } from '../../../models/defect/DefectStatusHistory';
import { MAX_ATTACHMENT_SIZE_IN_MB } from '../../../Constants/attachment';
import {
    ImageResults,
    VideoResults,
} from '@baronha/react-native-multiple-image-picker';
import Colors from '../../../Constants/Colors';

jest.mock('@react-navigation/native', () => {
    return {
        useNavigation: jest.fn(),
        useFocusEffect: jest.fn(),
    };
});

jest.mock('../../../utils/hooks/useRtkResponse.ts', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        showToast: false,
    })),
}));

jest.mock('../../../navigation/NavProps', () => ({}));

const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.EDIT_DEFECT_SCREEN>
> = {
    navigate: jest.fn(),
};

const mockMultiImagePicker = jest.fn();
jest.mock('@baronha/react-native-multiple-image-picker', () => {
    return {
        openPicker: (...args: (VideoResults | ImageResults)[]) =>
            mockMultiImagePicker(...args),
    };
});
const mockImageResizer = jest.fn();
jest.mock('react-native-image-resizer', () => {
    return {
        createResizedImage: (...args: (VideoResults | ImageResults)[]) =>
            mockImageResizer(...args),
    };
});

const mockRefetch = jest.fn();

let mockRoute: RouteProp<NavParamList, PageName.EDIT_DEFECT_SCREEN> = {
    key: 'EditDefectPageTestKey',
    name: PageName.EDIT_DEFECT_SCREEN,
    params: {
        defectData: {
            id: 1,
            defectReferenceNo: 'D-240101-c4',
            condoId: 1,
            name: 'Lift is not working',
            description: 'Cannot access third floor',
            latestStatus: DefectStatus.IN_PROGRESS,
            reporterId: 1,
            createdAt: new Date(),
            attachments: [
                {
                    id: 1,
                    attachmentPath: 'path1',
                    attachmentType: 'image/jpeg',
                    thumbnailPath: 'thumbnail/path1',
                    createdAt: new Date(),
                },
                {
                    id: 2,
                    attachmentPath: 'path2',
                    attachmentType: 'image/jpeg',
                    thumbnailPath: 'thumbnail/path2',
                    createdAt: new Date(),
                },
            ],
            statusHistories: [],
            priorityId: 1,
            dueDate: new Date(),
        },
        onEditSuccess: mockRefetch,
    },
};

const mediaAttachments = [
    {
        path: 'path1',
        mime: 'image/png',
        realPath: 'realPath',
        filename: 'filename1',
        size: MAX_ATTACHMENT_SIZE_IN_MB * 1024 * 1024 - 1,
    },
    {
        path: 'path2',
        mime: 'image/png',
        realPath: 'realPath',
        filename: 'filename2',
        size: MAX_ATTACHMENT_SIZE_IN_MB * 1024 * 1024 - 1,
    },
    {
        path: 'path3',
        mime: 'image/png',
        realPath: 'realPath',
        filename: 'filename3',
        size: MAX_ATTACHMENT_SIZE_IN_MB * 1024 * 1024 - 1,
    },
    {
        path: 'path4',
        mime: 'image/png',
        realPath: 'realPath',
        filename: 'filename4',
        size: MAX_ATTACHMENT_SIZE_IN_MB * 1024 * 1024 - 1,
    },
];

const videoAttachment = [
    {
        path: 'path5',
        mime: 'video/mp4',
        realPath: 'realPath',
        filename: 'filename5',
        size: MAX_ATTACHMENT_SIZE_IN_MB * 1024 * 1024 - 1,
    },
];

const exceedMBVideoAttachment = [
    {
        path: 'path6',
        mime: 'video/mp4',
        realPath: 'realPath',
        filename: 'filename6',
        size: MAX_ATTACHMENT_SIZE_IN_MB * 1024 * 1024 + 1,
    },
];

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

const component = (
    <EditDefectPage
        navigation={
            mockNavigation as NativeStackNavigationProp<
                NavParamList,
                PageName.EDIT_DEFECT_SCREEN
            >
        }
        route={mockRoute}
    />
);

const handlers = [
    http.post('http://localhost:3001/defect', async () => {
        return HttpResponse.json({});
    }),
];

const server = setupServer(...handlers);

describe('Edit Defect Page', () => {
    beforeAll(() => {
        server.listen();
        server.events.on('request:start', ({ request }) => {
            console.log('MSW intercepted:', request.method, request.url);
        });
    });

    beforeEach(() => {
        jest.clearAllMocks();
        cleanup();
    });

    afterEach(() => {
        server.resetHandlers();
    });

    afterAll(() => server.close());
    it('should render all major components', async () => {
        const { getByTestId, getByText } = renderWithProviders(component, {
            preloadedState: {
                loading: { isLoading: false },
            },
        });
        const asterisks = screen.queryAllByText('*');
        const header = getByTestId('edit-defect.header-bar-text');
        const nameHeader = getByText('Defect Name');
        const descriptionHeader = getByText('Defect Description');
        const tipText = getByText('(max. 30 characters)');
        const warningText = getByText('* mandatory fields');
        const submitButton = getByTestId('submit-edit-defect-button');
        const uploadImageButton = getByTestId('upload-media-button');

        expect(asterisks).toHaveLength(2);
        expect(header).toBeTruthy();
        expect(nameHeader).toBeTruthy();
        expect(descriptionHeader).toBeTruthy();
        expect(tipText).toBeTruthy();
        expect(warningText).toBeTruthy();
        expect(submitButton).toBeTruthy();
        expect(uploadImageButton).toBeTruthy();
    });
    it('should change text in name field', async () => {
        const { getByTestId } = renderWithProviders(component, {
            preloadedState: {
                loading: { isLoading: false },
            },
        });
        const nameField = getByTestId('name-input-box');
        const mockName = 'Elevator light spoilt';
        fireEvent.changeText(nameField, mockName);
        expect(nameField.props.value).toEqual(mockName);
    });
    it('should change text in description field', async () => {
        const user = userEvent.setup();
        const { getByTestId } = renderWithProviders(component, {
            preloadedState: {
                loading: { isLoading: false },
            },
        });
        const descriptionField = getByTestId('description-input-box');
        const mockDescription = 'Cannot access fourth floor.';
        await user.clear(descriptionField);
        await user.type(descriptionField, mockDescription);
        expect(descriptionField.props.value).toEqual(mockDescription);
    });
    it('should display modal when inputs are invalid', async () => {
        const user = userEvent.setup();
        const { getByTestId } = renderWithProviders(component, {
            preloadedState: {
                loading: { isLoading: false },
            },
        });
        const modal = getByTestId('edit-defect-input-error-modal');
        const submitButton = getByTestId('submit-edit-defect-button');
        await user.press(submitButton);
        expect(modal).toBeTruthy();
    });
    it('should display modal when upload is successful', async () => {
        const user = userEvent.setup();
        const { getByTestId } = renderWithProviders(component, {
            preloadedState: {
                loading: { isLoading: false },
            },
        });
        const modal = getByTestId('defect-edit-successful-modal');
        const submitButton = getByTestId('submit-edit-defect-button');
        await user.press(submitButton);
        expect(modal).toBeTruthy();
    });
    it('should display modal when back button is pressed', async () => {
        const user = userEvent.setup();
        const { getByTestId } = renderWithProviders(component, {
            preloadedState: {
                loading: { isLoading: false },
            },
        });
        const backButton = getByTestId('header-bar-back-button');
        const modal = getByTestId('cancel-edit-report-modal');
        await user.press(backButton);
        expect(modal).toBeTruthy();
    });
    it('should have 2 existing attachment', async () => {
        const { getAllByTestId, getByTestId } = renderWithProviders(component, {
            preloadedState: {
                loading: { isLoading: false },
            },
        });
        const imageAttachments = getAllByTestId('image-attachment');
        const uploadImage = getByTestId('upload-media-button');
        const deleteButtons = getAllByTestId('delete-media-button');
        expect(imageAttachments).toHaveLength(2);
        expect(uploadImage).toBeTruthy();
        expect(deleteButtons).toHaveLength(2);
    });
    it('should be able to remove media attachment', async () => {
        const user = userEvent.setup();
        const { getAllByTestId } = renderWithProviders(component, {
            preloadedState: {
                loading: { isLoading: false },
            },
        });
        const deleteButtons = getAllByTestId('delete-media-button');
        await user.press(deleteButtons[0]);
        const imageAttachments = getAllByTestId('image-attachment');
        expect(imageAttachments).toHaveLength(1);
    });
    it('should open multiple image picker', async () => {
        const user = userEvent.setup();
        mockMultiImagePicker.mockResolvedValue([]);
        const { getByTestId } = renderWithProviders(component, {
            preloadedState: {
                loading: { isLoading: false },
            },
        });
        const uploadImage = getByTestId('upload-media-button');
        await user.press(uploadImage);
        expect(mockMultiImagePicker).toBeCalledWith({
            maxSelectedAssets: 4,
            mediaType: 'all',
            usedCameraButton: true,
            isPreview: false,
            maxVideoDuration: 11,
            allowedVideoRecording: true,
            selectedColor: Colors.$backgroundPrimary,
            maxVideo: 1,
        });
        expect(uploadImage).toBeTruthy();
    });
    it('should only allow to upload images when a video is already selected', async () => {
        const user = userEvent.setup();
        mockMultiImagePicker.mockResolvedValue(videoAttachment);
        mockImageResizer.mockResolvedValue({
            uri: 'resizedPath',
            name: 'resizedFilename',
        });
        const { getByTestId, getAllByTestId } = renderWithProviders(component, {
            preloadedState: {
                loading: { isLoading: false },
            },
        });
        const uploadImage = getByTestId('upload-media-button');
        await user.press(uploadImage);

        const imageAttachments = getAllByTestId('image-attachment');
        const deleteButtons = getAllByTestId('delete-media-button');
        expect(deleteButtons).toHaveLength(3);
        expect(imageAttachments).toHaveLength(3);

        const uploadImage2 = getByTestId('upload-media-button');
        await user.press(uploadImage2);
        expect(mockMultiImagePicker).toBeCalledWith({
            maxSelectedAssets: 3,
            mediaType: 'image',
            usedCameraButton: true,
            isPreview: false,
            maxVideoDuration: 11,
            allowedVideoRecording: false,
            selectedColor: Colors.$backgroundPrimary,
        });
        expect(uploadImage2).toBeTruthy();
    });
    it('should not be able to add more than 6 attachment', async () => {
        const user = userEvent.setup();
        mockMultiImagePicker.mockResolvedValue(mediaAttachments);
        mockImageResizer.mockResolvedValue({
            uri: 'resizedPath',
            name: 'resizedFilename',
        });
        const { getByTestId, getAllByTestId, queryByTestId } =
            renderWithProviders(component, {
                preloadedState: {
                    loading: { isLoading: false },
                },
            });
        const uploadImage = getByTestId('upload-media-button');
        await user.press(uploadImage);

        const imageAttachments = getAllByTestId('image-attachment');
        const deleteButtons = getAllByTestId('delete-media-button');
        const uploadImageAgain = queryByTestId('upload-media-button');
        expect(deleteButtons).toHaveLength(6);
        expect(imageAttachments).toHaveLength(6);
        expect(uploadImageAgain).toBeNull();
    });
    it('should allow to add attachment after removing one of the 6 attachments', async () => {
        const user = userEvent.setup();
        mockMultiImagePicker.mockResolvedValue(mediaAttachments);
        mockImageResizer.mockResolvedValue({
            uri: 'resizedPath',
            name: 'resizedFilename',
        });
        const { getByTestId, queryByTestId, getAllByTestId } =
            renderWithProviders(component, {
                preloadedState: {
                    loading: { isLoading: false },
                },
            });
        const uploadImage = getByTestId('upload-media-button');
        await user.press(uploadImage);

        const uploadImage1 = queryByTestId('upload-media-button');
        const deleteButtons = getAllByTestId('delete-media-button');
        const imageAttachments = getAllByTestId('image-attachment');
        expect(uploadImage1).toBeNull();
        expect(deleteButtons).toHaveLength(6);
        expect(imageAttachments).toHaveLength(6);
        await user.press(deleteButtons[0]);

        const uploadImage2 = queryByTestId('upload-media-button');
        const deleteButtons2 = getAllByTestId('delete-media-button');
        const imageAttachments2 = getAllByTestId('image-attachment');
        expect(uploadImage2).toBeTruthy();
        expect(deleteButtons2).toHaveLength(5);
        expect(imageAttachments2).toHaveLength(5);
    });

    it('should not be able to add attachment more than 60MB', async () => {
        const user = userEvent.setup();
        mockMultiImagePicker.mockResolvedValue(exceedMBVideoAttachment);
        mockImageResizer.mockResolvedValue({
            uri: 'resizedPath',
            name: 'resizedFilename',
        });

        const { getByTestId, getAllByTestId, getByText } = renderWithProviders(
            component,
            {
                preloadedState: {
                    loading: { isLoading: false },
                },
            },
        );
        const uploadImage = getByTestId('upload-media-button');
        await user.press(uploadImage);

        const maxAttachmentSize = getAllByTestId('attachment-size-error-modal');
        const exceedAttachmentSizeError = getByText(
            'Your video does not fulfil the requirement. Please upload a video that is less than 60MB.',
        );
        expect(maxAttachmentSize).toBeTruthy();
        expect(exceedAttachmentSizeError).toBeTruthy();
        const imageAttachments = getAllByTestId('image-attachment');
        expect(imageAttachments).toHaveLength(2);
    });
});
