import { StyleSheet } from 'react-native';
import { Spacings } from 'react-native-ui-lib';
import Colors from '../../../Constants/Colors';
import Typography from 'react-native-ui-lib/src/style/typographyPresets';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
    },
    formContainer: {
        flex: 1,
        paddingHorizontal: Spacings.s4,
    },
    warningText: {
        ...Typography.text80,
        color: Colors.$textError,
        marginTop: Spacings.s1,
    },
    buttonContainer: {
        marginTop: Spacings.s4,
    },
});
