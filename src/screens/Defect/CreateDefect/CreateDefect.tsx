import React, { useState, useEffect, useCallback } from 'react';
import { FC } from 'react';
import { View } from 'react-native';
import Text from '../../../components/ui/titles/Text';
import styles from './CreateDefectStyles';
import { CreateDefectScreenNavProps } from '../../../navigation/NavProps';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import PrimaryButton from '../../../components/ui/buttons/PrimaryButton';
import PopModal from '../../../components/modal/PopModal';
import ScrollableContainer from '../../../components/container/ScrollableContainer';
import { useCreateDefectMutation } from '../../../store/defect/api';
import { DefectAttributes } from '../../../store/defect/slice';
import useRtkResponse from '../../../utils/hooks/useRtkResponse';
import { useAppSelector } from '../../../store/hook';
import Loading from '../../../components/ui/loading/Loading';
import { InputValidation } from '../../../utils/InputValidation';
import DefectForm from '../../../components/form/DefectForm/DefectForm';
import ImageUpload from '../../../components/ui/attachmentUpload/ImageUpload';
import { useCreateDefectAttachmentMutation } from '../../../store/defect/attachment/api';
import { ImageData } from '../../../store/defect/attachment/api';

const CreateDefectPage: FC<CreateDefectScreenNavProps> = ({ navigation }) => {
    const [name, setName] = useState<string>('');
    const [description, setDescription] = useState<string>('');
    const [inputError, setInputError] = useState<boolean>(false);
    const [uploadError, setUploadError] = useState<boolean>(false);
    const [cancel, setCancel] = useState<boolean>(false);
    const { isLoading } = useAppSelector(state => state.loading);
    const [selectedAttachment, setSelectedAttachment] = useState<ImageData[]>(
        [],
    );
    const token = useAppSelector(state => state.user.token);

    const [
        createDefect,
        {
            isLoading: isLoadingUpload,
            error: rtkError,
            isSuccess,
            data: createDefectData,
        },
    ] = useCreateDefectMutation();
    const [
        createDefectAttachment,
        { isLoading: isLoadingAttachment, error: isLoadingAttachmenError },
    ] = useCreateDefectAttachmentMutation();

    const backButtonOnPressHandler = () => {
        setCancel(true);
    };
    useRtkResponse(isLoadingUpload, rtkError, false);
    useRtkResponse(isLoadingAttachment, isLoadingAttachmenError, false);

    const handleNameChange = (name: string) => setName(name);
    const handleDescriptionChange = (description: string) =>
        setDescription(description);

    const handleSubmit = async () => {
        const validInputs = InputValidation.checkInputsNotEmpty([
            name,
            description,
        ]);
        setInputError(!validInputs);
        if (validInputs) {
            const parseName = name.trimStart();
            const parseDescription = description.trimStart();
            const defect: Partial<DefectAttributes> = {
                name: parseName,
                description: parseDescription,
            };
            createDefect(defect);
        }
    };

    const uploadImages = useCallback(async () => {
        if (selectedAttachment && token && isSuccess && createDefectData) {
            await Promise.all(
                selectedAttachment.map(attachment => {
                    return createDefectAttachment({
                        defectId: createDefectData.id,
                        attachment,
                    });
                }),
            );
        }
    }, [
        selectedAttachment,
        token,
        isSuccess,
        createDefectData,
        createDefectAttachment,
    ]);

    useEffect(() => {
        uploadImages();
    }, [uploadImages, isSuccess, createDefectData]);

    useEffect(() => {
        if (rtkError) {
            setUploadError(true);
        }
    }, [rtkError]);

    return (
        <ScrollableContainer style={styles.rootContainer}>
            <PopModal
                modalTitle={'Input Error'}
                modalBody={<Text>{'Both fields must be filled!' || ''}</Text>}
                visibility={inputError}
                confirmText="Okay"
                onConfirm={() => {
                    setInputError(!inputError);
                }}
                testID="input-error-modal"
            />
            <PopModal
                modalTitle={'Cancel Report'}
                modalBody={
                    <Text>
                        {'Are you sure? All changes will be lost.' || ''}
                    </Text>
                }
                visibility={cancel}
                confirmText="Confirm"
                cancelText="Cancel"
                onConfirm={() => {
                    navigation.goBack();
                }}
                onCancel={() => {
                    setCancel(false);
                }}
                testID="cancel-report-modal"
            />
            <PopModal
                modalTitle={'Report Submitted'}
                modalBody={
                    <Text>{'Report submitted successfully!' || ''}</Text>
                }
                visibility={isSuccess}
                confirmText="Okay"
                onConfirm={() => {
                    navigation.goBack();
                }}
                testID="report-submitted-modal"
            />
            <PopModal
                modalTitle={'Error Submitting Defect'}
                modalBody={
                    <Text>
                        {'Oops! Something went wrong. Please try again.' || ''}
                    </Text>
                }
                visibility={uploadError}
                confirmText="Okay"
                onConfirm={() => {
                    setUploadError(false);
                }}
                testID="submit-error-modal"
            />
            <HeaderBar
                title="Report a Defect"
                onBackButtonPress={backButtonOnPressHandler}
                testID="report-defect.header-bar-text"
                size="medium"
                showBackButton
            />
            <View style={styles.formContainer}>
                <DefectForm
                    defectName={name}
                    defectDescription={description}
                    onHandleDescriptionChange={handleDescriptionChange}
                    onHandleNameChange={handleNameChange}
                />
                <ImageUpload
                    existingAttachment={null}
                    setSelectedAttachments={setSelectedAttachment}
                />
                <View style={styles.buttonContainer}>
                    <PrimaryButton
                        testID="submit-defect-button"
                        accessibilityLabel="submit-defect-button"
                        label="Submit"
                        onPress={handleSubmit}
                    />
                    <Text style={styles.warningText}>* mandatory fields</Text>
                </View>
            </View>
            {isLoading && <Loading />}
        </ScrollableContainer>
    );
};

export default CreateDefectPage;
