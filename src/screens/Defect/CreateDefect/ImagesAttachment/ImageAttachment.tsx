import React from 'react';
import { View, Image, TouchableOpacity, Dimensions } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import styles from './ImageAttachmentStyles';
import VideoPlayer from 'react-native-media-console';
import { getImageUrl } from '../../../../Constants/api';

interface IImageAttachment {
    imageUrl: string;
    onPressRemove: () => void;
}

const ImageAttachment = ({ imageUrl, onPressRemove }: IImageAttachment) => {
    const screenWidth = Dimensions.get('window').width;
    const imageWidth = (screenWidth - 60) / 2;

    return (
        <View>
            <View style={styles.imageBox}>
                {imageUrl.includes('mp4') ||
                imageUrl.includes('quicktime') ||
                imageUrl.includes('MOV') ? (
                    <VideoPlayer
                        testID="image-attachment"
                        accessibilityLabel="image-attachment"
                        source={{
                            uri: getImageUrl(imageUrl),
                        }}
                        resizeMode="contain"
                        paused={true}
                        repeat={false}
                        alwaysShowControls={false}
                        disableSeekButtons={true}
                        disableBack={true}
                        muted={true}
                        disableFullscreen={true}
                        disablePlayPause={true}
                        disableOverlay={true}
                        disableSeekbar={true}
                        disableTimer={true}
                        disableVolume={true}
                        contentStartTime={1000}
                        currentPlaybackTime={1000}
                        containerStyle={[styles.video, { width: imageWidth }]}
                    />
                ) : (
                    <Image
                        testID="image-attachment"
                        accessibilityLabel="image-attachment"
                        style={[styles.image, { width: imageWidth }]}
                        source={{
                            uri: getImageUrl(imageUrl),
                        }}
                    />
                )}

                <TouchableOpacity
                    testID="delete-media-button"
                    accessibilityLabel="delete-media-button"
                    onPress={onPressRemove}
                    style={styles.removeButton}>
                    <FontAwesomeIcon
                        icon={faTimes}
                        style={styles.removeIcon}
                        size={20}
                    />
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default ImageAttachment;
