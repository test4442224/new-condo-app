import { StyleSheet } from 'react-native';
import { Spacings } from 'react-native-ui-lib';
import Colors from '../../../../Constants/Colors';

export default StyleSheet.create({
    imageBox: {
        marginBottom: Spacings.s5,
        borderWidth: 1,
        borderColor: Colors.$outlineDefault,
    },

    image: {
        height: 120,
        objectFit: 'cover',
    },
    video: {
        height: 120,
    },
    removeButton: {
        position: 'absolute',
        top: -10,
        right: -10,
        backgroundColor: Colors.$backgroundDisabled,
        borderRadius: 20,
        padding: Spacings.s1,
    },
    removeIcon: {
        color: Colors.$iconDefault,
    },
});
