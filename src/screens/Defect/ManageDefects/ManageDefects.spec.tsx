import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { RouteProp } from '@react-navigation/native';
import { cleanup, fireEvent, waitFor } from '@testing-library/react-native';
import React from 'react';
import { http, HttpResponse } from 'msw';
import { setupServer } from 'msw/native';
import ManageDefects from './ManageDefects';
import PageName from '../../../navigation/PageNameEnum';
import NavParamList from '../../../navigation/NavParmList';
import { renderWithProviders } from '../../../test/utils';

jest.mock('@react-navigation/native', () => {
    return {
        useNavigation: jest.fn(),
    };
});

jest.mock('../../../utils/hooks/useRtkResponse.ts', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        showToast: false,
    })),
}));

jest.mock('../../../navigation/NavProps', () => ({}));

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.MANAGE_DEFECTS_SCREEN>
> = {
    navigate: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.MANAGE_DEFECTS_SCREEN> = {
    key: 'ManageDefectsTestKey',
    name: PageName.MANAGE_DEFECTS_SCREEN,
};

const handlers = [
    http.get('http://localhost:3001/defect', async () => {
        return HttpResponse.json({
            data: [],
        });
    }),
];

const server = setupServer(...handlers);

// TODO
xdescribe('Manage Defects component', () => {
    beforeAll(() => {
        server.listen();
        server.events.on('request:start', ({ request }) => {
            console.log('MSW intercepted:', request.method, request.url);
        });
    });

    beforeEach(() => {
        jest.clearAllMocks();
        cleanup();
    });

    afterEach(() => {
        server.resetHandlers();
    });

    afterAll(() => server.close());

    it('should renders correctly', async () => {
        const { getByTestId, findByTestId } = renderWithProviders(
            <ManageDefects
                navigation={
                    mockNavigation as NativeStackNavigationProp<
                        NavParamList,
                        PageName.MANAGE_DEFECTS_SCREEN
                    >
                }
                route={mockRoute}
            />,
            {
                preloadedState: {
                    loading: { isLoading: false },
                },
            },
        );

        // mock and check data is visible

        await waitFor(() => {});
        expect(getByTestId('manage-defects.header-bar-text')).toBeTruthy();
        expect(getByTestId('manage-defects.add-btn')).toBeTruthy();
        expect(findByTestId('manage-defects.active-defects-tab')).toBeTruthy();
        expect(findByTestId('manage-defects.past-defects-tab')).toBeTruthy();
        expect(getByTestId('manage-defects.defectlist')).toBeTruthy();
    });

    it('should display noDefectLabel at Active tab', async () => {
        const { getByTestId, getAllByTestId, findByText } = renderWithProviders(
            <ManageDefects
                navigation={
                    mockNavigation as NativeStackNavigationProp<
                        NavParamList,
                        PageName.MANAGE_DEFECTS_SCREEN
                    >
                }
                route={mockRoute}
            />,
        );

        const primaryButton = getAllByTestId(
            'manage-defects.active-defects-tab',
        );
        await waitFor(() => fireEvent.press(primaryButton[0]));

        expect(getByTestId('defect.no-defect')).toBeTruthy();
        expect(
            await findByText(`You have no active defects at the moment.`),
        ).toBeDefined();
    });

    it('should display noDefectLabel at Past tab', async () => {
        const { getByTestId, getAllByTestId, findByText } = renderWithProviders(
            <ManageDefects
                navigation={
                    mockNavigation as NativeStackNavigationProp<
                        NavParamList,
                        PageName.MANAGE_DEFECTS_SCREEN
                    >
                }
                route={mockRoute}
            />,
        );

        const primaryButton = getAllByTestId('manage-defects.past-defects-tab');
        await waitFor(() => fireEvent.press(primaryButton[1]));

        expect(getByTestId('defect.no-defect')).toBeTruthy();
        expect(
            findByText(`You have no active defects at the moment.`),
        ).toBeDefined();
    });

    // it('should display noDefectLabel at Past tab', async () => {
    //     const { getByTestId, findByText } = renderWithProviders(
    //         <ManageDefects
    //             navigation={
    //                 mockNavigation as NativeStackNavigationProp<
    //                     NavParamList,
    //                     PageName.MANAGE_DEFECTS_SCREEN
    //                 >
    //             }
    //             route={mockRoute}
    //         />,
    //     );

    //     const primaryButton = getByTestId('manage-defects.past-defects-tab');
    //     await waitFor(() => fireEvent.press(primaryButton));
    //     expect(
    //         findByText(`You have no past defects at the moment.`),
    //     ).toBeDefined();
    // });
});
