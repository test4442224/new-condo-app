import PageName from '../../../navigation/PageNameEnum';
import { ManageDefectssNavProps } from '../../../navigation/NavProps';
import { FC, useRef, useState } from 'react';
import { Pressable, View } from 'react-native';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import React from 'react';
import styles from './ManageDefectsStyle';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import TabButton from '../../../components/ui/buttons/TabButton';
import Loading from '../../../components/ui/loading/Loading';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import useRtkResponse from '../../../utils/hooks/useRtkResponse';
import DefectList from '../../../components/lists/defectList/DefectList';
import { DefectStatus } from '../../../models/defect/DefectStatusHistory';
import { DefectAttributes, defectAction } from '../../../store/defect/slice';
import { useLazyGetAllDefectsByStatusQuery } from '../../../store/defect/api';
import { useFocusEffect } from '@react-navigation/native';
import { useGetCondoDefectPriorityLevelsQuery } from '../../../store/defect/defectPriority/api';

const ManageDefects: FC<ManageDefectssNavProps> = ({ navigation, route }) => {
    const dispatch = useAppDispatch();
    const defects = useAppSelector(state => state.defect);
    const [isOnActiveTab, setIsOnActiveTab] = useState<boolean>(true);
    const token = useAppSelector(state => state.user?.token);
    const isFirstLoadRef = useRef<boolean>(true);

    const { currentUserRole } = useAppSelector(state => state.userRole);

    const [
        getAllDefects,
        { isFetching: isFetchingDefects, error: getAllDefectsError },
    ] = useLazyGetAllDefectsByStatusQuery();

    const {
        error: getCondoDefectPriorityLevelsError,
        isFetching: isFetchingCondoDefectPriorityLevels,
    } = useGetCondoDefectPriorityLevelsQuery(
        {
            condoId: currentUserRole?.condoId ?? 0,
        },
        {
            skip: !currentUserRole?.condoId,
        },
    );

    useRtkResponse(isFetchingDefects, getAllDefectsError);
    useRtkResponse(
        isFetchingCondoDefectPriorityLevels,
        getCondoDefectPriorityLevelsError,
    );

    useFocusEffect(
        React.useCallback(() => {
            if (isFirstLoadRef.current) {
                isFirstLoadRef.current = false;
                return;
            }

            if (token) {
                if (
                    route.params?.tabStatus === 'Active' ||
                    route.params?.tabStatus === undefined
                ) {
                    setIsOnActiveTab(true);
                    getAllDefects({
                        status: [
                            DefectStatus.IN_PROGRESS,
                            DefectStatus.UNASSIGNED,
                        ],
                        sortBy: 'createdAt',
                    });
                } else {
                    setIsOnActiveTab(false);
                    getAllDefects({
                        status: [
                            DefectStatus.CANCELLED,
                            DefectStatus.COMPLETED,
                        ],
                        sortBy: 'updatedAt',
                    });
                }
            }
        }, [token, getAllDefects, route.params?.tabStatus]),
    );

    const onPressActiveTabHandler = async () => {
        if (isOnActiveTab) return;
        await getAllDefects({
            status: [DefectStatus.IN_PROGRESS, DefectStatus.UNASSIGNED],
            sortBy: 'createdAt',
        });
        setIsOnActiveTab(true);
    };

    const onPressPastTabHandler = async () => {
        if (!isOnActiveTab) return;
        await getAllDefects({
            status: [DefectStatus.COMPLETED, DefectStatus.CANCELLED],
            sortBy: 'updatedAt',
        });
        setIsOnActiveTab(false);
    };

    const onBackButtonPressHandler = () => {
        navigation.navigate(PageName.HOME_PAGE_SCREEN);
        setIsOnActiveTab(true);
    };

    const onAddButtonPressHandler = () => {
        navigation.navigate(PageName.CREATE_DEFECT_SCREEN);
        setIsOnActiveTab(true);
    };

    const onDefectCardPressHandler = (defect: DefectAttributes) => {
        dispatch(defectAction.selectedDefectForDetails(defect));
        navigation.navigate(PageName.DEFECT_DETAILS_SCREEN, {
            id: defect.id,
            statusType: isOnActiveTab ? 'Active' : 'Past',
        });
    };

    return (
        <>
            <View style={styles.rootContainer}>
                <View style={styles.headersContainer}>
                    <View style={styles.headerBarContainer}>
                        <HeaderBar
                            title="My Defects"
                            onBackButtonPress={onBackButtonPressHandler}
                            testID="manage-defects.header-bar-text"
                            additionalIcon={
                                <View>
                                    <Pressable
                                        accessibilityLabel="manage-defects.add-btn"
                                        onPress={onAddButtonPressHandler}>
                                        <FontAwesomeIcon
                                            icon={faPlus}
                                            style={styles.addButton}
                                            size={28}
                                        />
                                    </Pressable>
                                </View>
                            }
                        />
                    </View>
                </View>
                <View style={styles.buttonsContainer}>
                    <View
                        style={
                            isOnActiveTab
                                ? styles.pressed
                                : styles.buttonContainer
                        }>
                        <TabButton
                            testID="manage-defects.active-defects-tab"
                            onPress={onPressActiveTabHandler}
                            isActive={isOnActiveTab}>
                            Active
                        </TabButton>
                    </View>
                    <View
                        style={
                            !isOnActiveTab
                                ? styles.pressed
                                : styles.buttonContainer
                        }>
                        <TabButton
                            testID="manage-defects.past-defects-tab"
                            onPress={onPressPastTabHandler}
                            isActive={!isOnActiveTab}>
                            Past
                        </TabButton>
                    </View>
                </View>
                <DefectList
                    defects={defects.defectArray}
                    onPressed={defect => onDefectCardPressHandler(defect)}
                    noDefectLabel={
                        isOnActiveTab
                            ? 'You have no active defects at the moment.'
                            : 'You have no past defects at the moment.'
                    }
                />
                {isFetchingDefects && <Loading />}
            </View>
        </>
    );
};

export default ManageDefects;
