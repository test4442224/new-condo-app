import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import NavParamList from '../../../navigation/NavParmList';
import PageName from '../../../navigation/PageNameEnum';
import { RouteProp } from '@react-navigation/native';
import { cleanup } from '@testing-library/react-native';
import DefectDetails from './DefectDetails';
import React from 'react';
import { http, HttpResponse } from 'msw';
import { setupServer } from 'msw/native';
import { renderWithProviders } from '../../../test/utils';
import { DefectStatus } from '../../../models/defect/DefectStatusHistory';
import * as ReactNavigation from '@react-navigation/native';
import { RoleType } from '../../../models/userRole/RoleType';

jest.mock('@react-navigation/native', () => {
    return {
        __esModule: true,
        ...jest.requireActual('@react-navigation/native'),
        useFocusEffect: jest.fn(),
    };
});

jest.spyOn(ReactNavigation, 'useNavigation').mockImplementation(() => {});

jest.mock('../../../navigation/NavProps.ts', () => ({}));

jest.mock('../../../utils/hooks/useRtkResponse.ts', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        showToast: false,
    })),
}));
jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));
jest.mock('../../../store/hook', () => {
    return {
        useAppSelector: jest.fn(() => ({
            condoDefectPriorities: mockCondoDefectPriorities,
            allAvailableUseRoles: mockVendorEmployeeRole,
        })),
    };
});
const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.DEFECT_DETAILS_SCREEN>
> = {
    navigate: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.DEFECT_DETAILS_SCREEN> = {
    key: 'DefectDetailsKey',
    name: PageName.DEFECT_DETAILS_SCREEN,
    params: {
        id: 1,
        statusType: 'Active',
    },
};

const mockCondoDefectPriorities = [
    {
        id: 1,
        name: 'High',
        condoId: 1,
        dueAt: 1,
        dueAtUnit: 'DAYS',
    },
    {
        id: 2,
        name: 'Medium',
        condoId: 1,
        dueAt: 3,
        dueAtUnit: 'DAYS',
    },
    {
        id: 3,
        name: 'Low',
        condoId: 1,
        dueAt: 5,
        dueAtUnit: 'DAYS',
    },
];

const mockDefect = {
    id: 1,
    name: 'Poolside Chair Defect',
    description: 'There are cracks on the poolside chairs.',
    condoId: 1,
    latestStatus: DefectStatus.IN_PROGRESS,
    reporterId: 7,
    defectReferenceNo: 'D-240116-7h',
    createdAt: new Date(),
    updatedAt: new Date(),
    reporter: {
        id: 7,
        email: 'ongsiying00@gmail.com',
        firstName: 'Si Ying',
        lastName: 'Ong',
        createdAt: new Date(),
        updatedAt: new Date(),
    },
    attachments: [
        {
            id: 1,
            attachmentPath:
                'http://localhost:3001/image/facility-type/bbqpit.jpg',
            attachmentType: 'image',
            defectId: 1,
            defectStatusHistoryId: null,
            createdAt: new Date(),
            updatedAt: new Date(),
        },
    ],
    statusHistories: [
        {
            id: 1,
            status: DefectStatus.UNASSIGNED,
            defectId: 1,
            updatedById: 7,
            notes: 'Vendor inspected the defect.',
            createdAt: new Date(),
            updatedAt: new Date(),
            updatedBy: {
                id: 2,
                email: 'casper@mavericks-consulting.com',
                phoneNumber: 98765432,
                firstName: 'Casper',
                lastName: 'Lee',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            attachments: [
                {
                    id: 2,
                    attachmentPath:
                        'http://localhost:3001/image/facility-type/bbqpit.jpg',
                    attachmentType: 'image',
                    defectId: null,
                    defectStatusHistoryId: 1,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
        },
    ],
};

const component = (
    <DefectDetails
        navigation={
            mockNavigation as NativeStackNavigationProp<
                NavParamList,
                PageName.DEFECT_DETAILS_SCREEN
            >
        }
        route={mockRoute}
    />
);

const handlers = [
    // * at the end is for getting the id
    http.get('http://localhost:3001/defect/*', async () => {
        return HttpResponse.json({
            data: mockDefect,
        });
    }),
];

const mockVendorEmployeeRole = [
    {
        id: 1,
        roleType: RoleType.VENDOR_EMPLOYEE,
        condoId: 1,
        userId: 1,
    },
];
const server = setupServer(...handlers);
describe('Defect Details component for vendor employee', () => {
    beforeAll(() => {
        server.listen();
        server.events.on('request:start', ({ request }) => {
            console.log('MSW intercepted:', request.method, request.url);
        });
    });

    beforeEach(() => {
        jest.clearAllMocks();
        cleanup();
    });
    afterEach(() => {
        server.resetHandlers();
    });

    afterAll(() => server.close());

    jest.useFakeTimers();
    it('should render everything but cancel and edit button for vendor employee', async () => {
        const { getByTestId, queryByTestId, findByTestId } =
            renderWithProviders(component);
        expect(findByTestId('defect-details.header-bar-text')).toBeTruthy();
        expect(await findByTestId('defect-details.name')).toBeTruthy();
        expect(getByTestId('defect-details.referenceNo')).toBeTruthy();
        expect(getByTestId('defect-details.date')).toBeTruthy();
        expect(getByTestId('defect-details.description')).toBeTruthy();
        expect(
            queryByTestId('defect-details.status-history-status-text-label'),
        ).toBeTruthy();
        expect(
            queryByTestId('defect-details.status-history-note-label'),
        ).toBeTruthy();
        expect(
            queryByTestId('defect-details.status-history-details-createdAt'),
        ).toBeTruthy();
        expect(
            queryByTestId('defect-details.status-history-details-note'),
        ).toBeTruthy();
        expect(queryByTestId('defect-details.attachment-button')).toBeTruthy();
        expect(queryByTestId('edit-defect-page-button')).toBeNull();
        expect(
            queryByTestId('ios-defect-details.cancel-defect-btn'),
        ).toBeNull();
    });
});
