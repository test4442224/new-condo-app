import { StyleSheet } from 'react-native';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';
import { Spacings, Typography } from 'react-native-ui-lib';

import { Dimensions } from 'react-native';

const screenWidth = Dimensions.get('window').width;

const containerWidth = screenWidth / 4;

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
    },
    headerRowContainer: {
        alignItems: 'center',
    },
    rowContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: Spacings.s1,
        flexWrap: 'wrap',
    },
    badgeContainer: {
        justifyContent: 'space-between',
    },
    standardText: {
        ...Typography.text80,
    },
    dateText: {
        ...Typography.text90,
    },
    statusHistoryBadgeText: {
        ...Typography.text100,
    },
    boldText: {
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    titleText: {
        marginVertical: Spacings.s2,
    },
    cellLeftContainer: {
        flex: 1,
    },
    cellRightContainer: {
        flex: 2,
    },
    generalContainer: {
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
    statusGeneralContainer: {
        paddingTop: Spacings.s7,
        paddingBottom: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
    alignText: {
        textAlign: 'left',
    },
    statusesContainer: {
        flex: 3,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignContent: 'center',
    },
    statusHeaderContainer: {
        width: containerWidth,
        justifyContent: 'flex-start',
        marginRight: Spacings.s2,
    },
    attachmentStatusHeaderContainer: {
        justifyContent: 'flex-start',
        marginRight: Spacings.s2,
    },
    horizontalLine: {
        borderWidth: 0.7,
        borderColor: Colors.$outlinePrimary,
        marginVertical: Spacings.s3,
    },
    sectionLine: {
        borderWidth: 0.3,
        borderColor: Colors.$outlinePrimary,
        marginVertical: Spacings.s3,
    },
    subHeaderText: {
        ...Typography.text70,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    emptyStatus: {
        textAlign: 'center',
    },

    smallBadgesContainer: {
        paddingRight: Spacings.s1,
        justifyContent: 'flex-start',
    },
    attachmentIconContainer: {
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    statusNotesContainer: {
        flex: 1,
        width: containerWidth,
        justifyContent: 'flex-start',
        marginBottom: Spacings.s3,
    },
    statusHeaderText: {
        marginBottom: Spacings.s3,
    },
    buttonOuterContainer: {
        paddingHorizontal: Spacings.s4,
        paddingVertical: Spacings.s1,
    },
    buttonContainer: {
        flex: 1,
    },
    popModalExtraStyle: {
        flex: 2,
    },
    editButton: {
        color: Colors.$iconPrimary,
    },
});
