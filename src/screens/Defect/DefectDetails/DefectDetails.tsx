import React, { FC, useCallback, useEffect, useState } from 'react';
import { Pressable, StyleProp, View, ViewStyle } from 'react-native';
import styles from './DefectDetailsStyle';
import { DefectDetailScreenNavProps } from '../../../navigation/NavProps';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import ImageSwiper from '../../../components/ui/swiper/ImageSwiper';
import NormalText from '../../../components/ui/titles/NormalText';
import Badge from '../../../components/ui/badge/Badge';
import SubHeaderText from '../../../components/ui/titles/SubHeaderText';
import Text from '../../../components/ui/titles/Text';
import HeaderText from '../../../components/ui/titles/HeaderText';
import ScrollableContainer from '../../../components/container/ScrollableContainer';
import { toDateTimeFormat } from '../../../utils/dateTimeUtil';
import {
    useCancelDefectMutation,
    useGetDefectbyIdQuery,
} from '../../../store/defect/api';
import { generateDefectStatusBadge } from '../../../utils/GenerateDefectStatusBadge';
import useRtkResponse from '../../../utils/hooks/useRtkResponse';
import {
    DefectStatus,
    IDefectStatusHistory,
} from '../../../models/defect/DefectStatusHistory';
import { DefectAttachment } from '../../../models/defect/DefectAttachment';
import { faFileLines, faPenToSquare } from '@fortawesome/free-solid-svg-icons';
import IconButton from '../../../components/ui/buttons/IconButton';
import AttachmentPopModal from '../../../components/modal/AttachmentPopModal';
import { trimTruncatedText } from '../../../utils/TruncateText';
import PrimaryButton from '../../../components/ui/buttons/PrimaryButton';
import PopModal from '../../../components/modal/PopModal';
import Loading from '../../../components/ui/loading/Loading';
import PageName from '../../../navigation/PageNameEnum';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { DefectAttributes } from '../../../store/defect/slice';
import { useAppSelector } from '../../../store/hook';
import { useFocusEffect } from '@react-navigation/native';
import { RoleType } from '../../../models/userRole/RoleType';

const DefectDetails: FC<DefectDetailScreenNavProps> = ({
    navigation,
    route,
}) => {
    // TODO: REFACTOR; currently is only short term solution
    const [isPageLoading, setPageLoading] = useState<boolean>(true);
    const [isVendorEmployee, setIsVendorEmployee] = useState<boolean>(false);
    const defectTitle = 'Defect';
    const [cancel, setCancel] = useState<boolean>(false);
    const [selectedDefect, setSelectedDefect] = useState<DefectAttributes>();
    const [attachmentData, setAttachmentData] = useState<DefectAttachment[]>(
        [],
    );
    const [successCancelError, setSuccessCancelError] =
        useState<boolean>(false);
    const [cancelError, setCancelError] = useState<boolean>(false);
    const [cancelModalVisibility, setCancelModalVisibility] =
        useState<boolean>(false);
    const [attachmentPath, setAttachmentPath] = useState<string[]>([]);

    const { condoDefectPriorities } = useAppSelector(state => state.defect);
    const [isFirstLoadRef, setIsFirstLoadRef] = useState<boolean>(true);

    const [
        cancelDefect,
        {
            isLoading: isLoadingCancelDefect,
            error: isErrorCancelDefect,
            isSuccess,
        },
    ] = useCancelDefectMutation();

    const {
        data,
        isFetching: isFetchingDefects,
        error: getAllDefectsError,
        refetch,
    } = useGetDefectbyIdQuery(
        { defectId: route.params.id },
        {
            refetchOnMountOrArgChange: true,
            refetchOnReconnect: true,
        },
    );
    const { allAvailableUseRoles } = useAppSelector(state => state.userRole);
    const backButtonOnClickHandler = () => {
        let tabName: string = 'Active';
        if (selectedDefect?.latestStatus === DefectStatus.CANCELLED)
            tabName = 'Past';
        isVendorEmployee
            ? navigation.navigate(PageName.VENDOR_HOME_PAGE, {
                  tabStatus: tabName,
              })
            : navigation.navigate(PageName.MANAGE_DEFECTS_SCREEN, {
                  tabStatus: tabName,
              });
    };

    const badge = (status: string, textStyles: StyleProp<ViewStyle>) => {
        return (
            <Badge
                details={generateDefectStatusBadge(status)}
                textStyles={textStyles}
                accessibilityLabel="android-defect-status-badge"
                testID="ios-defect-status-badge"
            />
        );
    };

    const onOpenAttachmentPressHandler = (
        itemAttachments: DefectAttachment[],
    ) => {
        setAttachmentData(itemAttachments);
        setCancel(true);
    };

    const cancelDefectPressHandler = async () => {
        if (selectedDefect) await cancelDefect({ defectId: selectedDefect.id });
        else setCancelError(true);
    };

    const editDefectPressHandler = () => {
        navigation.navigate(PageName.EDIT_DEFECT_SCREEN, {
            defectData: selectedDefect,
            onEditSuccess: () => {
                refetch();
            },
        });
    };

    useEffect(() => {
        if (isErrorCancelDefect) {
            setCancelError(true);
        }
        if (isSuccess) {
            setSuccessCancelError(true);
        }
    }, [isErrorCancelDefect, isSuccess]);

    useEffect(() => {
        const hasVendorEmployeeRole = allAvailableUseRoles.find(
            role => role.roleType === RoleType.VENDOR_EMPLOYEE,
        );
        if (hasVendorEmployeeRole) {
            setIsVendorEmployee(true);
        } else {
            setIsVendorEmployee(false);
        }
    }, [allAvailableUseRoles]);

    useRtkResponse(isLoadingCancelDefect, isErrorCancelDefect, false);
    useRtkResponse(isFetchingDefects, getAllDefectsError);

    useEffect(() => {
        if (!data) return;
        if (isFirstLoadRef) {
            setIsFirstLoadRef(false);
            return;
        }
        setSelectedDefect(data.data);
    }, [data, isFirstLoadRef]);

    useFocusEffect(
        useCallback(() => {
            if (selectedDefect) {
                const defects = [...selectedDefect.attachments];

                defects.sort((a, b) => a.id - b.id);

                setAttachmentPath(
                    defects.map(
                        (attachment: DefectAttachment) =>
                            attachment.attachmentPath,
                    ),
                );
            }
        }, [selectedDefect]),
    );

    // TODO: REFACTOR; currently is only short term solution
    useEffect(() => {
        if (!isFetchingDefects && !isLoadingCancelDefect && data) {
            setPageLoading(false);
        }
    }, [isFetchingDefects, isLoadingCancelDefect, data]);

    // TODO: REFACTOR; currently is only short term solution
    if (isFetchingDefects) {
        return <Loading />;
    }

    return (
        <>
            {/* TODO: REFACTOR; currently is only short term solution */}
            {isPageLoading && <Loading />}
            <View style={styles.rootContainer}>
                <HeaderBar
                    additionalIcon={
                        (selectedDefect?.latestStatus ===
                            DefectStatus.IN_PROGRESS ||
                            selectedDefect?.latestStatus ===
                                DefectStatus.UNASSIGNED) &&
                        !isVendorEmployee ? (
                            <Pressable
                                accessibilityLabel="edit-defect-page-button"
                                testID="edit-defect-page-button"
                                onPress={editDefectPressHandler}>
                                <FontAwesomeIcon
                                    icon={faPenToSquare}
                                    size={26}
                                    style={styles.editButton}
                                />
                            </Pressable>
                        ) : null
                    }
                    title={`View ${defectTitle} Report`}
                    onBackButtonPress={backButtonOnClickHandler}
                    testID="defect-details.header-bar-text"
                    size="medium"
                    showBackButton
                />
                <ScrollableContainer>
                    {selectedDefect && (
                        <>
                            <View
                                testID="defect-details.image"
                                accessibilityLabel="defect-details.image">
                                <ImageSwiper
                                    imageURL={attachmentPath}
                                    autoplay={false}
                                    isDefectPage={true}
                                    onPressDefectHandler={() =>
                                        onOpenAttachmentPressHandler(
                                            selectedDefect.attachments,
                                        )
                                    }
                                />
                            </View>

                            <View style={styles.generalContainer}>
                                <View
                                    accessibilityLabel="defect-details.view-name"
                                    testID="defect-details.view-name"
                                    style={[
                                        styles.headerRowContainer,
                                        styles.rowContainer,
                                        styles.badgeContainer,
                                    ]}>
                                    <HeaderText
                                        style={styles.titleText}
                                        center={false}
                                        testID="defect-details.name">
                                        {trimTruncatedText(
                                            selectedDefect.name,
                                            25,
                                        )}
                                    </HeaderText>
                                    {badge(
                                        selectedDefect?.latestStatus,
                                        styles.standardText,
                                    )}
                                </View>

                                <View>
                                    <View
                                        style={styles.rowContainer}
                                        accessibilityLabel="defect-details.view-referenceNo">
                                        <NormalText
                                            style={[
                                                styles.cellLeftContainer,
                                                styles.boldText,
                                            ]}>
                                            {'Defect ID: '}
                                        </NormalText>
                                        <NormalText
                                            style={styles.cellRightContainer}
                                            testID="defect-details.referenceNo">
                                            {selectedDefect?.defectReferenceNo}
                                        </NormalText>
                                    </View>
                                    <View
                                        style={styles.rowContainer}
                                        accessibilityLabel="defect-details.view-date">
                                        <NormalText
                                            style={[
                                                styles.cellLeftContainer,
                                                styles.boldText,
                                            ]}>
                                            {'Reported Time : '}
                                        </NormalText>
                                        <NormalText
                                            style={styles.cellRightContainer}
                                            testID="defect-details.date">
                                            {toDateTimeFormat(
                                                selectedDefect?.createdAt,
                                            )}
                                        </NormalText>
                                    </View>
                                    <View
                                        style={styles.rowContainer}
                                        accessibilityLabel="defect-details.view-description">
                                        <NormalText
                                            style={[
                                                styles.cellLeftContainer,
                                                styles.boldText,
                                            ]}>
                                            {'Description: '}
                                        </NormalText>
                                        <NormalText
                                            style={styles.cellRightContainer}
                                            testID="defect-details.description">
                                            {selectedDefect?.description}
                                        </NormalText>
                                    </View>
                                    <View
                                        style={styles.rowContainer}
                                        accessibilityLabel="defect-details.view-priority">
                                        <NormalText
                                            style={[
                                                styles.cellLeftContainer,
                                                styles.boldText,
                                            ]}>
                                            {'Priority: '}
                                        </NormalText>
                                        <NormalText
                                            style={styles.cellRightContainer}
                                            testID="defect-details.priority">
                                            {selectedDefect?.priorityId === null
                                                ? '-'
                                                : condoDefectPriorities.filter(
                                                      priority =>
                                                          priority.id ===
                                                          selectedDefect.priorityId,
                                                  )[0]?.name ?? '-'}
                                        </NormalText>
                                    </View>
                                    <View
                                        style={styles.rowContainer}
                                        accessibilityLabel="defect-details.view-due-date">
                                        <NormalText
                                            style={[
                                                styles.cellLeftContainer,
                                                styles.boldText,
                                            ]}>
                                            {'Est. Completion: '}
                                        </NormalText>
                                        <NormalText
                                            style={styles.cellRightContainer}
                                            testID="defect-details.due-date">
                                            {selectedDefect?.dueDate === null
                                                ? '-'
                                                : toDateTimeFormat(
                                                      selectedDefect?.dueDate,
                                                  )}
                                        </NormalText>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.statusGeneralContainer}>
                                <SubHeaderText style={styles.statusHeaderText}>
                                    {'Status History:'}
                                </SubHeaderText>
                                <View style={styles.statusesContainer}>
                                    <View
                                        style={styles.statusHeaderContainer}
                                        accessibilityLabel="defect-details.view-status-history-status-label">
                                        <NormalText
                                            style={styles.subHeaderText}
                                            underline={false}
                                            testID="defect-details.status-history-status-text-label">
                                            Status
                                        </NormalText>
                                    </View>
                                    <View
                                        style={styles.statusHeaderContainer}
                                        accessibilityLabel="defect-details.view-status-history-note-label">
                                        <NormalText
                                            style={styles.subHeaderText}
                                            underline={false}
                                            testID="defect-details.status-history-note-label">
                                            Notes
                                        </NormalText>
                                    </View>
                                    <View
                                        style={styles.statusHeaderContainer}
                                    />
                                </View>

                                <View style={styles.horizontalLine} />

                                {selectedDefect &&
                                selectedDefect.statusHistories?.length > 0 ? (
                                    selectedDefect.statusHistories.map(
                                        (
                                            item: IDefectStatusHistory,
                                            index: number,
                                        ) => {
                                            return (
                                                <View
                                                    key={index}
                                                    style={
                                                        styles.statusesContainer
                                                    }>
                                                    <View
                                                        style={
                                                            styles.statusHeaderContainer
                                                        }>
                                                        <View
                                                            style={
                                                                styles.smallBadgesContainer
                                                            }>
                                                            {badge(
                                                                item.status,
                                                                styles.statusHistoryBadgeText,
                                                            )}
                                                        </View>
                                                    </View>

                                                    <View
                                                        style={
                                                            styles.statusNotesContainer
                                                        }>
                                                        <View>
                                                            <NormalText
                                                                underline={
                                                                    false
                                                                }
                                                                style={[
                                                                    styles.alignText,
                                                                    styles.dateText,
                                                                    styles.boldText,
                                                                ]}
                                                                testID="defect-details.status-history-details-createdAt"
                                                                center={true}>
                                                                {toDateTimeFormat(
                                                                    item?.createdAt,
                                                                )}
                                                            </NormalText>
                                                        </View>

                                                        <View>
                                                            <NormalText
                                                                underline={
                                                                    false
                                                                }
                                                                style={[
                                                                    styles.alignText,
                                                                    styles.standardText,
                                                                ]}
                                                                testID="defect-details.status-history-details-note"
                                                                center={true}>
                                                                {item.notes}
                                                            </NormalText>
                                                        </View>
                                                    </View>
                                                    <View
                                                        style={
                                                            styles.attachmentStatusHeaderContainer
                                                        }>
                                                        {item?.attachments
                                                            ?.length > 0 ? (
                                                            <View
                                                                testID="defect-details.view-attachment-button"
                                                                accessibilityLabel="defect-details.view-attachment-button"
                                                                style={
                                                                    styles.attachmentIconContainer
                                                                }>
                                                                <IconButton
                                                                    testID="defect-details.attachment-button"
                                                                    icon={
                                                                        faFileLines
                                                                    }
                                                                    size={20}
                                                                    onPress={() =>
                                                                        onOpenAttachmentPressHandler(
                                                                            item.attachments,
                                                                        )
                                                                    }
                                                                />
                                                            </View>
                                                        ) : null}
                                                    </View>
                                                </View>
                                            );
                                        },
                                    )
                                ) : (
                                    <Text
                                        style={styles.emptyStatus}
                                        testID="defect-details.no-status-history-text">
                                        There are no status entries at the
                                        moment.
                                    </Text>
                                )}
                                <View style={styles.sectionLine} />
                            </View>
                            {selectedDefect &&
                                selectedDefect.latestStatus !==
                                    DefectStatus.CANCELLED &&
                                route.params.statusType === 'Active' &&
                                !isVendorEmployee && (
                                    <View style={styles.buttonOuterContainer}>
                                        <View style={styles.buttonContainer}>
                                            <PrimaryButton
                                                label="Cancel Defect"
                                                onPress={() =>
                                                    setCancelModalVisibility(
                                                        true,
                                                    )
                                                }
                                                accessibilityLabel="android-defect-details.cancel-defect-btn"
                                                testID="ios-defect-details.cancel-defect-btn"
                                            />
                                        </View>
                                    </View>
                                )}
                            <AttachmentPopModal
                                defectAttachment={attachmentData}
                                visibility={cancel}
                                onCancel={() => {
                                    setCancel(false);
                                }}
                            />
                            <PopModal
                                modalTitle={`Cancel Defect`}
                                modalBody={
                                    <NormalText>{`Are you sure you want to cancel your defect?\nThis cannot be undone.`}</NormalText>
                                }
                                modalData={`Defect Name: ${
                                    selectedDefect ? selectedDefect.name : null
                                }\nReported: ${
                                    selectedDefect
                                        ? toDateTimeFormat(
                                              selectedDefect.createdAt,
                                          )
                                        : null
                                }`}
                                style={styles.popModalExtraStyle}
                                visibility={cancelModalVisibility}
                                confirmText="Confirm"
                                onCancel={() => setCancelModalVisibility(false)}
                                onConfirm={() => {
                                    setCancelModalVisibility(false);
                                    cancelDefectPressHandler();
                                }}
                                testID="defect-details.popmodal-cancel"
                            />
                            <PopModal
                                modalTitle={'Successfully Cancelled Defect'}
                                modalBody={
                                    <Text>
                                        {
                                            'The defect has been successfully cancelled.'
                                        }
                                    </Text>
                                }
                                visibility={successCancelError}
                                confirmText="Okay"
                                onConfirm={() => {
                                    setSuccessCancelError(false);
                                }}
                                testID="defect-details.popmodal-cancel-success"
                            />
                            <PopModal
                                modalTitle={'Failed to Cancel Defect'}
                                modalBody={
                                    <NormalText>
                                        Your defect cannot be cancelled. Please
                                        try again later.
                                    </NormalText>
                                }
                                visibility={cancelError}
                                confirmText="Okay"
                                onConfirm={() => {
                                    setCancelError(false);
                                }}
                                testID="defect-details.popmodal-cancel-fail"
                            />
                        </>
                    )}
                </ScrollableContainer>
            </View>
        </>
    );
};

export default DefectDetails;
