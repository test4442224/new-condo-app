import { StyleSheet } from 'react-native';
import Colors from '../../Constants/Colors';
import { BorderRadiuses, Spacings, Typography } from 'react-native-ui-lib';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
    },
    condoLogo: {
        width: 160,
        height: 40,
    },
    condoLogoContainerIos: {
        marginLeft: -Spacings.s8 * 4,
    },
    condoLogoContainerAndroid: {
        marginTop: Spacings.s4,
        marginLeft: -Spacings.s8 * 4,
    },
    imageSwiper: {
        marginVertical: Spacings.s4,
    },
    buttonsContainer: {
        flexDirection: 'row',
        marginHorizontal: Spacings.s3,
        flexWrap: 'wrap',
        // justifyContent: 'space-between',
    },
    buttonContainer: {
        width: '20%',
        alignItems: 'center',
        marginHorizontal: Spacings.s2,
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: Spacings.s4,
        paddingVertical: Spacings.s4,
        borderRadius: BorderRadiuses.br60,
        backgroundColor: Colors.$backgroundPrimary,
    },
    icon: {
        color: Colors.$iconDefault,
    },
    text: {
        ...Typography.text90,
    },
    bookingContainer: {
        marginHorizontal: Spacings.s3,
        marginVertical: Spacings.s3,
        textAlign: 'center',
    },
    bookingTextTitle: {
        justifyContent: 'center',
        textAlign: 'center',
        margin: 'auto',
        color: Colors.$backgroundPrimary,
        fontWeight: 'bold',
        fontSize: 17,
        marginBottom: Spacings.s5,
    },
    bookingText: {
        marginTop: 20,
        justifyContent: 'center',
        textAlign: 'center',
        margin: 'auto',
    },
    errorText: {
        color: Colors.$textError,
    },
});
