import { Platform, Pressable, View } from 'react-native';
import React, { FC, useCallback, useEffect } from 'react';
import {
    faCalendarCheck,
    faBullhorn,
    faScrewdriverWrench,
    faCreditCard,
    faFileInvoiceDollar,
} from '@fortawesome/free-solid-svg-icons';
import { HomePageNavProps } from '../../navigation/NavProps';
import styles from './HomePageStyles';
import IconButton from '../../components/ui/buttons/IconButton';
import PageName from '../../navigation/PageNameEnum';
import HeaderBar from '../../components/headerbar/HeaderBar';
import { Image } from 'react-native-ui-lib';
import { useAppDispatch, useAppSelector } from '../../store/hook';
import ImageSwiper from '../../components/ui/swiper/ImageSwiper';
import SubHeaderText from '../../components/ui/titles/SubHeaderText';
import Divider from '../../components/ui/divider/Divider';
import {
    ManageBookingAttributes,
    bookingAction,
} from '../../store/manageBooking/slice';
import NormalText from '../../components/ui/titles/NormalText';
import { formatUnitNumber } from '../../utils/FormatUnitNumber';
import ScrollableContainer from '../../components/container/ScrollableContainer';
import {
    NoticeAttributes,
    NoticeType,
    noticeAction,
} from '../../store/notice/slice';
import {
    useGetAllCarouselNoticeApiQuery,
    useLazyGetAllCarouselNoticeApiQuery,
    useLazyGetAllNoticesByNoticeTypeQuery,
} from '../../store/notice/api';
import {
    useLazyGetAllBookingsQuery,
    useLazyGetUpcomingBookingsQuery,
} from '../../store/manageBooking/api';
import { useGetUserDetailsQuery } from '../../store/user/userApi';
import { useFocusEffect } from '@react-navigation/native';
import { loadingActions } from '../../store/loading/slice';
import Loading from '../../components/ui/loading/Loading';
import Colors from '../../Constants/Colors';
import UpcomingBookingCard from './UpcomingBookingCard';
import { DefectStatus } from '../../models/defect/DefectStatusHistory';
import { useLazyGetAllDefectsByStatusQuery } from '../../store/defect/api';
import { useLazyGetAllConservancyPaymentsQuery } from '../../store/payment/api';
import { useLazyGetAllStatementOfAccountsQuery } from '../../store/statementOfAccount/api';

const { setLoading } = loadingActions;

const HomePage: FC<HomePageNavProps> = ({ navigation }) => {
    const dispatch = useAppDispatch();
    const isDefectReporter = useAppSelector(
        state => state.userRole.isDefectReporter,
    );
    const isPrimaryUser = useAppSelector(state => state.user.isPrimaryUser);
    const token = useAppSelector(state => state.user.token);
    const residentName = useAppSelector(
        state => state.user.profileDetails.firstName,
    )!;
    const residentUnitDetails = useAppSelector(
        state => state.user.profileDetails.units[0],
    )!;
    const carouselArray = useAppSelector(state => state.notice.carouselArray);
    // const { upcomingBookings } = useAppSelector(state => state.booking);
    const { isLoading } = useAppSelector(state => state.loading);

    const { isLoading: isGetAllCarouselNoticeLoading } =
        useGetAllCarouselNoticeApiQuery(token!, {
            skip: !token,
            refetchOnMountOrArgChange: true,
        });
    const { isLoading: isGetUserDetailsLoading } = useGetUserDetailsQuery(
        undefined,
        {
            refetchOnMountOrArgChange: true,
        },
    );
    const [
        getCarouselNotice,
        { isLoading: isLazyGetAllCarouselNoticeLoading },
    ] = useLazyGetAllCarouselNoticeApiQuery();
    const [
        getUpcomingBookings,
        {
            data: getUpcomingBookingData,
            isLoading: isUpcomingBookingsLoading,
            isError: isGetUpcomingBookingsError,
            isSuccess: isGetUpcomingBookingsSuccess,
        },
    ] = useLazyGetUpcomingBookingsQuery();
    const [getAllNotices, { isFetching: isFetchingNotices }] =
        useLazyGetAllNoticesByNoticeTypeQuery();
    const [getAllBookings, { isFetching: isFetchingBookings }] =
        useLazyGetAllBookingsQuery();

    const [getAllDefects, { isFetching: isFetchingDefects }] =
        useLazyGetAllDefectsByStatusQuery();

    const [getAllConservancyPayments, { isFetching: isFetchingPayments }] =
        useLazyGetAllConservancyPaymentsQuery();

    const [getAllStatementOfAccount, { isFetching: isFetchingSOA }] =
        useLazyGetAllStatementOfAccountsQuery();

    const onCondoImagePressHandler = () => {
        navigation.navigate(PageName.ABOUT_US_SCREEN);
    };

    const onProfileButtonPressHandler = () => {
        navigation.navigate(PageName.PROFILE_PAGE_SCREEN);
    };
    const onPressManageBookingsButtonHandler = async () => {
        await getAllBookings(undefined);
        navigation.navigate(PageName.MANAGE_BOOKINGS_SCREEN);
    };
    const onPressNoticeBoardButtonHandler = async () => {
        await getAllNotices({ noticeType: NoticeType.ANNOUNCEMENT });
        navigation.navigate(PageName.NOTICE_SCREEN);
    };

    const onPressStatementOfAccountButtonHandler = async () => {
        await getAllStatementOfAccount();
        navigation.navigate(PageName.STATEMENT_OF_ACCOUNT);
    };

    const onPressManageDefectsButtonHandler = async () => {
        await getAllDefects({
            status: [DefectStatus.IN_PROGRESS, DefectStatus.UNASSIGNED],
            sortBy: 'createdAt',
        });
        navigation.navigate(PageName.MANAGE_DEFECTS_SCREEN);
    };
    const onCarouselImagePressHandler = (notice: NoticeAttributes) => {
        dispatch(noticeAction.selectNoticeForDetails(notice));
        navigation.navigate(PageName.NOTICE_DETAILS_SCREEN, {
            noticeTitle: notice.noticeType,
        });
    };
    const onBookingCardPressHandler = (item: ManageBookingAttributes) => {
        dispatch(bookingAction.selectBookingForDetail(item));

        navigation.navigate(PageName.BOOKING_DETAILS_SCREEN, {
            bookingTitle: 'Upcoming',
        });
    };
    const onPressManagePaymentsButtonHandler = async () => {
        await getAllConservancyPayments();
        navigation.navigate(PageName.MANAGE_PAYMENTS_SCREEN);
    };
    const onRefreshHandler = useCallback(() => {
        if (token) {
            getCarouselNotice(token);
            getUpcomingBookings(undefined);
        }
    }, [token, getCarouselNotice, getUpcomingBookings]);

    let residentUnitNumber = residentUnitDetails.unitNumber;
    if (residentUnitNumber) {
        residentUnitNumber = formatUnitNumber(residentUnitNumber);
    }

    useFocusEffect(
        useCallback(() => {
            getUpcomingBookings(undefined);
        }, [getUpcomingBookings]),
    );
    useEffect(() => {
        const isLoadingHomePage =
            isGetAllCarouselNoticeLoading ||
            isGetUserDetailsLoading ||
            isLazyGetAllCarouselNoticeLoading ||
            isUpcomingBookingsLoading ||
            isFetchingNotices ||
            isFetchingDefects ||
            isFetchingSOA ||
            isFetchingBookings ||
            isFetchingPayments;

        dispatch(setLoading(isLoadingHomePage));
    }, [
        isGetAllCarouselNoticeLoading,
        isGetUserDetailsLoading,
        isLazyGetAllCarouselNoticeLoading,
        isUpcomingBookingsLoading,
        isFetchingSOA,
        isFetchingNotices,
        isFetchingDefects,
        isFetchingBookings,
        isFetchingPayments,
        dispatch,
    ]);

    if (
        isLoading &&
        !isFetchingNotices &&
        !isFetchingBookings &&
        !isFetchingDefects &&
        !isFetchingSOA &&
        !isGetAllCarouselNoticeLoading &&
        !isGetUserDetailsLoading &&
        !isLazyGetAllCarouselNoticeLoading &&
        !isUpcomingBookingsLoading &&
        !isFetchingPayments
    ) {
        return <Loading backgroundColor={Colors.$backgroundDefault} />;
    }

    return (
        <>
            <View style={styles.rootContainer}>
                <HeaderBar
                    title=""
                    showBackButton={false}
                    onProfileButtonPress={onProfileButtonPressHandler}
                    additionalIcon={
                        <View
                            style={
                                Platform.OS === 'ios'
                                    ? styles.condoLogoContainerIos
                                    : styles.condoLogoContainerAndroid
                            }>
                            <Pressable
                                onPress={onCondoImagePressHandler}
                                accessibilityLabel="condoImage"
                                testID="condoImage">
                                <Image
                                    style={styles.condoLogo}
                                    source={require('../../assets/logo/trevose.png')}
                                    resizeMode="contain"
                                />
                            </Pressable>
                        </View>
                    }
                    residentName={residentName ?? ''}
                    residentUnitDetails={
                        residentUnitNumber ? `#${residentUnitNumber}` : ''
                    }
                />
                <ScrollableContainer
                    refresh={true}
                    onRefresh={onRefreshHandler}>
                    <View
                        accessibilityLabel="image-carousel"
                        style={styles.imageSwiper}>
                        <ImageSwiper
                            carouselNoticeArray={carouselArray}
                            autoplay={true}
                            onPressHandler={onCarouselImagePressHandler}
                        />
                    </View>
                    <View style={styles.buttonsContainer}>
                        <View style={styles.buttonContainer}>
                            <IconButton
                                container={true}
                                style={styles.button}
                                iconStyle={styles.icon}
                                icon={faCalendarCheck}
                                onPress={onPressManageBookingsButtonHandler}
                                testID="manage-booking-btn"
                            />
                            <SubHeaderText
                                style={styles.text}
                                underline={false}
                                testID="manage-bookings-text"
                                center={true}>
                                My Bookings
                            </SubHeaderText>
                        </View>
                        <View style={styles.buttonContainer}>
                            <IconButton
                                container={true}
                                style={styles.button}
                                iconStyle={styles.icon}
                                icon={faCreditCard}
                                onPress={onPressManagePaymentsButtonHandler}
                                testID="payments-btn"
                            />
                            <SubHeaderText
                                style={styles.text}
                                underline={false}
                                testID="payment-text"
                                center={true}>
                                Payments
                            </SubHeaderText>
                        </View>
                        <View style={styles.buttonContainer}>
                            <IconButton
                                container={true}
                                style={styles.button}
                                iconStyle={styles.icon}
                                icon={faBullhorn}
                                onPress={onPressNoticeBoardButtonHandler}
                                testID="notice-board-btn"
                            />
                            <SubHeaderText
                                style={styles.text}
                                underline={false}
                                testID="notice-board-text"
                                center={true}>
                                Notice Board
                            </SubHeaderText>
                        </View>
                        {isPrimaryUser && (
                            <View style={styles.buttonContainer}>
                                <IconButton
                                    container={true}
                                    style={styles.button}
                                    iconStyle={styles.icon}
                                    icon={faFileInvoiceDollar}
                                    onPress={
                                        onPressStatementOfAccountButtonHandler
                                    }
                                    testID="statement-of-account-btn"
                                />
                                <SubHeaderText
                                    style={styles.text}
                                    underline={false}
                                    testID="statement-of-account-text"
                                    center={true}>
                                    Statement of Accounts
                                </SubHeaderText>
                            </View>
                        )}
                        {isDefectReporter && (
                            <View style={styles.buttonContainer}>
                                <IconButton
                                    container={true}
                                    style={styles.button}
                                    iconStyle={styles.icon}
                                    icon={faScrewdriverWrench}
                                    onPress={onPressManageDefectsButtonHandler}
                                    testID="manage-defects-btn"
                                />
                                <SubHeaderText
                                    style={styles.text}
                                    underline={false}
                                    testID="manage-defects-text"
                                    center={true}>
                                    Defect Reporting
                                </SubHeaderText>
                            </View>
                        )}
                    </View>
                    <Divider />
                    <View style={styles.bookingContainer}>
                        <NormalText style={styles.bookingTextTitle}>
                            Your next upcoming booking
                        </NormalText>
                        <UpcomingBookingCard
                            isGetUpcomingBookingsError={
                                isGetUpcomingBookingsError
                            }
                            isGetUpcomingBookingsSuccess={
                                isGetUpcomingBookingsSuccess
                            }
                            upcomingBookings={getUpcomingBookingData!}
                            onBookingCardPressHandler={
                                onBookingCardPressHandler
                            }
                        />
                    </View>
                </ScrollableContainer>
            </View>
            <Loading />
        </>
    );
};

export default HomePage;
