import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import NavParamList from '../../navigation/NavParmList';
import PageName from '../../navigation/PageNameEnum';
import { RouteProp } from '@react-navigation/native';
import { cleanup, waitFor } from '@testing-library/react-native';
import HomePage from './HomePage';
import React from 'react';
import { http, HttpResponse } from 'msw';
import { setupServer } from 'msw/native';
import { renderWithProviders } from '../../test/utils';

jest.mock('@react-navigation/native', () => {
    return {
        useNavigation: jest.fn(),
        useFocusEffect: jest.fn(),
    };
});

jest.mock('../../navigation/NavProps', () => ({}));

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.HOME_PAGE_SCREEN>
> = {
    navigate: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.HOME_PAGE_SCREEN> = {
    key: 'HomePageTestKey',
    name: PageName.HOME_PAGE_SCREEN,
};

const handlers = [
    http.get('http://localhost:3001/booking', async () => {
        return HttpResponse.json([]);
    }),

    http.get('http://localhost:3001/booking/upcoming', async () => {
        return HttpResponse.json([]);
    }),

    // * at the end is for getting the id
    http.get('http://localhost:3001/booking/*', async () => {
        return HttpResponse.json({});
    }),

    http.get('http://localhost:3001/notice/carousel', async () => {
        return HttpResponse.json([]);
    }),
];

const server = setupServer(...handlers);

describe('HomePage component', () => {
    beforeAll(() => {
        server.listen();
        server.events.on('request:start', ({ request }) => {
            console.log('MSW intercepted:', request.method, request.url);
        });
    });

    beforeEach(() => {
        jest.clearAllMocks();
        cleanup();
    });

    afterEach(() => {
        server.resetHandlers();
    });

    afterAll(() => server.close());

    const getComponent = ({ isPrimaryUser }: { isPrimaryUser: boolean }) =>
        renderWithProviders(
            <HomePage
                navigation={
                    mockNavigation as NativeStackNavigationProp<
                        NavParamList,
                        PageName.HOME_PAGE_SCREEN
                    >
                }
                route={mockRoute}
            />,
            {
                preloadedState: {
                    user: {
                        condo: null,
                        token: 'test',
                        id: null,
                        isPasswordReset: false,
                        loggedOutReason: null,
                        profileDetails: {
                            firstName: null,
                            lastName: null,
                            email: null,
                            phoneNumber: null,
                            units: [{ blockNumber: null, unitNumber: null }],
                        },
                        isAgreedToS: false,
                        agreedToSAt: null,
                        isPrimaryUser,
                    },
                    loading: { isLoading: false },
                },
            },
        );

    it('renders correctly for a usual user', async () => {
        const { getByTestId, getByLabelText, queryByTestId } = getComponent({
            isPrimaryUser: false,
        });
        await waitFor(() => {});
        // Assert that key elements are rendered
        expect(getByLabelText('condoImage')).toBeTruthy();
        expect(getByLabelText('image-carousel')).toBeTruthy();
        expect(getByTestId('manage-booking-btn')).toBeTruthy();
        expect(getByTestId('notice-board-btn')).toBeTruthy();
        expect(queryByTestId('statement-of-account-btn')).toBeNull();
        // Add more assertions as needed
    });

    it('renders correctly for primary user', async () => {
        const { getByTestId, getByLabelText } = getComponent({
            isPrimaryUser: true,
        });
        await waitFor(() => {});
        // Assert that key elements are rendered
        expect(getByLabelText('condoImage')).toBeTruthy();
        expect(getByLabelText('image-carousel')).toBeTruthy();
        expect(getByTestId('manage-booking-btn')).toBeTruthy();
        expect(getByTestId('notice-board-btn')).toBeTruthy();
        expect(getByTestId('statement-of-account-btn')).toBeTruthy();
        // Add more assertions as needed
    });
});
