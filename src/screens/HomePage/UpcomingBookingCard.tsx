import React, { FC } from 'react';
import { ManageBookingAttributes } from '../../store/manageBooking/slice';
import NormalText from '../../components/ui/titles/NormalText';
import styles from './HomePageStyles';
import BookingCard from '../../components/ui/buttons/BookingCard/BookingCard';

interface Props {
    isGetUpcomingBookingsError: boolean;
    isGetUpcomingBookingsSuccess: boolean;
    upcomingBookings: ManageBookingAttributes[];
    onBookingCardPressHandler: (item: ManageBookingAttributes) => void;
}

const UpcomingBookingCard: FC<Props> = ({
    isGetUpcomingBookingsError,
    isGetUpcomingBookingsSuccess,
    upcomingBookings,
    onBookingCardPressHandler,
}) => {
    if (isGetUpcomingBookingsError) {
        return (
            <NormalText style={[styles.bookingText, styles.errorText]}>
                Failed to get upcoming booking. Please try again later.
            </NormalText>
        );
    }

    if (isGetUpcomingBookingsSuccess && upcomingBookings.length === 0) {
        return (
            <NormalText style={styles.bookingText}>
                You have no upcoming bookings at the moment.
            </NormalText>
        );
    }

    if (isGetUpcomingBookingsSuccess) {
        return (
            <>
                <BookingCard
                    booking={upcomingBookings[0]}
                    onPress={() =>
                        onBookingCardPressHandler(upcomingBookings[0])
                    }
                    label={`BookingCard${upcomingBookings[0]}`}
                />

                <NormalText style={styles.bookingText}>
                    View more bookings under Manage Bookings.
                </NormalText>
            </>
        );
    }

    return <></>;
};

export default UpcomingBookingCard;
