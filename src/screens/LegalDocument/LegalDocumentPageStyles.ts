import { StyleSheet } from 'react-native';
import { Spacings } from 'react-native-ui-lib';
import Colors from '../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
    },
    legalDocumentContainer: {
        marginTop: Spacings.s2,
        marginBottom: Spacings.s5,
        paddingHorizontal: Spacings.s5,
    },
    agreeToSDateTextBox: {
        marginBottom: Spacings.s2,
        marginHorizontal: Spacings.s5,
    },
});
