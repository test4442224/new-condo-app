import { StyleSheet } from 'react-native';
import { Spacings } from 'react-native-ui-lib';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
    },
    headerContainer: {
        marginVertical: Spacings.s2,
    },
    tncContainer: {
        marginBottom: Spacings.s5,
        paddingHorizontal: Spacings.s5,
    },
    buttonOuterContainer: {
        flexDirection: 'row',
        gap: Spacings.s3,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: Spacings.s4,
        paddingVertical: Spacings.s4,
    },
    buttonContainer: {
        flex: 1,
    },
});
