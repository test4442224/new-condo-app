import React, { FC, useEffect, useState } from 'react';
import { ScrollView, View } from 'react-native';
import { useGetDocumentQuery } from '../../../store/legalDocument/legalDocumentApi';
import TextDocument from '../../../components/quillDocument/TextDocument';
import { LegalDocumentName } from '../../../models/legalDocuments/legalDocumentName.enum';
import { AcceptTermsofServiceScreenNavProps } from '../../../navigation/NavProps';
import styles from './AcceptTermsOfServiceScreenStyle';
import SecondaryButton from '../../../components/ui/buttons/SecondaryButton';
import PrimaryButton from '../../../components/ui/buttons/PrimaryButton';
import HeaderText from '../../../components/ui/titles/HeaderText';
import { useAgreeToSMutation } from '../../../store/user/userApi';
import PageName from '../../../navigation/PageNameEnum';
import { userActions } from '../../../store/user/UserActions';
import { useAppDispatch } from '../../../store/hook';
import DeclineToSPopup from '../../../components/modal/DeclineToSPopup/DeclineToSPopup.component';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { QueryStatus } from '@reduxjs/toolkit/dist/query';
import { RtkErrorHandler } from '../../../utils/error/RtkErrorHandler.util';
import Toast from 'react-native-simple-toast';
import { Fader } from 'react-native-ui-lib';
import { ScrollEvent } from '../../../models/event/scrollEvent.model';

const AcceptTermsOfService: FC<AcceptTermsofServiceScreenNavProps> = ({
    navigation,
}) => {
    const [isDeclinePopupOpen, setIsDeclinePopupOpen] =
        useState<boolean>(false);
    const [isScrollAtBottom, setIsScrollAtBottom] = useState<boolean>(false);

    const dispatch = useAppDispatch();

    const { data: getDocumentData } = useGetDocumentQuery({
        documentName: LegalDocumentName.TERMS_OF_SERVICE,
    });
    const [
        agreeToS,
        { status: agreeToSStatus, reset: resetAgreeToS, error: agreeToSError },
    ] = useAgreeToSMutation({
        fixedCacheKey: 'acceptToSResult',
    });

    const isCloseToBottom = ({
        layoutMeasurement,
        contentOffset,
        contentSize,
    }: ScrollEvent) => {
        const paddingToBottom = 20;
        return (
            layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom
        );
    };

    const declineButtonOnPressHandler = () => {
        setIsDeclinePopupOpen(true);
    };

    const acceptToSHandler = () => {
        agreeToS(undefined);
    };

    const closeDeclinePopup = () => {
        setIsDeclinePopupOpen(false);
    };

    const confirmDeclineHandler = async () => {
        closeDeclinePopup();

        dispatch(userActions.logout());

        await AsyncStorage.clear();

        navigation.navigate(PageName.LOGIN_SCREEN);
    };

    // Handle agreeToS success and fail cases
    useEffect(() => {
        if (agreeToSStatus === QueryStatus.rejected && agreeToSError) {
            const errorMessage = RtkErrorHandler.getMessage(agreeToSError);

            Toast.show(errorMessage, Toast.LONG);

            resetAgreeToS();

            return;
        }
    }, [agreeToSStatus, agreeToSError, resetAgreeToS, navigation]);

    // Comment out, in case we have to bring back the last updated on top
    // const lastUpdatedAt = () => {
    //     if (getDocumentData?.updatedAt) {
    //         return format(new Date(getDocumentData?.updatedAt), 'MMM yyyy');
    //     }
    // };

    return (
        <View style={styles.rootContainer}>
            <View style={styles.headerContainer}>
                <HeaderText>Terms of Service</HeaderText>
            </View>
            <ScrollView
                onScroll={({ nativeEvent }) => {
                    const closeToBottom = isCloseToBottom(nativeEvent);
                    setIsScrollAtBottom(closeToBottom);
                }}
                scrollEventThrottle={400}>
                <View style={styles.tncContainer}>
                    {/* <View>
                        <Text>Last updated: {lastUpdatedAt()}</Text>
                    </View> */}
                    {getDocumentData && (
                        <TextDocument content={getDocumentData.content} />
                    )}
                </View>
            </ScrollView>
            <View>
                <Fader position={Fader.position.BOTTOM} tintColor="white" />
            </View>
            <View>
                <View style={styles.buttonOuterContainer}>
                    <View style={styles.buttonContainer}>
                        <SecondaryButton
                            label="Decline"
                            testID="decline-tos-btn"
                            onPress={declineButtonOnPressHandler}
                        />
                    </View>

                    <View style={styles.buttonContainer}>
                        <PrimaryButton
                            label="Accept"
                            testID="accept-tos-btn"
                            onPress={acceptToSHandler}
                            disabled={!isScrollAtBottom}
                        />
                    </View>
                </View>
            </View>
            <DeclineToSPopup
                onConfirm={confirmDeclineHandler}
                onClose={closeDeclinePopup}
                isOpen={isDeclinePopupOpen}
            />
        </View>
    );
};

export default AcceptTermsOfService;
