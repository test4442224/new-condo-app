import { FC, useEffect } from 'react';
import { TermsOfServiceScreenNavProps } from '../../../navigation/NavProps';
import PageName from '../../../navigation/PageNameEnum';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import { QueryStatus } from '@reduxjs/toolkit/dist/query';
import React from 'react';
import { Fader, Toast } from 'react-native-ui-lib';
import { LegalDocumentName } from '../../../models/legalDocuments/legalDocumentName.enum';
import { useGetDocumentQuery } from '../../../store/legalDocument/legalDocumentApi';
import { loadingActions } from '../../../store/loading/slice';
import { RtkErrorHandler } from '../../../utils/error/RtkErrorHandler.util';
import { View } from 'react-native';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import Loading from '../../../components/ui/loading/Loading';
import styles from '../LegalDocumentPageStyles';
import NormalText from '../../../components/ui/titles/NormalText';
import BoldText from '../../../components/ui/titles/BoldText';
import TextDocument from '../../../components/quillDocument/TextDocument';
import ScrollableContainer from '../../../components/container/ScrollableContainer';
import { ConvertDateTimeService } from '../../../utils/ConvertDateTimeService';

const TermsOfServicePage: FC<TermsOfServiceScreenNavProps> = ({
    navigation,
}) => {
    const backButtonOnPressHandler = () => {
        navigation.navigate(PageName.SETTINGS_SCREEN);
    };

    const { termOfService } = useAppSelector(state => state.legalDocument);
    const agreedToSDate = useAppSelector(state => state.user.agreedToSAt);
    const { setLoading } = loadingActions;

    const dispatch = useAppDispatch();

    const {
        status: getDocumentStatus,
        data: getDocumentData,
        error: getDocumentError,
    } = useGetDocumentQuery(
        {
            documentName: LegalDocumentName.TERMS_OF_SERVICE,
        },
        {
            skip: termOfService !== null,
        },
    );

    // Handle Error
    useEffect(() => {
        if (getDocumentStatus === QueryStatus.rejected && getDocumentError) {
            const errorMessage = RtkErrorHandler.getMessage(getDocumentError);

            Toast.show(errorMessage, Toast.LONG);
        }
    }, [getDocumentStatus, getDocumentData, getDocumentError, termOfService]);

    // Handle Loading
    useEffect(() => {
        const isLoading = getDocumentStatus === QueryStatus.pending;

        dispatch(setLoading(isLoading));
    }, [getDocumentStatus, dispatch, setLoading]);

    const getAgreedToSDate = new ConvertDateTimeService().convertDateTimeFormat(
        agreedToSDate || '',
    );

    return (
        <View style={styles.rootContainer}>
            <HeaderBar
                title="Terms of Service"
                onBackButtonPress={backButtonOnPressHandler}
                testID="terms-of-service.header-bar-text"
                size="medium"
                showBackButton
            />
            <View style={styles.agreeToSDateTextBox}>
                <NormalText>
                    You have agreed to the Terms of Services on{' '}
                    <BoldText>{getAgreedToSDate}</BoldText>
                </NormalText>
            </View>
            <ScrollableContainer>
                {termOfService && (
                    <View style={styles.legalDocumentContainer}>
                        <TextDocument content={termOfService.content} />
                    </View>
                )}
            </ScrollableContainer>
            <View>
                <Fader position={Fader.position.BOTTOM} tintColor="white" />
            </View>
            <Loading />
        </View>
    );
};

export default TermsOfServicePage;
