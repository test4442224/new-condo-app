import { FC, useEffect } from 'react';
import { PrivacyPolicyScreenNavProps } from '../../../navigation/NavProps';
import PageName from '../../../navigation/PageNameEnum';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import { QueryStatus } from '@reduxjs/toolkit/dist/query';
import React from 'react';
import { Fader, Toast } from 'react-native-ui-lib';
import { LegalDocumentName } from '../../../models/legalDocuments/legalDocumentName.enum';
import { useGetDocumentQuery } from '../../../store/legalDocument/legalDocumentApi';
import { loadingActions } from '../../../store/loading/slice';
import { RtkErrorHandler } from '../../../utils/error/RtkErrorHandler.util';
import { View } from 'react-native';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import Loading from '../../../components/ui/loading/Loading';
import styles from '../LegalDocumentPageStyles';
import TextDocument from '../../../components/quillDocument/TextDocument';
import ScrollableContainer from '../../../components/container/ScrollableContainer';

const PrivacyPolicyPage: FC<PrivacyPolicyScreenNavProps> = ({ navigation }) => {
    const backButtonOnPressHandler = () => {
        navigation.navigate(PageName.SETTINGS_SCREEN);
    };

    const { privacyPolicy } = useAppSelector(state => state.legalDocument);
    const { setLoading } = loadingActions;

    const dispatch = useAppDispatch();

    const {
        status: getDocumentStatus,
        data: getDocumentData,
        error: getDocumentError,
    } = useGetDocumentQuery(
        {
            documentName: LegalDocumentName.PRIVACY_POLICY,
        },
        {
            skip: privacyPolicy !== null,
        },
    );

    // Handle Error
    useEffect(() => {
        if (getDocumentStatus === QueryStatus.rejected && getDocumentError) {
            const errorMessage = RtkErrorHandler.getMessage(getDocumentError);

            Toast.show(errorMessage, Toast.LONG);
        }
    }, [getDocumentStatus, getDocumentData, getDocumentError, privacyPolicy]);

    // Handle Loading
    useEffect(() => {
        const isLoading = getDocumentStatus === QueryStatus.pending;

        dispatch(setLoading(isLoading));
    }, [getDocumentStatus, dispatch, setLoading]);

    return (
        <View style={styles.rootContainer}>
            <HeaderBar
                title="Privacy Policy"
                onBackButtonPress={backButtonOnPressHandler}
                testID="privacy-policy.header-bar-text"
                size="medium"
                showBackButton
            />
            <ScrollableContainer>
                {privacyPolicy && (
                    <View style={styles.legalDocumentContainer}>
                        <TextDocument content={privacyPolicy.content} />
                    </View>
                )}
            </ScrollableContainer>
            <View>
                <Fader position={Fader.position.BOTTOM} tintColor="white" />
            </View>
            <Loading />
        </View>
    );
};

export default PrivacyPolicyPage;
