import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { RouteProp } from '@react-navigation/native';
import { fireEvent, waitFor } from '@testing-library/react-native';
import React from 'react';
import VendorHomePage from '../VendorHomePage';
import PageName from '../../../navigation/PageNameEnum';
import NavParamList from '../../../navigation/NavParmList';
import { renderWithProviders } from '../../../test/utils';
import { DefectStatus } from '../../../models/defect/DefectStatusHistory';

jest.mock('@react-navigation/native', () => {
    return {
        useNavigation: jest.fn(),
        useFocusEffect: jest.fn(),
    };
});

jest.mock('../../../utils/hooks/useRtkResponse.ts', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        showToast: false,
    })),
}));

jest.mock('../../../navigation/NavProps', () => ({}));

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.VENDOR_HOME_PAGE>
> = {
    navigate: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.VENDOR_HOME_PAGE> = {
    key: 'VendorHomePageTestKey',
    name: PageName.VENDOR_HOME_PAGE,
};


const component = (
    <VendorHomePage
        navigation={
            mockNavigation as NativeStackNavigationProp<
                NavParamList,
                PageName.VENDOR_HOME_PAGE
            >
        }
        route={mockRoute}
    />
);
const mockDefect = {condoDefectPriorities: [],
    defectArray: [{
        attachments: [],
        condoId: 1,
        defectReferenceNo: "D-240116-v8",
        description: "Lobby light at block 6 has blown. Need to replace.",
        dueDate: "2024-04-12T08:00:00.000Z",
        id: 1,
        latestStatus: DefectStatus.IN_PROGRESS,
        name: "Replace Light",
        priorityId: 1,
        reporterId: 5,
        vendorCompanyId: 1, 
        vendorEmployeeId: 1,
        statusHistories: [],
        createdAt: new Date(),
    },
    {
        attachments: [],
        condoId: 1,
        defectReferenceNo: "D-2240116-v9",
        description: "Lift takes too long to come",
        dueDate: "2024-04-12T08:00:00.000Z",
        id: 2,
        latestStatus: DefectStatus.COMPLETED,
        name: "Elevator spoilt",
        priorityId: 1,
        reporterId: 5,
        vendorCompanyId: 1, 
        vendorEmployeeId: 1,
        statusHistories: [],
        createdAt: new Date(),
    }],
        selectedDefect: null,
    }

describe('Vendor Home Page', () => {
    jest.useFakeTimers();
    it('should render correctly', async () => {
        const { getByTestId } = renderWithProviders(
            component, {preloadedState: {loading: {isLoading: false}, defect: mockDefect}}
        );
        const activeButton = getByTestId(
            'vendor-manage-defects.active-defects-tab',
        );
        const pastButton = getByTestId(
            'vendor-manage-defects.past-defects-tab',
        );
        const header = getByTestId('vendor-manage-defects.header-bar-text')
        const defectList = getByTestId('vendor-defect-list');
        expect(defectList).toBeTruthy();
        expect(activeButton).toBeTruthy();
        expect(pastButton).toBeTruthy();
        expect(header).toBeTruthy();
    });

    it('should only display in progress and unassigned defects in Active tab', async () => {
        const { getByTestId, getByText } = renderWithProviders(
            component, {preloadedState: {loading: {isLoading: false}, defect: mockDefect}}
        );
        const activeButton = getByTestId(
            'vendor-manage-defects.active-defects-tab',
        );
        await waitFor(() => fireEvent.press(activeButton));
        const activeDefectRefNo = mockDefect.defectArray[0].defectReferenceNo;
        const activeDefectName = mockDefect.defectArray[0].name;
        expect(getByText(activeDefectRefNo)).toBeTruthy();
        expect(getByText(activeDefectName)).toBeTruthy();
    });

    it('should only display completed and cancelled defects in Past tab', async () => {
        const { getByTestId, getByText } = renderWithProviders(
            component, {preloadedState: {loading: {isLoading: false}, defect: mockDefect}}
        );
        const pastButton = getByTestId(
            'vendor-manage-defects.past-defects-tab',
        );
        await waitFor(() => fireEvent.press(pastButton));
        const pastDefectRefNo = mockDefect.defectArray[1].defectReferenceNo;
        const pastDefectName = mockDefect.defectArray[1].name;
        expect(getByText(pastDefectRefNo)).toBeTruthy();
        expect(getByText(pastDefectName)).toBeTruthy();
    });
});