import React, { FC, useState } from 'react';
import { View } from 'react-native';
import { NoticeScreenNavProps } from '../../navigation/NavProps';
import HeaderBar from '../../components/headerbar/HeaderBar';
import PageName from '../../navigation/PageNameEnum';
import styles from './NoticePageStyles';
import TabButton from '../../components/ui/buttons/TabButton';
import { useAppDispatch, useAppSelector } from '../../store/hook';
import {
    NoticeAttributes,
    NoticeType,
    noticeAction,
} from '../../store/notice/slice';
import NoticeList from '../../components/lists/noticeList/NoticeList';
import { useLazyGetAllNoticesByNoticeTypeQuery } from '../../store/notice/api';
import Loading from '../../components/ui/loading/Loading';
import useRtkResponse from '../../utils/hooks/useRtkResponse';

const NoticePage: FC<NoticeScreenNavProps> = ({ navigation }) => {
    const dispatch = useAppDispatch();
    const notices = useAppSelector(state => state.notice);
    const { isLoading } = useAppSelector(state => state.loading);

    const [isAnnouncementsTabActive, setAnnouncementsTabActive] =
        useState<boolean>(true);

    const [
        getAllNotices,
        { isFetching: isFetchingNotices, error: getAllNoticesError },
    ] = useLazyGetAllNoticesByNoticeTypeQuery();

    useRtkResponse(isFetchingNotices, getAllNoticesError);

    const onPressAnnouncementTabHandler = async () => {
        if (isAnnouncementsTabActive) return;
        await getAllNotices({ noticeType: NoticeType.ANNOUNCEMENT });
        setAnnouncementsTabActive(true);
    };

    const onPressEventsTabHandler = async () => {
        if (!isAnnouncementsTabActive) return;
        await getAllNotices({ noticeType: NoticeType.EVENT });
        setAnnouncementsTabActive(false);
    };

    const onBackButtonPressHandler = () => {
        navigation.navigate(PageName.HOME_PAGE_SCREEN);
        setAnnouncementsTabActive(true);
    };

    const onNoticeCardPressHandler = (notice: NoticeAttributes) => {
        dispatch(noticeAction.selectNoticeForDetails(notice));

        navigation.navigate(PageName.NOTICE_DETAILS_SCREEN, {
            noticeTitle: notice.noticeType,
        });
    };

    return (
        <View style={styles.rootContainer}>
            <View style={styles.headersContainer}>
                <View style={styles.headerBarContainer}>
                    <HeaderBar
                        title="Notice Board"
                        onBackButtonPress={onBackButtonPressHandler}
                        testID="notice-board.header-bar-text"
                        showBackButton={true}
                        size="xSmall"
                    />
                </View>
            </View>
            <View style={styles.buttonsContainer}>
                <View
                    style={
                        isAnnouncementsTabActive
                            ? styles.pressed
                            : styles.buttonContainer
                    }>
                    <TabButton
                        testID="manage-bookings.announcement-booking-tab"
                        onPress={onPressAnnouncementTabHandler}
                        isActive={isAnnouncementsTabActive}>
                        Announcements
                    </TabButton>
                </View>
                <View
                    style={
                        !isAnnouncementsTabActive
                            ? styles.pressed
                            : styles.buttonContainer
                    }>
                    <TabButton
                        testID="manage-bookings.event-booking-tab"
                        onPress={onPressEventsTabHandler}
                        isActive={!isAnnouncementsTabActive}>
                        Events
                    </TabButton>
                </View>
            </View>
            <NoticeList
                notices={notices.noticesArray}
                onPressed={notice => onNoticeCardPressHandler(notice)}
                noNoticeLabel={
                    isAnnouncementsTabActive
                        ? 'You have no announcements at the moment.'
                        : 'You have no events at the moment.'
                }
            />
            {isLoading && <Loading />}
        </View>
    );
};

export default NoticePage;
