import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import NavParamList from '../../navigation/NavParmList';
import PageName from '../../navigation/PageNameEnum';
import { RouteProp } from '@react-navigation/native';
import { cleanup } from '@testing-library/react-native';
import NoticeDetail from './NoticeDetail';
import React from 'react';
import { http, HttpResponse } from 'msw';
import { setupServer } from 'msw/native';
import { renderWithProviders } from '../../test/utils';
import * as ReactNavigation from '@react-navigation/native';
import { NoticeType } from '../../store/notice/slice';

jest.mock('@react-navigation/native', () => {
    return {
        __esModule: true,
        ...jest.requireActual('@react-navigation/native'),
        useFocusEffect: jest.fn(),
    };
});

jest.spyOn(ReactNavigation, 'useNavigation').mockImplementation(() => {});

jest.mock('../../utils/hooks/useRtkResponse.ts', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        showToast: false,
    })),
}));

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));
jest.mock('../../store/hook.ts', () => {
    return {
        useAppSelector: jest.fn(() => ({
            selectedNotice: mockNotice,
        })),
    };
});
const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.DEFECT_DETAILS_SCREEN>
> = {
    navigate: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.NOTICE_DETAILS_SCREEN> = {
    key: 'NoticeDetailsKey',
    name: PageName.NOTICE_DETAILS_SCREEN,
    params: {
        noticeTitle: NoticeType.ANNOUNCEMENT,
    },
};

const component = (
    <NoticeDetail
        navigation={
            mockNavigation as NativeStackNavigationProp<
                NavParamList,
                PageName.NOTICE_DETAILS_SCREEN
            >
        }
        route={mockRoute}
    />
);

const handlers = [
    http.get('http://localhost:3001/notice', async () => {
        return HttpResponse.json({
            data: mockNotice,
        });
    }),
];

const mockNotice = {
    id: 1,
    noticeType: NoticeType.ANNOUNCEMENT,
    condoId: 1,
    description: `{"ops":[{"insert":"New Badminton hall has been built for residents of The Trevose! Come make a booking now!\\n"}]}`,
    imageUrl: 'facility/badmintonhall.png',
    title: 'New Badminton Hall',
    createdBy: 1,
    isArchived: false,
    orderNumber: 1,
    isOnBanner: true,
    startDateTime: new Date(),
    endDateTime: new Date(),
    createdAt: new Date(),
};
const server = setupServer(...handlers);

describe('Notice Details component', () => {
    beforeAll(() => {
        server.listen();
        server.events.on('request:start', ({ request }) => {
            console.log('MSW intercepted:', request.method, request.url);
        });
    });

    beforeEach(() => {
        jest.clearAllMocks();
        cleanup();
    });
    afterEach(() => {
        server.resetHandlers();
    });

    afterAll(() => server.close());

    jest.useFakeTimers();
    it('render everything for notices', async () => {
        const { findByTestId } = renderWithProviders(component, {
            preloadedState: {
                loading: { isLoading: false },
                notice: {
                    noticesArray: [],
                    isLoading: false,
                    selectedNotice: mockNotice,
                    carouselArray: [],
                },
            },
        });
        expect(await findByTestId('notice-details.view-title')).toBeTruthy();
        expect(
            await findByTestId('notice-details.view-description'),
        ).toBeTruthy();

        expect(
            await findByTestId('notice-details.view-start-date'),
        ).toBeTruthy();
        expect(await findByTestId('notice-details.view-end-date')).toBeTruthy();
        expect(
            await findByTestId('notice-details.view-created-date'),
        ).toBeTruthy();
    });
});
