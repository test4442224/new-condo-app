import { StyleSheet } from 'react-native';
import { BorderRadiuses, Spacings, Typography } from 'react-native-ui-lib';
import Colors from '../../Constants/Colors';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
    },
    generalContainer: {
        paddingHorizontal: Spacings.s4,
    },
    boldText: {
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    titleText: {
        marginVertical: Spacings.s2,
        ...Typography.text60,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    dateContainer: {
        marginTop: Spacings.s4,
        textAlign: 'flex-start',
    },
    descriptionContainer: {
        marginTop: Spacings.s4,
    },
    footerContainer: {
        marginTop: Spacings.s10,
    },
    footerColor: {
        color: Colors.$backgroundPrimary,
    },
    imageContainer: {
        width: '100%',
        height: 250,
        justifyContent: 'center',
        paddingHorizontal: Spacings.s3,
    },
    image: {
        width: '100%',
        height: 200,
        borderRadius: BorderRadiuses.br60,
        backgroundColor: Colors.$backgroundDefault,
    },
    rowContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    labelContainer: {
        width: '20%',
    },
});
