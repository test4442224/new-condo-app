import React, { FC } from 'react';
import { View, useWindowDimensions } from 'react-native';
import { NoticeDetailScreenNavProps } from '../../navigation/NavProps';
import styles from './NoticeDetailStyles';
import HeaderBar from '../../components/headerbar/HeaderBar';
import { NoticeType } from '../../store/notice/slice';
import { useAppSelector } from '../../store/hook';
import ScrollableContainer from '../../components/container/ScrollableContainer';
import NormalText from '../../components/ui/titles/NormalText';
import { format } from 'date-fns';
import SubHeaderText from '../../components/ui/titles/SubHeaderText';
import RenderHtml from 'react-native-render-html';
import { QuillDeltaToHtmlConverter } from 'quill-delta-to-html';
import Delta from 'quill-delta';
import sanitizeHtml from 'sanitize-html';
import { Image } from 'react-native-ui-lib';
import { getImageUrl } from '../../Constants/api';

const NoticeDetail: FC<NoticeDetailScreenNavProps> = ({
    navigation,
    route,
}) => {
    const { selectedNotice } = useAppSelector(state => state.notice);
    const imagePath: string | null = selectedNotice?.imageUrl || null;
    const descriptionDelta = JSON.parse(selectedNotice!.description) as Delta;
    const descriptionHtml = new QuillDeltaToHtmlConverter(
        descriptionDelta.ops,
        {},
    ).convert();
    const sanitizedDescriptionHtml = sanitizeHtml(descriptionHtml);
    const { width } = useWindowDimensions();

    const getTitle = () => {
        return new Map([
            [NoticeType.ANNOUNCEMENT, 'Announcement Details'],
            [NoticeType.EVENT, 'Event Details'],
        ]).get(route.params.noticeTitle);
    };

    return (
        <View style={styles.rootContainer}>
            <HeaderBar
                title={`${getTitle()}`}
                onBackButtonPress={() => navigation.goBack()}
                testID="notice-board.header-bar-text"
                showBackButton={true}
                size="xSmall"
            />
            <ScrollableContainer>
                {imagePath !== null && (
                    <View style={styles.imageContainer}>
                        <Image
                            source={{
                                uri: getImageUrl(imagePath!),
                            }}
                            errorSource={require('../../assets/logo/condo-app-logo.jpg')}
                            style={styles.image}
                        />
                    </View>
                )}
                <View style={styles.generalContainer}>
                    <SubHeaderText
                        accessibilityLabel="notice-details.view-title"
                        testID="notice-details.view-title"
                        underline={false}
                        style={styles.titleText}
                        center={false}>
                        {selectedNotice!.title}
                    </SubHeaderText>

                    {selectedNotice!.startDateTime &&
                        selectedNotice!.endDateTime && (
                            <View style={styles.dateContainer}>
                                <View style={styles.rowContainer}>
                                    <View style={styles.labelContainer}>
                                        <NormalText style={styles.boldText}>
                                            Starts:{' '}
                                        </NormalText>
                                    </View>
                                    <View
                                        accessibilityLabel="notice-details.view-start-date"
                                        testID="notice-details.view-start-date">
                                        <NormalText>
                                            {format(
                                                new Date(
                                                    selectedNotice!.startDateTime!,
                                                ),
                                                'd MMM yyyy, h:mm a',
                                            )}
                                        </NormalText>
                                    </View>
                                </View>
                                <View style={styles.rowContainer}>
                                    <View style={styles.labelContainer}>
                                        <NormalText style={styles.boldText}>
                                            Ends:{' '}
                                        </NormalText>
                                    </View>
                                    <View
                                        accessibilityLabel="notice-details.view-end-date"
                                        testID="notice-details.view-end-date">
                                        <NormalText>
                                            {format(
                                                new Date(
                                                    selectedNotice!.endDateTime!,
                                                ),
                                                'd MMM yyyy, h:mm a',
                                            )}
                                        </NormalText>
                                    </View>
                                </View>
                            </View>
                        )}
                    <View
                        style={styles.descriptionContainer}
                        accessibilityLabel="notice-details.view-description"
                        testID="notice-details.view-description">
                        <RenderHtml
                            source={{ html: sanitizedDescriptionHtml }}
                            contentWidth={width}
                        />
                    </View>
                    <View
                        style={styles.footerContainer}
                        accessibilityLabel="notice-details.view-created-date"
                        testID="notice-details.view-created-date">
                        <NormalText style={styles.footerColor}>
                            Posted on:{' '}
                            {format(
                                new Date(selectedNotice!.createdAt!),
                                'eeee, d MMM yyyy',
                            )}
                        </NormalText>
                    </View>
                </View>
            </ScrollableContainer>
        </View>
    );
};

export default NoticeDetail;
