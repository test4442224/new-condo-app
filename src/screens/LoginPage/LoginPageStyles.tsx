import { StyleSheet } from 'react-native';
import Colors from '../../Constants/Colors';
import { BorderRadiuses, Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: Colors.$backgroundDefault,
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '60%',
        height: '30%',
        marginTop: Spacings.s1 * 15,
    },
    logo: {
        width: '80%',
    },
    generalContainer: {
        width: '100%',
        paddingHorizontal: Spacings.s4,
        alignContent: 'center',
        justifyContent: 'center',
    },
    inputBox: {
        flex: 1,
        borderWidth: 1,
        borderRadius: BorderRadiuses.br30,
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
    passwordContainer: {
        marginTop: Spacings.s5,
        marginBottom: Spacings.s1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    eyeContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    eyeIcon: {
        position: 'absolute',
        right: Spacings.s3,
    },
    forgetPasswordContainer: {
        display: 'flex',
        alignSelf: 'flex-end',
        marginTop: Spacings.s1,
        marginBottom: Spacings.s5,
    },
    forgetPasswordText: {
        textDecorationLine: 'underline',
    },
    buttonOuterContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        gap: Spacings.s3,
    },
    buttonContainer: {
        flex: 1,
        marginHorizontal: Spacings.s4,
        marginVertical: Spacings.s3,
    },
});
