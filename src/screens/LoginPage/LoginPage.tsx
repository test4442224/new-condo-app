import React, { FC, useEffect, useState } from 'react';
import {
    Image,
    Keyboard,
    Platform,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
} from 'react-native';
import styles from './LoginPageStyles';
import PrimaryButton from '../../components/ui/buttons/PrimaryButton';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { useAppDispatch } from '../../store/hook';
import { userActions } from '../../store/user/UserActions';
import Toast from 'react-native-simple-toast';
import { LoginNavProps } from '../../navigation/NavProps';
import PageName from '../../navigation/PageNameEnum';
import ScrollableContainer from '../../components/container/ScrollableContainer';
import { useLoginMutation } from '../../store/user/userApi';
import { QueryStatus } from '@reduxjs/toolkit/dist/query';
import { RtkErrorHandler } from '../../utils/error/RtkErrorHandler.util';
import { loadingActions } from '../../store/loading/slice';
import Loading from '../../components/ui/loading/Loading';
import messaging from '@react-native-firebase/messaging';
import Config from 'react-native-config';
import { HitSlopArea } from './HitSlopArea';

const LoginPage: FC<LoginNavProps> = ({ navigation }) => {
    const dispatch = useAppDispatch();

    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [isHidden, setIsHidden] = useState(true);
    const { setLoading } = loadingActions;
    const [
        login,
        {
            status: loginStatus,
            data: loginData,
            error: loginError,
            reset: resetLogin,
            isLoading: isLoginLoading,
        },
    ] = useLoginMutation({
        fixedCacheKey: 'loginResult',
    });

    const toggleContentVisibility = () => {
        Keyboard.dismiss();
        setIsHidden(prev => !prev);
    };

    const onForgetPasswordHandler = () => {
        setEmail('');
        setPassword('');
        navigation.navigate(PageName.FORGET_PASSWORD_SCREEN);
    };

    const onLoginHandler = async () => {
        let deviceToken;
        if (!email || !password) {
            Toast.show('Please enter your email and password.', Toast.LONG);
            return;
        }

        if (Platform.OS === 'ios') {
            await messaging().getAPNSToken();
        }

        dispatch(setLoading(true));

        if (Config.ENV === 'test') {
            deviceToken = 'string';
        } else {
            deviceToken = await messaging().getToken();
        }

        login({ email, password, deviceToken });
    };

    // Handle login success and fail cases
    useEffect(() => {
        // if it is a newly created user, the token will be null so user will be redirected to reset password
        if (
            loginStatus === QueryStatus.fulfilled &&
            loginData &&
            loginData.token === null
        ) {
            dispatch(userActions.setEmail(email));
            setEmail('');
            setPassword('');
            navigation.navigate(PageName.RESET_PASSWORD_SCREEN);
            resetLogin();
            return;
        }

        if (loginStatus === QueryStatus.rejected && loginError) {
            const errorMessage = RtkErrorHandler.getMessage(loginError);

            if (
                errorMessage === 'Invalid Credentials' ||
                errorMessage === RtkErrorHandler.UNAUTHORIZED_MESSAGE
            ) {
                Toast.show(
                    'Email or Password is incorrect, please try again.',
                    Toast.LONG,
                );
                resetLogin();
                return;
            }

            Toast.show(errorMessage, Toast.LONG);

            resetLogin();
            return;
        }
    }, [
        loginStatus,
        loginData,
        loginError,
        email,
        dispatch,
        resetLogin,
        navigation,
    ]);

    // Handle laoding
    useEffect(() => {
        const isLoading = isLoginLoading;

        dispatch(setLoading(isLoading));
    }, [isLoginLoading, dispatch, setLoading]);

    return (
        <>
            <ScrollableContainer>
                <View style={styles.rootContainer}>
                    <View style={styles.logoContainer}>
                        <Image
                            testID={'login.logo-image'}
                            style={styles.logo}
                            resizeMode={'contain'}
                            source={require('../../assets/logo/condo-app-logo.jpg')}
                        />
                    </View>
                    <View style={styles.generalContainer}>
                        <TextInput
                            testID={'login.email-input'}
                            placeholder={'Email'}
                            onChangeText={text => setEmail(text.toLowerCase())}
                            value={email}
                            style={styles.inputBox}
                            autoCapitalize={'none'}
                            textContentType="oneTimeCode"
                            accessibilityLabel="email-input"
                        />
                        <View style={styles.passwordContainer}>
                            <TextInput
                                testID={'login.password-input'}
                                placeholder={'Password'}
                                onChangeText={text => setPassword(text)}
                                value={password}
                                style={styles.inputBox}
                                secureTextEntry={isHidden}
                                textContentType="oneTimeCode"
                                accessibilityLabel="password-input"
                            />
                            <TouchableWithoutFeedback
                                style={styles.eyeContainer}
                                hitSlop={HitSlopArea}
                                onPress={toggleContentVisibility}>
                                <View
                                    testID={'login.show-password-btn'}
                                    style={styles.eyeIcon}>
                                    {isHidden ? (
                                        <FontAwesomeIcon icon={faEye} />
                                    ) : (
                                        <FontAwesomeIcon icon={faEyeSlash} />
                                    )}
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style={styles.generalContainer}>
                        <TouchableOpacity
                            style={styles.forgetPasswordContainer}
                            testID={'login.forget-password-btn'}
                            onPress={onForgetPasswordHandler}>
                            <View>
                                <Text style={styles.forgetPasswordText}>
                                    Forgot Password?
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.buttonOuterContainer}>
                        <View style={styles.buttonContainer}>
                            <PrimaryButton
                                accessibilityLabel="login-button"
                                label="Login"
                                testID={'login.login-btn'}
                                onPress={onLoginHandler}
                            />
                        </View>
                    </View>
                </View>
            </ScrollableContainer>
            <Loading />
        </>
    );
};

export default LoginPage;
