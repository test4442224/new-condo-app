// The touchable area for the icons:
export const HitSlopArea = {
    top: 50,
    bottom: 50,
    left: 50,
    right: 15,
};
