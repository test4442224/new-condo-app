import React from 'react';
import LoginPage from './LoginPage';
import renderer from 'react-test-renderer';
import { fireEvent, render } from '@testing-library/react-native';

jest.mock('../../store/user/UserActions.ts', () => {
    return {
        login: jest.fn(),
    };
});

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

jest.mock('@react-native-firebase/messaging', () => ({}));

jest.mock('react-native-simple-toast', () => {});

jest.mock('../../store/hook.ts', () => {
    return {
        useAppSelector: jest.fn(),
        useAppDispatch: jest.fn(),
    };
});

jest.mock('@react-navigation/native', () => {
    return {
        useNavigation: jest.fn(),
    };
});

const mockNavigate = jest.fn();
xdescribe('Loginpage Component', () => {
    const navigation = {
        navigate: mockNavigate,
    };
    it('should render the login page correctly', () => {
        const props: any = {
            navigation,
        };
        const { useAppSelector, useAppDispatch } = require('../../store/hook');

        const mockDispatch = jest.fn();

        useAppSelector.mockReturnValue({
            currentUserRole: 'USER',
        });

        useAppDispatch.mockReturnValueOnce(mockDispatch);
        const loginPage = renderer.create(<LoginPage {...props} />).toJSON();

        expect(loginPage).toMatchSnapshot();
    });

    it('should dispatch a login call when login button is pressed', async () => {
        const props: any = {
            navigation,
        };
        const { useAppDispatch } = require('../../store/hook');
        const { login } = require('../../store/user/UserActions');

        const mockUnwrap = jest.fn();

        const mockDispatch = jest.fn(() => {
            return {
                unwrap: mockUnwrap,
                then: (callback: any) => {
                    callback({ email, password });
                    return { catch: jest.fn() };
                },
            };
        });
        useAppDispatch.mockReturnValue(mockDispatch);

        const email = 'test@email.com';
        const password = 'password';
        const isPasswordReset = true;

        login.mockResolvedValue({
            respons: { email, password, isPasswordReset },
        });

        mockUnwrap.mockResolvedValueOnce({
            response: { email, password, isPasswordReset },
        });

        const { getByTestId } = render(<LoginPage {...props} />);
        fireEvent.changeText(getByTestId('login.email-input'), email);
        fireEvent.changeText(getByTestId('login.password-input'), password);
        fireEvent.press(getByTestId('login.login-btn'));

        expect(login).toBeCalledWith({ email, password });
        expect(mockDispatch).toBeCalledWith(login({ email, password }));
    });
});
