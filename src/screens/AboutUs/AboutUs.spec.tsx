import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import NavParamList from '../../navigation/NavParmList';
import PageName from '../../navigation/PageNameEnum';
import { RouteProp } from '@react-navigation/native';
import { cleanup, waitFor } from '@testing-library/react-native';
import React from 'react';
import { http, HttpResponse } from 'msw';
import { setupServer } from 'msw/native';
import { renderWithProviders } from '../../test/utils';
import AboutUsPage from './AboutUs';
import { ICondo } from '../../../src/models/condo/condo.model';

jest.mock('../../utils/hooks/useRtkResponse.ts', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        showToast: false,
    })),
}));

const mockCondo: ICondo = {
    name: 'Mocked Condo Name',
    uen: 'Mocked UEN',
    aboutUs: JSON.stringify('Mocked about us content'),
    aboutUsImagePath: '/mocked/image/path.jpg',
};

jest.mock('@react-navigation/native', () => {
    return {
        useNavigation: jest.fn(),
        useFocusEffect: jest.fn(),
    };
});

jest.mock('../../navigation/NavProps', () => ({}));

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.ABOUT_US_SCREEN>
> = {
    navigate: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.ABOUT_US_SCREEN> = {
    key: 'AboutUsPageTestKey',
    name: PageName.ABOUT_US_SCREEN,
};

const handlers = [
    http.get('http://localhost:3001/condo', async () => {
        return HttpResponse.json({});
    }),
];

const server = setupServer(...handlers);

describe('About Us component', () => {
    beforeAll(() => {
        server.listen();
        server.events.on('request:start', ({ request }) => {
            console.log('MSW intercepted:', request.method, request.url);
        });
    });

    beforeEach(() => {
        jest.clearAllMocks();
        cleanup();
    });

    afterEach(() => {
        server.resetHandlers();
    });

    afterAll(() => server.close());

    it('renders correctly', async () => {
        const { getByTestId, queryByTestId } = renderWithProviders(
            <AboutUsPage
                navigation={
                    mockNavigation as NativeStackNavigationProp<
                        NavParamList,
                        PageName.ABOUT_US_SCREEN
                    >
                }
                route={mockRoute}
            />,
            {
                // Provide a custom Redux store with the mocked condo state
                preloadedState: {
                    condo: { condo: mockCondo },
                    // Add other preloaded states as needed
                },
            },
        );
        await waitFor(() => {});
        // Assert that key elements are rendered
        expect(getByTestId('about-us-text')).toBeTruthy();
        expect(getByTestId('about-us.header-bar-text')).toBeTruthy();
        expect(queryByTestId('about-us-image')).toBeTruthy();
        // Add more assertions as needed
    });
});
