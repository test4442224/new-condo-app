import { FC } from 'react';
import { AboutUsScreenNavProps } from '../../navigation/NavProps';
import PageName from '../../navigation/PageNameEnum';
import HeaderBar from '../../components/headerbar/HeaderBar';
import { Fader, Image, View } from 'react-native-ui-lib';
import React from 'react';
import styles from './AboutUsStyles';
import { useAppSelector } from '../../store/hook';
import TextDocument from '../../components/quillDocument/TextDocument';
import ScrollableContainer from '../../components/container/ScrollableContainer';
import { getImageUrl } from '../../Constants/api';
import { useGetCondoDetailsQuery } from '../../store/condo/api';
import useRtkResponse from '../../utils/hooks/useRtkResponse';
import Loading from '../../components/ui/loading/Loading';

const AboutUsPage: FC<AboutUsScreenNavProps> = ({ navigation }) => {
    const backButtonOnPressHandler = () => {
        navigation.navigate(PageName.HOME_PAGE_SCREEN);
    };
    const { condo } = useAppSelector(state => state.condo);

    const { isFetching: isGetCondoFetching, error: getCondoError } =
        useGetCondoDetailsQuery(undefined, {
            skip: condo !== null,
        });

    // Handle Error
    useRtkResponse(isGetCondoFetching, getCondoError);

    const imagePath: string | null = condo?.aboutUsImagePath || null;

    return (
        <View style={styles.rootContainer}>
            <HeaderBar
                title="About"
                onBackButtonPress={backButtonOnPressHandler}
                testID="about-us.header-bar-text"
                size="medium"
                showBackButton
            />
            <ScrollableContainer>
                {imagePath !== null && (
                    <Image
                        source={{ uri: getImageUrl(imagePath) }}
                        style={styles.image}
                        testID="about-us-image"
                    />
                )}
                {condo?.aboutUs && (
                    <View
                        style={styles.aboutUsContainer}
                        testID="about-us-text">
                        <TextDocument content={condo.aboutUs} />
                    </View>
                )}
            </ScrollableContainer>
            <View>
                <Fader position={Fader.position.BOTTOM} tintColor="white" />
            </View>
            <Loading />
        </View>
    );
};

export default AboutUsPage;
