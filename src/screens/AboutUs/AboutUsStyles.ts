import { StyleSheet } from 'react-native';
import Colors from '../../Constants/Colors';
import { Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
    },
    aboutUsContainer: {
        marginBottom: Spacings.s5,
        paddingHorizontal: Spacings.s5,
    },
    image: {
        width: '100%',
        height: 240,
        marginBottom: Spacings.s2,
    },
});
