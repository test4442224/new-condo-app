import { StyleSheet } from 'react-native';
import { Spacings, Typography } from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    redText: {
        color: Colors.$textError,
    },
    rootContainer: {
        backgroundColor: Colors.$backgroundDefault,
        height: '100%',
    },
    pageContentContainer: {
        paddingTop: Spacings.s5,
        paddingHorizontal: Spacings.s5,
    },
    description: {
        marginTop: Spacings.s2,
    },
    buttonContainer: {
        marginTop: Spacings.s7,
        marginBottom: Spacings.s4,
    },
    noFacilityNoticeContainer: {
        height: '90%',
        justifyContent: 'center',
    },
    noFacilityNotice: {
        ...Typography.text65,
    },
    generalContainer: {
        marginBottom: Spacings.s5,
    },
    rowContainer: {
        flexDirection: 'row',
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s3,
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    boldText: {
        fontWeight: WEIGHT_TYPES.BOLD,
    },
});
