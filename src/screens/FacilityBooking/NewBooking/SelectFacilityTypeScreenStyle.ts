import { StyleSheet } from 'react-native';
import { Spacings, Typography } from 'react-native-ui-lib';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        backgroundColor: Colors.$backgroundDefault,
        height: '100%',
    },
    listContainer: {
        marginBottom: Spacings.s1 * 20,
        marginVertical: Spacings.s4,
        marginHorizontal: Spacings.s2,
    },
    nofacilityNoticeContainer: {
        height: '90%',
        justifyContent: 'center',
    },
    noFacilityNotice: {
        ...Typography.text65,
    },
});
