import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import NavParamList from '../../../navigation/NavParmList';
import PageName from '../../../navigation/PageNameEnum';
import { NavigationContainer, RouteProp } from '@react-navigation/native';
import { fireEvent, render, waitFor } from '@testing-library/react-native';
import SelectFacilityTypeScreen from './SelectFacilityTypeScreen';
import React from 'react';
import UserRole from '../../../models/userRole/UserRole';
import { RoleType } from '../../../models/userRole/RoleType';
import newBookingSlice from '../../../store/newBooking/slice';
import IFacilityAvailableTimeSlot from '../../../models/facility/FacilityAvailableTimeSlot';
import { IFacilityType } from '../../../models/facility/FacilityTypeModel';

jest.mock('../../../navigation/NavProps', () => ({}));

jest.mock('@fortawesome/react-native-fontawesome', () => {
    return {
        FontAwesomeIcon: '',
    };
});

const mockedUseAppSelector = jest.spyOn(
    require('../../../store/hook'),
    'useAppSelector',
);
const mockedUseAppDispatch = jest.spyOn(
    require('../../../store/hook'),
    'useAppDispatch',
);

// jest.spyOn(
//     require('../../../store/newBooking/thunk'),
//     'fetchFacilityAvailableTimeThunk',
// );
// jest.spyOn(
//     require('../../../store/newBooking/thunk'),
//     'fetchFacilityTypeThunk',
// );

jest.spyOn(newBookingSlice.actions, 'setChosenFacilityType');

jest.spyOn(newBookingSlice.actions, 'resetAvailableFacilityTypes');

const mockedSetChosenDate = jest.spyOn(
    newBookingSlice.actions,
    'setChosenDate',
);

const mockedSetChosenFacility = jest.spyOn(
    newBookingSlice.actions,
    'setChosenFacility',
);

const mockedSetChosenTimeSlot = jest.spyOn(
    newBookingSlice.actions,
    'setChosenTimeSlot',
);

const mockedResetFacilityAvailableTimeSlots = jest.spyOn(
    newBookingSlice.actions,
    'resetFacilityAvailableTimeSlots',
);

const mockedUnwrapFn = jest.fn();

const mockNavigation: Partial<
    NativeStackNavigationProp<
        NavParamList,
        PageName.SELECT_FACILITY_TYPE_SCREEN
    >
> = {
    navigate: jest.fn(),
};

let mockedRoute: RouteProp<NavParamList, PageName.SELECT_FACILITY_TYPE_SCREEN> =
    {
        key: 'SelectFacilityTypeTestKey',
        name: PageName.SELECT_FACILITY_TYPE_SCREEN,
    };

xdescribe('Test SelectFacilityTypeScreen Component', () => {
    const testToken = 'valid token';
    const testTitle = 'valid title';

    const testAvailableTimeSlots: IFacilityAvailableTimeSlot[] = [
        {
            facility: {
                id: 1,
                name: 'test facility',
                facilityTypeId: 1,
                pax: 1,
                location: 'test location',
                tnc: 'test tnc',
                minimumBookingDurationInMinutes: 120,
                openingTime: new Date('1970-01-01T10:00:00Z'),
                closingTime: new Date('1970-01-01T10:00:00Z'),
                depositPercentage: 10,
                isArchived: false,
                description: 'test description',
            },
            availableTimeSlots: [
                {
                    start: '10:00',
                    end: '12:00',
                },
            ],
        },
    ];

    beforeEach(() => {
        jest.clearAllMocks();
        jest.resetAllMocks();

        mockedUseAppDispatch.mockReturnValue(() => {
            return {
                unwrap: mockedUnwrapFn,
                catch: jest.fn(),
                finally: jest.fn(),
            };
        });
    });

    it('should render the SelectFacilityTypeScreen correctly', () => {
        const testSelector = {
            newBooking: {
                title: 'Facilities',
                availableFacilityTypes: [],
                chosenDate: null,
                chosenFacility: null,
                chosenFacilityType: null,
                chosenTimeSlot: null,
                facilityAvailableTimeSlots: [],
            },
            pageHeader: {
                title: testTitle,
            },
            userRole: {
                currentUserRole: {
                    id: 1,
                    userId: 1,
                    condoId: 1,
                    roleType: RoleType.USER,
                } as UserRole,
            },
            user: {
                token: testToken,
            },
            loading: {
                isLoading: false,
            },
        };

        mockedUseAppSelector.mockImplementation((selector: any) => {
            return selector(testSelector);
        });

        mockedUnwrapFn.mockResolvedValue(undefined);

        const selectFacilityTypeScreen = render(
            <NavigationContainer>
                <SelectFacilityTypeScreen
                    navigation={
                        mockNavigation as NativeStackNavigationProp<
                            NavParamList,
                            PageName.SELECT_FACILITY_TYPE_SCREEN
                        >
                    }
                    route={mockedRoute}
                />
            </NavigationContainer>,
        ).toJSON();

        expect(selectFacilityTypeScreen).toMatchSnapshot();
    });

    it('should clear unused data when mounted', () => {
        const testChosenDate = new Date().toDateString();
        const testChosenFacility = testAvailableTimeSlots[0].facility;
        const testChosenTimeSlot =
            testAvailableTimeSlots[0].availableTimeSlots[0];

        const testSelector = {
            newBooking: {
                title: 'Facilities',
                availableFacilityTypes: [],
                chosenDate: testChosenDate,
                chosenFacility: testChosenFacility,
                chosenFacilityType: null,
                chosenTimeSlot: testChosenTimeSlot,
                facilityAvailableTimeSlots: testAvailableTimeSlots,
            },
            pageHeader: {
                title: testTitle,
            },
            userRole: {
                currentUserRole: {
                    id: 1,
                    userId: 1,
                    condoId: 1,
                    roleType: RoleType.USER,
                } as UserRole,
            },
            user: {
                token: testToken,
            },
            loading: {
                isLoading: false,
            },
        };

        mockedUseAppSelector.mockImplementation((selector: any) => {
            return selector(testSelector);
        });

        mockedUnwrapFn.mockResolvedValue(undefined);

        render(
            <NavigationContainer>
                <SelectFacilityTypeScreen
                    navigation={
                        mockNavigation as NativeStackNavigationProp<
                            NavParamList,
                            PageName.SELECT_FACILITY_TYPE_SCREEN
                        >
                    }
                    route={mockedRoute}
                />
            </NavigationContainer>,
        ).toJSON();

        expect(mockedResetFacilityAvailableTimeSlots).toBeCalledWith();
        expect(mockedSetChosenDate).toBeCalledWith(null);
        expect(mockedSetChosenFacility).toBeCalledWith(null);
        expect(mockedSetChosenTimeSlot).toBeCalledWith(null);
    });

    it('should redirect to Manage Booking Screen after pressing the backButton', async () => {
        const testSelector = {
            newBooking: {
                title: 'Facilities',
                availableFacilityTypes: [],
                chosenDate: null,
                chosenFacility: null,
                chosenFacilityType: null,
                chosenTimeSlot: null,
                facilityAvailableTimeSlots: [],
            },
            pageHeader: {
                title: testTitle,
            },
            userRole: {
                currentUserRole: {
                    id: 1,
                    userId: 1,
                    condoId: 1,
                    roleType: RoleType.USER,
                } as UserRole,
            },
            user: {
                token: testToken,
            },
            loading: {
                isLoading: false,
            },
        };

        mockedUseAppSelector.mockImplementation((selector: any) => {
            return selector(testSelector);
        });

        mockedUnwrapFn.mockReturnValue(
            new Promise(resolve => {
                resolve(undefined);
            }),
        );

        try {
            const { getByTestId } = render(
                <NavigationContainer>
                    <SelectFacilityTypeScreen
                        navigation={
                            mockNavigation as NativeStackNavigationProp<
                                NavParamList,
                                PageName.SELECT_FACILITY_TYPE_SCREEN
                            >
                        }
                        route={mockedRoute}
                    />
                </NavigationContainer>,
            );

            const backButton = getByTestId('icon-button');
            expect(backButton).toBeDefined();

            await waitFor(() => fireEvent.press(backButton));

            expect(mockNavigation.navigate).toBeCalled();
            expect(mockNavigation.navigate).toBeCalledWith(
                PageName.MANAGE_BOOKINGS_SCREEN,
            );
        } catch (error) {
            expect(error).toBeUndefined();
        }
    });

    xit('should redirect to Select Date Screen after selecting the facility type', async () => {
        const testFacilityTypeId = 1;

        const testFacilityType: IFacilityType = {
            id: testFacilityTypeId,
            name: 'test facility type',
            condoId: 1,
            imageUrl: 'valid image url',
        };

        const testSelector = {
            newBooking: {
                title: 'Facilities',
                availableFacilityTypes: [testFacilityType],
                chosenDate: null,
                chosenFacility: null,
                chosenFacilityType: null,
                chosenTimeSlot: null,
                facilityAvailableTimeSlots: [],
            },
            pageHeader: {
                title: testTitle,
            },
            userRole: {
                currentUserRole: {
                    id: 1,
                    userId: 1,
                    condoId: 1,
                    roleType: RoleType.USER,
                } as UserRole,
            },
            user: {
                token: testToken,
            },
            loading: {
                isLoading: false,
            },
        };

        mockedUseAppSelector.mockImplementation((selector: any) => {
            return selector(testSelector);
        });

        mockedUnwrapFn.mockReturnValue(
            new Promise(resolve => {
                resolve(undefined);
            }),
        );

        try {
            const { getByTestId } = render(
                <NavigationContainer>
                    <SelectFacilityTypeScreen
                        navigation={
                            mockNavigation as NativeStackNavigationProp<
                                NavParamList,
                                PageName.SELECT_FACILITY_TYPE_SCREEN
                            >
                        }
                        route={mockedRoute}
                    />
                </NavigationContainer>,
            );

            const facilityTypeCard = getByTestId(
                `facilityTypeList-${testFacilityTypeId}`,
            );
            expect(facilityTypeCard).toBeDefined();

            await waitFor(() => fireEvent.press(facilityTypeCard));

            expect(mockNavigation.navigate).toBeCalled();
            expect(mockNavigation.navigate).toBeCalledWith(
                PageName.SELECT_DATE_SCREEN,
            );
        } catch (error) {
            expect(error).toBeUndefined();
        }
    });
});
