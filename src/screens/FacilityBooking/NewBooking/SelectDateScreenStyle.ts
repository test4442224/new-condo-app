import { StyleSheet } from 'react-native';

import Typography, {
    WEIGHT_TYPES,
} from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';
import { Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    rootContainer: {
        height: '100%',
        backgroundColor: Colors.$backgroundDefault,
    },
    contentContainer: {
        margin: Spacings.s4,
    },
    descriptionContainer: {
        marginBottom: Spacings.s4,
    },
    generalContainer: {
        paddingTop: Spacings.s3,
        paddingHorizontal: Spacings.s2,
        marginBottom: Spacings.s2,
    },
    rowContainer: {
        flexDirection: 'row',
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    boldText: {
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    subheaderText: {
        ...Typography.text70,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
});
