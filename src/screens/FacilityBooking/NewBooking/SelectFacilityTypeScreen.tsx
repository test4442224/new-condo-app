import styles from './SelectFacilityTypeScreenStyle';

import React, { FC, useCallback, useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import { View } from 'react-native';
import FacilityTypeList from '../../../components/lists/facilityTypeList/FacilityTypeList';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import { pageHeaderActions } from '../../../store/pageHeader/slice';
import { SelectFacilityTypeNavProps } from '../../../navigation/NavProps';
import PageName from '../../../navigation/PageNameEnum';
import Loading from '../../../components/ui/loading/Loading';
import { newBookingActions } from '../../../store/newBooking/slice';
import { IFacilityType } from '../../../models/facility/FacilityTypeModel';
import NormalText from '../../../components/ui/titles/NormalText';
import { loadingActions } from '../../../store/loading/slice';
import { useFocusEffect } from '@react-navigation/native';
import {
    ISetErrorDialog,
    NewBookingError,
} from '../../../models/errorsType/NewBookingErrorTypes';
import PopModal from '../../../components/modal/PopModal';
import { useGetAllFacilityTypesQuery } from '../../../store/api/facilityTypeApi';
import { QueryStatus } from '@reduxjs/toolkit/dist/query';
import { RtkErrorHandler } from '../../../utils/error/RtkErrorHandler.util';

const SelectFacilityTypeScreen: FC<SelectFacilityTypeNavProps> = ({
    navigation,
}) => {
    const pageTitle = 'Choose Facility Type';

    const { title } = useAppSelector(state => state.pageHeader);
    const {
        availableFacilityTypes,
        chosenDate,
        chosenFacility,
        chosenTimeSlots: chosenTimeSlot,
        facilityAvailableTimeSlots,
    } = useAppSelector(state => state.newBooking);

    const { currentUserRole } = useAppSelector(state => state.userRole);

    const { isLoading: loading } = useAppSelector(state => state.loading);

    const {
        status: getAllFacilityTypesStatus,
        error: getAllFacilityTypesError,
    } = useGetAllFacilityTypesQuery(
        {
            condoId: currentUserRole?.condoId || 0,
        },
        {
            skip: typeof currentUserRole?.condoId !== 'number',
            refetchOnFocus: true,
            refetchOnMountOrArgChange: true,
        },
    );

    const dispatch = useAppDispatch();

    const { setLoading } = loadingActions;

    const { setTitle } = pageHeaderActions;
    const {
        setChosenFacilityType,
        resetAvailableFacilityTypes,
        setChosenDate,
        setChosenFacility,
        setChosenTimeSlot,
        resetFacilityAvailableTimeSlots,
    } = newBookingActions;

    const [isFailedToFetchFacilityType, setIsFailedToFetchFacilityType] =
        useState<boolean>(false);
    const [
        isFailedToFetchFacilityTypeError,
        setIsFailedToFetchFacilityTypeError,
    ] = useState<ISetErrorDialog | null>(null);

    const backButtonOnPressHandler = () => {
        dispatch(setChosenFacilityType(null));
        dispatch(resetAvailableFacilityTypes());

        navigation.navigate(PageName.MANAGE_BOOKINGS_SCREEN);
    };

    const facilityTypeOnSelectFn = (facilityType: IFacilityType) => {
        dispatch(setChosenFacilityType(facilityType));

        navigation.navigate(PageName.SELECT_DATE_SCREEN);
        dispatch(setTitle(facilityType.name));
    };

    const clearUnusedData = useCallback(() => {
        if (facilityAvailableTimeSlots.length > 0) {
            dispatch(resetFacilityAvailableTimeSlots());
        }

        if (chosenDate !== null) {
            dispatch(setChosenDate(null));
        }

        if (chosenFacility !== null) {
            dispatch(setChosenFacility(null));
        }

        if (chosenTimeSlot !== null) {
            dispatch(setChosenTimeSlot(null));
        }
    }, [
        chosenDate,
        chosenFacility,
        chosenTimeSlot,
        facilityAvailableTimeSlots,
        dispatch,
        setChosenDate,
        setChosenFacility,
        setChosenTimeSlot,
        resetFacilityAvailableTimeSlots,
    ]);

    useEffect(() => {
        dispatch(setTitle(pageTitle));
    }, [dispatch, setTitle]);

    useFocusEffect(clearUnusedData);

    // Handle get all facility type error
    useEffect(() => {
        if (
            getAllFacilityTypesStatus === QueryStatus.rejected &&
            getAllFacilityTypesError
        ) {
            const errorMessage = RtkErrorHandler.getMessage(
                getAllFacilityTypesError,
            );

            setIsFailedToFetchFacilityTypeError({
                title: NewBookingError.FACILITY_TYPES_NOT_AVAILABLE,
                message: errorMessage,
            });
            setIsFailedToFetchFacilityType(true);
        }
    }, [getAllFacilityTypesStatus, getAllFacilityTypesError]);

    // Handle Loading
    useEffect(() => {
        const isLoading = getAllFacilityTypesStatus === QueryStatus.pending;

        if (isLoading) {
            dispatch(setLoading(true));
        } else {
            dispatch(setLoading(false));
        }
    }, [getAllFacilityTypesStatus, dispatch, setLoading]);

    return (
        <View style={styles.rootContainer}>
            <HeaderBar
                title={title}
                onBackButtonPress={backButtonOnPressHandler}
            />
            {!loading && availableFacilityTypes.length > 0 && (
                <View style={styles.listContainer}>
                    <FacilityTypeList
                        onSelect={facilityTypeOnSelectFn}
                        facilityTypes={availableFacilityTypes}
                        testID="facility-type-list"
                        numColumns={2}
                    />
                </View>
            )}
            {!loading && availableFacilityTypes.length === 0 && (
                <>
                    <View style={styles.nofacilityNoticeContainer}>
                        <NormalText
                            center={true}
                            style={styles.noFacilityNotice}>
                            No facility available.{'\n'}Please contact admin
                            office for assistance.
                        </NormalText>
                    </View>
                </>
            )}

            <Loading />
            <PopModal
                modalTitle={isFailedToFetchFacilityTypeError?.title || ''}
                modalBody={
                    <NormalText>
                        {isFailedToFetchFacilityTypeError?.message || ''}
                    </NormalText>
                }
                visibility={isFailedToFetchFacilityType}
                confirmText="Okay"
                onConfirm={() => {
                    setIsFailedToFetchFacilityType(false);
                    navigation.navigate(PageName.HOME_PAGE_SCREEN);
                }}
            />
        </View>
    );
};

export default SelectFacilityTypeScreen;
