import { NavigationContainer, RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import NavParamList from '../../../navigation/NavParmList';
import PageName from '../../../navigation/PageNameEnum';
import newBookingSlice from '../../../store/newBooking/slice';
import SelectDateScreen from './SelectDateScreen';
import { render } from '@testing-library/react-native';
import React from 'react';

jest.mock('../../../navigation/NavProps', () => ({}));

jest.mock('@fortawesome/react-native-fontawesome', () => {
    return {
        FontAwesomeIcon: '',
    };
});

const mockedUseAppSelector = jest.spyOn(
    require('../../../store/hook'),
    'useAppSelector',
);
const mockedUseAppDispatch = jest.spyOn(
    require('../../../store/hook'),
    'useAppDispatch',
);

// jest.spyOn(
//     require('../../../store/newBooking/thunk'),
//     'fetchFacilityAvailableTimeThunk',
// );
// jest.spyOn(
//     require('../../../store/newBooking/thunk'),
//     'fetchFacilityTypeThunk',
// );

jest.spyOn(newBookingSlice.actions, 'setChosenFacilityType');

jest.spyOn(newBookingSlice.actions, 'resetAvailableFacilityTypes');

const mockedUnwrapFn = jest.fn();

const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.SELECT_DATE_SCREEN>
> = {
    navigate: jest.fn(),
};

let mockedRoute: RouteProp<NavParamList, PageName.SELECT_DATE_SCREEN> = {
    key: 'SelectFacilityTypeTestKey',
    name: PageName.SELECT_DATE_SCREEN,
};

xdescribe('Test SelectDateScreen Component', () => {
    const testToken = 'valid token';
    const testTitle = 'valid title';

    beforeEach(() => {
        jest.clearAllMocks();
        jest.resetAllMocks();

        mockedUseAppDispatch.mockReturnValue(() => {
            return {
                unwrap: mockedUnwrapFn,
                catch: jest.fn(),
                finally: jest.fn(),
            };
        });
    });

    it('should render the SelectDateScreen correctly', () => {
        const testSelector = {
            pageHeader: {
                title: testTitle,
            },
            newBooking: {
                chosenDate: null,
                chosenFacilityType: null,
            },
            user: {
                token: testToken,
            },
            loading: {
                isLoading: false,
            },
        };

        mockedUseAppSelector.mockImplementation((selector: any) => {
            return selector(testSelector);
        });

        mockedUnwrapFn.mockResolvedValue(undefined);

        const selectDateScreen = render(
            <NavigationContainer>
                <SelectDateScreen
                    navigation={
                        mockNavigation as NativeStackNavigationProp<
                            NavParamList,
                            PageName.SELECT_DATE_SCREEN
                        >
                    }
                    route={mockedRoute}
                />
            </NavigationContainer>,
        ).toJSON();

        expect(selectDateScreen).toMatchSnapshot();
    });
});
