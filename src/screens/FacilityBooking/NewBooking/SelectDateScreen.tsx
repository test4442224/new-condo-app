import styles from './SelectDateScreenStyle';

import React, { FC, useCallback, useEffect, useState } from 'react';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import { SelectDateNavProps } from '../../../navigation/NavProps';
import PageName from '../../../navigation/PageNameEnum';
import { pageHeaderActions } from '../../../store/pageHeader/slice';
import { DateChangedCallback } from 'react-native-calendar-picker';
import { newBookingActions } from '../../../store/newBooking/slice';
import SubHeaderText from '../../../components/ui/titles/SubHeaderText';
import NormalText from '../../../components/ui/titles/NormalText';
import { SafeAreaView, View } from 'react-native';
import Loading from '../../../components/ui/loading/Loading';
import { loadingActions } from '../../../store/loading/slice';
import { convertDateToDateString, getMaxDate } from '../../../utils/Date';
import Calendar from '../../../components/ui/calendar/Calendar';
import { useFocusEffect } from '@react-navigation/native';
import RoundContainer from '../../../components/container/RoundContainer';
import {
    ISetErrorDialog,
    NewBookingError,
} from '../../../models/errorsType/NewBookingErrorTypes';
import PopModal from '../../../components/modal/PopModal';
import {
    useGetFacilityAvailableTimeQuery,
    useGetUnavailableDatesQuery,
} from '../../../store/api/facilityDateTimeApi';
import { QueryStatus } from '@reduxjs/toolkit/dist/query';
import { RtkErrorHandler } from '../../../utils/error/RtkErrorHandler.util';

const SelectDateScreen: FC<SelectDateNavProps> = ({ navigation }) => {
    const [disabledDates, setDisabledDates] = useState<Date[]>([]);
    const [isFailedToFetchDates, setIsFailedToFetchDates] =
        useState<boolean>(false);
    const [
        isFailedToFetchAvailableTimeSlots,
        setIsFailedToFetchAvailableTimeSlots,
    ] = useState<boolean>(false);
    const [isFailedToFetchDatesError, setIsFailedToFetchDatesError] =
        useState<ISetErrorDialog | null>(null);
    const [
        isFailedToFetchAvailableTimeSlotsError,
        setIsFailedToFetchAvailableTimeSlotsError,
    ] = useState<ISetErrorDialog | null>(null);

    const { title } = useAppSelector(state => state.pageHeader);
    const { chosenDate, chosenFacilityType } = useAppSelector(
        state => state.newBooking,
    );
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const {
        status: getUnavailableDatesStatus,
        data: getUnavailableDatesData,
        error: getUnavailableDatesError,
    } = useGetUnavailableDatesQuery(
        {
            facilityTypeId: chosenFacilityType?.id || 0,
            totalDays: 30,
        },
        {
            refetchOnMountOrArgChange: true,
            refetchOnReconnect: true,
            skip: typeof chosenFacilityType?.id !== 'number',
            // refetch every 5 minutes
            pollingInterval: 5 * 60 * 1000,
        },
    );

    const {
        status: getFacilityAvailableTimeStatus,
        error: getFacilityAvailableTimeError,
    } = useGetFacilityAvailableTimeQuery(
        {
            dateString: chosenDate || '',
            facilityTypeId: chosenFacilityType?.id || 0,
        },
        {
            skip:
                !chosenDate ||
                (chosenDate && isNaN(new Date(chosenDate).getTime())) ||
                typeof chosenFacilityType?.id !== 'number',
            refetchOnMountOrArgChange: true,
            refetchOnReconnect: true,
        },
    );

    const dispatch = useAppDispatch();

    const { setTitle } = pageHeaderActions;
    const {
        setChosenDate,
        setChosenFacilityType,
        setChosenTimeSlot,
        setChosenFacility,
    } = newBookingActions;
    const { setLoading } = loadingActions;

    const backButtonOnPressHandler = () => {
        dispatch(setChosenDate(null));
        dispatch(setChosenFacilityType(null));

        navigation.navigate(PageName.SELECT_FACILITY_TYPE_SCREEN);
        dispatch(setTitle('Choose Facility Type'));
    };

    const onDateChange: DateChangedCallback = date => {
        const dateObj = date.toDate();
        const dateString = convertDateToDateString(dateObj);

        dispatch(setChosenDate(dateString));

        dispatch(setLoading(true));
    };

    const clearUnusedData = useCallback(() => {
        dispatch(setChosenDate(null));
        dispatch(setChosenFacility(null));
        dispatch(setChosenTimeSlot(null));
    }, [dispatch, setChosenDate, setChosenTimeSlot, setChosenFacility]);

    // Handle get facility available time success and fail cases
    useEffect(() => {
        if (getFacilityAvailableTimeStatus === QueryStatus.fulfilled) {
            navigation.navigate(PageName.SELECT_TIME_SCREEN);
            return;
        }

        if (
            getFacilityAvailableTimeStatus === QueryStatus.rejected &&
            getFacilityAvailableTimeError
        ) {
            setIsFailedToFetchAvailableTimeSlotsError({
                title: NewBookingError.TIMESLOT_NOT_AVAILABLE,
                message: 'Oops! Something went wrong. Please try again.',
            });
            setIsFailedToFetchAvailableTimeSlots(true);
            return;
        }
    }, [
        getFacilityAvailableTimeStatus,
        getFacilityAvailableTimeError,
        navigation,
    ]);

    // Handle get unavailable dates success and fail cases
    useEffect(() => {
        if (
            getUnavailableDatesStatus === QueryStatus.fulfilled &&
            getUnavailableDatesData
        ) {
            const unavailableDates =
                getUnavailableDatesData.unavailableDates.map(
                    dateString => new Date(dateString),
                );

            setDisabledDates(unavailableDates);

            return;
        }

        if (
            getUnavailableDatesStatus === QueryStatus.rejected &&
            getUnavailableDatesError
        ) {
            const errorMessage = RtkErrorHandler.getMessage(
                getUnavailableDatesError,
            );

            setIsFailedToFetchDatesError({
                title: NewBookingError.DATE_NOT_AVAILABLE,
                message: errorMessage,
            });
            setIsFailedToFetchDates(true);
        }
    }, [
        getUnavailableDatesStatus,
        getUnavailableDatesData,
        getUnavailableDatesError,
    ]);

    // Handle Loading
    useEffect(() => {
        const isPending =
            getUnavailableDatesStatus === QueryStatus.pending ||
            getFacilityAvailableTimeStatus === QueryStatus.pending;

        setIsLoading(isPending);
        dispatch(setLoading(isPending));
    }, [
        getUnavailableDatesStatus,
        getFacilityAvailableTimeStatus,
        dispatch,
        setLoading,
    ]);

    useFocusEffect(clearUnusedData);

    return (
        <SafeAreaView style={styles.rootContainer}>
            <HeaderBar
                testID="select-date-screen.header-bar-text"
                title={'Select a Date'}
                onBackButtonPress={backButtonOnPressHandler}
            />
            <View style={styles.generalContainer}>
                <RoundContainer style={styles.rowContainer}>
                    <NormalText>{`You are making a booking for a `}</NormalText>
                    <NormalText
                        style={styles.boldText}
                        testID="select-date-screen.facility-type">
                        {`${title}.`}
                    </NormalText>
                </RoundContainer>
            </View>
            <SafeAreaView style={styles.contentContainer}>
                <View style={styles.descriptionContainer}>
                    <SubHeaderText
                        underline={false}
                        style={styles.subheaderText}>
                        Please select an available date:
                    </SubHeaderText>
                </View>

                <Calendar
                    onDateChange={onDateChange}
                    chosenDate={chosenDate}
                    maxDate={getMaxDate(30)}
                    minDate={new Date()}
                    disabledDates={isLoading ? _ => true : disabledDates}
                />
            </SafeAreaView>

            <Loading />
            <PopModal
                modalTitle={isFailedToFetchDatesError?.title || ''}
                modalBody={
                    <NormalText>
                        {isFailedToFetchDatesError?.message || ''}
                    </NormalText>
                }
                visibility={isFailedToFetchDates}
                confirmText="Okay"
                onConfirm={() => {
                    setIsFailedToFetchDates(false);
                    navigation.navigate(PageName.SELECT_FACILITY_TYPE_SCREEN);
                }}
            />
            <PopModal
                modalTitle={isFailedToFetchAvailableTimeSlotsError?.title || ''}
                modalBody={
                    <NormalText>
                        {isFailedToFetchAvailableTimeSlotsError?.message || ''}
                    </NormalText>
                }
                visibility={isFailedToFetchAvailableTimeSlots}
                confirmText="Okay"
                onConfirm={() => {
                    setIsFailedToFetchAvailableTimeSlots(false);
                    navigation.navigate(PageName.SELECT_FACILITY_TYPE_SCREEN);
                }}
            />
        </SafeAreaView>
    );
};

export default SelectDateScreen;
