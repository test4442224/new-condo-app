import styles from './SelectFacilityAndTimeScreenStyle';

import React, { FC, useState } from 'react';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import { SelectFacilityAndTimeNavProps } from '../../../navigation/NavProps';
import PageName from '../../../navigation/PageNameEnum';
import SubHeaderText from '../../../components/ui/titles/SubHeaderText';
import NormalText from '../../../components/ui/titles/NormalText';
import PrimaryButton from '../../../components/ui/buttons/PrimaryButton';
import FacilityTimeSlotList from '../../../components/lists/facilityTimeSlotList/FacilityTimeSlotList';
import { View } from 'react-native';
import { newBookingActions } from '../../../store/newBooking/slice';
import RoundContainer from '../../../components/container/RoundContainer';
import PopModal from '../../../components/modal/PopModal';
import { toDateFormat } from '../../../utils/dateTimeUtil';
import {
    ISetErrorDialog,
    NewBookingError,
} from '../../../models/errorsType/NewBookingErrorTypes';

const SelectFacilityAndTimeScreen: FC<SelectFacilityAndTimeNavProps> = ({
    navigation,
}) => {
    const { title } = useAppSelector(state => state.pageHeader);
    const {
        facilityAvailableTimeSlots,
        chosenDate: chosenDate,
        chosenFacility: chosenFacility,
        chosenTimeSlots: chosenTimeSlot,
    } = useAppSelector(state => state.newBooking);

    const dispatch = useAppDispatch();

    const [isTimeslotNonConsecutive, setIsTimeslotNonConsecutive] =
        useState(false);
    const [isChosenTimeSlot, setIsChosenTimeSlot] = useState<boolean>(false);
    const [isFailFetchDates, setIsFailFetchDates] = useState<boolean>(false);
    const [isFailFetchDatesError, setIsFailFetchDatesError] =
        useState<ISetErrorDialog | null>(null);

    const togglePopup = (isVisible: boolean) => {
        setIsTimeslotNonConsecutive(isVisible);
    };

    const {
        setChosenFacility,
        setChosenTimeSlot,
        resetFacilityAvailableTimeSlots,
    } = newBookingActions;

    const backButtonOnClickHandler = () => {
        dispatch(resetFacilityAvailableTimeSlots());
        dispatch(setChosenFacility(null));
        dispatch(setChosenTimeSlot(null));

        navigation.navigate(PageName.SELECT_DATE_SCREEN);
    };

    const proceedButtonOnPressHandler = () => {
        if (
            chosenDate &&
            chosenFacility &&
            chosenTimeSlot &&
            chosenTimeSlot.length > 0
        ) {
            navigation.navigate(PageName.REVIEW_BOOKING_DETAILS_SCREEN);
            return;
        }

        if (!chosenDate) {
            setIsFailFetchDatesError({
                title: NewBookingError.GENERAL,
                message: 'Something went wrong! Please choose the date again',
            });
            setIsFailFetchDates(true);
            return;
        }
        setIsChosenTimeSlot(true);
    };

    return (
        <View style={styles.rootContainer}>
            <HeaderBar
                title={'Select a Time'}
                onBackButtonPress={backButtonOnClickHandler}
            />

            {/* Page Content */}
            <View style={styles.pageContentContainer}>
                <View style={styles.generalContainer}>
                    <RoundContainer style={styles.rowContainer}>
                        <NormalText>{`You are making a booking for a `}</NormalText>
                        <NormalText
                            style={styles.boldText}
                            testID="select-time-screen.facility-type">
                            {`${title}`}
                        </NormalText>
                        <NormalText>{` on `}</NormalText>
                        <NormalText>{`${toDateFormat(
                            chosenDate!,
                        )}.`}</NormalText>
                    </RoundContainer>
                </View>

                {facilityAvailableTimeSlots.length > 0 && (
                    <>
                        <SubHeaderText underline={false}>
                            Please select available timeslots:
                        </SubHeaderText>

                        <NormalText style={styles.description}>
                            You may only select{' '}
                            {
                                <NormalText underline={true}>
                                    consecutive
                                </NormalText>
                            }{' '}
                            timeslots for one facility.
                        </NormalText>
                        <View style={styles.buttonContainer}>
                            <PrimaryButton
                                onPress={proceedButtonOnPressHandler}
                                label="Proceed"
                                testID="proceed"
                            />
                        </View>
                        <View>
                            <FacilityTimeSlotList
                                facilityAvailableTimeSlots={
                                    facilityAvailableTimeSlots
                                }
                                togglePopup={togglePopup}
                            />
                        </View>
                    </>
                )}
                {facilityAvailableTimeSlots.length === 0 && (
                    <>
                        <View style={styles.noFacilityNoticeContainer}>
                            <NormalText
                                center={true}
                                style={styles.noFacilityNotice}>
                                No facility available on{' '}
                                {toDateFormat(chosenDate!)}.{'\n'}
                                Please choose another day.
                            </NormalText>
                        </View>
                    </>
                )}
            </View>

            <PopModal
                modalTitle="Nonconsecutive Time Slots"
                modalBody={
                    <NormalText>
                        Please select consecutive timeslots.
                    </NormalText>
                }
                visibility={isTimeslotNonConsecutive}
                confirmText="Okay"
                onConfirm={() => {
                    setIsTimeslotNonConsecutive(false);
                }}
            />
            <PopModal
                modalTitle={isFailFetchDatesError?.title || ''}
                modalBody={
                    <NormalText>
                        {isFailFetchDatesError?.message || ''}
                    </NormalText>
                }
                visibility={isFailFetchDates}
                confirmText="Okay"
                onConfirm={() => {
                    setIsFailFetchDates(false);
                    navigation.navigate(PageName.SELECT_DATE_SCREEN);
                }}
            />
            <PopModal
                modalTitle={NewBookingError.GENERAL}
                modalBody={<NormalText>Please choose a time slot.</NormalText>}
                visibility={isChosenTimeSlot}
                confirmText="Okay"
                onConfirm={() => {
                    setIsChosenTimeSlot(false);
                }}
            />
        </View>
    );
};

export default SelectFacilityAndTimeScreen;
