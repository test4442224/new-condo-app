import React from 'react';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import NavParamList from '../../../navigation/NavParmList';
import PageName from '../../../navigation/PageNameEnum';
import { RouteProp } from '@react-navigation/native';
import { fireEvent, waitFor } from '@testing-library/react-native';
import BookingSuccess from './BookingCompleteSuccess';
import { renderWithProviders } from '../../../test/utils';
import { FacilityImage } from '../../../store/manageBooking/slice';
import { PaymentStatus } from '../../../store/manageBooking/slice';

jest.mock('../../../utils/hooks/useRtkResponse', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        showToast: false,
    })),
}));

jest.mock('@react-native-clipboard/clipboard', () => ({
    Clipboard: '',
}));

jest.mock('react-native-simple-toast', () => {});

jest.mock('../../../navigation/NavProps', () => ({}));

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

jest.mock('../../../store/hook', () => {
    return {
        useAppSelector: jest.fn(),
        useAppDispatch: jest.fn(() => jest.fn()),
    };
});

const sampleDate = new Date('2022-01-01T12:00:00Z');
const mockFacility = {
    name: 'Sample Facility',
    location: 'Sample Location',
    description: 'Sample Description',
    type: 'Sample Type',
    pax: 100,
};

const mockUser = {
    lastName: 'Sample last',
    firstName: 'Sample first',
    phoneNumber: '9999 9999',
};

const mockImages: FacilityImage = {
    images: ['Sample'],
};

const mockState = {
    id: 123,
    bookingReferenceId: 'BOOK-123',
    facilityId: 123,
    facility: mockFacility,
    startDateTime: 'Sample Date',
    endDateTime: 'Sample End Date',
    user: mockUser,
    createdAt: null,
    updatedAt: null,
    facilityImages: mockImages,
    isCancellable: false,
    isCancelledByUser: null,
    cancelReason: null,
    paymentMethod: null,
    paymentDueAt: sampleDate,
    priceBreakdowns: [],
    bookingId: 123,
    facilityName: 'Sample Facility',
    facilityLocation: 'Sample Location',
    bookingDate: '2023-09-30',
    bookingTime: '2:00 PM - 3:00 PM',
    totalPrice: 100,
    paymentStatus: PaymentStatus.FREE,
    cancelledAt: null,
    paidAt: null,
    refundedAt: null,
};

const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.BOOKING_SUCCESS_SCREEN>
> = {
    navigate: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.BOOKING_SUCCESS_SCREEN> = {
    key: 'BookingSuccessKey',
    name: PageName.BOOKING_SUCCESS_SCREEN,
    params: { bookingDetails: mockState },
};

describe('BookingSuccess Component', () => {
    beforeEach(() => {
        jest.clearAllMocks();
        jest.useFakeTimers();
        jest.setSystemTime(new Date('2023-05-14T13:15:00.000Z'));
    });

    const { useAppDispatch } = require('../../../store/hook');
    const mockDispatch = jest.fn();
    useAppDispatch.mockReturnValue(mockDispatch);

    it('should render the page correctly', () => {
        const page = renderWithProviders(
            <BookingSuccess
                navigation={
                    mockNavigation as NativeStackNavigationProp<
                        NavParamList,
                        PageName.BOOKING_SUCCESS_SCREEN
                    >
                }
                route={mockRoute}
            />,
        );
        const bookingConfirmed = page.getByText(
            'Your booking has been confirmed.',
        );
        expect(bookingConfirmed).toBeTruthy();
    });

    it('should retrieve and display the book id, payment date and button correctly', () => {
        const { getByText } = renderWithProviders(
            <BookingSuccess
                navigation={
                    mockNavigation as NativeStackNavigationProp<
                        NavParamList,
                        PageName.BOOKING_SUCCESS_SCREEN
                    >
                }
                route={mockRoute}
            />,
            {
                preloadedState: {
                    booking: {
                        bookingsArray: [mockState],
                        isLoading: false,
                        selectedBooking: null,
                        upcomingBookings: [],
                    },
                },
            },
        );
        const bookidText = getByText('BOOK-123');
        expect(bookidText).toBeTruthy();
    });

    it('should render the confirm and cancel button correctly', () => {
        const page = renderWithProviders(
            <BookingSuccess
                navigation={
                    mockNavigation as NativeStackNavigationProp<
                        NavParamList,
                        PageName.BOOKING_SUCCESS_SCREEN
                    >
                }
                route={mockRoute}
            />,
        );
        const button = page.getByTestId('back-to-my-bookings');
        expect(button).toBeDefined();
    });

    it('should handle on button press', async () => {
        const { getByTestId } = renderWithProviders(
            <BookingSuccess
                navigation={
                    mockNavigation as NativeStackNavigationProp<
                        NavParamList,
                        PageName.BOOKING_SUCCESS_SCREEN
                    >
                }
                route={mockRoute}
            />,
        );

        const button = getByTestId('back-to-my-bookings');
        await waitFor(() => fireEvent.press(button));

        expect(mockNavigation.navigate).toBeCalledTimes(1);
        expect(mockNavigation.navigate).toHaveBeenCalledWith(
            PageName.MANAGE_BOOKINGS_SCREEN,
        );
    });
});
