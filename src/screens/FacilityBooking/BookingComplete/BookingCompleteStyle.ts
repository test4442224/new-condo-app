import { StyleSheet } from 'react-native';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import { Spacings } from 'react-native-ui-lib';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        opacity: 0.95,
    },
    successBg: {
        backgroundColor: Colors.$backgroundSuccessLight,
    },
    pendingBg: {
        backgroundColor: Colors.$backgroundPrimaryLight,
    },
    boldText: {
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    iconContainer: {
        marginTop: Spacings.s10,
        marginBottom: Spacings.s6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    generalContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    labelText: {
        color: Colors.$textDisabled,
        fontWeight: WEIGHT_TYPES.BOLD,
        marginBottom: Spacings.s1,
    },
    labelDotText: {
        color: Colors.$textDisabled,
    },
    textContainer: {
        paddingVertical: Spacings.s1,
        paddingHorizontal: Spacings.s5,
    },
    cardContainer: {
        flexGrow: 2,
        marginVertical: Spacings.s2,
        paddingVertical: Spacings.s4,
        paddingHorizontal: Spacings.s5,
    },
    textCardContainer: {
        paddingVertical: Spacings.s2,
    },
    buttonContainer: {
        paddingVertical: Spacings.s5,
        paddingHorizontal: Spacings.s5,
    },
    roundContainer: {
        backgroundColor: Colors.$backgroundDefault,
        paddingVertical: Spacings.s3,
        paddingHorizontal: Spacings.s5,
    },
    rowContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    successIcon: {
        color: Colors.$iconSuccess,
        marginHorizontal: Spacings.s3,
    },
    copyIcon: {
        color: Colors.$iconDark,
    },
    infoIcon: {
        color: Colors.$iconPrimary,
        marginHorizontal: Spacings.s3,
    },
    buttonMarginBottom: {
        marginBottom: Spacings.s4,
    },
    italicGreyText: {
        fontStyle: 'italic',
        color: Colors.$textDisabled,
    },
    scrollableContainer: {
        opacity: 0.95,
    },
});
