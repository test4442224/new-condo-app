import { SetStateAction } from 'react';
import { Asset, ErrorCode } from 'react-native-image-picker';
import { Error } from './BookingCompletePending';
import { ImageData } from '../../../store/manageBooking/api';

export const handleImagePickerError = (
    errorCode: ErrorCode,
    setError: (error: SetStateAction<Error>) => void,
) => {
    switch (errorCode) {
        case 'camera_unavailable':
            setError({
                hasError: true,
                title: 'Camera Unavailable',
                message: 'Camera not available on device.',
            });
            return;
        case 'permission':
            setError({
                hasError: true,
                title: 'Camera Access Not Granted',
                message: 'Permission to access camera not granted.',
            });
            return;
        case 'others':
            setError({
                hasError: true,
                title: 'An Error Occurred',
                message: 'Please contact condo management for assistance.',
            });
            return;
    }
};

export const validatePhoto = (
    photo: Asset,
    setSelectedPhoto: (selectedPhoto: SetStateAction<ImageData | null>) => void,
    setError: (error: SetStateAction<Error>) => void,
): boolean => {
    if (!['image/jpg', 'image/jpeg', 'image/png'].includes(photo.type!)) {
        setSelectedPhoto(null);
        setError({
            hasError: true,
            title: 'Invalid File Format',
            message: 'Only .jpeg, .jpg or .png file allowed.',
        });
        return false;
    }
    if (photo.fileSize! > 2000000) {
        setSelectedPhoto(null);
        setError({
            hasError: true,
            title: 'Invalid File Size',
            message: 'Maximum file size is 2MB.',
        });
        return false;
    }
    return true;
};
