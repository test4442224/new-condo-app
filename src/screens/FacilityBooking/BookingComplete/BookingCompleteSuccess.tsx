import React, { FC, useState } from 'react';
import styles from './BookingCompleteStyle';
import { View } from 'react-native';
import NormalText from '../../../components/ui/titles/NormalText';
import PrimaryButton from '../../../components/ui/buttons/PrimaryButton';
import RoundContainer from '../../../components/container/RoundContainer';

import { BookingSuccessNavProps } from '../../../navigation/NavProps';
import PageName from '../../../navigation/PageNameEnum';
import { newBookingActions } from '../../../store/newBooking/slice';
import { useAppDispatch } from '../../../store/hook';
import { PaymentMethod } from '../../../store/manageBooking/slice';
import ScrollableContainer from '../../../components/container/ScrollableContainer';
import { toDateTimeFormat } from '../../../utils/dateTimeUtil';
import { DEFAULT_PAYMENT_METHOD } from '../../../store/newBooking/constants';
import Toast from 'react-native-simple-toast';
import Header from './components/Header';
import Field from './components/Field';
import CopyButton from './components/CopyButton';

export interface ICondo {
    uen: string;
    name: string;
}
export interface IBookingSuccessProps {
    bookingReferenceId: string;
    bookingId: number;
    facilityName: string;
    facilityLocation: string;
    bookingDate?: string;
    bookingTime?: string;
    totalPrice: number;
    paymentMethod: PaymentMethod | null;
    createdAt: string | null;
}

const BookingSuccess: FC<BookingSuccessNavProps> = ({ route, navigation }) => {
    const { bookingDetails } = route.params;

    const [isRefNoCopiedShown, setIsRefNoCopiedShown] =
        useState<boolean>(false);

    const formattedCreatedAt = bookingDetails.createdAt
        ? toDateTimeFormat(bookingDetails.createdAt)
        : toDateTimeFormat(new Date());

    const dispatch = useAppDispatch();

    const {
        resetAvailableFacilityTypes,
        resetFacilityAvailableTimeSlots,
        setChosenDate,
        setChosenFacility,
        setChosenFacilityType,
        setChosenTimeSlot,
        setPaymentMethod,
    } = newBookingActions;

    const resetNewBookingRedux = () => {
        dispatch(setPaymentMethod(DEFAULT_PAYMENT_METHOD));
        dispatch(resetAvailableFacilityTypes());
        dispatch(resetFacilityAvailableTimeSlots());
        dispatch(setChosenDate(null));
        dispatch(setChosenFacility(null));
        dispatch(setChosenFacilityType(null));
        dispatch(setChosenTimeSlot(null));
    };

    const showCopiedFeedback = () => {
        Toast.showWithGravityAndOffset(
            'Copied to clipboard!',
            Toast.SHORT,
            Toast.BOTTOM,
            0,
            20,
        );
        setIsRefNoCopiedShown(true);
    };

    return (
        <>
            <ScrollableContainer
                style={[styles.scrollableContainer, styles.successBg]}>
                <Header
                    type="success"
                    paymentMethod={bookingDetails.paymentMethod}
                />

                <View style={styles.cardContainer}>
                    <RoundContainer style={styles.roundContainer}>
                        <View style={styles.rowContainer}>
                            <Field
                                label="Booking Reference No."
                                value={bookingDetails.bookingReferenceId}
                            />
                            <CopyButton
                                valueToCopy={bookingDetails.bookingReferenceId}
                                isValueCopiedShown={isRefNoCopiedShown}
                                setIsValueCopiedShown={setIsRefNoCopiedShown}
                                showCopiedFeedback={showCopiedFeedback}
                            />
                        </View>
                        <Field
                            label="Date of Booking"
                            value={formattedCreatedAt}
                        />
                        <View style={styles.textCardContainer}>
                            <NormalText
                                center={true}
                                style={styles.labelDotText}>
                                ................................................
                            </NormalText>
                        </View>
                        <Field
                            label="Booked Facility & Location"
                            value={`${bookingDetails.facilityName} - ${bookingDetails.facilityLocation}`}
                        />
                        <Field
                            label="Booked Date & Time"
                            value={`${bookingDetails.bookingDate}, ${bookingDetails.bookingTime}`}
                        />
                        {bookingDetails.paymentMethod ===
                        PaymentMethod.PAYNOW ? (
                            <>
                                <Field
                                    label="Amount Paid"
                                    value={`$${bookingDetails.totalPrice.toFixed(
                                        2,
                                    )}`}
                                />
                                <Field
                                    label="Payment Method"
                                    value="PayNow to UEN"
                                />
                            </>
                        ) : null}
                    </RoundContainer>
                </View>

                <View style={styles.buttonContainer}>
                    <PrimaryButton
                        label="Back to My Bookings"
                        testID="back-to-my-bookings"
                        onPress={() => {
                            resetNewBookingRedux();
                            navigation.navigate(
                                PageName.MANAGE_BOOKINGS_SCREEN,
                            );
                        }}
                    />
                </View>
            </ScrollableContainer>
        </>
    );
};

export default BookingSuccess;
