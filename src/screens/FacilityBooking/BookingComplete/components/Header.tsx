import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React, { FC } from 'react';
import { View } from 'react-native';
import {
    faCircleCheck,
    faExclamationCircle,
} from '@fortawesome/free-solid-svg-icons';
import styles from '../BookingCompleteStyle';
import HeaderText from '../../../../components/ui/titles/HeaderText';
import NormalText from '../../../../components/ui/titles/NormalText';
import Colors from '../../../../Constants/Colors';
import { PaymentMethod } from '../../../../store/manageBooking/slice';

type Props = {
    type: 'pending' | 'success';
    paymentMethod: PaymentMethod | null;
    paymentDueDateTime?: string;
};

const Header: FC<Props> = ({ type, paymentMethod, paymentDueDateTime }) => {
    let headerTitle = '';
    if (type === 'pending') {
        headerTitle = 'Pending Payment';
    } else if (paymentMethod === PaymentMethod.PAYNOW) {
        headerTitle = 'Pending Verification';
    } else {
        headerTitle = 'Booking Confirmed';
    }

    let headerText = <></>;
    if (type === 'pending' && paymentMethod === PaymentMethod.PAYNOW) {
        headerText = (
            <>
                Please make payment by{' '}
                <NormalText style={styles.boldText}>
                    {paymentDueDateTime}
                </NormalText>{' '}
                to the UEN provided. Kindly include the Booking Reference No.
                during payment and upload the payment receipt for verification
                purposes.
            </>
        );
    } else if (type === 'pending' && paymentMethod === PaymentMethod.CASH) {
        headerText = (
            <>
                <NormalText center={true}>
                    Your booking has been reserved.{'\n'}An email with your
                    booking details will be sent to you shortly. Please refer to
                    the payment instructions in the email. Please make payment
                    by{' '}
                    <NormalText style={styles.boldText}>
                        {paymentDueDateTime}
                    </NormalText>
                    .
                </NormalText>
            </>
        );
    } else if (type === 'success' && paymentMethod === PaymentMethod.PAYNOW) {
        headerText = (
            <>
                {
                    'Your booking has been reserved.\nAn email with your booking details will be sent to you shortly. Condo management will verify your payment and confirm your booking.'
                }
            </>
        );
    } else {
        headerText = <>{'Your booking has been confirmed.'}</>;
    }

    return (
        <>
            <View style={styles.iconContainer}>
                <FontAwesomeIcon
                    icon={
                        type === 'pending' ? faExclamationCircle : faCircleCheck
                    }
                    style={
                        type === 'pending'
                            ? styles.infoIcon
                            : styles.successIcon
                    }
                    size={40}
                />
            </View>

            <View style={styles.generalContainer}>
                <View style={styles.textContainer}>
                    <HeaderText style={{ color: Colors.$textDefault }}>
                        {headerTitle}
                    </HeaderText>
                </View>
                <View style={styles.textContainer} testID="confirmation-text">
                    <NormalText center={true}>{headerText}</NormalText>
                </View>
            </View>
        </>
    );
};

export default Header;
