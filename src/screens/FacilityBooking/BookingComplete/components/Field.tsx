import React, { FC } from 'react';
import { View } from 'react-native';
import NormalText from '../../../../components/ui/titles/NormalText';
import styles from '../BookingCompleteStyle';

type Props = {
    label: string;
    value: string | null;
};

const Field: FC<Props> = ({ label, value }) => {
    return (
        <View style={styles.textCardContainer}>
            <NormalText style={styles.labelText}>{label}</NormalText>
            {value && <NormalText>{value}</NormalText>}
        </View>
    );
};

export default Field;
