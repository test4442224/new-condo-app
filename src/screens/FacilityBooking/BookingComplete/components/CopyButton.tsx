import React, { FC } from 'react';
import { TouchableOpacity } from 'react-native';
import Clipboard from '@react-native-clipboard/clipboard';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faCopy } from '@fortawesome/free-regular-svg-icons';
import styles from '../BookingCompleteStyle';
import { CopiedFeedback } from '../BookingCompletePending';

type Props = {
    valueToCopy: string;
    isValueCopiedShown: boolean;
    setIsValueCopiedShown: React.Dispatch<React.SetStateAction<boolean>>;
    showCopiedFeedback: (copiedValue?: CopiedFeedback) => void;
    testID?: string;
};

const CopyButton: FC<Props> = ({
    valueToCopy,
    isValueCopiedShown,
    setIsValueCopiedShown,
    showCopiedFeedback,
    testID,
}) => {
    return (
        <TouchableOpacity
            onPress={() => {
                Clipboard.setString(valueToCopy);
                showCopiedFeedback();
                setTimeout(() => {
                    setIsValueCopiedShown(false);
                }, 1000);
            }}
            testID={testID}>
            {isValueCopiedShown ? (
                <FontAwesomeIcon
                    icon={faCheck}
                    style={styles.copyIcon}
                    size={20}
                />
            ) : (
                <FontAwesomeIcon
                    icon={faCopy}
                    style={styles.copyIcon}
                    size={20}
                />
            )}
        </TouchableOpacity>
    );
};

export default CopyButton;
