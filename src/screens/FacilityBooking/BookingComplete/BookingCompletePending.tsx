import React, { FC, useState } from 'react';
import styles from './BookingCompleteStyle';
import { Platform, View } from 'react-native';
import NormalText from '../../../components/ui/titles/NormalText';
import PrimaryButton from '../../../components/ui/buttons/PrimaryButton';
import RoundContainer from '../../../components/container/RoundContainer';
import { BookingPendingNavProps } from '../../../navigation/NavProps';
import PageName from '../../../navigation/PageNameEnum';
import { newBookingActions } from '../../../store/newBooking/slice';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import ScrollableContainer from '../../../components/container/ScrollableContainer';
import {
    to12HourFormat,
    toDateFormat,
    toDateTimeFormat,
} from '../../../utils/dateTimeUtil';
import { PaymentMethod } from '../../../store/manageBooking/slice';
import { IBookingSuccessProps, ICondo } from './BookingCompleteSuccess';
import { DEFAULT_PAYMENT_METHOD } from '../../../store/newBooking/constants';
import { launchImageLibrary } from 'react-native-image-picker';
import {
    ImageData,
    useMakePaynowPaymentMutation,
} from '../../../store/manageBooking/api';
import { loadingActions } from '../../../store/loading/slice';
import PopModal from '../../../components/modal/PopModal';
import Loading from '../../../components/ui/loading/Loading';
import {
    handleImagePickerError,
    validatePhoto,
} from './BookingCompletePending.utils';
import Toast from 'react-native-simple-toast';
import Header from './components/Header';
import Field from './components/Field';
import CopyButton from './components/CopyButton';

export interface IBookingPendingProps {
    condo: ICondo;
    bookingReferenceId: string;
    bookingId: number;
    totalPrice: number;
    paymentMethod: PaymentMethod;
    paymentDueAt: Date | null;
    facilityName?: string;
    facilityLocation?: string;
    bookingDate?: string;
    bookingTime?: string;
    createdAt?: string;
}

export type Error = {
    hasError: boolean;
    title: string;
    message: string;
};

export enum CopiedFeedback {
    RefNo = 'RefNo',
    UEN = 'UEN',
}

const BookingPending: FC<BookingPendingNavProps> = ({ route, navigation }) => {
    const dispatch = useAppDispatch();
    const { bookingDetails } = route.params;

    const [isRefNoCopiedShown, setIsRefNoCopiedShown] =
        useState<boolean>(false);
    const [isUENCopiedShown, setIsUENCopiedShown] = useState<boolean>(false);
    const [selectedPhoto, setSelectedPhoto] = useState<ImageData | null>(null);
    const [error, setError] = useState<Error>({
        hasError: false,
        title: '',
        message: '',
    });

    const token = useAppSelector(state => state.user.token);

    const {
        resetAvailableFacilityTypes,
        resetFacilityAvailableTimeSlots,
        setChosenDate,
        setChosenFacility,
        setChosenFacilityType,
        setChosenTimeSlot,
        setPaymentMethod,
    } = newBookingActions;
    const { setLoading } = loadingActions;
    const [makePaynowPayment] = useMakePaynowPaymentMutation();

    const formattedPaymentDueDateTime = toDateTimeFormat(
        bookingDetails.paymentDueAt!,
    );
    let formattedBookingCreatedAt = '';
    if (bookingDetails.createdAt)
        formattedBookingCreatedAt = toDateTimeFormat(bookingDetails.createdAt!);

    const resetNewBookingRedux = () => {
        dispatch(resetAvailableFacilityTypes());
        dispatch(resetFacilityAvailableTimeSlots());
        dispatch(setChosenDate(null));
        dispatch(setChosenFacility(null));
        dispatch(setChosenFacilityType(null));
        dispatch(setChosenTimeSlot(null));
        dispatch(setPaymentMethod(DEFAULT_PAYMENT_METHOD));
    };

    const handleChoosePhoto = async () => {
        const result = await launchImageLibrary({
            mediaType: 'photo',
        });
        if (result.didCancel) return;
        if (result.errorCode) {
            handleImagePickerError(result.errorCode, setError);
            return;
        }

        const photo = result.assets![0];
        const photoIsValid = validatePhoto(photo, setSelectedPhoto, setError);
        if (!photoIsValid) return;

        setSelectedPhoto({
            name: photo.fileName as string,
            type: photo.type as string,
            uri:
                Platform.OS === 'ios'
                    ? photo.uri!.replace('file://', '')
                    : photo.uri!,
        });
    };

    const handleSubmitPhoto = async () => {
        if (!token) return;
        if (!selectedPhoto) {
            setError({
                hasError: true,
                title: 'No Image Uploaded',
                message: 'Please upload a screenshot of your PayNow receipt.',
            });
            return;
        }

        try {
            dispatch(setLoading(true));
            const updatedBooking = await makePaynowPayment({
                token,
                bookingId: bookingDetails.bookingId,
                image: selectedPhoto,
            }).unwrap();

            const successBookingDetails: IBookingSuccessProps = {
                bookingReferenceId: updatedBooking.bookingReferenceId,
                bookingId: updatedBooking.id,
                facilityName: updatedBooking.facility.name,
                facilityLocation: updatedBooking.facility.location,
                bookingDate: toDateFormat(updatedBooking.startDateTime),
                bookingTime: `${to12HourFormat(
                    updatedBooking.startDateTime,
                )} - ${to12HourFormat(updatedBooking.endDateTime)}`,
                totalPrice: +updatedBooking.totalPrice,
                paymentMethod: updatedBooking.paymentMethod,
                createdAt: updatedBooking.createdAt
                    ? updatedBooking.createdAt
                    : null,
            };

            navigation.replace(PageName.BOOKING_SUCCESS_SCREEN, {
                bookingDetails: successBookingDetails,
            });
        } catch (err) {
            setError({
                hasError: true,
                title: 'An Error Occurred',
                message:
                    'Please try again later or contact condo management for assistance.',
            });
            return;
        } finally {
            dispatch(setLoading(false));
        }
    };

    const showCopiedFeedback = (feedback: CopiedFeedback) => {
        Toast.showWithGravityAndOffset(
            'Copied to clipboard!',
            Toast.SHORT,
            Toast.BOTTOM,
            0,
            20,
        );
        if (feedback === CopiedFeedback.RefNo) {
            setIsRefNoCopiedShown(true);
        } else if (feedback === CopiedFeedback.UEN) {
            setIsUENCopiedShown(true);
        }
    };

    return (
        <View style={[styles.rootContainer, styles.pendingBg]}>
            <ScrollableContainer
                style={[styles.scrollableContainer, styles.pendingBg]}>
                <Header
                    type="pending"
                    paymentMethod={bookingDetails.paymentMethod}
                    paymentDueDateTime={formattedPaymentDueDateTime}
                />

                <View style={styles.cardContainer}>
                    <RoundContainer style={styles.roundContainer}>
                        <View style={styles.rowContainer}>
                            <Field
                                label="Booking Reference No."
                                value={bookingDetails.bookingReferenceId}
                            />
                            <CopyButton
                                valueToCopy={bookingDetails.bookingReferenceId}
                                isValueCopiedShown={isRefNoCopiedShown}
                                setIsValueCopiedShown={setIsRefNoCopiedShown}
                                showCopiedFeedback={showCopiedFeedback.bind(
                                    null,
                                    CopiedFeedback.RefNo,
                                )}
                            />
                        </View>
                        {bookingDetails.paymentMethod ===
                            PaymentMethod.CASH && (
                            <Field
                                label="Date of Booking"
                                value={formattedBookingCreatedAt}
                            />
                        )}
                        <View style={styles.textCardContainer}>
                            <NormalText
                                center={true}
                                style={styles.labelDotText}>
                                ................................................
                            </NormalText>
                        </View>
                        {bookingDetails.paymentMethod ===
                            PaymentMethod.CASH && (
                            <>
                                <Field
                                    label="Booked Facility & Location"
                                    value={`${bookingDetails.facilityName} - ${bookingDetails.facilityLocation}`}
                                />
                                <Field
                                    label="Booked Date & Time"
                                    value={`${bookingDetails.bookingDate}, ${bookingDetails.bookingTime}`}
                                />
                            </>
                        )}
                        <Field
                            label="Amount Payable"
                            value={`$${bookingDetails.totalPrice.toFixed(2)}`}
                        />
                        <Field
                            label="Payment Method"
                            value={
                                bookingDetails.paymentMethod ===
                                PaymentMethod.PAYNOW
                                    ? 'PayNow to UEN'
                                    : 'Cash'
                            }
                        />
                        {bookingDetails.paymentMethod ===
                            PaymentMethod.PAYNOW && (
                            <>
                                <Field
                                    label="Entity Name"
                                    value={bookingDetails.condo?.name}
                                />
                                <View style={styles.rowContainer}>
                                    <Field
                                        label="UEN"
                                        value={bookingDetails.condo?.uen}
                                    />
                                    <CopyButton
                                        valueToCopy={bookingDetails.condo.uen}
                                        isValueCopiedShown={isUENCopiedShown}
                                        setIsValueCopiedShown={
                                            setIsUENCopiedShown
                                        }
                                        showCopiedFeedback={showCopiedFeedback.bind(
                                            null,
                                            CopiedFeedback.UEN,
                                        )}
                                    />
                                </View>
                                <Field
                                    label="Uploaded Image"
                                    value={
                                        selectedPhoto
                                            ? selectedPhoto.name
                                            : null
                                    }
                                />
                                <View style={styles.textCardContainer}>
                                    <PrimaryButton
                                        label="Upload PayNow Receipt"
                                        testID=""
                                        onPress={handleChoosePhoto}
                                    />
                                </View>
                                <View style={styles.textCardContainer}>
                                    <NormalText style={styles.italicGreyText}>
                                        Maximum file size is 2MB.
                                    </NormalText>
                                    <NormalText style={styles.italicGreyText}>
                                        Only .jpeg, .jpg or .png file allowed.
                                    </NormalText>
                                </View>
                            </>
                        )}
                    </RoundContainer>
                </View>

                <View style={styles.buttonContainer}>
                    {bookingDetails.paymentMethod === PaymentMethod.PAYNOW && (
                        <PrimaryButton
                            disabled={selectedPhoto === null}
                            label="Proceed"
                            testID=""
                            onPress={handleSubmitPhoto}
                            style={[styles.buttonMarginBottom]}
                        />
                    )}
                    <PrimaryButton
                        label="Back to My Bookings"
                        testID="back-to-my-bookings"
                        onPress={() => {
                            resetNewBookingRedux();
                            navigation.navigate(
                                PageName.MANAGE_BOOKINGS_SCREEN,
                            );
                        }}
                    />
                </View>
            </ScrollableContainer>

            <PopModal
                visibility={error.hasError}
                modalTitle={error.title}
                modalBody={error.message}
                confirmText="OK"
                onConfirm={() => {
                    setError({
                        hasError: false,
                        title: '',
                        message: '',
                    });
                }}
            />
            <Loading />
        </View>
    );
};

export default BookingPending;
