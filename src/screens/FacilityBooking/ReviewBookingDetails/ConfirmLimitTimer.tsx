import moment from 'moment';
import React, { FC, useEffect, useRef, useState } from 'react';
import { Colors } from 'react-native-ui-lib';
import RoundContainer from '../../../components/container/RoundContainer';
import styles from './ReviewBookingDetailsStyle';
import NormalText from '../../../components/ui/titles/NormalText';
import { AppState, AppStateStatus, View } from 'react-native';
import {
    createBookingLockThunk,
    deleteBookingLockThunk,
} from '../../../store/manageBooking/thunk';
import { useAppDispatch } from '../../../store/hook';
import { loadingActions } from '../../../store/loading/slice';
import {
    ReviewBookingDetailError,
    ISetErrorDialog,
} from '../../../models/errorsType/ReviewBookingDetailsErrorTypes';
import PageName from '../../../navigation/PageNameEnum';

interface AppStateRef {
    currentAppState: AppStateStatus;
    startTime: moment.Moment | null;
}

interface ConfirmLimitTimerProps {
    facilityId: number;
    bookingDate: string;
    startTime: string;
    endTime: string;
    setError: React.Dispatch<React.SetStateAction<ISetErrorDialog | null>>;
}
const ConfirmLimitTimer: FC<ConfirmLimitTimerProps> = ({
    facilityId,
    bookingDate,
    startTime,
    endTime,
    setError,
}) => {
    const dispatch = useAppDispatch();
    const { setLoading } = loadingActions;
    const bookingLockRef = useRef<number | null>(null);
    const [confirmTimerInSec, setConfirmTimerInSec] = useState<number | null>(
        null,
    );
    const [durationInSec, setDurationInSec] = useState<number>(0);
    const appState = useRef<AppStateRef>({
        currentAppState: AppState.currentState,
        startTime: null,
    });
    useEffect(() => {
        let timer: NodeJS.Timer;
        dispatch(setLoading(true));
        dispatch(
            createBookingLockThunk({
                facilityId,
                bookingDate,
                startTime,
                endTime,
            }),
        )
            .unwrap()
            .then(resp => {
                if (resp?.statusCode === 409) {
                    setError({
                        title: ReviewBookingDetailError.SLOT_NOT_AVAILABLE,
                        message:
                            'Your selected booking slot is blocked as it has been selected by another resident. \n\nPlease select a different slot.',
                    });
                    dispatch(setLoading(false));
                    return;
                }
                if (resp?.statusCode !== 200) {
                    setError({
                        title: ReviewBookingDetailError.GENERAL,
                        message:
                            'Unable to take the booking slot. \n\nPlease contact administrator',
                    });

                    dispatch(setLoading(false));
                    return;
                }
                if (!resp?.response?.lockDetail.id) {
                    setError({
                        title: ReviewBookingDetailError.GENERAL,
                        message:
                            'Unable to take the booking slot. \n\nPlease contact administrator',
                    });
                    dispatch(setLoading(false));
                    return;
                }
                dispatch(setLoading(false));
                bookingLockRef.current = resp.response.lockDetail.id;
                appState.current.startTime = moment(
                    resp.response.lockDetail.createdAt,
                );
                setConfirmTimerInSec(resp.response.lockDurationInMinutes * 60);
                setDurationInSec(resp.response.lockDurationInMinutes * 60);
                timer = setInterval(() => {
                    setConfirmTimerInSec(prev => {
                        if (prev === null) return null;

                        if (prev > 0) {
                            return prev - 1; // Reduce by 1 second
                        } else {
                            setError({
                                title: ReviewBookingDetailError.BOOKING_TIME_OUT,
                                message:
                                    'Your selected booking slot has been released as booking was not confirmed within the time limit. \n\nPlease make your booking again.',
                                backToPage: PageName.SELECT_TIME_SCREEN,
                            });
                            clearInterval(timer);
                            return prev; // Timer reached 0, no change
                        }
                    });
                }, 1000); // Update the timer every 1000 milliseconds (1 second)
            });

        return () => {
            if (bookingLockRef.current) {
                dispatch(deleteBookingLockThunk(bookingLockRef.current));
            }
            clearInterval(timer);
        };
    }, [
        bookingDate,
        dispatch,
        endTime,
        facilityId,
        setError,
        setLoading,
        startTime,
    ]);

    AppState.addEventListener('change', nextAppState => {
        if (
            nextAppState === 'active' &&
            appState.current.currentAppState === 'background' &&
            appState.current.startTime
        ) {
            const currentTime = moment();
            const timerStartTime = moment(appState.current.startTime);
            const elapsedTimeInSec = Math.floor(
                moment.duration(currentTime.diff(timerStartTime)).asSeconds(),
            );
            const updatedTimerInSec = durationInSec - elapsedTimeInSec;
            setConfirmTimerInSec(updatedTimerInSec > 0 ? updatedTimerInSec : 0);
        }

        appState.current.currentAppState = nextAppState;
    });
    return (
        <>
            {confirmTimerInSec !== null && (
                <View style={styles.generalContainer}>
                    <RoundContainer
                        style={{
                            ...styles.rowContainer,
                            ...styles.confirmTimeLimitContainer,
                            backgroundColor: Colors.$backgroundErrorLight,
                        }}>
                        <NormalText testID="review-booking-details.confirm-time-limit-text">
                            {`Please confirm your booking within the time limit: ${moment
                                .utc(confirmTimerInSec * 1000)
                                .format('mm:ss')}`}
                        </NormalText>
                    </RoundContainer>
                </View>
            )}
        </>
    );
};

export default ConfirmLimitTimer;
