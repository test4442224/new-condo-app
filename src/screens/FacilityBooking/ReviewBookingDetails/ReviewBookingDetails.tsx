import React, { FC, useEffect, useState } from 'react';
import { useAppSelector, useAppDispatch } from '../../../store/hook';
import styles from './ReviewBookingDetailsStyle';
import { View } from 'react-native';
import PrimaryButton from '../../../components/ui/buttons/PrimaryButton';
import SecondaryButton from '../../../components/ui/buttons/SecondaryButton';
import NormalText from '../../../components/ui/titles/NormalText';
import ImageSwiper from '../../../components/ui/swiper/ImageSwiper';
import SubHeaderText from '../../../components/ui/titles/SubHeaderText';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import RoundContainer from '../../../components/container/RoundContainer';
import { ReviewBookingDetailsNavProps } from '../../../navigation/NavProps';
import { createBookingThunk } from '../../../store/manageBooking/thunk';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faLocationDot } from '@fortawesome/free-solid-svg-icons';
import PopModal from '../../../components/modal/PopModal';
import PageName from '../../../navigation/PageNameEnum';
import { newBookingActions } from '../../../store/newBooking/slice';
import { IBookingSuccessProps } from '../BookingComplete/BookingCompleteSuccess';
import HeaderText from '../../../components/ui/titles/HeaderText';
import ScrollableContainer from '../../../components/container/ScrollableContainer';
import PriceBreakdownList from '../../../components/priceBreakdown/PriceBreakdownList';
import { PaymentMethod } from '../../../store/manageBooking/slice';
import Loading from '../../../components/ui/loading/Loading';
import { loadingActions } from '../../../store/loading/slice';
import { convertFacilityFees, getImageUrls } from './ReviewBookingDetailsUtils';
import { calculateBookingHours } from '../../../utils/CalculateBookingHours';
import {
    ReviewBookingDetailError,
    ISetErrorDialog,
} from '../../../models/errorsType/ReviewBookingDetailsErrorTypes';
import ConfirmLimitTimer from './ConfirmLimitTimer';
import ComputeConsecutiveTimeSlotUtil from '../../../utils/ComputeConsecutiveTimeSlots';
import { ITimeSlot } from '../../../models/booking/TimeSlotModel';
import { toDateFormat } from '../../../utils/dateTimeUtil';
import PaymentMethodSelect from '../../../components/PaymentMethodSelect';
import { IBookingPendingProps } from '../BookingComplete/BookingCompletePending';
import AsyncStorage from '@react-native-async-storage/async-storage';
import BoldText from '../../../components/ui/titles/BoldText';

const ReviewBookingDetails: FC<ReviewBookingDetailsNavProps> = ({
    navigation,
}) => {
    const dispatch = useAppDispatch();
    const { setLoading } = loadingActions;
    const [isCancelBookingModelVisible, setIsCancelBookingModelVisible] =
        useState<boolean>(false);
    const [isPayNowConfirmModelVisible, setIsPayNowConfirmModelVisible] =
        useState<boolean>(false);
    const [
        isRequiredPaymentSelectModelVisible,
        setIsRequiredPaymentSelectModelVisible,
    ] = useState<boolean>(false);
    const [isFailedToFetchDetails, setIsFailedToFetchDetails] =
        useState<boolean>(false);

    const [error, setError] = useState<ISetErrorDialog | null>(null);

    const {
        chosenFacility,
        chosenDate,
        paymentMethod,
        chosenTimeSlots: selectedTimeSlots,
    } = useAppSelector(state => state.newBooking);
    const token = useAppSelector(state => state.user.token);
    const selectedPaymentMethod = useAppSelector(
        state => state.newBooking.paymentMethod,
    );

    const isPaidBooking = chosenFacility?.facilityFees?.length !== 0;
    let chosenTimeSlot: ITimeSlot | null = null;
    if (selectedTimeSlots) {
        chosenTimeSlot =
            ComputeConsecutiveTimeSlotUtil.getTimeSlots(selectedTimeSlots);
    }

    const bookingDate = toDateFormat(chosenDate!);
    const bookingTiming = `${chosenTimeSlot?.start} - ${chosenTimeSlot?.end}`;

    const {
        resetAvailableFacilityTypes,
        resetFacilityAvailableTimeSlots,
        setChosenDate,
        setChosenFacility,
        setChosenFacilityType,
        setChosenTimeSlot,
        setPaymentMethod,
    } = newBookingActions;

    useEffect(() => {
        if (
            chosenDate === null ||
            chosenFacility === null ||
            chosenTimeSlot === null
        ) {
            setIsFailedToFetchDetails(true);
        }
    }, [chosenDate, chosenFacility, chosenTimeSlot, navigation]);

    const resetNewBookingRedux = () => {
        dispatch(setPaymentMethod(null));
        dispatch(resetAvailableFacilityTypes());
        dispatch(resetFacilityAvailableTimeSlots());
        dispatch(setChosenDate(null));
        dispatch(setChosenFacility(null));
        dispatch(setChosenFacilityType(null));
        dispatch(setChosenTimeSlot(null));
    };

    const backButtonOnClickHandler = () => {
        navigation.navigate(PageName.SELECT_TIME_SCREEN);
    };

    const onCreateBookingHandler = async (
        proceedWithoutConfirmation: boolean = false,
    ) => {
        if (isPaidBooking && paymentMethod === null) {
            setIsRequiredPaymentSelectModelVisible(true);
            return;
        }
        if (
            !proceedWithoutConfirmation &&
            isPaidBooking &&
            paymentMethod === PaymentMethod.PAYNOW
        ) {
            setIsPayNowConfirmModelVisible(true);
            return;
        }
        dispatch(setLoading(true));
        const response = await dispatch(
            createBookingThunk({
                facilityId: chosenFacility!.id,
                bookingDate: bookingDate,
                startTime: chosenTimeSlot!.start,
                endTime: chosenTimeSlot!.end,
                token: token!,
                paymentMethod: isPaidBooking ? paymentMethod : null,
            }),
        ).unwrap();
        dispatch(setLoading(false));
        if (response?.statusCode === 200) {
            if (isPaidBooking) {
                let condoName = await AsyncStorage.getItem('condo_name');
                let condoUEN = await AsyncStorage.getItem('condo_uen');
                if (!condoName || !condoUEN) {
                    console.error(`condo name or uen doesn't exist`);
                    return;
                }
                let condo = {
                    name: condoName,
                    uen: condoUEN,
                };
                const pendingBookingDetails: IBookingPendingProps = {
                    bookingReferenceId:
                        response.response.createdBooking.bookingReferenceId,
                    bookingId: response.response.createdBooking.id,
                    condo,
                    facilityName: chosenFacility!.name,
                    facilityLocation: chosenFacility!.location,
                    bookingDate,
                    bookingTime: bookingTiming,
                    totalPrice: +response.response.createdBooking.totalPrice,
                    paymentDueAt: response.response.createdBooking.paymentDueAt,
                    paymentMethod:
                        response.response.createdBooking.paymentMethod,
                    createdAt: response.response.createdBooking.createdAt,
                };
                navigation.replace(PageName.BOOKING_PENDING_SCREEN, {
                    bookingDetails: pendingBookingDetails,
                });
                return;
            }
            const successBookingDetails: IBookingSuccessProps = {
                bookingReferenceId:
                    response.response.createdBooking.bookingReferenceId,
                bookingId: response.response.createdBooking.id,
                facilityName: chosenFacility!.name,
                facilityLocation: chosenFacility!.location,
                bookingDate: bookingDate,
                bookingTime: bookingTiming,
                totalPrice: +response.response.createdBooking.totalPrice,
                paymentMethod:
                    chosenFacility?.facilityFees?.length === 0
                        ? null
                        : PaymentMethod.CASH,
                createdAt: null,
            };
            navigation.replace(PageName.BOOKING_SUCCESS_SCREEN, {
                bookingDetails: successBookingDetails,
            });

            return;
        }
        if (response?.statusCode === 409) {
            setError({
                title: ReviewBookingDetailError.CREATION_FAILED,
                message:
                    'The booking slot that you want is no longer available. Please select a different booking date/time.',
            });
            return;
        }
        if (response?.statusCode === 400) {
            setError({
                title: ReviewBookingDetailError.CREATION_FAILED,
                message:
                    response.error ||
                    'Failed to make the booking. Please try again.',
            });
            return;
        }
        setError({
            title: ReviewBookingDetailError.CREATION_FAILED,
            message: 'Failed to make the booking.Please try again',
        });
    };

    return (
        <>
            <View style={styles.rootContainer}>
                <HeaderBar
                    testID={'review-booking-details.header-bar-text'}
                    title={'Submit Booking'}
                    onBackButtonPress={backButtonOnClickHandler}
                />
                {chosenDate && chosenFacility && chosenTimeSlot && (
                    <ScrollableContainer>
                        <ImageSwiper
                            imageURL={getImageUrls(chosenFacility)}
                            autoplay={false}
                        />
                        <View style={styles.generalContainer}>
                            <HeaderText
                                center={false}
                                style={styles.titleText}
                                testID="booking-details.facility-name">
                                {chosenFacility?.name || ''}
                            </HeaderText>

                            <RoundContainer style={styles.rowContainer}>
                                <FontAwesomeIcon
                                    icon={faLocationDot}
                                    style={styles.icon}
                                    size={13}
                                />
                                <NormalText testID="review-booking-details.location-text">
                                    {chosenFacility?.location}
                                </NormalText>
                            </RoundContainer>
                        </View>

                        <View style={styles.generalContainer}>
                            <NormalText testID="review-booking-details.description-text">
                                {chosenFacility.description}
                            </NormalText>
                        </View>

                        <View style={styles.generalContainer}>
                            <View style={styles.rowContainer}>
                                <NormalText style={styles.italicText}>
                                    {chosenFacility?.pax === 0
                                        ? `No Recommended Capacity`
                                        : `Recommended Capacity: ${chosenFacility?.pax} pax`}
                                </NormalText>
                            </View>
                        </View>

                        <View style={styles.bookingContainer}>
                            <SubHeaderText>
                                {'Your Booking Details:'}
                            </SubHeaderText>

                            <View style={styles.rowContainer}>
                                <NormalText
                                    style={[
                                        styles.cellLeftContainer,
                                        styles.boldText,
                                    ]}>
                                    {'Date: '}
                                </NormalText>
                                <NormalText
                                    style={styles.cellRightContainer}
                                    testID="review-booking-details.chosen-date-text">
                                    {bookingDate}
                                </NormalText>
                            </View>
                            <View style={styles.rowContainer}>
                                <NormalText
                                    testID="review-booking-details.time-text"
                                    style={[
                                        styles.cellLeftContainer,
                                        styles.boldText,
                                    ]}>
                                    {'Time: '}
                                </NormalText>
                                <NormalText
                                    style={styles.cellRightContainer}
                                    testID="review-booking-details.time-slot-text">
                                    {bookingTiming}
                                </NormalText>
                            </View>
                            {isPaidBooking && (
                                <>
                                    <View
                                        style={styles.pricebreakdownContainer}>
                                        <PriceBreakdownList
                                            pageName={
                                                PageName.REVIEW_BOOKING_DETAILS_SCREEN
                                            }
                                            breakdownList={convertFacilityFees(
                                                chosenFacility,
                                            )}
                                            bookingHours={calculateBookingHours(
                                                PageName.REVIEW_BOOKING_DETAILS_SCREEN,
                                                chosenTimeSlot?.start,
                                                chosenTimeSlot.end,
                                            )}
                                        />
                                    </View>
                                    <PaymentMethodSelect
                                        onPressPayNowHandler={() =>
                                            dispatch(
                                                setPaymentMethod(
                                                    PaymentMethod.PAYNOW,
                                                ),
                                            )
                                        }
                                        onPressCashHandler={() =>
                                            dispatch(
                                                setPaymentMethod(
                                                    PaymentMethod.CASH,
                                                ),
                                            )
                                        }
                                        isViaUen={true}
                                        selectedPaymentMethod={
                                            selectedPaymentMethod
                                        }
                                    />
                                </>
                            )}
                        </View>
                        <ConfirmLimitTimer
                            setError={setError}
                            facilityId={chosenFacility.id}
                            bookingDate={bookingDate}
                            startTime={chosenTimeSlot.start}
                            endTime={chosenTimeSlot.end}
                        />
                        <View>
                            <View style={styles.buttonOuterContainer}>
                                <View style={styles.buttonContainer}>
                                    <SecondaryButton
                                        label="Cancel"
                                        testID="review-booking-details.cancel-btn"
                                        onPress={() =>
                                            setIsCancelBookingModelVisible(true)
                                        }
                                    />
                                </View>

                                <View style={styles.buttonContainer}>
                                    <PrimaryButton
                                        label="Confirm Booking"
                                        testID="confirm"
                                        onPress={() => {
                                            onCreateBookingHandler();
                                        }}
                                    />
                                </View>
                            </View>
                        </View>
                    </ScrollableContainer>
                )}

                <Loading />
            </View>
            <PopModal
                visibility={error !== null}
                modalTitle={error?.title || ''}
                modalBody={<NormalText>{error?.message || ''}</NormalText>}
                confirmText="Okay"
                onConfirm={() => {
                    if (
                        error?.backToPage &&
                        error.backToPage === PageName.SELECT_TIME_SCREEN
                    ) {
                        navigation.navigate(PageName.SELECT_TIME_SCREEN);
                        return;
                    }
                    navigation.navigate(PageName.SELECT_DATE_SCREEN);
                }}
            />
            <PopModal
                modalTitle="Confirm Submission?"
                modalBody={
                    <NormalText>
                        You are making a booking using{' '}
                        <BoldText>PayNow via UEN</BoldText>. You are required to
                        screenshot the proof of transaction for verification
                        purposes.
                    </NormalText>
                }
                visibility={isPayNowConfirmModelVisible}
                confirmText="Confirm"
                onCancel={() => setIsPayNowConfirmModelVisible(false)}
                onConfirm={() => {
                    setIsPayNowConfirmModelVisible(false);
                    onCreateBookingHandler(true);
                }}
            />
            <PopModal
                modalTitle="Payment Method is required"
                modalBody={
                    <NormalText>
                        Please select payment method to confirm booking
                    </NormalText>
                }
                visibility={isRequiredPaymentSelectModelVisible}
                confirmText="Okay"
                onConfirm={() => {
                    setIsRequiredPaymentSelectModelVisible(false);
                }}
            />
            <PopModal
                modalTitle="Cancel Booking"
                modalBody={
                    <NormalText>
                        All current booking details will be lost. Confirm cancel
                        booking?
                    </NormalText>
                }
                visibility={isCancelBookingModelVisible}
                confirmText="Confirm"
                onCancel={() => setIsCancelBookingModelVisible(false)}
                onConfirm={() => {
                    setIsCancelBookingModelVisible(false);
                    resetNewBookingRedux();
                    navigation.navigate(PageName.MANAGE_BOOKINGS_SCREEN);
                }}
            />
            <PopModal
                modalTitle="Failed to Retrieve Details"
                modalBody={<NormalText>Please try again.</NormalText>}
                visibility={isFailedToFetchDetails}
                confirmText="Okay"
                onConfirm={() => {
                    setIsFailedToFetchDetails(false);
                    navigation.navigate(PageName.SELECT_FACILITY_TYPE_SCREEN);
                }}
            />
        </>
    );
};

export default ReviewBookingDetails;
