// import React from 'react';
// import ReviewBookingDetails from './ReviewBookingDetails';
// import { NativeStackNavigationProp } from '@react-navigation/native-stack';
// import NavParamList from '../../../navigation/NavParmList';
// import PageName from '../../../navigation/PageNameEnum';
// import { RouteProp } from '@react-navigation/native';
// import { fireEvent, render, waitFor } from '@testing-library/react-native';
// import { ITimeSlot } from '../../../models/booking/TimeSlotModel';
// import { Alert } from 'react-native';

jest.mock('../../../navigation/NavProps', () => ({}));

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

jest.mock('../../../store/hook', () => {
    return {
        useAppSelector: jest.fn(),
        useAppDispatch: jest.fn(() => jest.fn()),
    };
});

// jest.mock('../../../store/ManageBooking/thunk', () => ({
//     ...jest.requireActual('../../../store/ManageBooking/thunk'),
//     createBookingThunk: jest.fn().mockReturnValue({
//         type: 'booking/createBooking',
//         payload: {
//             response: {
//                 id: 123,
//                 facilityId: 1,
//                 userId: 2,
//                 bookingDate: '2023-05-14',
//                 startTime: '15:00:00',
//                 endTime: '16:00:00',
//                 totalPrice: 0,
//                 token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEifQ.tS7RfrG4Ai3IB1SRFjlxrJq2zPAbUSU-MS10JfqqfaE',
//             },
//             statusCode: 200,
//         },
//     }),
// }));

// const mockNavigation: Partial<
//     NativeStackNavigationProp<
//         NavParamList,
//         PageName.REVIEW_BOOKING_DETAILS_SCREEN
//     >
// > = {
//     navigate: jest.fn(),
// };

// let mockRoute: RouteProp<NavParamList, PageName.REVIEW_BOOKING_DETAILS_SCREEN> =
//     {
//         key: 'ReviewBookingDetailsTestKey',
//         name: PageName.REVIEW_BOOKING_DETAILS_SCREEN,
//     };

xdescribe('ReviewBookingDetails Component', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('', () => {});

    // const facility = {
    //     id: 1,
    //     name: 'Badminton Hall 1',
    //     location: 'Condo Mock Level 2 (near Blk 60)',
    //     description:
    //         'Located in Condo Mock, Court 1 is indoor to ensure a delightful workout experience, and it comes with water collers near the exits.',
    //     primaryFacilityImageUrl:
    //         'http://localhost:3001/image/facility/functionroom.png',
    //     facilityImageUrls: [
    //         'http://localhost:3001/image/facility/functionroom.png',
    //         'http://localhost:3001/image/facility/functionroom.png',
    //         'http://localhost:3001/image/facility/functionroom.png',
    //     ],
    // };

    // // GMT+0730
    // const date = '2023-05-14';
    // const startime = '1970-01-01T07:30:00.000Z';
    // const endtime = '1970-01-01T08:30:00.000Z';

    // const time: ITimeSlot = {
    //     start: startime,
    //     end: endtime,
    // };

    // const { useAppSelector, useAppDispatch } = require('../../../store/hook');
    // const mockDispatch = jest.fn();
    // useAppDispatch.mockReturnValue(mockDispatch);

    // useAppSelector.mockImplementation((selector: any) =>
    //     selector({
    //         newBooking: {
    //             chosenFacility: facility,
    //             chosenDate: date,
    //             chosenTimeSlot: time,
    //         },
    //         user: {
    //             token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEifQ.tS7RfrG4Ai3IB1SRFjlxrJq2zPAbUSU-MS10JfqqfaE',
    //         },
    //         userRole: {
    //             currentUserRole: {
    //                 userId: 1,
    //             },
    //         },
    //     }),
    // );

    // it('should render the page correctly', async () => {
    //     const page = render(
    //         <ReviewBookingDetails
    //             navigation={
    //                 mockNavigation as NativeStackNavigationProp<
    //                     NavParamList,
    //                     PageName.REVIEW_BOOKING_DETAILS_SCREEN
    //                 >
    //             }
    //             route={mockRoute}
    //         />,
    //     );

    //     expect(page).toMatchSnapshot();
    // });

    // it('should retrieve and display the fetched details correctly', async () => {
    //     const { getByText, getByTestId } = render(
    //         <ReviewBookingDetails
    //             navigation={
    //                 mockNavigation as NativeStackNavigationProp<
    //                     NavParamList,
    //                     PageName.REVIEW_BOOKING_DETAILS_SCREEN
    //                 >
    //             }
    //             route={mockRoute}
    //         />,
    //     );

    //     await waitFor(() => getByText('Badminton Hall 1'));
    //     expect(getByText('Badminton Hall 1')).toBeTruthy();
    //     expect(
    //         getByTestId('review-booking-details.location-text'),
    //     ).toBeDefined();
    //     expect(getByTestId('review-booking-details.pax-text')).toBeDefined();
    //     expect(
    //         getByTestId('review-booking-details.description-text'),
    //     ).toBeDefined();
    //     expect(
    //         getByTestId('review-booking-details.chosen-date-text'),
    //     ).toBeDefined();
    //     expect(getByTestId('review-booking-details.time-text')).toBeDefined();
    //     expect(
    //         getByTestId('review-booking-details.time-slot-text'),
    //     ).toBeDefined();
    // });

    // it('should render the confirm and cancel button correctly', () => {
    //     const page = render(
    //         <ReviewBookingDetails
    //             navigation={
    //                 mockNavigation as NativeStackNavigationProp<
    //                     NavParamList,
    //                     PageName.REVIEW_BOOKING_DETAILS_SCREEN
    //                 >
    //             }
    //             route={mockRoute}
    //         />,
    //     );

    //     const confirmBtn = page.getByTestId(
    //         'review-booking-details.confirm-btn',
    //     );
    //     const cancelBtn = page.getByTestId('review-booking-details.cancel-btn');
    //     expect(confirmBtn).toBeDefined();
    //     expect(cancelBtn).toBeDefined();
    // });

    // it('should handle booking creation on confirm press', async () => {
    //     useAppDispatch.mockReturnValue((action: any) => {
    //         if (action.type === 'booking/createBooking') {
    //             const successResponse = {
    //                 response: {
    //                     id: 123,
    //                     facilityId: 1,
    //                     userId: 2,
    //                     bookingDate: '2023-05-14',
    //                     startTime: '15:00:00',
    //                     endTime: '16:00:00',
    //                     totalPrice: 0,
    //                     token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEifQ.tS7RfrG4Ai3IB1SRFjlxrJq2zPAbUSU-MS10JfqqfaE',
    //                 },
    //                 statusCode: 200,
    //             };

    //             return {
    //                 ...successResponse,
    //                 unwrap: function () {
    //                     return successResponse;
    //                 },
    //             };
    //         }
    //     });

    //     const { getByTestId } = render(
    //         <ReviewBookingDetails
    //             navigation={
    //                 mockNavigation as NativeStackNavigationProp<
    //                     NavParamList,
    //                     PageName.REVIEW_BOOKING_DETAILS_SCREEN
    //                 >
    //             }
    //             route={mockRoute}
    //         />,
    //     );

    //     const primaryButton = getByTestId('review-booking-details.confirm-btn');
    //     await waitFor(() => fireEvent.press(primaryButton));

    //     expect(mockNavigation.navigate).toBeCalledTimes(1);
    //     expect(mockNavigation.navigate).toHaveBeenCalledWith(
    //         PageName.BOOKING_SUCCESS_SCREEN,
    //         { bookid: 123 },
    //     );
    // });

    // it('shows the modal when cancel button is pressed', () => {
    //     const { getByTestId, queryByText } = render(
    //         <ReviewBookingDetails
    //             navigation={
    //                 mockNavigation as NativeStackNavigationProp<
    //                     NavParamList,
    //                     PageName.REVIEW_BOOKING_DETAILS_SCREEN
    //                 >
    //             }
    //             route={mockRoute}
    //         />,
    //     );
    //     const btn = getByTestId('review-booking-details.cancel-btn');
    //     fireEvent.press(btn);
    //     expect(queryByText('Cancel Booking')).toBeTruthy();
    //     expect(
    //         queryByText(
    //             'All current booking details will be lost. Confirm cancel booking?',
    //         ),
    //     ).toBeTruthy();
    // });

    // it('should show alert when needed', async () => {
    //     useAppSelector.mockImplementation((selector: any) =>
    //         selector({
    //             newBooking: {
    //                 chosenFacility: null,
    //                 chosenDate: null,
    //                 chosenTimeSlot: null,
    //             },
    //             user: {
    //                 token: '',
    //             },
    //             userRole: {
    //                 currentUserRole: {
    //                     userId: -1,
    //                 },
    //             },
    //         }),
    //     );

    //     const alertSpy = jest.spyOn(Alert, 'alert');

    //     render(
    //         <ReviewBookingDetails
    //             navigation={
    //                 mockNavigation as NativeStackNavigationProp<
    //                     NavParamList,
    //                     PageName.REVIEW_BOOKING_DETAILS_SCREEN
    //                 >
    //             }
    //             route={mockRoute}
    //         />,
    //     );

    //     expect(Alert.alert).toBeCalledWith(
    //         'Fail to Retrieve Details',
    //         'Please try again.',
    //     );

    //     alertSpy.mockRestore();
    // });
});
