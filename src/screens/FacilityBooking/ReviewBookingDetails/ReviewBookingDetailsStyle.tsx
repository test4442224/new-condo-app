import { StyleSheet } from 'react-native';
import { Spacings, Typography } from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    confirmTimeLimitContainer: {
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s2,
        justifyContent: 'center',
    },
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
    },
    generalContainer: {
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
    labelContainer: {
        paddingVertical: Spacings.s1,
        paddingHorizontal: Spacings.s4,
    },
    bookingContainer: {
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
    rowContainer: {
        flexDirection: 'row',
        paddingVertical: Spacings.s1,
        alignItems: 'center',
    },
    colContainer: {
        paddingHorizontal: Spacings.s1,
        gap: Spacings.s1,
        flexDirection: 'column',
    },
    buttonOuterContainer: {
        flex: 1,
        flexDirection: 'row',
        gap: Spacings.s3,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: Spacings.s4,
        marginTop: Spacings.s3,
    },
    titleText: {
        marginVertical: Spacings.s2,
    },
    buttonContainer: {
        flex: 1,
    },
    icon: {
        color: Colors.$iconPrimary,
        marginHorizontal: Spacings.s2,
    },
    boldText: { fontWeight: WEIGHT_TYPES.BOLD },
    italicText: {
        fontStyle: 'italic',
    },
    cellLeftContainer: {
        flex: 1,
    },
    cellRightContainer: {
        flex: 2,
    },
    labelDotText: {
        color: Colors.$textDisabled,
    },
    subheaderText: {
        ...Typography.text70,
    },
    pricebreakdownContainer: {
        marginVertical: Spacings.s3,
    },
    warningText: {
        fontWeight: WEIGHT_TYPES.BOLD,
        color: Colors.$textWarning,
        paddingTop: Spacings.s3,
    },
});
