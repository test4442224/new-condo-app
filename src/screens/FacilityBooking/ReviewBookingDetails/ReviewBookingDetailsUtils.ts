import { getImageUrl } from '../../../Constants/api';
import { IPriceSummary } from '../../../models/booking/PriceBreakdown';
import { IFacility } from '../../../models/facility/FacilityModel';

export const convertFacilityFees = (
    chosenFacility: IFacility | null,
): IPriceSummary[] => {
    let convertedFacilityFees: IPriceSummary[] = [];
    if (chosenFacility && chosenFacility.facilityFees) {
        convertedFacilityFees = chosenFacility.facilityFees.map(item => ({
            id: item.id,
            description: item.feeDescription,
            unitPriceAmount: item.feeAmount,
            priceType: item.feeType,
            // no subtotalPriceAmount
        }));
    }
    return convertedFacilityFees;
};

export const getImageUrls = (facility: IFacility): string[] => {
    const urls = [
        facility.primaryFacilityImageUrl || '',
        ...(facility.facilityImageUrls || ''),
    ];

    return urls.filter(url => url.length > 0).map(url => getImageUrl(url));
};
