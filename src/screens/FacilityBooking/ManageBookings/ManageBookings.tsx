import { View, Pressable } from 'react-native';
import React, { FC, useCallback, useRef, useState } from 'react';
import styles from './ManageBookingsStyle';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import TabButton from '../../../components/ui/buttons/TabButton';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import { ManageBookingsNavProps } from '../../../navigation/NavProps';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import PageName from '../../../navigation/PageNameEnum';
import { useFocusEffect } from '@react-navigation/native';
import {
    ManageBookingAttributes,
    bookingAction,
} from '../../../store/manageBooking/slice';
import Toast from 'react-native-simple-toast';
import { userActions } from '../../../store/user/UserActions';
import { isAfterCurrentTime } from '../../../utils/dateTimeUtil';
import BookingList from '../../../components/lists/bookingList/BookingList';
import {
    useLazyGetAllBookingsQuery,
    useLazyGetBookingQuery,
} from '../../../store/manageBooking/api';
import ErrorTypeVerificationUtilService from '../../../utils/error/ErrorTypeVerification.util';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { RtkErrorHandler } from '../../../utils/error/RtkErrorHandler.util';
import Loading from '../../../components/ui/loading/Loading';
import { loadingActions } from '../../../store/loading/slice';
import useRtkResponse from '../../../utils/hooks/useRtkResponse';

const ManageBookings: FC<ManageBookingsNavProps> = ({ navigation, route }) => {
    const dispatch = useAppDispatch();
    const isFirstLoadRef = useRef<boolean>(true);
    const token = useAppSelector(state => state.user?.token);
    const [isUpcomingTabActive, setUpcomingTabActive] = useState<boolean>(true);
    const isUpcomingBooking = (booking: ManageBookingAttributes) => {
        return !booking.cancelledAt && isAfterCurrentTime(booking.endDateTime);
    };
    const { setLoading } = loadingActions;

    const [
        getAllBookings,
        { isFetching: isFetchingBookings, error: getAllBookingsError },
    ] = useLazyGetAllBookingsQuery();

    const [getBooking] = useLazyGetBookingQuery();

    const onPressUpcomingTabHandler = () => {
        setUpcomingTabActive(true);
    };

    const onPressPastTabHandler = () => {
        setUpcomingTabActive(false);
    };

    const onAddButtonPressHandler = () => {
        navigation.navigate(PageName.SELECT_FACILITY_TYPE_SCREEN);
        setUpcomingTabActive(true);
    };

    const onBackButtonPressHandler = () => {
        navigation.navigate(PageName.HOME_PAGE_SCREEN);
        setUpcomingTabActive(true);
    };

    const onBookingCardPressHandler = async (item: ManageBookingAttributes) => {
        let title;
        if (isUpcomingTabActive) title = 'Upcoming';
        else title = 'Past';

        try {
            dispatch(setLoading(true));

            const booking = await getBooking(item.id).unwrap();

            dispatch(bookingAction.selectBookingForDetail(booking));
            navigation.navigate(PageName.BOOKING_DETAILS_SCREEN, {
                bookingTitle: title,
            });
        } catch (err: any) {
            const isFetchBaseQueryError =
                ErrorTypeVerificationUtilService.isFetchBaseQueryError(err);

            if (isFetchBaseQueryError && err.status === 401) {
                Toast.show(
                    'Your session has ended. Please login again.',
                    Toast.LONG,
                );

                logout();

                return;
            }

            const errorMessage = RtkErrorHandler.getMessage(err);

            Toast.show(errorMessage, Toast.LONG);
        } finally {
            dispatch(setLoading(false));
        }
    };

    const logout = useCallback(async () => {
        dispatch(userActions.logout());

        await AsyncStorage.clear();
    }, [dispatch]);

    let fetchedBookings = useAppSelector(state => state.booking.bookingsArray);
    const upcomingBookings = fetchedBookings.filter(isUpcomingBooking);
    const pastBookings = fetchedBookings.filter(
        booking => !isUpcomingBooking(booking),
    );

    pastBookings.sort((a, b) => {
        return (
            new Date(b.startDateTime).getTime() -
            new Date(a.startDateTime).getTime()
        );
    });

    useFocusEffect(
        React.useCallback(() => {
            if (isFirstLoadRef.current) {
                isFirstLoadRef.current = false;
                return;
            }

            if (token) {
                if (
                    route.params?.tabStatus === 'Upcoming' ||
                    route.params?.tabStatus === undefined
                )
                    setUpcomingTabActive(true);
                else setUpcomingTabActive(false);
                getAllBookings(undefined);
            }
        }, [
            token,
            getAllBookings,
            setUpcomingTabActive,
            route.params?.tabStatus,
        ]),
    );

    // Handle getAllBookings Error
    useRtkResponse(isFetchingBookings, getAllBookingsError);

    return (
        <>
            <View style={styles.rootContainer}>
                <View style={styles.headersContainer}>
                    <View style={styles.headerBarContainer}>
                        <HeaderBar
                            title="My Bookings"
                            onBackButtonPress={onBackButtonPressHandler}
                            testID="manage-bookings.header-bar-text"
                            additionalIcon={
                                <View>
                                    <Pressable
                                        onPress={onAddButtonPressHandler}
                                        testID="make-new-booking">
                                        <FontAwesomeIcon
                                            icon={faPlus}
                                            style={styles.addButton}
                                            size={28}
                                        />
                                    </Pressable>
                                </View>
                            }
                        />
                    </View>
                </View>
                <View style={styles.buttonsContainer}>
                    <View
                        style={
                            isUpcomingTabActive
                                ? styles.pressed
                                : styles.buttonContainer
                        }>
                        <TabButton
                            testID="manage-bookings.upcoming-booking-tab"
                            onPress={onPressUpcomingTabHandler}
                            isActive={isUpcomingTabActive}>
                            Upcoming
                        </TabButton>
                    </View>
                    <View
                        style={
                            !isUpcomingTabActive
                                ? styles.pressed
                                : styles.buttonContainer
                        }>
                        <TabButton
                            testID="manage-bookings.past-booking-tab"
                            onPress={onPressPastTabHandler}
                            isActive={!isUpcomingTabActive}>
                            Past
                        </TabButton>
                    </View>
                </View>
                {isUpcomingTabActive ? (
                    <BookingList
                        bookings={upcomingBookings}
                        noBookingLabel="You have no upcoming bookings at the moment."
                        onPressed={booking =>
                            onBookingCardPressHandler(booking)
                        }
                    />
                ) : (
                    <BookingList
                        bookings={pastBookings}
                        noBookingLabel="
                                You have no past bookings at the moment."
                        onPressed={booking =>
                            onBookingCardPressHandler(booking)
                        }
                    />
                )}
            </View>
            <Loading />
        </>
    );
};

export default ManageBookings;
