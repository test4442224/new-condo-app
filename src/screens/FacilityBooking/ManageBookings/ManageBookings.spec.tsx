import React from 'react';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import NavParamList from '../../../navigation/NavParmList';
import PageName from '../../../navigation/PageNameEnum';
import { NavigationContainer, RouteProp } from '@react-navigation/native';
import { render } from '@testing-library/react-native';
import ManageBookings from './ManageBookings';

jest.mock('../../../navigation/NavProps', () => ({}));

jest.mock('@fortawesome/react-native-fontawesome', () => ({
    FontAwesomeIcon: '',
}));

jest.mock('../../../store/hook', () => {
    return {
        useAppSelector: jest.fn(),
        useAppDispatch: jest.fn(() => jest.fn()),
    };
});

jest.mock('react-native-simple-toast', () => {
    return {
        show: jest.fn(),
    };
});

jest.mock('@react-native-async-storage/async-storage', () => {
    return {
        setItem: jest.fn(),
    };
});

// jest.mock('../../../store/ManageBooking/thunk', () => ({
//     ...jest.requireActual('../../../store/ManageBooking/thunk'),
//     fetchAllBookingsThunk: jest.fn().mockReturnValue({
//         type: 'booking/fetchAllBookings',
//         payload: {
//             response: {
//                 id: 1,
//                 facilityId: 1,
//                 userId: 1,
//                 bookingDate: '2023-08-16T00:00:00.000Z',
//                 startTime: '15:00:00',
//                 endTime: '19:00:00',
//                 totalPrice: 0,
//                 paymentStatus: 'PAID',
//                 createdAt: '2023-08-14T08:00:23.979Z',
//                 updatedAt: '2023-08-16T08:00:23.979Z',
//                 user: {
//                     fullName: 'User full name',
//                     phoneNumber: 87645321,
//                 },
//                 facility: {
//                     name: 'Badminton Court 1 - Dummy',
//                 },
//                 facilityImage: {
//                     images: ['image.png'],
//                 },
//             },
//             statusCode: 200,
//         },
//     }),
// }));

const mockNavigation: Partial<
    NativeStackNavigationProp<NavParamList, PageName.MANAGE_BOOKINGS_SCREEN>
> = {
    navigate: jest.fn(),
};

let mockRoute: RouteProp<NavParamList, PageName.MANAGE_BOOKINGS_SCREEN> = {
    key: 'ManageBookingsTestKey',
    name: PageName.MANAGE_BOOKINGS_SCREEN,
};

xdescribe('ManageBookings Component', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    const facilityBooking = [
        {
            bookingDate: 'Aug 16, 2023',
            startTime: '3PM',
            endTime: '7PM',
            facility: { name: 'Badminton Court 2 - Dummy' },
            facilityImage: { images: ['image.png'] },
        },
    ];

    const { useAppSelector, useAppDispatch } = require('../../../store/hook');
    const mockDispatch = jest.fn();
    useAppDispatch.mockReturnValue(mockDispatch);

    useAppSelector.mockImplementation((selector: any) =>
        selector({
            booking: {
                bookingsArray: facilityBooking,
            },
            user: {
                token: 'abc',
            },
        }),
    );

    xit('should render the page correctly', async () => {
        const page = render(
            <NavigationContainer>
                <ManageBookings
                    navigation={
                        mockNavigation as NativeStackNavigationProp<
                            NavParamList,
                            PageName.MANAGE_BOOKINGS_SCREEN
                        >
                    }
                    route={mockRoute}
                />
                ,
            </NavigationContainer>,
        );

        expect(page).toMatchSnapshot();
    });

    xit('should retrieve and display the fetched details correctly', () => {
        const { getByTestId } = render(
            <NavigationContainer>
                <ManageBookings
                    navigation={
                        mockNavigation as NativeStackNavigationProp<
                            NavParamList,
                            PageName.MANAGE_BOOKINGS_SCREEN
                        >
                    }
                    route={mockRoute}
                />
                ,
            </NavigationContainer>,
        );

        expect(getByTestId('manage-bookings.header-bar-text')).toBeDefined();
        expect(getByTestId('manage-bookings.add-btn')).toBeDefined();
        expect(getByTestId('UpcomingBookingTab')).toBeDefined();
        expect(getByTestId('PastBookingTab')).toBeDefined();
        expect(getByTestId('data-display')).toBeDefined();
    });

    // it('should render the booking cards if there are bookings', async() => {
    //     const { getByTestId} = render(
    //         <NavigationContainer>
    //             <ManageBookings
    //                 navigation={
    //                     mockNavigation as NativeStackNavigationProp<
    //                         NavParamList,
    //                         PageName.MANAGE_BOOKINGS_SCREEN
    //                     >
    //                 }
    //                 route={mockRoute}
    //             />
    //             ,
    //         </NavigationContainer>,
    //     );
    //     await waitFor(() => {expect(getByTestId('flatlist')).toBeDefined()});
    // });

    xit('should render a text if there are no bookings', () => {
        useAppSelector.mockImplementationOnce((selector: any) =>
            selector({
                booking: {
                    bookingsArray: [],
                },
                user: {
                    token: 'abc',
                },
            }),
        );
        const { getByTestId } = render(
            <NavigationContainer>
                <ManageBookings
                    navigation={
                        mockNavigation as NativeStackNavigationProp<
                            NavParamList,
                            PageName.MANAGE_BOOKINGS_SCREEN
                        >
                    }
                    route={mockRoute}
                />
                ,
            </NavigationContainer>,
        );

        expect(getByTestId('manage-bookings.no-bookings')).toBeDefined();
    });
});
