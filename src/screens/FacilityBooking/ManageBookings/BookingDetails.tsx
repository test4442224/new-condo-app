import React, { FC, useEffect, useState } from 'react';
import { useAppSelector, useAppDispatch } from '../../../store/hook';
import { View } from 'react-native';
import styles from './BookingDetailsStyle';
import { BookingDetailsNavProps } from '../../../navigation/NavProps';
import { cancelBookingThunk } from '../../../store/manageBooking/cancelBookingThunk';
import PopModal from '../../../components/modal/PopModal';
import PageName from '../../../navigation/PageNameEnum';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import PrimaryButton from '../../../components/ui/buttons/PrimaryButton';
import ImageSwiper from '../../../components/ui/swiper/ImageSwiper';
import { getImageUrl } from '../../../Constants/api';
import RoundContainer from '../../../components/container/RoundContainer';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faLocationDot } from '@fortawesome/free-solid-svg-icons';
import NormalText from '../../../components/ui/titles/NormalText';
import Badge from '../../../components/ui/badge/Badge';
import SubHeaderText from '../../../components/ui/titles/SubHeaderText';
import HeaderText from '../../../components/ui/titles/HeaderText';
import ScrollableContainer from '../../../components/container/ScrollableContainer';
import Colors from '../../../Constants/Colors';
import { formatTimeRange, toDateFormat } from '../../../utils/dateTimeUtil';
import PriceBreakdownList from '../../../components/priceBreakdown/PriceBreakdownList';
import { IPriceSummary } from '../../../models/booking/PriceBreakdown';
import {
    PaymentMethod,
    PaymentStatus,
} from '../../../store/manageBooking/slice';
import { generateCancelReason } from '../../../utils/GenerateCancelReason';
import { generatePaymentStatusBadge } from '../../../utils/GeneratePaymentStatusBadge';
import { loadingActions } from '../../../store/loading/slice';
import Loading from '../../../components/ui/loading/Loading';
import { calculateBookingHours } from '../../../utils/CalculateBookingHours';
import { generatePendingPaymentMsg } from './bookingDetailsUtils';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { IBookingPendingProps } from '../BookingComplete/BookingCompletePending';
import { useGetAllFacilityImageUrlsQuery } from '../../../store/facilityImage/api';
import { QueryStatus } from '@reduxjs/toolkit/dist/query';
import { RtkErrorHandler } from '../../../utils/error/RtkErrorHandler.util';
import Toast from 'react-native-simple-toast';

const BookingDetails: FC<BookingDetailsNavProps> = ({ navigation, route }) => {
    const { bookingTitle } = route.params;

    const dispatch = useAppDispatch();
    const { setLoading } = loadingActions;
    const [cancelModalVisibility, setCancelModalVisibility] =
        useState<boolean>(false);
    const [cancelSuccessModal, setSuccessCancelModal] = useState({
        isVisible: false,
        message: '',
    });
    const [isFailedToFetchDetails, setIsFailedToFetchDetails] =
        useState<boolean>(false);
    const [isFailedToCancelBooking, setIsFailedToCancelBooking] =
        useState<boolean>(false);

    const token = useAppSelector(state => state.user.token);
    const selectedBooking = useAppSelector(
        state => state.booking.selectedBooking,
    );

    const facilityName = selectedBooking!.facility.name;
    const bookingDate = toDateFormat(selectedBooking!.startDateTime);
    const bookingTime = formatTimeRange({
        start: selectedBooking!.startDateTime,
        end: selectedBooking!.endDateTime,
        separator: ' - ',
    });
    const isBookingCancelled =
        selectedBooking?.cancelledAt || selectedBooking?.isCancelledByUser;
    const isPendingPayNowPayment =
        selectedBooking?.paymentStatus === PaymentStatus.PENDING_PAYMENT &&
        selectedBooking?.paymentMethod === PaymentMethod.PAYNOW;

    const {
        status: getAllFacilityImageUrlsStatus,
        error: getAllFacilityImageUrlsError,
    } = useGetAllFacilityImageUrlsQuery(
        {
            facilityId: selectedBooking?.facilityId || 0,
        },
        {
            skip: typeof selectedBooking?.facilityId !== 'number',
            refetchOnMountOrArgChange: true,
        },
    );

    const navigateToPendingBooking = async () => {
        let condoName = await AsyncStorage.getItem('condo_name');
        let condoUEN = await AsyncStorage.getItem('condo_uen');
        if (!condoName || !condoUEN) {
            console.error(`condo name or uen doesn't exist`);
            return;
        }
        let condo = {
            name: condoName,
            uen: condoUEN,
        };
        let bookingDetails: IBookingPendingProps = {
            bookingReferenceId: selectedBooking?.bookingReferenceId!,
            bookingId: selectedBooking?.id!,
            paymentMethod: selectedBooking?.paymentMethod!,
            paymentDueAt: selectedBooking?.paymentDueAt!,
            // TODO: fix totalPrice data type in the backend
            totalPrice: parseInt(`${selectedBooking?.totalPrice!}`, 10),
            condo,
        };
        navigation.navigate(PageName.BOOKING_PENDING_SCREEN, {
            bookingDetails: bookingDetails,
        });
    };
    const images = useAppSelector(state => state.facilityImage.images);

    const getImageUrls = (imageUrls: string[]): string[] => {
        if (!imageUrls) {
            return [];
        }

        return imageUrls
            .filter(url => url.length > 0)
            .map(url => getImageUrl(url));
    };
    const cancelBooking = async () => {
        let bookingId = selectedBooking?.id;

        if (!(bookingId && token)) {
            setIsFailedToFetchDetails(true);
            return;
        }

        try {
            dispatch(setLoading(true));
            await dispatch(
                cancelBookingThunk({
                    bookingId,
                    token,
                }),
            ).unwrap();

            const message =
                selectedBooking?.paymentStatus === PaymentStatus.FREE ||
                selectedBooking?.paymentStatus === PaymentStatus.PENDING_PAYMENT
                    ? 'Your booking has been successfully cancelled.'
                    : 'Your booking has been successfully cancelled. Please refer to your email inbox for your refund instructions.';
            setSuccessCancelModal({ isVisible: true, message });
        } catch (rejectedValue) {
            setIsFailedToCancelBooking(true);
        } finally {
            dispatch(setLoading(false));
        }
    };

    const backButtonOnClickHandler = () => {
        let tabName: string = 'Upcoming';
        if (
            selectedBooking?.paymentStatus === PaymentStatus.CANCELLED ||
            selectedBooking?.paymentStatus === PaymentStatus.PENDING_REFUND
        )
            tabName = 'Past';

        navigation.navigate(PageName.MANAGE_BOOKINGS_SCREEN, {
            tabStatus: tabName,
        });
    };
    const convertPriceBreakdown = (): IPriceSummary[] => {
        let convertedPriceBreakdown: IPriceSummary[] = [];
        if (selectedBooking && selectedBooking.priceBreakdowns) {
            convertedPriceBreakdown = selectedBooking.priceBreakdowns.map(
                item => ({
                    id: item.id,
                    description: item.description,
                    unitPriceAmount: item.unitPriceAmount,
                    priceType: item.priceType,
                    subtotalPriceAmount: item.subtotalPriceAmount,
                }),
            );
        }
        return convertedPriceBreakdown;
    };

    // Handle Error
    useEffect(() => {
        if (
            getAllFacilityImageUrlsStatus === QueryStatus.rejected &&
            getAllFacilityImageUrlsError
        ) {
            const errorMessage = RtkErrorHandler.getMessage(
                getAllFacilityImageUrlsError,
            );

            Toast.show(errorMessage, Toast.LONG);
        }
    }, [getAllFacilityImageUrlsError, getAllFacilityImageUrlsStatus]);

    return (
        <>
            <View style={styles.rootContainer}>
                <HeaderBar
                    testID="booking-details.header-bar-text"
                    title={`View ${bookingTitle} Booking`}
                    onBackButtonPress={backButtonOnClickHandler}
                />

                <ScrollableContainer>
                    <ImageSwiper
                        imageURL={getImageUrls(images)}
                        autoplay={false}
                    />
                    <View style={styles.generalContainer}>
                        <HeaderText
                            style={styles.titleText}
                            center={false}
                            testID="booking-details.facility-name">
                            {selectedBooking!.facility.name}
                        </HeaderText>

                        <RoundContainer style={styles.rowContainer}>
                            <FontAwesomeIcon
                                icon={faLocationDot}
                                style={styles.icon}
                                size={13}
                            />
                            <NormalText testID="booking-details.location">
                                {selectedBooking?.facility.location}
                            </NormalText>
                        </RoundContainer>
                    </View>

                    <View style={styles.generalContainer}>
                        <NormalText testID="booking-details.description">
                            {selectedBooking?.facility.description}
                        </NormalText>
                    </View>

                    <View style={styles.generalContainer}>
                        <NormalText style={styles.italicText}>
                            {selectedBooking?.facility.pax === 0
                                ? `No Recommended Capacity`
                                : `Recommended Capacity: ${selectedBooking?.facility.pax} pax`}
                        </NormalText>
                    </View>

                    <View style={styles.bookingContainer}>
                        <View
                            style={[
                                styles.rowContainer,
                                styles.badgeContainer,
                            ]}>
                            <SubHeaderText>
                                {'Your Booking Details: '}
                            </SubHeaderText>

                            {selectedBooking && (
                                <Badge
                                    details={generatePaymentStatusBadge(
                                        selectedBooking,
                                    )}
                                    textStyles={styles.badge}
                                    accessibilityLabel="android-booking-status-badge"
                                    testID="ios-booking-status-badge"
                                />
                            )}
                        </View>

                        <View>
                            <View style={styles.rowContainer}>
                                <NormalText
                                    style={[
                                        styles.cellLeftContainer,
                                        styles.boldText,
                                    ]}>
                                    {'Reference No.: '}
                                </NormalText>
                                <NormalText
                                    style={styles.cellRightContainer}
                                    testID="booking-details.booking-id-text">
                                    {selectedBooking?.bookingReferenceId}
                                </NormalText>
                            </View>
                            <View style={styles.rowContainer}>
                                <NormalText
                                    style={[
                                        styles.cellLeftContainer,
                                        styles.boldText,
                                    ]}>
                                    {'Booked Date : '}
                                </NormalText>
                                <NormalText
                                    style={styles.cellRightContainer}
                                    testID="booking-details.booking-date-text">
                                    {bookingDate}
                                </NormalText>
                            </View>
                            <View style={styles.rowContainer}>
                                <NormalText
                                    style={[
                                        styles.cellLeftContainer,
                                        styles.boldText,
                                    ]}>
                                    {'Booked Time: '}
                                </NormalText>
                                <NormalText
                                    style={styles.cellRightContainer}
                                    testID="booking-details.booking-timing-text">
                                    {bookingTime}
                                </NormalText>
                            </View>
                        </View>

                        {isBookingCancelled ? (
                            <>
                                <View style={styles.cancelContainer}>
                                    <View style={styles.rowContainer}>
                                        <NormalText
                                            style={[
                                                styles.cellLeftContainer,
                                                styles.boldText,
                                                styles.redTitleText,
                                            ]}>
                                            {`Cancel\nComment: `}
                                        </NormalText>
                                        <NormalText
                                            style={[
                                                styles.cellRightContainer,
                                                { color: Colors.$textError },
                                            ]}
                                            testID="booking-details.booking-cancelled-text">
                                            {generateCancelReason(
                                                selectedBooking,
                                            )}
                                        </NormalText>
                                    </View>
                                </View>
                            </>
                        ) : (
                            <>
                                {selectedBooking?.paymentStatus ===
                                PaymentStatus.PENDING_PAYMENT ? (
                                    <NormalText
                                        style={styles.pendingText}
                                        testID="booking-details.pending-payment-text">
                                        {generatePendingPaymentMsg(
                                            selectedBooking?.paymentMethod,
                                            selectedBooking?.paymentDueAt,
                                        )}
                                    </NormalText>
                                ) : null}
                            </>
                        )}

                        <View style={styles.generalContainer}>
                            <NormalText
                                center={true}
                                style={styles.labelDotText}>
                                ................................................
                            </NormalText>
                        </View>

                        {selectedBooking?.priceBreakdowns ? (
                            <PriceBreakdownList
                                paymentMethod={selectedBooking?.paymentMethod!}
                                pageName={PageName.BOOKING_DETAILS_SCREEN}
                                breakdownList={convertPriceBreakdown()}
                                paymentStatus={selectedBooking.paymentStatus}
                                paidAt={selectedBooking!.paidAt}
                                totalPrice={selectedBooking.totalPrice}
                                bookingHours={calculateBookingHours(
                                    PageName.BOOKING_DETAILS_SCREEN,
                                    selectedBooking?.startDateTime,
                                    selectedBooking.endDateTime,
                                )}
                            />
                        ) : null}
                    </View>

                    {selectedBooking?.isCancellable && (
                        <View style={styles.buttonOuterContainer}>
                            <View style={styles.buttonContainer}>
                                {isPendingPayNowPayment && (
                                    <PrimaryButton
                                        style={styles.payNowBtn}
                                        label="Make Payment via PayNow"
                                        testID="booking-details.paynow-payment-btn"
                                        onPress={() => {
                                            navigateToPendingBooking();
                                        }}
                                    />
                                )}
                                <PrimaryButton
                                    accessibilityLabel="android-booking-details.cancel-btn"
                                    label="Cancel Booking"
                                    testID={'ios-booking-details.cancel-btn'}
                                    onPress={() =>
                                        setCancelModalVisibility(true)
                                    }
                                />
                            </View>
                        </View>
                    )}
                </ScrollableContainer>
            </View>
            <Loading />
            <PopModal
                modalTitle="Successfully Cancelled Booking"
                modalBody={
                    <NormalText>{cancelSuccessModal.message}</NormalText>
                }
                visibility={cancelSuccessModal.isVisible}
                confirmText="Okay"
                onConfirm={() => {
                    setSuccessCancelModal({ isVisible: false, message: '' });
                }}
            />
            <PopModal
                modalTitle="Cancel Booking"
                modalBody={
                    <NormalText>
                        Do you wish to cancel your booking? This cannot be
                        undone.
                    </NormalText>
                }
                modalData={`Facility: ${facilityName}\nBooked Date: ${bookingDate}\nBooked Time: ${bookingTime}`}
                visibility={cancelModalVisibility}
                confirmText="Confirm"
                onCancel={() => setCancelModalVisibility(false)}
                onConfirm={() => {
                    setCancelModalVisibility(false);
                    cancelBooking();
                }}
            />
            <PopModal
                modalTitle="Failed to Retrieve Details"
                modalBody={<NormalText>Please try again.</NormalText>}
                visibility={isFailedToFetchDetails}
                confirmText="Okay"
                onConfirm={() => {
                    setIsFailedToFetchDetails(false);
                    navigation.navigate(PageName.HOME_PAGE_SCREEN);
                }}
            />
            <PopModal
                modalTitle="Failed to Cancel Booking"
                modalBody={
                    <NormalText>Your booking cannot be cancelled.</NormalText>
                }
                visibility={isFailedToCancelBooking}
                confirmText="Okay"
                onConfirm={() => {
                    setIsFailedToCancelBooking(false);
                }}
            />
        </>
    );
};

export default BookingDetails;
