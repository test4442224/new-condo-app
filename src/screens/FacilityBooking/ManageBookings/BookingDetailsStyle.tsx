import { StyleSheet } from 'react-native';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';
import { Spacings, Typography } from 'react-native-ui-lib';
export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
    },
    generalContainer: {
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
    bookingContainer: {
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
    cancelContainer: {
        marginTop: Spacings.s4,
    },
    rowContainer: {
        flexDirection: 'row',
        paddingVertical: Spacings.s1,
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    buttonOuterContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: Spacings.s4,
        paddingVertical: Spacings.s5,
    },
    buttonContainer: {
        flex: 1,
    },
    icon: {
        color: Colors.$iconPrimary,
        marginHorizontal: Spacings.s2,
    },
    boldText: {
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    italicText: {
        fontStyle: 'italic',
    },
    badgeContainer: {
        justifyContent: 'space-between',
    },
    titleText: {
        marginVertical: Spacings.s2,
    },
    cellLeftContainer: {
        flex: 1,
    },
    cellRightContainer: {
        flex: 2,
    },
    labelDotText: {
        color: Colors.$textDisabled,
    },
    redTitleText: {
        color: Colors.$textError,
        height: '100%',
    },
    pendingText: {
        fontWeight: WEIGHT_TYPES.BOLD,
        color: Colors.$textWarning,
        paddingTop: Spacings.s3,
    },
    payNowBtn: {
        marginBottom: Spacings.s4,
    },
    badge: {
        ...Typography.text80,
    },
});
