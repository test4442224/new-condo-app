import { PaymentMethod } from '../../../store/manageBooking/slice';
import { to12HourFormat, toDateFormat } from '../../../utils/dateTimeUtil';

export const generatePendingPaymentMsg = (
    paymentMethod: PaymentMethod | null,
    paymentDueAt: Date,
): string => {
    const formattedPaymentDueAt = `${toDateFormat(
        paymentDueAt,
    )}, ${to12HourFormat(paymentDueAt)}`;
    if (paymentMethod === null) {
        return '';
    }
    if (paymentMethod === PaymentMethod.PAYNOW) {
        return `Note: If payment receipt is not received by ${formattedPaymentDueAt}, booking will be cancelled.`;
    }
    return `Note: If payment is not received by ${formattedPaymentDueAt}, booking will be cancelled.`;
};
