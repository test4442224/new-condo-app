import { StyleSheet } from 'react-native';
import Colors from '../../../Constants/Colors';
import { BorderRadiuses, Spacings } from 'react-native-ui-lib';

const styles = StyleSheet.create({
    rootContainer: {
        backgroundColor: Colors.$backgroundDefault,
        flex: 1,
    },
    generalContainer: {
        paddingVertical: Spacings.s1,
        paddingHorizontal: Spacings.s4,
    },
    textInputContainer: {
        marginTop: Spacings.s2,
    },
    inputBox: {
        flex: 1,
        borderWidth: 1,
        borderRadius: BorderRadiuses.br30,
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
    eyeIcon: {
        position: 'absolute',
        right: Spacings.s3,
    },
    eyeContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    passwordContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: Spacings.s2,
    },
    textContainer: {
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s1,
        fontWeight: 'bold',
        color: Colors.$textPrimary,
    },
    buttonContainer: {
        marginVertical: Spacings.s4,
    },
    errorTextContainer: {
        width: '100%',
    },
    errorText: {
        color: Colors.$textError,
    },
});

export default styles;
