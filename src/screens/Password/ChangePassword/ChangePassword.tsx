import { Keyboard, TextInput, TouchableOpacity, View } from 'react-native';
import React, { FC, useEffect, useState } from 'react';
import HeaderBar from '../../../components/headerbar/HeaderBar';
import { ChangePasswordScreenNavProps } from '../../../navigation/NavProps';
import PageName from '../../../navigation/PageNameEnum';
import styles from './ChangePasswordStyle';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import NormalText from '../../../components/ui/titles/NormalText';
import PasswordChecklist from '../../../components/ui/passwordChecklist/PasswordChecklist';
import PrimaryButton from '../../../components/ui/buttons/PrimaryButton';
import { checkPassword } from '../../../utils/CheckPassword';
import { useChangePasswordApiMutation } from '../../../store/user/userApi';
import { useAppSelector } from '../../../store/hook';
import Toast from 'react-native-simple-toast';
import { QueryStatus } from '@reduxjs/toolkit/query/react';
import ErrorTypeVerificationUtilService from '../../../utils/error/ErrorTypeVerification.util';
import {
    ISetErrorDialog,
    PasswordError,
} from '../../../models/errorsType/PasswordErrorTypes';
import PopModal from '../../../components/modal/PopModal';
import { HitSlopArea } from '../../LoginPage/HitSlopArea';

const ChangePassword: FC<ChangePasswordScreenNavProps> = ({ navigation }) => {
    const token = useAppSelector(state => state.user.token);
    const [currentPassword, setCurrentPassword] = useState<string>('');
    const [newPassword, setNewPassword] = useState<string>('');
    const [confirmPassword, setConfirmPassword] = useState<string>('');
    const [isCurrentPasswordHidden, setIsCurrentPasswordHidden] =
        useState<boolean>(true);
    const [isNewPasswordHidden, setIsNewPasswordHidden] =
        useState<boolean>(true);
    const [isConfirmPasswordHidden, setIsConfirmPasswordHidden] =
        useState<boolean>(true);
    const [newPasswordError, setNewPasswordError] = useState<string>('');
    const [confirmPasswordError, setConfirmPasswordError] =
        useState<string>('');
    const [arePasswordInputsValid, setArePasswordInputsValid] =
        useState<boolean>(false);
    const [isFailedToChangePassword, setIsFailedToChangePassword] =
        useState<boolean>(false);
    const [isFailedToChangePasswordError, setIsFailedToChangePasswordError] =
        useState<ISetErrorDialog | null>(null);
    const onBackButtonPressHandler = () => {
        navigation.navigate(PageName.PROFILE_PAGE_SCREEN);
    };
    const toggleCurrentPasswordVisibility = () => {
        Keyboard.dismiss();
        setIsCurrentPasswordHidden(toggle => !toggle);
    };
    const toggleNewPasswordVisibility = () => {
        Keyboard.dismiss();
        setIsNewPasswordHidden(toggle => !toggle);
    };
    const toggleConfirmPasswordVisibility = () => {
        Keyboard.dismiss();
        setIsConfirmPasswordHidden(toggle => !toggle);
    };
    useEffect(() => {
        const checkPasswordsValidity = () => {
            const passwordCheckResult = checkPassword(newPassword);
            const areOldAndNewPasswordsDifferent =
                currentPassword !== newPassword;
            if (
                currentPassword !== '' &&
                newPassword !== '' &&
                !areOldAndNewPasswordsDifferent
            ) {
                setNewPasswordError(
                    'New password must be different from current password.',
                );
            } else {
                setNewPasswordError('');
            }
            const areNewPasswordsConsistent = newPassword === confirmPassword;
            if (
                currentPassword !== '' &&
                confirmPassword !== '' &&
                !areNewPasswordsConsistent
            ) {
                setConfirmPasswordError('Password does not match.');
            } else {
                setConfirmPasswordError('');
            }
            setArePasswordInputsValid(
                passwordCheckResult === 'strong' &&
                    areNewPasswordsConsistent &&
                    areOldAndNewPasswordsDifferent,
            );
        };
        checkPasswordsValidity();
    }, [confirmPassword, currentPassword, newPassword]);
    const [changePassword, { status, error }] = useChangePasswordApiMutation();
    const onChangePasswordButtonPressHandler = () => {
        changePassword({
            token: token!,
            oldPassword: currentPassword,
            newPassword: confirmPassword,
        });
    };
    useEffect(() => {
        if (status === QueryStatus.fulfilled) {
            Toast.showWithGravity(
                'Password changed successfully!',
                Toast.SHORT,
                Toast.CENTER,
            );
            navigation.navigate(PageName.PROFILE_PAGE_SCREEN);
        } else if (status === QueryStatus.rejected) {
            if (ErrorTypeVerificationUtilService.isFetchBaseQueryError(error)) {
                setIsFailedToChangePasswordError({
                    title: PasswordError.CHANGE_PASSWORD,
                    message: 'Oops! Something went wrong. Please try again.',
                });
                setIsFailedToChangePassword(true);
            }
        }
    }, [error, navigation, status]);

    return (
        <View style={styles.rootContainer}>
            <HeaderBar
                title="Change Password"
                onBackButtonPress={onBackButtonPressHandler}
            />
            <View style={styles.generalContainer}>
                <View style={styles.textInputContainer}>
                    <NormalText style={styles.textContainer}>
                        Enter Current Password:
                    </NormalText>

                    <View style={styles.passwordContainer}>
                        <TextInput
                            testID={'changepassword.current-password-input'}
                            placeholder={'*************'}
                            onChangeText={text => setCurrentPassword(text)}
                            style={styles.inputBox}
                            secureTextEntry={isCurrentPasswordHidden}
                        />
                        <TouchableOpacity
                            style={styles.eyeContainer}
                            hitSlop={HitSlopArea}
                            onPress={toggleCurrentPasswordVisibility}>
                            <View
                                style={styles.eyeIcon}
                                testID={'changepassword.show-password-btn'}>
                                {isCurrentPasswordHidden ? (
                                    <FontAwesomeIcon icon={faEye} />
                                ) : (
                                    <FontAwesomeIcon icon={faEyeSlash} />
                                )}
                            </View>
                        </TouchableOpacity>
                    </View>
                    <NormalText style={styles.textContainer}>
                        Enter New Password:
                    </NormalText>

                    <View style={styles.passwordContainer}>
                        <TextInput
                            testID={'changepassword.new-password-input'}
                            placeholder={'*************'}
                            onChangeText={text => setNewPassword(text)}
                            style={styles.inputBox}
                            secureTextEntry={isNewPasswordHidden}
                        />
                        <TouchableOpacity
                            style={styles.eyeContainer}
                            hitSlop={HitSlopArea}
                            onPress={toggleNewPasswordVisibility}>
                            <View
                                style={styles.eyeIcon}
                                testID={'changepassword.show-new-password-btn'}>
                                {isNewPasswordHidden ? (
                                    <FontAwesomeIcon icon={faEye} />
                                ) : (
                                    <FontAwesomeIcon icon={faEyeSlash} />
                                )}
                            </View>
                        </TouchableOpacity>
                    </View>
                    {newPasswordError !== '' && (
                        <View style={styles.errorTextContainer}>
                            <NormalText style={styles.errorText}>
                                {newPasswordError}
                            </NormalText>
                        </View>
                    )}
                    <NormalText style={styles.textContainer}>
                        Re-Enter New Password:
                    </NormalText>

                    <View style={styles.passwordContainer}>
                        <TextInput
                            testID={'changepassword.confirm-password-input'}
                            placeholder={'*************'}
                            onChangeText={text => setConfirmPassword(text)}
                            style={styles.inputBox}
                            secureTextEntry={isConfirmPasswordHidden}
                        />
                        <TouchableOpacity
                            style={styles.eyeContainer}
                            hitSlop={HitSlopArea}
                            onPress={toggleConfirmPasswordVisibility}>
                            <View
                                style={styles.eyeIcon}
                                testID={
                                    'changepassword.show-confirm-password-btn'
                                }>
                                {isConfirmPasswordHidden ? (
                                    <FontAwesomeIcon icon={faEye} />
                                ) : (
                                    <FontAwesomeIcon icon={faEyeSlash} />
                                )}
                            </View>
                        </TouchableOpacity>
                    </View>
                    {confirmPasswordError !== '' && (
                        <View style={styles.errorTextContainer}>
                            <NormalText style={styles.errorText}>
                                {confirmPasswordError}
                            </NormalText>
                        </View>
                    )}
                </View>
                <PasswordChecklist password={newPassword} />
                <View style={styles.buttonContainer}>
                    <PrimaryButton
                        label="Change Password"
                        onPress={onChangePasswordButtonPressHandler}
                        disabled={
                            !currentPassword ||
                            !newPassword ||
                            !confirmPassword ||
                            !arePasswordInputsValid
                        }
                    />
                </View>
            </View>
            <PopModal
                modalTitle={isFailedToChangePasswordError?.title || ''}
                modalBody={
                    <NormalText>
                        {isFailedToChangePasswordError?.message || ''}
                    </NormalText>
                }
                visibility={isFailedToChangePassword}
                confirmText="Okay"
                onConfirm={() => {
                    setIsFailedToChangePassword(false);
                    navigation.navigate(PageName.PROFILE_PAGE_SCREEN);
                }}
            />
        </View>
    );
};

export default ChangePassword;
