import { StyleSheet } from 'react-native';
import { BorderRadiuses, Spacings } from 'react-native-ui-lib';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
        alignItems: 'center',
    },
    generalContainer: {
        width: '100%',
        paddingVertical: Spacings.s1,
        paddingHorizontal: Spacings.s4,
    },
    logoContainer: {
        marginTop: Spacings.s1,
        alignItems: 'center',
        justifyContent: 'center',
        height: '30%',
    },
    logo: {
        height: '80%',
    },
    textContainer: {
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s1,
        fontWeight: 'bold',
        color: Colors.$textPrimary,
    },
    buttonOuterContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        gap: Spacings.s3,
    },
    buttonContainer: {
        flex: 1,
        marginHorizontal: Spacings.s4,
        marginVertical: Spacings.s3,
    },
    passwordContainer: {
        flexDirection: 'row',
    },
    inputBox: {
        flex: 1,
        borderWidth: 1,
        borderRadius: BorderRadiuses.br30,
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
    eyeIcon: {
        position: 'absolute',
        right: Spacings.s3,
    },
    eyeContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerText: {
        marginBottom: Spacings.s5,
    },
});
