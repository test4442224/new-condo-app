import React, { FC, useEffect, useState } from 'react';
import styles from './ResetPasswordStyle';
import { Image, TextInput, TouchableOpacity, View } from 'react-native';
import HeaderText from '../../../components/ui/titles/HeaderText';
import NormalText from '../../../components/ui/titles/NormalText';
import PrimaryButton from '../../../components/ui/buttons/PrimaryButton';
import PopModal from '../../../components/modal/PopModal';
import { ResetPasswordProps } from '../../../navigation/NavProps';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { resetPassword } from '../../../store/user/UserActions';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import { loadingActions } from '../../../store/loading/slice';
import Loading from '../../../components/ui/loading/Loading';
import { checkPassword } from '../../../utils/CheckPassword';
import PasswordChecklist from '../../../components/ui/passwordChecklist/PasswordChecklist';
import PageName from '../../../navigation/PageNameEnum';
import { useLazyGetUserEmailApiQuery } from '../../../store/user/userApi';
import { PasswordError } from '../../../models/errorsType/PasswordErrorTypes';

const ResetPassword: FC<ResetPasswordProps> = ({ navigation, route }) => {
    const dispatch = useAppDispatch();
    const { setLoading } = loadingActions;
    const [getUserEmail] = useLazyGetUserEmailApiQuery();
    useEffect(() => {
        if (route.params) {
            getUserEmail(route.params.encryptedUserId);
        }
    }, [getUserEmail, route.params]);
    const email = useAppSelector(state => state.user.profileDetails.email)!;
    const [password, setPassword] = useState<string>('');
    const [confirmPassword, setConfirmPassword] = useState<string>('');
    const [message, setMessage] = useState<string>('');
    const [modalVisibility, setModalVisibility] = useState<boolean>(false);
    const [isPWHidden, setPWHidden] = useState(true);
    const [isConfirmPWHidden, setConfirmPWHidden] = useState(true);

    const [errorModalVisibility, setErrorModalVisibility] =
        useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState<string>('');

    const onResetPasswordHandler = async () => {
        const result = checkPassword(password);

        if (!result.match('strong')) {
            setErrorMessage(result);
            setErrorModalVisibility(true);
        } else {
            if (password !== confirmPassword) {
                setErrorMessage('Password did not match. Please try again.');
                setErrorModalVisibility(true);
            } else {
                dispatch(setLoading(true));
                const response = await dispatch(
                    resetPassword({
                        email,
                        newPassword: confirmPassword,
                    }),
                ).unwrap();

                dispatch(setLoading(false));
                if (response.statusCode === 200) {
                    setMessage(response.response);
                    setModalVisibility(true);
                } else if (
                    response.statusCode === 400 ||
                    response.statusCode === 404
                ) {
                    setErrorMessage(
                        'Please ensure that you have clicked on the link to reset password.',
                    );
                    setErrorModalVisibility(true);
                } else {
                    setErrorMessage('Something went wrong. Please try again.');
                    setErrorModalVisibility(true);
                }
            }
        }
    };

    const toggleContentVisibilityPW1 = () => {
        setPWHidden(prev1 => !prev1);
    };

    const toggleContentVisibilityPW2 = () => {
        setConfirmPWHidden(prev2 => !prev2);
    };

    return (
        <View style={styles.rootContainer}>
            <View style={styles.logoContainer}>
                <Image
                    testID={'reset-password.logo-image'}
                    style={styles.logo}
                    resizeMode={'contain'}
                    source={require('../../../assets/logo/condo-app-logo.jpg')}
                />
            </View>
            <View style={styles.generalContainer}>
                <HeaderText style={styles.headerText}>
                    Reset Password
                </HeaderText>
                <NormalText style={styles.textContainer}>
                    Enter New Password:
                </NormalText>

                <View style={styles.passwordContainer}>
                    <TextInput
                        testID={'reset-password.password-input'}
                        placeholder={'*************'}
                        onChangeText={pw1 => setPassword(pw1)}
                        style={styles.inputBox}
                        secureTextEntry={isPWHidden}
                    />
                    <TouchableOpacity
                        style={styles.eyeContainer}
                        onPress={toggleContentVisibilityPW1}>
                        <View
                            style={styles.eyeIcon}
                            testID={'reset-password.show-password-btn'}>
                            {isPWHidden ? (
                                <FontAwesomeIcon icon={faEye} />
                            ) : (
                                <FontAwesomeIcon icon={faEyeSlash} />
                            )}
                        </View>
                    </TouchableOpacity>
                </View>

                <NormalText style={styles.textContainer}>
                    Re-enter New Password:
                </NormalText>

                <View style={styles.passwordContainer}>
                    <TextInput
                        testID={'reset-password.confirm-password-input'}
                        placeholder={'*************'}
                        onChangeText={pw2 => setConfirmPassword(pw2)}
                        style={styles.inputBox}
                        secureTextEntry={isConfirmPWHidden}
                    />
                    <TouchableOpacity
                        style={styles.eyeContainer}
                        onPress={toggleContentVisibilityPW2}>
                        <View
                            style={styles.eyeIcon}
                            testID={'reset-password.show-confirm-password-btn'}>
                            {isConfirmPWHidden ? (
                                <FontAwesomeIcon icon={faEye} />
                            ) : (
                                <FontAwesomeIcon icon={faEyeSlash} />
                            )}
                        </View>
                    </TouchableOpacity>
                </View>

                <PasswordChecklist password={password} />
            </View>

            <View style={styles.buttonOuterContainer}>
                <View style={styles.buttonContainer}>
                    <PrimaryButton
                        label="Reset Password"
                        testID="reset-password.reset-password-btn"
                        onPress={onResetPasswordHandler}
                    />
                </View>
            </View>
            <Loading />
            <PopModal
                modalTitle="Reset Password"
                modalBody={<NormalText>{message}</NormalText>}
                visibility={modalVisibility}
                confirmText="Back to Login"
                onConfirm={() => {
                    setModalVisibility(false);
                    navigation.navigate(PageName.LOGIN_SCREEN);
                }}
            />
            <PopModal
                modalTitle={PasswordError.RESET_PASSWORD}
                modalBody={<NormalText>{errorMessage}</NormalText>}
                visibility={errorModalVisibility}
                confirmText="Okay"
                onConfirm={() => {
                    setErrorModalVisibility(false);
                }}
            />
        </View>
    );
};

export default ResetPassword;
