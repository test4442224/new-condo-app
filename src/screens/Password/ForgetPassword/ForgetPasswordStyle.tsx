import { StyleSheet } from 'react-native';
import { BorderRadiuses, Spacings } from 'react-native-ui-lib';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
        alignItems: 'center',
    },
    generalContainer: {
        width: '100%',
        paddingVertical: Spacings.s1,
        paddingHorizontal: Spacings.s4,
    },
    logoContainer: {
        marginTop: Spacings.s1 * 15,
        alignItems: 'center',
        justifyContent: 'center',
        height: '30%',
    },
    logo: {
        justifyContent: 'center',
        height: '80%',
    },
    textContainer: {
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s1,
    },
    inputBoxContainer: {
        width: '100%',
        paddingVertical: Spacings.s6,
        paddingHorizontal: Spacings.s4,
    },
    inputBox: {
        borderWidth: 1,
        borderRadius: BorderRadiuses.br30,
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
    buttonOuterContainer: {
        flexDirection: 'row',
        gap: Spacings.s3,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: Spacings.s4,
        marginTop: Spacings.s3,
    },
    buttonContainer: {
        flex: 1,
    },
});
