import React, { FC, useState } from 'react';
import styles from './ForgetPasswordStyle';
import { ForgetPasswordProps } from '../../../navigation/NavProps';
import { Image, TextInput, View } from 'react-native';
import HeaderText from '../../../components/ui/titles/HeaderText';
import NormalText from '../../../components/ui/titles/NormalText';
import SecondaryButton from '../../../components/ui/buttons/SecondaryButton';
import PrimaryButton from '../../../components/ui/buttons/PrimaryButton';
import PageName from '../../../navigation/PageNameEnum';
import PopModal from '../../../components/modal/PopModal';
import { useAppDispatch } from '../../../store/hook';
import { forgetPassword } from '../../../store/user/UserActions';
import Loading from '../../../components/ui/loading/Loading';
import { loadingActions } from '../../../store/loading/slice';
import { PasswordError } from '../../../models/errorsType/PasswordErrorTypes';

const ForgetPassword: FC<ForgetPasswordProps> = ({ navigation }) => {
    const dispatch = useAppDispatch();
    const { setLoading } = loadingActions;
    const [cancelModalVisibility, setCancelModalVisibility] =
        useState<boolean>(false);

    const [email, setEmail] = useState<string>('');
    const [message, setMessage] = useState<string>('');
    const [errorMessage, setErrorMessage] = useState<string>('');
    const [modalVisibility, setModalVisibility] = useState<boolean>(false);

    const onForgetPasswordHandler = async () => {
        if (!email) {
            setErrorMessage('Please enter an email to continue.');
            setCancelModalVisibility(true);
            return;
        }
        dispatch(setLoading(true));
        const response = await dispatch(
            forgetPassword({
                email: email,
            }),
        ).unwrap();

        dispatch(setLoading(false));

        if (response.statusCode === 200) {
            setMessage(response.response.message);
            setModalVisibility(true);
        } else if (response.statusCode === 404) {
            setErrorMessage(response.response);
            setCancelModalVisibility(true);
        } else if (response.statusCode === 400) {
            setErrorMessage('Invalid Email Format. Please try again.');
            setCancelModalVisibility(true);
        } else {
            setErrorMessage('Something went wrong. Please try again.');
            setCancelModalVisibility(true);
        }
    };

    const backButtonOnClickHandler = () => {
        navigation.navigate(PageName.LOGIN_SCREEN);
    };

    return (
        <View style={styles.rootContainer}>
            <View style={styles.logoContainer}>
                <Image
                    testID={'forget-password.logo-image'}
                    style={styles.logo}
                    resizeMode={'contain'}
                    source={require('../../../assets/logo/condo-app-logo.jpg')}
                />
            </View>
            <View style={styles.generalContainer}>
                <HeaderText>Reset Password</HeaderText>
                <NormalText center={true} style={styles.textContainer}>
                    Please enter the email associated with your account. We will
                    send instructions to reset your password.
                </NormalText>
            </View>
            <View style={styles.inputBoxContainer}>
                <TextInput
                    testID={'forget-password.email-input'}
                    placeholder={'Email'}
                    onChangeText={text => setEmail(text.toLowerCase())}
                    style={styles.inputBox}
                    autoCapitalize={'none'}
                />
            </View>
            <View style={styles.buttonOuterContainer}>
                <View style={styles.buttonContainer}>
                    <SecondaryButton
                        label="Cancel"
                        testID={'forget-password.cancel-btn'}
                        onPress={backButtonOnClickHandler}
                    />
                </View>

                <View style={styles.buttonContainer}>
                    <PrimaryButton
                        label="Reset Password"
                        testID={'forget-password.confirm-btn'}
                        onPress={onForgetPasswordHandler}
                    />
                </View>
            </View>

            <Loading />
            <PopModal
                modalTitle={PasswordError.FORGET_PASSWORD}
                modalBody={<NormalText>{errorMessage}</NormalText>}
                visibility={cancelModalVisibility}
                confirmText="Okay"
                onConfirm={() => {
                    setCancelModalVisibility(false);
                }}
            />
            <PopModal
                modalTitle="Reset Password"
                modalBody={<NormalText>{message}</NormalText>}
                visibility={modalVisibility}
                confirmText="Back to Login"
                onConfirm={() => {
                    setModalVisibility(false);
                    navigation.navigate(PageName.LOGIN_SCREEN);
                }}
            />
        </View>
    );
};

export default ForgetPassword;
