import styles from './TimeSlotListStyle';

import React, { FC, useEffect, useState } from 'react';
import { FlatList, View } from 'react-native';
import NormalText from '../../ui/titles/NormalText';
import { ITimeSlot } from '../../../models/booking/TimeSlotModel';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import { newBookingActions } from '../../../store/newBooking/slice';
import { IFacility } from '../../../models/facility/FacilityModel';
import TimeSlotButton from './TimeSlotButton';
import ComputeConsecutiveTimeSlotUtil from '../../../utils/ComputeConsecutiveTimeSlots';
import PopModal from '../../modal/PopModal';

interface TimeSlotListProps {
    timeSlots: ITimeSlot[];
    facility: IFacility;
    togglePopup: (isVisible: boolean) => void;
}

const TimeSlotList: FC<TimeSlotListProps> = ({
    timeSlots,
    facility,
    togglePopup,
}) => {
    const { chosenTimeSlots, chosenFacility } = useAppSelector(
        state => state.newBooking,
    );

    const dispatch = useAppDispatch();

    const [modalVisibility, setModalVisibility] = useState<boolean>(false);
    const { setChosenFacility, setChosenTimeSlot } = newBookingActions;

    const timeSlotButtonOnPress = (
        timeSlot: ITimeSlot,
        isSelected: boolean,
    ) => {
        return () => {
            // first timeslot selection and change facility id
            if (chosenFacility?.id !== facility.id || !chosenTimeSlots) {
                dispatch(setChosenFacility(facility));
                dispatch(setChosenTimeSlot([timeSlot]));
                return;
            }

            // unselect
            if (isSelected) {
                const indexOfUnselected = chosenTimeSlots.indexOf(timeSlot);
                const isFirstOrLast =
                    indexOfUnselected === 0 ||
                    indexOfUnselected === chosenTimeSlots.length - 1;

                if (isFirstOrLast) {
                    dispatch(
                        setChosenTimeSlot(
                            chosenTimeSlots.filter(
                                chosenTimeSlot => chosenTimeSlot !== timeSlot,
                            ),
                        ),
                    );
                } else {
                    togglePopup(true);
                }
                return;
            }

            const sortedTimeSlot =
                ComputeConsecutiveTimeSlotUtil.sortAscendingTimeSlots([
                    ...chosenTimeSlots,
                    timeSlot,
                ]);

            // not consecutive timeslot
            if (
                !ComputeConsecutiveTimeSlotUtil.isTimeSlotConsecutive(
                    sortedTimeSlot,
                )
            ) {
                togglePopup(true);
                return;
            }

            // consecutive timeslot
            dispatch(setChosenTimeSlot(sortedTimeSlot));
        };
    };

    useEffect(() => {}, [chosenFacility, chosenTimeSlots]);

    return (
        <>
            <View style={styles.rootContainer}>
                <NormalText style={styles.headerText}>Select Slot:</NormalText>
                <View style={styles.timeSlotsContainer}>
                    <FlatList
                        data={timeSlots}
                        renderItem={({ item }) => (
                            <View style={styles.timeSlotButtonContainer}>
                                <TimeSlotButton
                                    facilityId={facility.id}
                                    timeSlot={item}
                                    onPress={timeSlotButtonOnPress}
                                    isSelected={
                                        chosenTimeSlots?.includes(item) || false
                                    }
                                />
                            </View>
                        )}
                        keyExtractor={(_, index) => index.toString()}
                        numColumns={3}
                    />
                </View>
            </View>
            <PopModal
                modalTitle="Nonconsecutive Time Slots"
                modalBody={
                    <NormalText>
                        Please only select consecutive time slots
                    </NormalText>
                }
                visibility={modalVisibility}
                confirmText="Okay"
                onConfirm={() => {
                    setModalVisibility(false);
                }}
            />
        </>
    );
};

export default TimeSlotList;
