import styles from './FacilityTimeSlotListStyle';

import React, { FC, useEffect, useState } from 'react';
import IFacilityAvailableTimeSlot from '../../../models/facility/FacilityAvailableTimeSlot';
import { FlatList } from 'react-native';
import FacilityTimeSlotCard from './FacilityTimeSlotCard';

interface FacilityTimeSlotListProps {
    facilityAvailableTimeSlots: IFacilityAvailableTimeSlot[];
    togglePopup: (isVisible: boolean) => void;
}

const FacilityTimeSlotList: FC<FacilityTimeSlotListProps> = ({
    facilityAvailableTimeSlots,
    togglePopup,
}) => {
    const [displayedArray, setDisplayedArray] = useState<
        IFacilityAvailableTimeSlot[]
    >([]);

    useEffect(() => {
        setDisplayedArray(
            facilityAvailableTimeSlots.filter(
                facilityAvailableTimeSlot =>
                    facilityAvailableTimeSlot.availableTimeSlots.length > 0,
            ),
        );
    }, [facilityAvailableTimeSlots]);

    return (
        <>
            <FlatList
                data={displayedArray}
                renderItem={({ item }) => (
                    <FacilityTimeSlotCard
                        facilityAvailableTimeSlots={item}
                        togglePopup={togglePopup}
                    />
                )}
                keyExtractor={(_, index) => index.toString()}
                contentContainerStyle={styles.list}
            />
        </>
    );
};

export default FacilityTimeSlotList;
