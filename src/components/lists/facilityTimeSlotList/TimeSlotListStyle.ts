import { StyleSheet } from 'react-native';
import { BorderRadiuses, Spacings, Typography } from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        padding: Spacings.s2,
        zIndex: 1,
        backgroundColor: Colors.$backgroundDefault,
        width: '100%',
        borderWidth: 1,
        borderTopWidth: 0,
        borderColor: Colors.$outlineDefault,
        borderBottomLeftRadius: BorderRadiuses.br30,
        borderBottomRightRadius: BorderRadiuses.br30,
    },
    timeSlotsContainer: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    headerText: {
        ...Typography.text70,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    timeSlotButtonContainer: {
        width: '33%',
        minWidth: 105,
    },
});
