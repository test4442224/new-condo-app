import styles from './TimeSlotButtonStyle';
import React, { FC } from 'react';
import { ITimeSlot } from '../../../models/booking/TimeSlotModel';
import { Pressable } from 'react-native';
import NormalText from '../../ui/titles/NormalText';
import Colors from '../../../Constants/Colors';

interface TimeSlotButtonProps {
    timeSlot: ITimeSlot;
    facilityId: number;
    isSelected: boolean;
    onPress: (timeSlot: ITimeSlot, isSelected: boolean) => () => void;
}

const TimeSlotButton: FC<TimeSlotButtonProps> = ({
    timeSlot,
    onPress,
    isSelected,
}) => {
    return (
        <>
            <Pressable
                onPress={onPress(timeSlot, isSelected)}
                style={[
                    styles.button,
                    isSelected
                        ? styles.selectedButton
                        : styles.unselectedButton,
                ]}
                testID="select-time-slot"
                android_ripple={{ color: Colors.$backgroundPrimaryLight }}>
                <NormalText
                    center={true}
                    style={[
                        styles.text,
                        isSelected
                            ? styles.selectedText
                            : styles.unselectedText,
                    ]}>
                    {`${timeSlot.start}${'\n'}${timeSlot.end}`}
                </NormalText>
            </Pressable>
        </>
    );
};

export default TimeSlotButton;
