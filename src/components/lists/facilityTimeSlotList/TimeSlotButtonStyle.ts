import { StyleSheet } from 'react-native';
import { BorderRadiuses, Spacings, Typography } from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    button: {
        borderWidth: 1,
        borderRadius: BorderRadiuses.br30,
        margin: Spacings.s1,
        overflow: 'hidden',
        paddingVertical: Spacings.s4,
        paddingHorizontal: Spacings.s4,
        elevation: 2,
        justifyContent: 'center',
        borderColor: Colors.$outlinePrimary,
    },
    unselectedButton: {
        backgroundColor: Colors.$backgroundDefault,
    },
    selectedButton: {
        backgroundColor: Colors.$backgroundPrimary,
    },
    text: {
        ...Typography.text90,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    unselectedText: {
        color: Colors.$textPrimary,
    },
    selectedText: {
        color: Colors.$textInverted,
    },
});
