import styles from './FacilityTimeSlotCardStyle';

import React, { FC, useState } from 'react';
import IFacilityAvailableTimeSlot from '../../../models/facility/FacilityAvailableTimeSlot';
import { ImageBackground, Pressable, View } from 'react-native';
import { getImageUrl } from '../../../Constants/api';
import NormalText from '../../ui/titles/NormalText';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {
    faAngleRight,
    faLocationDot,
    faUser,
} from '@fortawesome/free-solid-svg-icons';
import TimeSlotList from './TimeSlotList';

interface FacilityTimeSlotCardProps {
    facilityAvailableTimeSlots: IFacilityAvailableTimeSlot;
    togglePopup: (isVisible: boolean) => void;
}

const FacilityTimeSlotCard: FC<FacilityTimeSlotCardProps> = ({
    facilityAvailableTimeSlots,
    togglePopup,
}) => {
    const [isOpen, setIsOpen] = useState<boolean>(false);

    const imageUrl = getImageUrl(
        facilityAvailableTimeSlots.facility.primaryFacilityImageUrl || '',
    );

    const onPressHandler = () => {
        setIsOpen(!isOpen);
    };

    return (
        <>
            <Pressable onPress={onPressHandler}>
                <View
                    testID={'select-facility'}
                    style={[
                        styles.facilityCardRootContainer,
                        !isOpen && styles.closedFacilityCardRootContainer,
                    ]}>
                    <ImageBackground
                        source={{
                            uri: imageUrl,
                        }}
                        resizeMode="cover"
                        style={styles.imageBackgound}>
                        <View style={styles.overlay} />
                        <View style={styles.contentContainer}>
                            {/* Text Container */}
                            <View>
                                <NormalText style={styles.facilityName}>
                                    {facilityAvailableTimeSlots.facility.name}
                                </NormalText>

                                <View style={styles.rowContainer}>
                                    <FontAwesomeIcon
                                        icon={faLocationDot}
                                        size={13}
                                        style={styles.iconStyle}
                                    />
                                    <View
                                        style={styles.informationTextContainer}>
                                        <NormalText
                                            style={styles.informationText}>
                                            {
                                                facilityAvailableTimeSlots
                                                    .facility.location
                                            }
                                        </NormalText>
                                    </View>
                                </View>

                                <View style={styles.rowContainer}>
                                    <FontAwesomeIcon
                                        icon={faUser}
                                        size={13}
                                        style={styles.iconStyle}
                                    />

                                    <View
                                        style={styles.informationTextContainer}>
                                        <NormalText
                                            style={styles.informationText}>
                                            {facilityAvailableTimeSlots.facility
                                                .pax === 0
                                                ? `No Recommended Capacity`
                                                : `Recommended Capacity: ${facilityAvailableTimeSlots.facility.pax} pax`}
                                        </NormalText>
                                    </View>
                                </View>
                            </View>
                            {/* Icon Container */}
                            <View style={styles.columnContainer}>
                                <NormalText
                                    style={[
                                        styles.informationText,
                                        styles.feeText,
                                    ]}>
                                    {facilityAvailableTimeSlots.facility
                                        .facilityFees?.length !== 0
                                        ? '* Fee Required'
                                        : null}
                                </NormalText>
                                <View style={styles.iconContainer}>
                                    <FontAwesomeIcon
                                        icon={faAngleRight}
                                        size={30}
                                        style={
                                            isOpen
                                                ? styles.openedAngleIcon
                                                : styles.closedAngleIcon
                                        }
                                    />
                                </View>
                            </View>
                        </View>
                    </ImageBackground>
                </View>
            </Pressable>
            {isOpen && (
                <TimeSlotList
                    timeSlots={facilityAvailableTimeSlots.availableTimeSlots}
                    facility={facilityAvailableTimeSlots.facility}
                    togglePopup={togglePopup}
                />
            )}
        </>
    );
};

export default FacilityTimeSlotCard;
