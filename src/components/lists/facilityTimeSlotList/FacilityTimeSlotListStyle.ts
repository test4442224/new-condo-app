import { StyleSheet } from 'react-native';
import { Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    list: {
        gap: 15,
        paddingBottom: Spacings.s1 * 125,
    },
});
