import { StyleSheet } from 'react-native';
import { Spacings, Typography } from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    facilityCardRootContainer: {
        height: 103,
        borderTopLeftRadius: Spacings.s2,
        borderTopRightRadius: Spacings.s2,
        overflow: 'hidden',
        flex: 1,
    },
    closedFacilityCardRootContainer: {
        borderBottomLeftRadius: Spacings.s2,
        borderBottomRightRadius: Spacings.s2,
    },
    contentContainer: {
        position: 'absolute',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        height: '100%',
        padding: Spacings.s2,
    },
    iconContainer: {
        flex: 3,
    },
    facilityName: {
        ...Typography.text60,
        color: Colors.$textInverted,
    },
    imageBackgound: {
        height: '100%',
    },
    overlay: {
        backgroundColor: Colors.$backgroundDefaultDark,
        opacity: 0.5,
        width: '100%',
        height: '100%',
    },
    closedAngleIcon: {
        alignSelf: 'flex-end',
        color: Colors.$iconDefault,
        transform: [{ rotate: '90deg' }],
    },
    openedAngleIcon: {
        alignSelf: 'flex-end',
        color: Colors.$iconDefault,
        transform: [{ rotate: '270deg' }],
    },
    rowContainer: {
        flexDirection: 'row',
        paddingHorizontal: Spacings.s2,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    iconStyle: {
        color: Colors.$iconDefault,
        marginRight: Spacings.s1,
    },
    informationText: {
        ...Typography.text90,
        fontWeight: WEIGHT_TYPES.REGULAR,
        color: Colors.$textInverted,
        fontStyle: 'italic',
        paddingVertical: Spacings.s1,
    },
    informationTextContainer: {
        paddingLeft: Spacings.s1,
        paddingTop: Spacings.s1,
    },
    columnContainer: {
        height: '100%',
        justifyContent: 'center',
    },
    feeText: {
        alignItems: 'flex-end',
        flex: 1,
    },
});
