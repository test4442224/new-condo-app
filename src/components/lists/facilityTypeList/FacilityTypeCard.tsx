import styles from './FacilityTypeCardStyle';

import React, { FC } from 'react';
import { IFacilityType } from '../../../models/facility/FacilityTypeModel';
import { ImageBackground, Pressable, View } from 'react-native';
import { getImageUrl } from '../../../Constants/api';
import HeaderText from '../../ui/titles/HeaderText';

interface FacilityTypeCardProps {
    facilityType: IFacilityType;
    onPress: () => void;
    identifier?: string;
}

const FacilityTypeCard: FC<FacilityTypeCardProps> = ({
    facilityType,
    onPress,
    identifier,
}) => {
    return (
        <>
            <View style={styles.rootContainer}>
                <Pressable onPress={onPress} testID={`${identifier}`}>
                    <ImageBackground
                        style={styles.backgroundImage}
                        source={{ uri: getImageUrl(facilityType.imageUrl) }}
                        resizeMode="cover">
                        <View style={[styles.textContainer, styles.overlay]} />
                        <HeaderText
                            center={false}
                            style={styles.facilityTypeName}>
                            {facilityType.name}
                        </HeaderText>
                    </ImageBackground>
                </Pressable>
            </View>
        </>
    );
};

export default FacilityTypeCard;
