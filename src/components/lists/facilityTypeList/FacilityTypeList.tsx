import styles from './FacilityTypeListStyle';

import React, { FC } from 'react';
import { FlatList } from 'react-native';
import FacilityTypeCard from './FacilityTypeCard';
import { IFacilityType } from '../../../models/facility/FacilityTypeModel';

interface FacilityTypeListProps {
    onSelect: (facilityTypeName: IFacilityType) => void;
    facilityTypes: IFacilityType[];
    testID?: string;
    numColumns?: number;
}

const FacilityTypeList: FC<FacilityTypeListProps> = ({
    onSelect,
    facilityTypes,
    testID,
    numColumns = 2,
}) => {
    const facilityTypeCardOnPressHandler = (facilityType: IFacilityType) => {
        return () => {
            onSelect(facilityType);
        };
    };

    return (
        <FlatList
            data={facilityTypes}
            renderItem={({ item: facilityType }) => (
                <FacilityTypeCard
                    facilityType={facilityType}
                    onPress={facilityTypeCardOnPressHandler(facilityType)}
                    identifier={`${testID}-${facilityType.name}`}
                />
            )}
            keyExtractor={item => item.id.toString()}
            contentContainerStyle={styles.list}
            testID={testID}
            numColumns={numColumns}
        />
    );
};

export default FacilityTypeList;
