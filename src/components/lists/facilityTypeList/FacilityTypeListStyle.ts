import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    list: {
        gap: 15,
    },
});
