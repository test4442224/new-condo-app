import { StyleSheet } from 'react-native';
import Colors from '../../../Constants/Colors';
import { BorderRadiuses, Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    rootContainer: {
        height: 180,
        width: '46%',
        borderRadius: BorderRadiuses.br30,
        overflow: 'hidden',
        margin: Spacings.s2,
    },
    textContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignContent: 'flex-start',
    },
    backgroundImage: {
        width: '100%',
        height: '100%',
    },
    facilityTypeName: {
        margin: Spacings.s2,
        color: Colors.$textInverted,
        position: 'absolute',
    },
    overlay: {
        backgroundColor: Colors.$backgroundDefaultDark,
        opacity: 0.3,
        width: '100%',
        height: '100%',
    },
});
