import React, { FC } from 'react';
import { FlatList, View } from 'react-native';
import NormalText from '../../ui/titles/NormalText';
import styles from './PaymentListStyle';
import PaymentCard from '../../ui/buttons/PaymentCard/PaymentCard';
import { IConservancyFeesAttribute } from '../../../store/payment/slice';

interface PaymentListProps {
    payments: IConservancyFeesAttribute[];
    onPressed: (payment: IConservancyFeesAttribute) => void;
    noPaymentLabel: string;
}

const PaymentList: FC<PaymentListProps> = ({
    payments,
    onPressed,
    noPaymentLabel,
}) => (
    <View testID="data-display">
        {payments && payments.length ? (
            <FlatList
                data={payments}
                renderItem={({ item: payment, index: i }) => (
                    <PaymentCard
                        payment={payment}
                        onPress={() => {
                            onPressed(payment);
                        }}
                        testID={`payment-card-${i}-${payment.referenceId}`}
                    />
                )}
                testID="payment.flat-list"
                keyExtractor={(_, i) => `${i}`}
                alwaysBounceVertical={false}
                contentContainerStyle={styles.flatList}
            />
        ) : (
            <NormalText center={true} testID="payment.no-payment">
                {noPaymentLabel}
            </NormalText>
        )}
    </View>
);

export default PaymentList;
