import React, { FC } from 'react';
import { NoticeAttributes } from '../../../store/notice/slice';
import { FlatList, View } from 'react-native';
import NoticeCard from '../../ui/buttons/NoticeCard/NoticeCard';
import NormalText from '../../ui/titles/NormalText';
import styles from './NoticeListStyle';

interface NoticeListProps {
    notices: NoticeAttributes[];
    onPressed: (booking: NoticeAttributes) => void;
    noNoticeLabel: string;
}

const NoticeList: FC<NoticeListProps> = ({
    notices,
    onPressed,
    noNoticeLabel,
}) => (
    <View testID="data-display">
        {notices && notices.length ? (
            <FlatList
                data={notices}
                renderItem={({ item: notice }) => (
                    <NoticeCard
                        notice={notice}
                        onPress={() => {
                            onPressed(notice);
                        }}
                        pressableTestId={`NoticeCard${notice}`}
                    />
                )}
                testID="notice.flat-list"
                keyExtractor={item => `${item.id}`}
                alwaysBounceVertical={false}
                contentContainerStyle={styles.flatList}
            />
        ) : (
            <NormalText center={true} testID="notice.no-notice">
                {noNoticeLabel}
            </NormalText>
        )}
    </View>
);

export default NoticeList;
