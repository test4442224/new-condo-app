import React, { FC } from 'react';
import { ManageBookingAttributes } from '../../../store/manageBooking/slice';
import { FlatList, View } from 'react-native';
import BookingCard from '../../ui/buttons/BookingCard/BookingCard';
import NormalText from '../../ui/titles/NormalText';
import styles from './BookingListStyle';

interface BookingListProps {
    bookings: ManageBookingAttributes[];
    onPressed: (booking: ManageBookingAttributes) => void;
    noBookingLabel?: string;
    render?: () => React.ReactNode;
}

const BookingList: FC<BookingListProps> = ({
    bookings,
    onPressed,
    noBookingLabel,
    render,
}) => (
    <View testID="data-display" style={styles.flatListContainer}>
        {bookings && bookings.length ? (
            <FlatList
                data={bookings}
                renderItem={({ item: booking }) => (
                    <>
                        <BookingCard
                            booking={booking}
                            onPress={() => {
                                onPressed(booking);
                            }}
                            label={`BookingCard${booking}`}
                            testID={`booking-card-${booking.bookingReferenceId}`}
                        />
                        {render && render()}
                    </>
                )}
                testID="booking-list.flat-list"
                keyExtractor={item => `${item.id}`}
                alwaysBounceVertical={false}
                contentContainerStyle={styles.flatList}
            />
        ) : (
            <NormalText center={true} testID="booking-list.no-bookings">
                {noBookingLabel}
            </NormalText>
        )}
    </View>
);

export default BookingList;
