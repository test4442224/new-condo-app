import { StyleSheet } from 'react-native';
import { Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    flatListContainer: {
        flex: 1,
    },
    flatList: {
        paddingBottom: Spacings.s1,
    },
});
