import React, { FC } from 'react';
import { FlatList, View } from 'react-native';
import NormalText from '../../ui/titles/NormalText';
import styles from './DefectListStyle';
import DefectCard from '../../ui/buttons/DefectCard/DefectCard';
import { DefectAttributes } from '../../../store/defect/slice';

interface DefectListProps {
    defects: DefectAttributes[];
    onPressed: (defect: DefectAttributes) => void;
    noDefectLabel: string;
    testID?: string;
}

const DefectList: FC<DefectListProps> = ({
    defects,
    onPressed,
    noDefectLabel,
    testID = 'defect-list',
}) => (
    <View testID={testID} data-testid={testID}>
        {defects && defects.length ? (
            <FlatList
                data={defects}
                renderItem={({ item: defect }) => (
                    <DefectCard
                        defect={defect}
                        onPress={() => {
                            onPressed(defect);
                        }}
                        accessibilityLabel={`android-defect-card-${defect.defectReferenceNo}`}
                        testID={`ios-defect-card-${defect.defectReferenceNo}`}
                    />
                )}
                testID="manage-defect.flat-list"
                accessibilityLabel="manage-defect.flat-list"
                keyExtractor={item => `${item.id}`}
                alwaysBounceVertical={false}
                contentContainerStyle={styles.flatList}
            />
        ) : (
            <NormalText center={true} testID="defect.no-defect">
                {noDefectLabel}
            </NormalText>
        )}
    </View>
);

export default DefectList;
