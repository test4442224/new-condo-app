import { StyleSheet } from 'react-native';
import { Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    flatList: {
        paddingBottom: Spacings.s1 * 40,
    },
});
