import { StyleSheet } from 'react-native';
import { BorderRadiuses, Typography } from 'react-native-ui-lib';
import Colors from '../../Constants/Colors';

export default StyleSheet.create({
    button: {
        height: 50,
        width: 50,
        borderRadius: BorderRadiuses.br100,
        backgroundColor: Colors.$backgroundIconDefault,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        ...Typography.text50,
    },
});
