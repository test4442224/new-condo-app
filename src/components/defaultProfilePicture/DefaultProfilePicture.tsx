import {
    View,
    GestureResponderEvent,
    StyleProp,
    TextStyle,
} from 'react-native';
import React from 'react';
import styles from './DefaultProfilePictureStyle';
import PrimaryButton from '../ui/buttons/PrimaryButton';
import { useAppSelector } from '../../store/hook';

interface IDefaultProfilePicture {
    onPress: (_: GestureResponderEvent) => void;
    size: 'xSmall' | 'small' | 'medium';
    buttonStyle?: StyleProp<TextStyle>;
    textStyle?: StyleProp<TextStyle>;
    testID?: string;
}

const DefaultProfilePicture = ({
    onPress,
    size,
    buttonStyle = {},
    textStyle = {},
    testID = 'profile-icon',
}: IDefaultProfilePicture) => {
    const firstName = useAppSelector(
        state => state.user.profileDetails.firstName,
    )!;

    const letterIcon = firstName ? firstName[0] : '';

    return (
        <View>
            <PrimaryButton
                style={[styles.button, buttonStyle]}
                size={size}
                label={letterIcon}
                labelStyle={[styles.text, textStyle]}
                onPress={onPress}
                avoidMinWidth={true}
                testID={testID}
                accessibilityLabel={testID}
            />
        </View>
    );
};

export default DefaultProfilePicture;
