import { cleanup } from '@testing-library/react-native';
import ImageUploadField from '.';
import React from 'react';
import { userEvent } from '@testing-library/react-native';
import { render } from '@testing-library/react-native';
import { Platform } from 'react-native';

jest.mock('react-native-image-picker', () => ({
    launchImageLibrary: jest.fn(),
}));

jest.mock('./utils', () => ({
    handleImagePickerError: jest.fn(),
    validatePhoto: jest.fn(),
}));

const launchImageLibraryMock =
    require('react-native-image-picker').launchImageLibrary;
const utilsMock = require('./utils');

describe('ImageUploadField component', () => {
    beforeEach(() => {
        cleanup();
        jest.clearAllMocks();
    });

    it('should render correctly', () => {
        const { getByRole, getByText } = render(
            <ImageUploadField
                setSelectedPhoto={() => {}}
                setError={() => {}}
                selectedPhoto={null}
            />,
        );

        expect(
            getByRole('button', { name: 'Upload PayNow Receipt' }),
        ).toBeOnTheScreen();
        expect(getByText('Maximum file size is 2MB.')).toBeOnTheScreen();
        expect(
            getByText('Only .jpeg, .jpg or .png file allowed.'),
        ).toBeOnTheScreen();
    });

    it('should handle pressing of Upload PayNow Receipt button', async () => {
        const user = userEvent.setup();
        const setSelectedPhotoMock = jest.fn();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: false,
            errorCode: null,
            assets: [
                { fileName: 'imageName', type: 'image/jpg', uri: 'imageUri' },
            ],
        });
        utilsMock.validatePhoto.mockReturnValueOnce(true);

        const { getByRole } = render(
            <ImageUploadField
                setSelectedPhoto={setSelectedPhotoMock}
                setError={() => {}}
                selectedPhoto={null}
            />,
        );

        await user.press(
            getByRole('button', { name: 'Upload PayNow Receipt' }),
        );

        expect(launchImageLibraryMock).toHaveBeenCalled();
        expect(setSelectedPhotoMock).toHaveBeenCalledWith({
            name: 'imageName',
            type: 'image/jpg',
            uri: expect.stringContaining('imageUri'),
        });
    });

    it('should handle user cancelling photo selection', async () => {
        const user = userEvent.setup();
        const setSelectedPhotoMock = jest.fn();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: true,
        });

        const { getByRole } = render(
            <ImageUploadField
                setSelectedPhoto={setSelectedPhotoMock}
                setError={() => {}}
                selectedPhoto={null}
            />,
        );

        await user.press(
            getByRole('button', { name: 'Upload PayNow Receipt' }),
        );

        expect(utilsMock.handleImagePickerError).not.toHaveBeenCalled();
        expect(utilsMock.validatePhoto).not.toHaveBeenCalled();
        expect(setSelectedPhotoMock).not.toHaveBeenCalled();
    });

    it('should handle image picker error', async () => {
        const user = userEvent.setup();
        const setSelectedPhotoMock = jest.fn();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: false,
            errorCode: 'errorCode',
        });

        const { getByRole } = render(
            <ImageUploadField
                setSelectedPhoto={setSelectedPhotoMock}
                setError={() => {}}
                selectedPhoto={null}
            />,
        );

        await user.press(
            getByRole('button', { name: 'Upload PayNow Receipt' }),
        );

        expect(utilsMock.validatePhoto).not.toHaveBeenCalled();
        expect(setSelectedPhotoMock).not.toHaveBeenCalled();
    });

    it('should handle invalid photo', async () => {
        const user = userEvent.setup();
        const setSelectedPhotoMock = jest.fn();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: false,
            errorCode: null,
            assets: [
                {
                    fileName: 'fileName',
                    type: 'application/pdf',
                    uri: 'fileUri',
                },
            ],
        });
        utilsMock.validatePhoto.mockReturnValueOnce(false);

        const { getByRole } = render(
            <ImageUploadField
                setSelectedPhoto={setSelectedPhotoMock}
                setError={() => {}}
                selectedPhoto={null}
            />,
        );

        await user.press(
            getByRole('button', { name: 'Upload PayNow Receipt' }),
        );

        expect(setSelectedPhotoMock).not.toHaveBeenCalled();
    });

    it('should get correct photo URI if on iOS', async () => {
        const originalOS = Platform.OS;
        Platform.OS = 'ios';
        const user = userEvent.setup();
        const setSelectedPhotoMock = jest.fn();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: false,
            errorCode: null,
            assets: [
                {
                    fileName: 'imageName',
                    type: 'image/jpg',
                    uri: 'file://imageUri',
                },
            ],
        });
        utilsMock.validatePhoto.mockReturnValueOnce(true);

        const { getByRole } = render(
            <ImageUploadField
                setSelectedPhoto={setSelectedPhotoMock}
                setError={() => {}}
                selectedPhoto={null}
            />,
        );

        await user.press(
            getByRole('button', { name: 'Upload PayNow Receipt' }),
        );

        expect(setSelectedPhotoMock).toHaveBeenCalledWith({
            name: 'imageName',
            type: 'image/jpg',
            uri: expect.stringContaining('imageUri'),
        });

        Platform.OS = originalOS;
    });

    it('should get correct photo URI if on Android', async () => {
        const originalOS = Platform.OS;
        Platform.OS = 'android';
        const user = userEvent.setup();
        const setSelectedPhotoMock = jest.fn();
        launchImageLibraryMock.mockResolvedValueOnce({
            didCancel: false,
            errorCode: null,
            assets: [
                {
                    fileName: 'imageName',
                    type: 'image/jpg',
                    uri: 'file://imageUri',
                },
            ],
        });
        utilsMock.validatePhoto.mockReturnValueOnce(true);

        const { getByRole } = render(
            <ImageUploadField
                setSelectedPhoto={setSelectedPhotoMock}
                setError={() => {}}
                selectedPhoto={null}
            />,
        );

        await user.press(
            getByRole('button', { name: 'Upload PayNow Receipt' }),
        );

        expect(setSelectedPhotoMock).toHaveBeenCalledWith({
            name: 'imageName',
            type: 'image/jpg',
            uri: 'file://imageUri',
        });

        Platform.OS = originalOS;
    });
});
