import React, { FC, SetStateAction } from 'react';
import { ImageData } from '../../../store/manageBooking/api';
import Field from '../../../screens/Payment/PaymentStatus/components/Field';
import { View } from 'react-native-ui-lib';
import PrimaryButton from '../../ui/buttons/PrimaryButton';
import NormalText from '../../ui/titles/NormalText';
import { launchImageLibrary } from 'react-native-image-picker';
import { handleImagePickerError, validatePhoto } from './utils';
import { Error } from '../../../screens/Payment/PaymentStatus/PendingPayNowPayment';
import { Platform } from 'react-native';
import styles from '../styles';

type Props = {
    selectedPhoto: ImageData | null;
    setSelectedPhoto: (photo: SetStateAction<ImageData | null>) => void;
    setError: (error: SetStateAction<Error>) => void;
};

const ImageUploadField: FC<Props> = ({
    selectedPhoto,
    setSelectedPhoto,
    setError,
}) => {
    const handleChoosePhoto = async () => {
        const result = await launchImageLibrary({
            mediaType: 'photo',
        });
        if (result.didCancel) return;
        if (result.errorCode) {
            handleImagePickerError(result.errorCode, setError);
            return;
        }

        const photo = result.assets![0];
        const photoIsValid = validatePhoto(photo, setSelectedPhoto, setError);
        if (!photoIsValid) return;

        setSelectedPhoto({
            name: photo.fileName as string,
            type: photo.type as string,
            uri:
                Platform.OS === 'ios'
                    ? photo.uri!.replace('file://', '')
                    : photo.uri!,
        });
    };

    return (
        <>
            <Field label="Uploaded Image" value={selectedPhoto?.name} />
            <View style={styles.textCardContainer}>
                <PrimaryButton
                    label="Upload PayNow Receipt"
                    testID="upload-paynow-receipt-btn"
                    onPress={handleChoosePhoto}
                />
            </View>
            <View style={styles.textCardContainer}>
                <NormalText style={styles.italicGreyText}>
                    Maximum file size is 2MB.
                </NormalText>
                <NormalText style={styles.italicGreyText}>
                    Only .jpeg, .jpg or .png file allowed.
                </NormalText>
            </View>
        </>
    );
};

export default ImageUploadField;
