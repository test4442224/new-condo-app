import { StyleSheet } from 'react-native';
import Colors from '../../Constants/Colors';
import { Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    textCardContainer: {
        paddingVertical: Spacings.s2,
    },
    italicGreyText: {
        fontStyle: 'italic',
        color: Colors.$textDisabled,
    },
});
