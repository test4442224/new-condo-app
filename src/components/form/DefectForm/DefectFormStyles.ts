import { StyleSheet } from 'react-native';
import { BorderRadiuses, Spacings } from 'react-native-ui-lib';
import Colors from '../../../Constants/Colors';
import Typography, {
    WEIGHT_TYPES,
} from 'react-native-ui-lib/src/style/typographyPresets';

export default StyleSheet.create({
    formContainer: {
        flex: 1,
        paddingHorizontal: Spacings.s4,
    },
    topFormFieldContainer: {
        flex: 0.1,
    },
    bottomFormFieldContainer: {
        flex: 0.3,
    },
    headerContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'flex-end',
        gap: Spacings.s1,
    },
    subHeaderText: {
        ...Typography.text70,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    asterisk: {
        ...Typography.text70,
        fontWeight: WEIGHT_TYPES.BOLD,
        color: Colors.$iconError,
    },
    tipText: {
        ...Typography.text90,
        fontStyle: 'italic',
    },
    nameInputBox: {
        flex: 0.3,
        ...Typography.text60R,
        borderWidth: 1.5,
        borderRadius: BorderRadiuses.br30,
        borderColor: Colors.$outlinePrimary,
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
    descriptionInputBox: {
        flex: 1,
        marginBottom: Spacings.s4,
        ...Typography.text60R,
        borderWidth: 1.5,
        borderRadius: BorderRadiuses.br30,
        borderColor: Colors.$outlinePrimary,
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
});
