import React from 'react';
import { FC } from 'react';
import styles from './DefectFormStyles';
import { TextInput, View } from 'react-native';
import Text from '../../ui/titles/Text';

interface IDefectFormProps {
    defectName: string;
    defectDescription: string;
    onHandleNameChange: (name: string) => void;
    onHandleDescriptionChange: (description: string) => void;
}

const DefectForm: FC<IDefectFormProps> = ({
    defectName,
    defectDescription,
    onHandleDescriptionChange,
    onHandleNameChange,
}: IDefectFormProps) => {
    return (
        <>
            <View style={styles.topFormFieldContainer}>
                <View style={styles.headerContainer}>
                    <Text style={styles.subHeaderText} underline={false}>
                        Defect Name
                    </Text>
                    <Text style={styles.asterisk}>*</Text>
                    <Text style={styles.tipText}>(max. 30 characters)</Text>
                </View>
                <TextInput
                    style={styles.nameInputBox}
                    value={defectName}
                    maxLength={30}
                    onChangeText={onHandleNameChange}
                    multiline={true}
                    textAlignVertical="top"
                    spellCheck={false}
                    testID="name-input-box"
                    accessibilityLabel="name-input-box"
                />
            </View>
            <View style={styles.bottomFormFieldContainer}>
                <View style={styles.headerContainer}>
                    <Text style={styles.subHeaderText} underline={false}>
                        Defect Description
                    </Text>
                    <Text style={styles.asterisk}>*</Text>
                </View>
                <TextInput
                    style={styles.descriptionInputBox}
                    value={defectDescription}
                    maxLength={500}
                    onChangeText={onHandleDescriptionChange}
                    multiline={true}
                    textAlignVertical="top"
                    spellCheck={false}
                    testID="description-input-box"
                    accessibilityLabel="description-input-box"
                />
            </View>
        </>
    );
};

export default DefectForm;
