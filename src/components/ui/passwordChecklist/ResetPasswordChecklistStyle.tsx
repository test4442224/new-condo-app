import { StyleSheet } from 'react-native';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    generalContainer: {
        flexDirection: 'row',
    },
    requirementContainer: {
        alignContent: 'center',
    },
    successIcon: {
        color: Colors.$iconSuccess,
    },
    errorIcon: {
        color: Colors.$iconError,
    },
});
