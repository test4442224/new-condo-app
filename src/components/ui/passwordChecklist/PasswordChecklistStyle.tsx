import { StyleSheet } from 'react-native';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import { BorderRadiuses, Spacings } from 'react-native-ui-lib';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    textReminder: {
        color: Colors.$textError,
        fontWeight: WEIGHT_TYPES.BOLD,
        marginBottom: Spacings.s2,
    },

    checklistContainer: {
        borderRadius: BorderRadiuses.br30,
        marginTop: Spacings.s4,
        paddingVertical: Spacings.s3,
        paddingHorizontal: Spacings.s3,
    },

    checklistSucess: {
        backgroundColor: Colors.$backgroundSuccessLight,
    },
    checklistError: {
        backgroundColor: Colors.$backgroundErrorLight,
    },
});
