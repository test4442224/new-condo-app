import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import { View } from 'react-native';
import styles from './ResetPasswordChecklistStyle';
import NormalText from '../titles/NormalText';

const ResetPasswordChecklist = ({
    isFulfilled,
    requirement,
    testID,
}: {
    isFulfilled: boolean;
    requirement: string;
    testID: string;
}) => {
    return (
        <View style={styles.generalContainer}>
            <NormalText testID={testID}>
                {isFulfilled ? (
                    <FontAwesomeIcon
                        icon={faCheck}
                        style={styles.successIcon}
                    />
                ) : (
                    <FontAwesomeIcon icon={faTimes} style={styles.errorIcon} />
                )}{' '}
            </NormalText>
            <NormalText style={styles.requirementContainer}>
                {requirement}
            </NormalText>
        </View>
    );
};

export default ResetPasswordChecklist;
