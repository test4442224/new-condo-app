import { useEffect, useState } from 'react';
import styles from './PasswordChecklistStyle';
import ResetPasswordChecklist from './ResetPasswordChecklist';
import { View } from 'react-native';
import NormalText from '../titles/NormalText';
import React from 'react';

const PasswordChecklist = ({ password }: { password: string }) => {
    const [isEightCharacters, setIsEightCharacters] = useState(false);
    const [hasUppercase, setHasUppercase] = useState(false);
    const [hasLowercase, setHasLowercase] = useState(false);
    const [hasNumber, setHasNumber] = useState(false);
    const [hasSpecialCharacter, setHasSpecialCharacter] = useState(false);

    const isAllChecklistFulfilled =
        isEightCharacters &&
        hasUppercase &&
        hasLowercase &&
        hasNumber &&
        hasSpecialCharacter;

    const checklistContainer = isAllChecklistFulfilled
        ? [styles.checklistContainer, styles.checklistSucess]
        : [styles.checklistContainer, styles.checklistError];

    useEffect(() => {
        setIsEightCharacters(password.length >= 8);
        setHasUppercase(/[A-Z]/.test(password));
        setHasLowercase(/[a-z]/.test(password));
        setHasNumber(/\d/.test(password));
        setHasSpecialCharacter(/[!@#$%^&*()_+{}[\]:;<>,.?~\\-]/.test(password));
    }, [password]);

    return (
        <View style={checklistContainer}>
            {isAllChecklistFulfilled ? null : (
                <NormalText style={styles.textReminder}>
                    Your password is not strong enough. New password must:
                </NormalText>
            )}

            <ResetPasswordChecklist
                isFulfilled={isEightCharacters}
                requirement="Be at least 8 characters long"
                testID="ResetPasswordChecklist-EightCharacters"
            />
            <ResetPasswordChecklist
                isFulfilled={hasUppercase}
                requirement="Contain an uppercase letter"
                testID="ResetPasswordChecklist-Uppercase"
            />
            <ResetPasswordChecklist
                isFulfilled={hasLowercase}
                requirement="Contain a lowercase letter"
                testID="ResetPasswordChecklist-Lowercase"
            />
            <ResetPasswordChecklist
                isFulfilled={hasNumber}
                requirement="Contain a number"
                testID="ResetPasswordChecklist-Number"
            />
            <ResetPasswordChecklist
                isFulfilled={hasSpecialCharacter}
                requirement="Contain a special character"
                testID="ResetPasswordChecklist-SpecialCharacters"
            />
        </View>
    );
};

export default PasswordChecklist;
