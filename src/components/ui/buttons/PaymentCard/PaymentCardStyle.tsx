import { StyleSheet } from 'react-native';

import {
    BorderRadiuses,
    Shadows,
    Spacings,
    Typography,
} from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        borderRadius: BorderRadiuses.br30,
        overflow: 'hidden',
    },
    btnContainer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: Colors.$backgroundDefault,
        marginHorizontal: Spacings.s3,
        marginVertical: Spacings.s2,
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s3,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderColor: Colors.$outlinePrimary,
        borderWidth: 2,
        borderRadius: BorderRadiuses.br30,
    },
    rowContainer: {
        flexDirection: 'row',
        paddingTop: Spacings.s2,
    },
    dateTimeContainer: {
        flexDirection: 'row',
        paddingTop: Spacings.s2,
        justifyContent: 'flex-end',
    },
    textContainer: {
        flex: 1,
    },
    badgeContainer: {
        alignSelf: 'flex-start',
    },
    titleText: {
        ...Typography.text70,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    subTitleText: {
        ...Typography.text70,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    textSize: { ...Typography.text70 },
    pressed: {
        ...Shadows.sh30.bottom,
    },
    badge: {
        ...Typography.text80,
    },
    purpleColorStyle: {
        color: Colors.$textPrimary,
    },
});
