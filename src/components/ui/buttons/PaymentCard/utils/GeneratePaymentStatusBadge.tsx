import { StyleProp, ViewStyle } from 'react-native';
import { PaymentStatus } from '../../../../../store/manageBooking/slice';
import { isAfterCurrentTime } from '../../../../../utils/dateTimeUtil';
import Colors from '../../../../../Constants/Colors';
import { IConservancyFeesAttribute } from '../../../../../store/payment/slice';

export interface IPaymentStatusBadge {
    badgeStatus: string;
    badgeStyle: StyleProp<ViewStyle>;
}

export const generatePaymentStatusBadge = (
    details: IConservancyFeesAttribute,
) => {
    let badge: IPaymentStatusBadge = { badgeStatus: '', badgeStyle: {} };
    const status = details.payments.map(payment => payment.status);
    const isPending = isAfterCurrentTime(details!.conservancyPayment.dueDate);

    // PENDING REFUND
    if (status[0] === PaymentStatus.PENDING_REFUND) {
        return (badge = {
            badgeStatus: 'PENDING REFUND',
            badgeStyle: { backgroundColor: Colors.$backgroundWarning },
        });
    }

    // REFUNDED
    if (status[0] === PaymentStatus.REFUNDED) {
        return (badge = {
            badgeStatus: 'REFUNDED',
            badgeStyle: { backgroundColor: Colors.$backgroundError },
        });
    }

    // PAID
    if (status[0] === PaymentStatus.PAID) {
        return (badge = {
            badgeStatus: 'PAID',
            badgeStyle: { backgroundColor: Colors.$backgroundSuccess },
        });
    }

    // PENDING VERIFICATION
    if (status[0] === PaymentStatus.PENDING_VERIFICATION) {
        return (badge = {
            badgeStatus: 'PENDING VERIFICATION',
            badgeStyle: { backgroundColor: Colors.$backgroundWarning },
        });
    }

    // CANCELLED
    if (status[0] === PaymentStatus.CANCELLED) {
        return (badge = {
            badgeStatus: 'CANCELLED',
            badgeStyle: { backgroundColor: Colors.$backgroundError },
        });
    }

    // PENDING PAYMENT
    // TODO WHEN VERIFY PAYMENT STORY IS IN
    if (isPending) {
        return (badge = {
            badgeStatus: 'PENDING PAYMENT',
            badgeStyle: { backgroundColor: Colors.$backgroundWarning },
        });
    }

    // OVERDUE
    // TODO WHEN VERIFY PAYMENT STORY IS IN
    if (!isPending) {
        return (badge = {
            badgeStatus: 'OVERDUE',
            badgeStyle: { backgroundColor: Colors.$backgroundError },
        });
    }

    return badge;
};
