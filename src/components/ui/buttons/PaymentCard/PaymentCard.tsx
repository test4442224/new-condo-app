import {
    Pressable,
    GestureResponderEvent,
    StyleProp,
    ViewStyle,
} from 'react-native';
import React from 'react';
import NormalText from '../../titles/NormalText';
import { View } from 'react-native-ui-lib';
import styles from './PaymentCardStyle';
import Colors from '../../../../Constants/Colors';
import { IConservancyFeesAttribute } from '../../../../store/payment/slice';
import { toDateFormat } from '../../../../utils/dateTimeUtil';
import { generatePaymentStatusBadge } from './utils/GeneratePaymentStatusBadge';
// import { PaymentStatus } from '../../../../store/manageBooking/slice';
import Badge from '../../badge/Badge';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

interface IPaymentCard {
    payment: IConservancyFeesAttribute;
    onPress: (_: GestureResponderEvent) => void;
    style?: StyleProp<ViewStyle>;
    testID?: string;
    pressableTestId?: string;
}

const PaymentCard = ({
    onPress,
    payment,
    style = {},
    pressableTestId,
    testID = 'payment-card',
}: IPaymentCard) => {
    const { conservancyPayment, totalAmount } = payment;
    // const paymentStatus = payment.payments.map(details => details.status);
    const dueDate = payment.conservancyPayment.dueDate;

    const badge = () => {
        return (
            <Badge
                details={generatePaymentStatusBadge(payment)}
                textStyles={styles.badge}
            />
        );
    };

    return (
        <View style={[styles.rootContainer]} testID={testID}>
            <Pressable
                style={({ pressed }) =>
                    pressed
                        ? [styles.btnContainer, style, styles.pressed]
                        : [styles.btnContainer, style]
                }
                onPress={onPress}
                testID={pressableTestId}
                android_ripple={{ color: Colors.$backgroundPrimaryLight }}>
                <View style={styles.textContainer}>
                    <View style={styles.badgeContainer}>{badge()}</View>
                    <View style={styles.rowContainer}>
                        <NormalText underline={false} style={styles.titleText}>
                            {`${conservancyPayment.name} - ${conservancyPayment.billingPeriod}`}
                        </NormalText>
                    </View>
                    <View style={styles.rowContainer}>
                        <NormalText
                            underline={false}
                            style={styles.subTitleText}>
                            {`$${totalAmount}`}
                        </NormalText>
                        <NormalText underline={false} style={styles.textSize}>
                            {' '}
                            due on{' '}
                        </NormalText>
                        <NormalText
                            underline={false}
                            style={styles.subTitleText}>{`${toDateFormat(
                            dueDate,
                        )}`}</NormalText>
                    </View>
                </View>
                <FontAwesomeIcon
                    icon={faChevronRight}
                    style={styles.purpleColorStyle}
                    size={24}
                />
            </Pressable>
        </View>
    );
};

export default PaymentCard;
