import { StyleSheet } from 'react-native';
import { Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    rootContainer: {
        overflow: 'hidden',
    },
    btnContainer: {
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s2,
        elevation: 2,
        justifyContent: 'center',
    },
    iconBtn: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    pressed: {
        opacity: 0.75,
    },
});
