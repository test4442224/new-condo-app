import React from 'react';
import { ButtonProps } from './Button';
import Button from './Button';
import { Typography } from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';

const PrimaryButton = ({
    onPress,
    style = {},
    label,
    testID = 'primary-button',
    disabled = false,
    size = 'medium',
    labelStyle = {},
    avoidMinWidth = false,
    accessibilityLabel,
}: ButtonProps) => {
    return (
        <Button
            accessibilityLabel={accessibilityLabel}
            testID={testID}
            label={label}
            onPress={onPress}
            disabled={disabled}
            style={[style]}
            labelStyle={[
                { ...Typography.text80, fontWeight: WEIGHT_TYPES.BOLD },
                labelStyle,
            ]}
            size={size}
            avoidMinWidth={avoidMinWidth}
        />
    );
};

export default PrimaryButton;
