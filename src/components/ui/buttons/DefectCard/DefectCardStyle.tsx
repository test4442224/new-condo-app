import { StyleSheet } from 'react-native';

import {
    BorderRadiuses,
    Shadows,
    Spacings,
    Typography,
} from 'react-native-ui-lib';
import Colors from '../../../../Constants/Colors';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';

export default StyleSheet.create({
    rootContainer: {
        borderRadius: BorderRadiuses.br30,
        overflow: 'hidden',
    },
    btnContainer: {
        flex: 1,
        flexDirection: 'row',
        marginHorizontal: Spacings.s3,
        marginVertical: Spacings.s2,
        paddingVertical: Spacings.s2,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderColor: Colors.$outlinePrimary,
        borderWidth: 2,
        borderRadius: BorderRadiuses.br30,
    },
    pressed: {
        ...Shadows.sh30.bottom,
    },
    rowContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    imageContainer: {
        justifyContent: 'center',
        paddingHorizontal: Spacings.s2,
    },
    image: {
        width: 100,
        height: 80,
        borderRadius: BorderRadiuses.br40,
    },
    rightContainer: {
        flex: 2,
    },
    innerRowContainer: {
        flexDirection: 'row',
        paddingBottom: Spacings.s1,
    },
    defectIdBadgeContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    badgeContainer: {
        alignContent: 'space-between',
    },
    badgeText: {
        ...Typography.text90,
    },
    defectRefNoText: {
        ...Typography.text90,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    defectDescText: {
        ...Typography.text80,
    },
    defectNameText: {
        ...Typography.text70,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    purpleColorStyle: {
        color: Colors.$textPrimary,
    },
    fontContainer: {
        paddingHorizontal: Spacings.s1,
    },
});
