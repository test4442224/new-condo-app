import {
    Pressable,
    GestureResponderEvent,
    StyleProp,
    ViewStyle,
} from 'react-native';
import React from 'react';
import SubHeaderText from '../../titles/SubHeaderText';
import NormalText from '../../titles/NormalText';
import { Image, View } from 'react-native-ui-lib';
import styles from './DefectCardStyle';
import Colors from '../../../../Constants/Colors';
import { DefectAttributes } from '../../../../store/defect/slice';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { getImageUrl } from '../../../../Constants/api';
import { generateDefectStatusBadge } from '../../../../utils/GenerateDefectStatusBadge';
import Badge from '../../badge/Badge';
import { trimTruncatedText } from '../../../../utils/TruncateText';
import VideoPlayer from 'react-native-media-console';

interface IDefectCard {
    defect: DefectAttributes;
    onPress: (_: GestureResponderEvent) => void;
    style?: StyleProp<ViewStyle>;
    testID?: string;
    accessibilityLabel?: string;
    label?: string;
}

const DefectCard = ({
    onPress,
    defect,
    style = {},
    testID = 'defect-card',
    accessibilityLabel = 'android-defect-card',
}: IDefectCard) => {
    const { defectReferenceNo, name, description, latestStatus } = defect;

    let imageUrl: string = '';
    if (defect.attachments.length !== 0) {
        const defects = [...defect.attachments];
        defects.sort((a, b) => b.id - a.id);

        defects.forEach(item => {
            imageUrl = item.attachmentPath;
        });
    }

    const truncatedName = trimTruncatedText(name, 25);
    const truncatedDescription = trimTruncatedText(description, 25);

    const badge = (textStyles: StyleProp<ViewStyle>) => {
        return (
            <Badge
                details={generateDefectStatusBadge(latestStatus)}
                textStyles={textStyles}
            />
        );
    };

    return (
        <View style={[styles.rootContainer]} testID={testID}>
            <Pressable
                style={({ pressed }) =>
                    pressed
                        ? [styles.btnContainer, style, styles.pressed]
                        : [styles.btnContainer, style]
                }
                onPress={onPress}
                testID={testID}
                accessibilityLabel={accessibilityLabel}
                android_ripple={{ color: Colors.$backgroundPrimaryLight }}>
                <View style={styles.rowContainer}>
                    {imageUrl.length > 0 ? (
                        <View style={styles.imageContainer}>
                            {imageUrl.includes('mp4') ||
                            imageUrl.includes('quicktime') ? (
                                <VideoPlayer
                                    source={{
                                        uri: getImageUrl(imageUrl!),
                                    }}
                                    resizeMode="contain"
                                    paused={true}
                                    alwaysShowControls={false}
                                    disableSeekButtons={true}
                                    disableBack={true}
                                    muted={true}
                                    repeat={false}
                                    disableFullscreen={true}
                                    disablePlayPause={true}
                                    disableOverlay={true}
                                    disableSeekbar={true}
                                    disableTimer={true}
                                    disableVolume={true}
                                    contentStartTime={1000}
                                    currentPlaybackTime={1000}
                                    containerStyle={styles.image}
                                />
                            ) : (
                                <Image
                                    source={{
                                        uri: getImageUrl(imageUrl!),
                                    }}
                                    errorSource={require('../../../../assets/noImage/default-no-image.jpg')}
                                    style={styles.image}
                                />
                            )}
                        </View>
                    ) : (
                        <View style={styles.imageContainer}>
                            <Image
                                source={require('../../../../assets/noImage/default-no-image.jpg')}
                                errorSource={require('../../../../assets/noImage/default-no-image.jpg')}
                                style={styles.image}
                            />
                        </View>
                    )}
                    <View style={styles.rightContainer}>
                        <View style={styles.defectIdBadgeContainer}>
                            <NormalText
                                underline={false}
                                style={[
                                    styles.defectRefNoText,
                                    styles.purpleColorStyle,
                                ]}>
                                {defectReferenceNo}
                            </NormalText>
                            <View style={styles.badgeContainer}>
                                {badge(styles.badgeText)}
                            </View>
                        </View>
                        <View style={styles.innerRowContainer}>
                            <SubHeaderText
                                underline={false}
                                style={styles.defectNameText}>
                                {truncatedName}
                            </SubHeaderText>
                        </View>

                        <View style={styles.innerRowContainer}>
                            <NormalText style={styles.defectDescText}>
                                {truncatedDescription}
                            </NormalText>
                        </View>
                    </View>
                </View>
                <View style={styles.fontContainer}>
                    <FontAwesomeIcon
                        icon={faChevronRight}
                        style={styles.purpleColorStyle}
                        size={24}
                    />
                </View>
            </Pressable>
        </View>
    );
};

export default DefectCard;
