import { StyleSheet } from 'react-native';

import { BorderRadiuses, Shadows, Spacings } from 'react-native-ui-lib';
import Colors from '../../../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        borderRadius: BorderRadiuses.br30,
        overflow: 'hidden',
    },
    btnContainer: {
        backgroundColor: Colors.$backgroundDefault,
        marginVertical: Spacings.s2,
        paddingVertical: Spacings.s3,
        paddingHorizontal: Spacings.s3,
        borderColor: Colors.$outlinePrimary,
        borderWidth: 2,
        borderRadius: BorderRadiuses.br30,
    },
    pressed: {
        ...Shadows.sh30.bottom,
    },
    rowContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    textContainer: {
        flex: 1,
    },
    titleText: {
        color: Colors.$textPrimary,
    },
    iconContainer: {
        justifyContent: 'flex-end',
    },
    icon: {
        color: Colors.$iconPrimary,
    },
});
