import {
    Pressable,
    GestureResponderEvent,
    StyleProp,
    ViewStyle,
} from 'react-native';
import React from 'react';
import { View } from 'react-native-ui-lib';
import styles from './SettingsCardStyle';
import Colors from '../../../../Constants/Colors';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import SubHeaderText from '../../titles/SubHeaderText';

interface ISettingsCard {
    onPress: (_: GestureResponderEvent) => void;
    style?: StyleProp<ViewStyle>;
    testID?: string;
    label: string;
    title: string;
}

const SettingsCard = ({
    onPress,
    style = {},
    label,
    testID = 'settings-card',
    title,
}: ISettingsCard) => {
    return (
        <View style={[styles.rootContainer]} testID={testID}>
            <Pressable
                style={({ pressed }) =>
                    pressed
                        ? [styles.btnContainer, style, styles.pressed]
                        : [styles.btnContainer, style]
                }
                onPress={onPress}
                testID={label}
                android_ripple={{ color: Colors.$backgroundPrimaryLight }}>
                <View style={styles.rowContainer}>
                    <View style={styles.textContainer}>
                        <SubHeaderText
                            underline={false}
                            style={styles.titleText}>
                            {title}
                        </SubHeaderText>
                    </View>
                    <View style={styles.iconContainer}>
                        <FontAwesomeIcon
                            icon={faChevronRight}
                            style={styles.icon}
                            size={24}
                        />
                    </View>
                </View>
            </Pressable>
        </View>
    );
};

export default SettingsCard;
