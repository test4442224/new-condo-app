import { StyleSheet } from 'react-native';
import {
    BorderRadiuses,
    Shadows,
    Spacings,
    Typography,
} from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';
export default StyleSheet.create({
    rootContainer: {
        borderRadius: BorderRadiuses.br30,
        overflow: 'hidden',
    },
    btnContainer: {
        backgroundColor: Colors.$backgroundPrimaryLight,
        paddingVertical: Spacings.s4,
        paddingHorizontal: Spacings.s4,
        justifyContent: 'center',
    },
    textBtn: {
        ...Typography.text70,
        color: Colors.$textDisabled,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    pressed: {
        ...Shadows.sh30.bottom,
    },
    pressedText: {
        ...Typography.text70,
        color: Colors.$textPrimary,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
});
