import {
    Pressable,
    GestureResponderEvent,
    StyleProp,
    ViewStyle,
} from 'react-native';
import React from 'react';
import {
    ManageBookingAttributes,
    PaymentStatus,
} from '../../../../store/manageBooking/slice';
import {
    faChevronRight,
    faLocationDot,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import SubHeaderText from '../../titles/SubHeaderText';
import NormalText from '../../titles/NormalText';
import Badge from '../../badge/Badge';
import { View } from 'react-native-ui-lib';
import styles from './BookingCardStyle';
import Colors from '../../../../Constants/Colors';
import {
    isBeforeCurrentTime,
    to12HourFormat,
    toDateFormat,
} from '../../../../utils/dateTimeUtil';
import { generatePaymentStatusBadge } from '../../../../utils/GeneratePaymentStatusBadge';

interface IBookingCard {
    booking: ManageBookingAttributes;
    onPress: (_: GestureResponderEvent) => void;
    style?: StyleProp<ViewStyle>;
    testID?: string;
    label: string;
}

const BookingCard = ({
    onPress,
    booking,
    style = {},
    testID,
}: IBookingCard) => {
    const { startDateTime, endDateTime, cancelledAt } = booking;
    const {
        name: facilityName,
        type: facilityType,
        location,
    } = booking.facility;
    const isCompleted = isBeforeCurrentTime(endDateTime);
    const isPast = cancelledAt || isCompleted;
    const badge = () => {
        if (
            cancelledAt ||
            isCompleted ||
            booking.paymentStatus === PaymentStatus.PENDING_PAYMENT ||
            booking.paymentStatus === PaymentStatus.PENDING_VERIFICATION
        ) {
            return (
                <Badge
                    details={generatePaymentStatusBadge(booking)}
                    textStyles={styles.badge}
                />
            );
        }
        return <View />;
    };

    return (
        <View style={[styles.rootContainer]} testID={testID}>
            <Pressable
                style={({ pressed }) =>
                    pressed
                        ? [styles.btnContainer, style, styles.pressed]
                        : [styles.btnContainer, style]
                }
                onPress={onPress}
                testID={testID}
                accessibilityLabel={testID}
                android_ripple={{ color: Colors.$backgroundPrimaryLight }}>
                <View>
                    <View style={styles.badgeContainer}>{badge()}</View>

                    <View style={styles.rowContainer}>
                        <NormalText
                            underline={false}
                            style={styles.facilityText}>
                            {facilityType}: {facilityName}
                        </NormalText>
                    </View>
                    <View style={styles.rowContainer}>
                        <SubHeaderText
                            underline={false}
                            style={[styles.dateText, styles.purpleColorStyle]}>
                            {toDateFormat(startDateTime)},{' '}
                            {to12HourFormat(startDateTime)} -{' '}
                            {to12HourFormat(endDateTime)}
                        </SubHeaderText>
                    </View>

                    {!isPast && (
                        <View style={styles.rowContainer}>
                            <FontAwesomeIcon
                                icon={faLocationDot}
                                style={styles.purpleColorStyle}
                            />
                            <View style={styles.locationTextContainer}>
                                <NormalText style={styles.locationText}>
                                    {location}
                                </NormalText>
                            </View>
                        </View>
                    )}
                </View>
                <FontAwesomeIcon
                    icon={faChevronRight}
                    style={styles.purpleColorStyle}
                    size={24}
                />
            </Pressable>
        </View>
    );
};

export default BookingCard;
