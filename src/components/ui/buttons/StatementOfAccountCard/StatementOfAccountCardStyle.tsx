import { StyleSheet } from 'react-native';

import {
    BorderRadiuses,
    Shadows,
    Spacings,
    Typography,
} from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        borderRadius: BorderRadiuses.br30,
        overflow: 'hidden',
    },
    btnContainer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: Colors.$backgroundDefault,
        marginHorizontal: Spacings.s3,
        marginVertical: Spacings.s2,
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s3,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderColor: Colors.$outlinePrimary,
        borderWidth: 2,
        borderRadius: BorderRadiuses.br30,
    },
    rowContainer: {
        flexDirection: 'row',
        paddingTop: Spacings.s2,
    },
    textContainer: {
        flex: 1,
    },
    titleText: {
        ...Typography.text70,
        fontWeight: WEIGHT_TYPES.BOLD,
        fontSize: 18,
        color: Colors.$backgroundPrimary,
    },
    subTitleText: {
        ...Typography.text70,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    iconColor: {
        color: Colors.$outlinePrimary,
        transform: [{ rotate: '90deg' }],
    },
    pressed: {
        ...Shadows.sh30.bottom,
    },
});
