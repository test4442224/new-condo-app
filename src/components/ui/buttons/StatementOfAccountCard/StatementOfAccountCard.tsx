import {
    Pressable,
    GestureResponderEvent,
    StyleProp,
    ViewStyle,
} from 'react-native';
import React from 'react';
import NormalText from '../../titles/NormalText';
import { View } from 'react-native-ui-lib';
import styles from './StatementOfAccountCardStyle';
import Colors from '../../../../Constants/Colors';
import { toDateFormat } from '../../../../utils/dateTimeUtil';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowRightToBracket } from '@fortawesome/free-solid-svg-icons';

interface IStatementOfAccountCard {
    onPress: (_: GestureResponderEvent) => void;
    data: { fileName: string; createdAt: Date };
    style?: StyleProp<ViewStyle>;
    testID?: string;
    pressableTestId?: string;
}

const StatementOfAccountCard = ({
    onPress,
    data,
    style = {},
    testID = 'statement-of-account-card',
}: IStatementOfAccountCard) => {
    const { fileName, createdAt } = data;

    return (
        <View
            style={[styles.rootContainer]}
            testID={testID}
            accessibilityLabel={testID}>
            <Pressable
                style={({ pressed }) =>
                    pressed
                        ? [styles.btnContainer, style, styles.pressed]
                        : [styles.btnContainer, style]
                }
                onPress={onPress}
                android_ripple={{ color: Colors.$backgroundPrimaryLight }}>
                <View style={styles.textContainer}>
                    <View style={styles.rowContainer}>
                        <NormalText underline={false} style={styles.titleText}>
                            {fileName}
                        </NormalText>
                    </View>
                    <View style={styles.rowContainer}>
                        <NormalText
                            underline={false}
                            style={
                                styles.subTitleText
                            }>{`Generated on ${toDateFormat(createdAt)}`}</NormalText>
                    </View>
                </View>
                <FontAwesomeIcon
                    style={styles.iconColor}
                    icon={faArrowRightToBracket}
                    size={24}
                />
            </Pressable>
        </View>
    );
};

export default StatementOfAccountCard;
