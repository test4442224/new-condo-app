import React from 'react';
import { ButtonProps } from './Button';
import Button from './Button';
import { Typography } from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';

const SecondaryButton = ({
    onPress,
    disabled,
    style = {},
    label,
    testID = 'secondary-button',
    size = 'medium',
    accessibilityLabel,
}: ButtonProps) => {
    return (
        <Button
            testID={testID}
            accessibilityLabel={accessibilityLabel}
            label={label}
            onPress={onPress}
            disabled={disabled}
            style={[style]}
            labelStyle={[
                { ...Typography.text80, fontWeight: WEIGHT_TYPES.BOLD },
            ]}
            size={size}
            outline
        />
    );
};

export default SecondaryButton;
