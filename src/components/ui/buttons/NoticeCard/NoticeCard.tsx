import {
    Pressable,
    GestureResponderEvent,
    StyleProp,
    ViewStyle,
} from 'react-native';
import React from 'react';
import SubHeaderText from '../../titles/SubHeaderText';
import NormalText from '../../titles/NormalText';
import { View } from 'react-native-ui-lib';
import styles from './NoticeCardStyle';
import Colors from '../../../../Constants/Colors';
import { NoticeAttributes } from '../../../../store/notice/slice';
import { formatDistanceToNow } from 'date-fns';

interface INoticeCard {
    notice: NoticeAttributes;
    onPress: (_: GestureResponderEvent) => void;
    style?: StyleProp<ViewStyle>;
    testID?: string;
    pressableTestId: string;
}

const NoticeCard = ({
    onPress,
    notice,
    style = {},
    pressableTestId,
    testID = 'notice-card',
}: INoticeCard) => {
    const { title, createdAt } = notice;

    const nearestDate = formatDistanceToNow(new Date(createdAt));

    return (
        <View style={[styles.rootContainer]} testID={testID}>
            <Pressable
                style={({ pressed }) =>
                    pressed
                        ? [styles.btnContainer, style, styles.pressed]
                        : [styles.btnContainer, style]
                }
                onPress={onPress}
                testID={pressableTestId}
                android_ripple={{ color: Colors.$backgroundPrimaryLight }}>
                <View style={styles.textContainer}>
                    <View style={styles.rowContainer}>
                        <NormalText underline={false} style={styles.titleText}>
                            {title}
                        </NormalText>
                    </View>
                    <View style={styles.dateTimeContainer}>
                        <SubHeaderText
                            underline={false}
                            style={[styles.dateText, styles.purpleColorStyle]}>
                            {`Posted ${nearestDate} ago`}
                        </SubHeaderText>
                    </View>
                </View>
            </Pressable>
        </View>
    );
};

export default NoticeCard;
