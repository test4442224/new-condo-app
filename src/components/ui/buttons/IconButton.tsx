import React from 'react';
import {
    View,
    Pressable,
    GestureResponderEvent,
    StyleProp,
    ViewStyle,
    TextStyle,
} from 'react-native';
import styles from './IconButtonStyle';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import Colors from '../../../Constants/Colors';

interface IIconButton {
    icon: IconProp;
    onPress: (_: GestureResponderEvent) => void;
    testID?: string;
    style?: StyleProp<ViewStyle>;
    iconStyle?: StyleProp<TextStyle>;
    container?: boolean;
    size?: number;
}

const IconButton = ({
    icon,
    onPress,
    testID = 'icon-button',
    style = {},
    iconStyle = {},
    container = false,
    size = 30,
}: IIconButton) => {
    return (
        <Pressable
            style={({ pressed }) =>
                pressed
                    ? [styles.btnContainer, styles.pressed]
                    : styles.btnContainer
            }
            testID={testID}
            accessibilityLabel={testID}
            onPress={onPress}>
            {container ? (
                <View style={[styles.rootContainer, style]}>
                    <FontAwesomeIcon
                        icon={icon}
                        style={[{ color: Colors.$iconPrimary }, iconStyle]}
                        size={size}
                    />
                </View>
            ) : (
                <FontAwesomeIcon
                    icon={icon}
                    style={[{ color: Colors.$iconPrimary }, iconStyle]}
                    size={size}
                />
            )}
        </Pressable>
    );
};

export default IconButton;
