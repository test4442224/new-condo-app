import React from 'react';
import {
    View,
    Pressable,
    GestureResponderEvent,
    ViewStyle,
    StyleProp,
} from 'react-native';
import styles from './TabButtonStyle';
import NormalText from '../titles/NormalText';
import Colors from '../../../Constants/Colors';
interface ITabButton {
    children: React.ReactNode;
    onPress: (_: GestureResponderEvent) => void;
    style?: StyleProp<ViewStyle>;
    isActive: boolean;
    testID?: string;
}
const TabButton = ({
    children,
    onPress,
    style = {},
    isActive,
    testID = 'booking-tab-button',
}: ITabButton) => {
    return (
        <View style={[styles.rootContainer]} testID={testID}>
            <Pressable
                style={({ pressed }) =>
                    pressed
                        ? [styles.btnContainer, style, styles.pressed]
                        : styles.btnContainer
                }
                onPress={onPress}
                data-testid={testID}
                accessibilityLabel={testID}
                android_ripple={{ color: Colors.$backgroundPrimaryLight }}>
                <NormalText
                    center={true}
                    style={isActive ? styles.pressedText : styles.textBtn}>
                    {children}
                </NormalText>
            </Pressable>
        </View>
    );
};
export default TabButton;
