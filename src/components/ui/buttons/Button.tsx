import React from 'react';
import { Button as RNUiLibButton } from 'react-native-ui-lib';
import { ButtonPaddings } from '../../../Constants/ButtonConstants';
import {
    GestureResponderEvent,
    StyleProp,
    TextStyle,
    ViewStyle,
} from 'react-native';
import { ButtonSizeProp } from 'react-native-ui-lib/src/components/button/ButtonTypes';

export interface ButtonProps {
    onPress: (_: GestureResponderEvent) => void;
    style?: StyleProp<ViewStyle>;
    testID?: string;
    label: string;
    disabled?: boolean;
    size?: ButtonSizeProp;
    labelStyle?: StyleProp<TextStyle>;
    outline?: boolean;
    avoidMinWidth?: boolean;
    accessibilityLabel?: string;
}

const Button = ({
    onPress,
    style = {},
    label,
    testID = 'primary-button',
    disabled = false,
    size = 'medium',
    labelStyle = {},
    outline = false,
    avoidMinWidth = false,
    accessibilityLabel,
}: ButtonProps) => {
    return (
        <RNUiLibButton
            accessibilityLabel={accessibilityLabel}
            testID={testID}
            label={label}
            avoidInnerPadding
            onPress={onPress}
            disabled={disabled}
            outline={outline}
            size={size}
            labelStyle={labelStyle}
            style={[
                {
                    paddingVertical: ButtonPaddings[size],
                },
                style,
            ]}
            avoidMinWidth={avoidMinWidth}
        />
    );
};

export default Button;
