import { StyleSheet } from 'react-native';
import { BorderRadiuses, Spacings } from 'react-native-ui-lib';
import Colors from '../../../Constants/Colors';
import Typography from 'react-native-ui-lib/src/style/typographyPresets';

export default StyleSheet.create({
    swiperContainer: {
        width: '100%',
        height: 250,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        paddingHorizontal: Spacings.s1,
    },
    innerContainer: {
        overflow: 'visible',
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        marginHorizontal: Spacings.s2,
    },
    attachment: {
        width: '100%',
        height: 200,
        borderRadius: BorderRadiuses.br60,
    },
    badgeContainer: {
        position: 'absolute',
        bottom: 15,
        left: 10,
    },
    badgeText: {
        ...Typography.text90,
    },
    containImage: {
        resizeMode: 'cover',
    },
    containVideo: {
        height: '100%',
        width: '100%',
        resizeMode: 'contain',
        borderColor: Colors.$outlineDefault,
        borderWidth: 1,
    },

    dotBase: {
        width: 8,
        height: 8,
        borderRadius: BorderRadiuses.br20,
        margin: Spacings.s1,
    },
    dot: {
        backgroundColor: Colors.$backgroundDisabled,
    },
    activeDot: {
        backgroundColor: Colors.$iconPrimary,
    },
    pagination: {
        bottom: 1, // -30 Adjust this value as per your design
    },
});
