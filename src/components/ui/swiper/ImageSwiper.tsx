import React, { useState } from 'react';
import {
    View,
    Image,
    TouchableOpacity,
    ViewStyle,
    StyleProp,
    Platform,
    Pressable,
} from 'react-native';
import styles from './ImageSwiperStyle';
import { Carousel } from 'react-native-ui-lib';
import { NoticeAttributes } from '../../../store/notice/slice';
import { getImageUrl } from '../../../Constants/api';
import VideoPlayer from 'react-native-media-console';
import ImageLoading from '../loading/ImageLoading';
import Badge from '../badge/Badge';
import { generateAttachmentTypeBadge } from '../../../utils/GenerateAttachmentTypeBadge';

interface ISwiper {
    imageURL?: string[];
    autoplay: boolean;
    onPressHandler?: (carouselNotice: NoticeAttributes) => void;
    onPressDefectHandler?: () => void;
    hidden?: boolean;
    carouselNoticeArray?: NoticeAttributes[];
    isDefectPage?: boolean;
}

const ImageSwiper = ({
    imageURL,
    autoplay,
    onPressHandler,
    onPressDefectHandler,
    hidden = false,
    carouselNoticeArray,
    isDefectPage = false,
}: ISwiper) => {
    let defaultImage: string = '../../../assets/noImage/default-no-image.jpg';

    const [loading, setLoading] = useState(true);

    const [pause, setPause] = useState(true);

    const handleLoad = () => {
        setLoading(false);
    };

    const badge = (
        textStyles: StyleProp<ViewStyle>,
        attachmentType: string,
    ) => {
        return (
            <Badge
                details={generateAttachmentTypeBadge(attachmentType)}
                textStyles={textStyles}
            />
        );
    };

    const renderedDefectDetailsPopup = () => {
        return imageURL?.map((image, index) => (
            <Pressable
                style={styles.slide}
                key={index}
                testID="defect-details-popup.image"
                accessibilityLabel="defect-details-popup.image"
                onPress={
                    onPressDefectHandler
                        ? () => onPressDefectHandler()
                        : () => setPause(!pause)
                }>
                {loading &&
                    ((Platform.OS === 'android' && !image.includes('mp4')) ||
                        Platform.OS === 'ios') && <ImageLoading />}
                {image.includes('mp4') || image.includes('quicktime') ? (
                    <>
                        <VideoPlayer
                            source={{
                                uri: getImageUrl(image),
                            }}
                            resizeMode="contain"
                            paused={pause}
                            repeat={false}
                            alwaysShowControls={false}
                            disableSeekButtons={true}
                            disableBack={true}
                            onLoad={handleLoad}
                            muted={true}
                            contentStartTime={1000}
                            currentPlaybackTime={1000}
                            containerStyle={
                                isDefectPage
                                    ? [styles.attachment, styles.containVideo]
                                    : styles.attachment
                            }
                            onEnterFullscreen={onPressDefectHandler}
                        />
                        <View style={styles.badgeContainer}>
                            {badge(styles.badgeText, 'Video')}
                        </View>
                    </>
                ) : (
                    <>
                        <Image
                            source={{
                                uri: getImageUrl(image),
                            }}
                            style={
                                isDefectPage
                                    ? [styles.attachment, styles.containImage]
                                    : styles.attachment
                            }
                            onLoad={handleLoad}
                        />
                        <View style={styles.badgeContainer}>
                            {badge(styles.badgeText, 'Image')}
                        </View>
                    </>
                )}
            </Pressable>
        ));
    };

    return (
        <View
            style={styles.swiperContainer}
            accessibilityLabel="carousel-image-swiper"
            testID="carousel-image-swiper">
            <Carousel
                key={imageURL ? imageURL.length : carouselNoticeArray?.length}
                autoplay={autoplay}
                loop={
                    imageURL
                        ? imageURL.length > 1
                        : carouselNoticeArray!.length > 1
                }
                pageControlPosition={
                    hidden ? undefined : Carousel.pageControlPositions.UNDER
                }
                allowAccessibleLayout>
                {imageURL?.length === 0 ? (
                    <View style={styles.slide}>
                        <Image
                            source={require(defaultImage)}
                            style={
                                isDefectPage
                                    ? [styles.attachment, styles.containImage]
                                    : styles.attachment
                            }
                        />
                    </View>
                ) : isDefectPage && imageURL ? (
                    renderedDefectDetailsPopup()
                ) : carouselNoticeArray && onPressHandler ? (
                    carouselNoticeArray.map(carouselNotice => (
                        <TouchableOpacity
                            style={styles.slide}
                            key={carouselNotice.id}
                            onPress={() => onPressHandler(carouselNotice)}>
                            <Image
                                source={{
                                    uri: getImageUrl(carouselNotice.imageUrl!),
                                }}
                                style={styles.attachment}
                            />
                        </TouchableOpacity>
                    ))
                ) : (
                    imageURL!.map((image, index) => (
                        <View style={styles.slide} key={index}>
                            <Image
                                source={{
                                    uri: getImageUrl(image),
                                }}
                                style={
                                    isDefectPage
                                        ? [
                                              styles.attachment,
                                              styles.containImage,
                                          ]
                                        : styles.attachment
                                }
                            />
                        </View>
                    ))
                )}
            </Carousel>
        </View>
    );
};

export default ImageSwiper;
