import { StyleSheet } from 'react-native';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    calendarHeaderText: {
        color: Colors.$textPrimary,
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    dateText: {
        color: Colors.$textPrimary,
    },
    todayTextStyle: {
        color: Colors.$backgroundDefault,
    },
});
