import styles from './CalendarStyle';

import { faCaretLeft, faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React, { FC } from 'react';
import CalendarPicker, {
    DateChangedCallback,
    DisabledDatesFunc,
} from 'react-native-calendar-picker';
import Colors from '../../../Constants/Colors';
import { View } from 'react-native-ui-lib';

interface CalendarProps {
    chosenDate: string | null;
    onDateChange: DateChangedCallback;
    maxDate: Date;
    minDate: Date;
    disabledDates?: Date[] | DisabledDatesFunc;
}

const Calendar: FC<CalendarProps> = ({
    onDateChange,
    chosenDate,
    maxDate,
    minDate,
    disabledDates,
}) => {
    return (
        <CalendarPicker
            onDateChange={onDateChange}
            minDate={minDate}
            maxDate={maxDate}
            selectedStartDate={new Date(chosenDate || '') || undefined}
            selectedDayColor={'none'}
            selectedDayTextColor={Colors.$textDefault}
            todayBackgroundColor={Colors.$textPrimary}
            todayTextStyle={styles.todayTextStyle}
            dayShape="circle"
            yearTitleStyle={styles.calendarHeaderText}
            monthTitleStyle={styles.calendarHeaderText}
            disabledDates={disabledDates}
            previousComponent={
                <FontAwesomeIcon
                    icon={faCaretLeft}
                    size={30}
                    color={Colors.$iconPrimary}
                />
            }
            nextComponent={
                <View testID="go-to-next-month">
                    <FontAwesomeIcon
                        icon={faCaretRight}
                        size={30}
                        color={Colors.$iconPrimary}
                    />
                </View>
            }
        />
    );
};

export default Calendar;
