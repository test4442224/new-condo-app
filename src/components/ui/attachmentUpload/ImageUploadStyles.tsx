import { StyleSheet } from 'react-native';
import { BorderRadiuses, Spacings } from 'react-native-ui-lib';
import Colors from '../../../Constants/Colors';
import Typography from 'react-native-ui-lib/src/style/typographyPresets';

export default StyleSheet.create({
    cameraContainer: {
        paddingVertical: Spacings.s2,
    },
    imageRow: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    imageBox: {
        flex: 1,
        borderWidth: 1,
        borderStyle: 'dotted',
        borderColor: Colors.$outlinePrimary,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: BorderRadiuses.br30,
        flexDirection: 'column',
    },
    largeImageBox: {
        paddingVertical: Spacings.s10,
        paddingHorizontal: Spacings.s3,
    },
    smallImageBox: {
        marginBottom: Spacings.s5,
    },
    iconContainer: {
        alignItems: 'center',
    },
    cameraIcon: {
        width: 60,
        height: 60,
    },
    smallCameraIcon: {
        width: 55 * 0.7,
        height: 55 * 0.7,
    },
    photoInstructions: {
        ...Typography.text70,
    },
    smallPhotoInstructions: {
        ...Typography.text90L,
    },
    textBox: {
        marginVertical: Spacings.s1,
        alignItems: 'center',
    },
    subInstructions: {
        ...Typography.text90L,
        fontStyle: 'italic',
    },
    smallSubInstructions: {
        ...Typography.text100L,
        fontStyle: 'italic',
    },
});
