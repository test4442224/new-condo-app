import React, { useEffect, useState } from 'react';
import { Platform, Pressable, View, Dimensions } from 'react-native';
import Text from '../titles/Text';
import styles from './ImageUploadStyles';
import ImageAttachment from '../../../screens/Defect/CreateDefect/ImagesAttachment/ImageAttachment';
import { ImageData } from '../../../store/defect/attachment/api';
import {
    ImageResults,
    MediaType,
    VideoResults,
    openPicker,
} from '@baronha/react-native-multiple-image-picker';
import ImageResizer from 'react-native-image-resizer';
import Colors from '../../../Constants/Colors';
import { Image } from 'react-native-ui-lib';
import {
    MAX_ATTACHMENT_SIZE_IN_MB,
    MAX_IMAGE_LIMIT,
    MAX_VIDEO_LIMIT,
} from '../../../Constants/attachment';
import { DefectAttachment } from '../../../models/defect/DefectAttachment';
import PopModal from '../../modal/PopModal';

interface IAttachmentUpload {
    existingAttachment: DefectAttachment[] | null;
    setSelectedAttachments: (attachment: ImageData[]) => void;
    setRemoveAttachmentIds?: (attachmentId: number[]) => void;
}
const ImageUpload = ({
    existingAttachment,
    setSelectedAttachments,
    setRemoveAttachmentIds,
}: IAttachmentUpload) => {
    const [images, setImages] = useState<ImageData[]>([]);
    const [noOfImages, setNoOfImages] = useState(MAX_IMAGE_LIMIT);
    const [noOfVideos, setNoOfVideos] = useState(MAX_VIDEO_LIMIT);
    const [removeImageIndex, setRemoveImageIndex] = useState<number[]>([]);
    const screenWidth = Dimensions.get('window').width;
    const imageWidth = (screenWidth - 60) / 2;
    const [attachmentSizeError, setAttachmentSizeError] =
        useState<boolean>(false);

    useEffect(() => {
        if (existingAttachment) {
            existingAttachment.forEach(attachment => {
                const attachmentData: ImageData = {
                    name: attachment.attachmentPath.substring(
                        attachment.attachmentPath.lastIndexOf('/') + 1,
                    ),
                    type: attachment.attachmentType,
                    uri: attachment.attachmentPath,
                };

                setImages(prevImages => [...prevImages, attachmentData]);

                if (attachment.attachmentType.includes('image')) {
                    setNoOfImages(prevNoOfImages => prevNoOfImages - 1);
                } else if (attachment.attachmentType.includes('video')) {
                    setNoOfVideos(prevNoOfVideos => prevNoOfVideos - 1);
                    setNoOfImages(prevNoOfImages => prevNoOfImages - 1);
                }
            });
        }
    }, [existingAttachment]);

    useEffect(() => {
        setSelectedAttachments(images);
    }, [images, setSelectedAttachments]);

    const handleRemoveImage = (index: number) => {
        if (existingAttachment && setRemoveImageIndex) {
            const imageName = images[index].name;
            const attachmentId = getAttachmentId(imageName);
            if (attachmentId) {
                setRemoveImageIndex(prevRemoveImageIndex => [
                    ...prevRemoveImageIndex,
                    attachmentId,
                ]);
            }
        }

        const newImages = [...images];
        const removedImage = newImages.splice(index, 1);
        if (removedImage[0].type.includes('video')) {
            setNoOfVideos(noOfVideos + 1);
        }
        setNoOfImages(noOfImages + 1);
        setImages(newImages);
    };

    useEffect(() => {
        if (setRemoveAttachmentIds) {
            setRemoveAttachmentIds(removeImageIndex);
        }
    }, [removeImageIndex, setRemoveAttachmentIds]);

    const uploadMedia = async () => {
        let type = 'all';
        let maxMedia = Math.max(noOfImages, noOfVideos);
        if (noOfVideos === 0) {
            type = 'image';
        }
        const response = await openPicker({
            maxSelectedAssets: maxMedia,
            mediaType: type as MediaType,
            usedCameraButton: true,
            isPreview: false,
            maxVideoDuration: 11,
            allowedVideoRecording: type === 'all',
            selectedColor: Colors.$backgroundPrimary,
            ...(type === 'all' ? { maxVideo: noOfVideos } : {}),
        });

        const filteredResponse = response.filter(file => {
            return file.size <= MAX_ATTACHMENT_SIZE_IN_MB * 1024 * 1024;
        });
        handleMediaSelection(filteredResponse);
        setAttachmentSizeError(response.length !== filteredResponse.length);
    };

    const handleMediaSelection = async (
        response: (VideoResults | ImageResults)[],
    ) => {
        let imageCount = noOfImages;
        let videoCount = noOfVideos;
        if (response.length === 0) {
            return;
        }

        for (const element of response) {
            let { mime, path, fileName, realPath } = element;

            imageCount = imageCount - 1;

            let filePath = path;
            let processedFileName = fileName;
            let processedMime = mime;

            if (mime.includes('video')) {
                videoCount = videoCount - 1;
            }

            if (Platform.OS === 'android') {
                filePath = 'file://' + realPath;
            }

            // Compress image
            if (mime.includes('image')) {
                let imageCompressed = await compressImage(filePath);
                filePath = imageCompressed.uri;
                fileName = imageCompressed.name;
                mime = 'image/png';
            }

            setImages(prevImages => [
                ...prevImages,
                {
                    name: processedFileName || '',
                    type: processedMime || '',
                    uri: filePath!,
                },
            ]);
        }

        setSelectedAttachments(images);
        setNoOfImages(imageCount);
        setNoOfVideos(videoCount);
    };
    const compressImage = (uri: string) => {
        return ImageResizer.createResizedImage(uri, 720, 720, 'PNG', 100);
    };
    const getAttachmentId = (imageName: string): number | undefined => {
        if (existingAttachment) {
            const matchingAttachment = existingAttachment.find(attachment =>
                attachment.attachmentPath.endsWith(imageName),
            );
            return matchingAttachment?.id;
        }
        return undefined;
    };

    return (
        <View style={styles.cameraContainer}>
            <View style={images.length > 0 && styles.imageRow}>
                {images.length < Math.max(MAX_IMAGE_LIMIT, MAX_VIDEO_LIMIT) && (
                    <Pressable
                        testID="upload-media-button"
                        accessibilityLabel="upload-media-button"
                        onPress={() => {
                            uploadMedia();
                        }}>
                        <View
                            style={
                                images.length === 0
                                    ? [styles.imageBox, styles.largeImageBox]
                                    : [
                                          styles.imageBox,
                                          styles.smallImageBox,
                                          { width: imageWidth },
                                      ]
                            }>
                            <View style={styles.iconContainer}>
                                <Image
                                    source={require('../../../assets/cameraAttachment/camera-add.png')}
                                    errorSource={require('../../../assets/cameraAttachment/camera-add.png')}
                                    style={
                                        images.length === 0
                                            ? styles.cameraIcon
                                            : styles.smallCameraIcon
                                    }
                                />
                            </View>
                            <View style={styles.textBox}>
                                <Text
                                    style={
                                        images.length === 0
                                            ? styles.photoInstructions
                                            : styles.smallPhotoInstructions
                                    }>
                                    Take a Photo / Video
                                </Text>
                                <Text
                                    style={
                                        images.length === 0
                                            ? styles.subInstructions
                                            : styles.smallSubInstructions
                                    }>
                                    Max 6 attachments
                                </Text>
                            </View>
                        </View>
                    </Pressable>
                )}
                {images.map((image, index) => (
                    <ImageAttachment
                        key={index}
                        imageUrl={image.uri}
                        onPressRemove={() => handleRemoveImage(index)}
                    />
                ))}
                <PopModal
                    modalTitle={'Error Uploading Video'}
                    modalBody={
                        <Text>
                            {
                                'Your video does not fulfil the requirement. Please upload a video that is less than 60MB.'
                            }
                        </Text>
                    }
                    visibility={attachmentSizeError}
                    confirmText="Okay"
                    onConfirm={() => {
                        setAttachmentSizeError(false);
                    }}
                    testID="attachment-size-error-modal"
                />
            </View>
        </View>
    );
};
export default ImageUpload;
