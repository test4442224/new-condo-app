import React from 'react';
import Text, { TextProps } from './Text';
import Colors from '../../../Constants/Colors';

const NormalText = ({
    style = {},
    testID = 'normal-text',
    children,
    center = false,
    underline = false,
}: TextProps) => {
    return (
        <Text
            style={[style]}
            testID={testID}
            accessibilityLabel={testID}
            center={center}
            underline={underline}
            color={Colors.$textDefault}>
            {children}
        </Text>
    );
};

export default NormalText;
