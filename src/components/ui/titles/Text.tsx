import React from 'react';
import { Text as RNUiLibText } from 'react-native-ui-lib';
import { StyleProp, TextStyle } from 'react-native';

export interface TextProps {
    style?: StyleProp<TextStyle>;
    testID?: string;
    children?: React.ReactNode;
    center?: boolean;
    underline?: boolean;
    color?: string;
    accessibilityLabel?: string;
}

const Text = ({
    style = {},
    testID = 'text',
    children,
    center = false,
    underline = false,
    color,
    accessibilityLabel,
}: TextProps) => {
    return (
        <RNUiLibText
            testID={testID}
            accessibilityLabel={accessibilityLabel}
            style={[style]}
            center={center}
            underline={underline}
            color={color}>
            {children}
        </RNUiLibText>
    );
};

export default Text;
