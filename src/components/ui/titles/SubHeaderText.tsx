import React from 'react';
import Text, { TextProps } from './Text';
import Typography, {
    WEIGHT_TYPES,
} from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';

const SubHeaderText = ({
    style = {},
    testID = 'subheader-text',
    children,
    center = false,
    underline = true,
}: TextProps) => {
    return (
        <Text
            style={[
                { ...Typography.text70, fontWeight: WEIGHT_TYPES.BOLD },
                style,
            ]}
            testID={testID}
            center={center}
            underline={underline}
            color={Colors.$textDefault}>
            {children}
        </Text>
    );
};

export default SubHeaderText;
