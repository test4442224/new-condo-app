import React from 'react';
import NormalText from './NormalText';
import { TextProps } from 'react-native-ui-lib';

const BoldText = ({
    style = {},
    testID = 'bold-text',
    children,
    center = false,
    underline = false,
}: TextProps) => {
    return (
        <NormalText
            style={[style, { fontWeight: 'bold' }]} // Apply the bold style
            testID={testID}
            center={center}
            underline={underline}>
            {children}
        </NormalText>
    );
};

export default BoldText;
