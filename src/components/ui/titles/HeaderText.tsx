import React from 'react';
import Text, { TextProps } from './Text';
import { Typography } from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';

const HeaderText = ({
    style = {},
    testID = 'header-text',
    children,
    center = true,
    underline = false,
}: TextProps) => {
    return (
        <Text
            style={[
                style,
                { ...Typography.text50, fontWeight: WEIGHT_TYPES.BOLD },
            ]}
            testID={testID}
            center={center}
            underline={underline}
            color={Colors.$textPrimary}>
            {children}
        </Text>
    );
};

export default HeaderText;
