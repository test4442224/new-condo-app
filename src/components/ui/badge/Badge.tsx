import React from 'react';
import { StyleProp, View, ViewStyle } from 'react-native';
import styles from './BadgeStyle';
import NormalText from '../titles/NormalText';
import { IPaymentStatusBadge } from '../../../utils/GeneratePaymentStatusBadge';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';

interface BadgeProps {
    details: IPaymentStatusBadge;
    textStyles: StyleProp<ViewStyle>;
    accessibilityLabel?: string;
    testID?: string;
}

const Badge: React.FC<BadgeProps> = ({
    details,
    textStyles,
    testID,
    accessibilityLabel,
}) => {
    if (!details) {
        return null;
    }

    const { badgeStatus, badgeStyle } = details;

    return (
        <View style={[badgeStyle, styles.badge]}>
            <NormalText
                accessibilityLabel={accessibilityLabel}
                testID={testID}
                style={[
                    styles.text,
                    textStyles,
                    { fontWeight: WEIGHT_TYPES.HEAVY },
                ]}>
                {badgeStatus}
            </NormalText>
        </View>
    );
};

export default Badge;
