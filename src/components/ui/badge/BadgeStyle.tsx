import { StyleSheet } from 'react-native';
import { BorderRadiuses, Spacings } from 'react-native-ui-lib';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../../Constants/Colors';

const styles = StyleSheet.create({
    badge: {
        paddingHorizontal: Spacings.s2,
        paddingVertical: Spacings.s1,
        borderRadius: BorderRadiuses.br40,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        fontWeight: WEIGHT_TYPES.HEAVY,
        color: Colors.$textInverted,
    },
});

export default styles;
