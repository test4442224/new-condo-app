import { StyleSheet } from 'react-native';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    whiteMessage: {
        color: Colors.$textInverted,
        fontWeight: 'bold',
    },
    translucentMessage: {
        color: Colors.$textPrimary,
        fontWeight: 'bold',
    },
});
