import styles from './LoadingStyle';

import React, { FC } from 'react';
import { useAppSelector } from '../../../store/hook';
import Colors from '../../../Constants/Colors';
import { LoaderScreen } from 'react-native-ui-lib';

interface ILoadingProps {
    backgroundColor?: string;
}

const Loading: FC<ILoadingProps> = ({
    // for scenario that requires pure white background, we have to pass in Colors.$backgroundDefault
    backgroundColor = `rgba(12, 17, 15, 0.7)`,
}: ILoadingProps) => {
    const { isLoading } = useAppSelector(state => state.loading) || {};

    return (
        <>
            {isLoading && (
                <>
                    <LoaderScreen
                        testID="loading-overlay"
                        message="Loading..."
                        messageStyle={
                            backgroundColor === Colors.$backgroundDefault
                                ? styles.translucentMessage
                                : styles.whiteMessage
                        }
                        loaderColor={
                            backgroundColor === Colors.$backgroundDefault
                                ? Colors.$iconPrimary
                                : Colors.$iconDefault
                        }
                        backgroundColor={backgroundColor}
                        overlay
                    />
                </>
            )}
        </>
    );
};

export default Loading;
