import styles from './ImageLoadingStyle';

import React, { FC } from 'react';
import Colors from '../../../Constants/Colors';
import { LoaderScreen } from 'react-native-ui-lib';

interface IImageLoadingProps {
    backgroundColor?: string;
}

const ImageLoading: FC<IImageLoadingProps> = ({
    // for scenario that requires pure white background, we have to pass in Colors.$backgroundDefault
    backgroundColor = 'none',
}: IImageLoadingProps) => {
    return (
        <>
            <LoaderScreen
                message="Loading..."
                messageStyle={
                    backgroundColor === Colors.$backgroundDefault
                        ? styles.translucentMessage
                        : styles.whiteMessage
                }
                loaderColor={
                    backgroundColor === Colors.$backgroundDefault
                        ? Colors.$iconPrimary
                        : Colors.$iconDefault
                }
                backgroundColor={backgroundColor}
                overlay
            />
        </>
    );
};

export default ImageLoading;
