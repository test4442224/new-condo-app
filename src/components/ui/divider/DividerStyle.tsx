import { StyleSheet } from 'react-native';
import { Spacings } from 'react-native-ui-lib';
import Colors from '../../../Constants/Colors';

export default StyleSheet.create({
    divider: {
        height: 2,
        backgroundColor: Colors.$backgroundDisabled,
        marginHorizontal: Spacings.s4,
        marginVertical: Spacings.s4,
    },
});
