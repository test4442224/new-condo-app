import { View } from 'react-native';
import React from 'react';
import styles from './DividerStyle';

const Divider = () => {
    return <View style={styles.divider} />;
};

export default Divider;
