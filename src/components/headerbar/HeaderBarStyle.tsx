import { StyleSheet } from 'react-native';
import { Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    rootContainer: {
        marginTop: Spacings.s2,
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: Spacings.s5,
        marginBottom: Spacings.s2,
        justifyContent: 'space-between',
    },
    backBtnContainer: {
        flex: 0.1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    txtContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    additionalIcon: {
        display: 'flex',
    },
    hideElement: {
        display: 'none',
    },
    profileContainerIos: {
        flexDirection: 'row',
        marginLeft: Spacings.s2,
        alignItems: 'center',
    },
    profileContainerAndroid: {
        marginTop: Spacings.s4,
        flexDirection: 'row',
        marginLeft: Spacings.s2,
        alignItems: 'center',
    },
    profileDetailsContainer: {
        marginLeft: Spacings.s3,
    },
    additionalIconContainer: {
        flex: 0.1,
    },
});
