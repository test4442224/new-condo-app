import React from 'react';
import {
    GestureResponderEvent,
    Platform,
    StyleProp,
    TextStyle,
    View,
} from 'react-native';
import styles from './HeaderBarStyle';
import HeaderText from '../ui/titles/HeaderText';
import IconButton from '../ui/buttons/IconButton';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import DefaultProfilePicture from '../defaultProfilePicture/DefaultProfilePicture';
import SubHeaderText from '../ui/titles/SubHeaderText';

interface IHeaderBar {
    title: string;
    onBackButtonPress?: (_: GestureResponderEvent) => void;
    onProfileButtonPress?: (_: GestureResponderEvent) => void;
    testID?: string;
    showBackButton?: boolean;
    additionalIcon?: React.ReactNode;
    iconStyle?: StyleProp<TextStyle>;
    labelStyle?: StyleProp<TextStyle>;
    size?: 'xSmall' | 'small' | 'medium';
    residentName?: string;
    residentUnitDetails?: string;
}

const HeaderBar = ({
    title,
    onBackButtonPress = () => {},
    onProfileButtonPress = () => {},
    testID = 'header-bar',
    showBackButton = true,
    additionalIcon,
    iconStyle = {},
    labelStyle = {},
    size = 'small',
    residentName,
    residentUnitDetails,
}: IHeaderBar) => {
    return (
        <View
            style={styles.rootContainer}
            testID={testID}
            accessibilityLabel={testID}>
            {showBackButton ? (
                <View style={styles.backBtnContainer}>
                    <IconButton
                        style={[
                            styles.additionalIcon,
                            showBackButton ? null : styles.hideElement,
                        ]}
                        iconStyle={iconStyle}
                        icon={faArrowLeft}
                        onPress={onBackButtonPress}
                        testID="header-bar-back-button"
                    />
                </View>
            ) : (
                <View
                    style={
                        Platform.OS === 'ios'
                            ? styles.profileContainerIos
                            : styles.profileContainerAndroid
                    }>
                    <DefaultProfilePicture
                        onPress={onProfileButtonPress}
                        size={size}
                    />
                    <View style={styles.profileDetailsContainer}>
                        <SubHeaderText underline={false}>
                            {residentName}
                        </SubHeaderText>
                        <SubHeaderText underline={false}>
                            {residentUnitDetails}
                        </SubHeaderText>
                    </View>
                </View>
            )}
            <View style={styles.txtContainer}>
                <HeaderText style={labelStyle}>{title}</HeaderText>
            </View>
            <View style={styles.additionalIconContainer}>{additionalIcon}</View>
        </View>
    );
};

export default HeaderBar;
