import { StyleSheet } from 'react-native';
import Colors from '../../Constants/Colors';
import { Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    imageContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 'auto',
        marginBottom: 'auto',
        flex: 1,
    },
    closeButton: {
        color: Colors.$iconDefault,
    },
    androidCloseButtonContainer: {
        position: 'absolute',
        top: Spacings.s5,
        right: Spacings.s3,
    },
    iosCloseButtonContainer: {
        position: 'absolute',
        top: Spacings.s1 * 15,
        right: Spacings.s3,
    },
});
