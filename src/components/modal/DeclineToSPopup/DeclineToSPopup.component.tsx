import React from 'react';
import { FC } from 'react';
import NormalText from '../../ui/titles/NormalText';
import PopModal from '../PopModal';

interface DeclineToSPopupProps {
    isOpen: boolean;
    onConfirm: () => void;
    onClose: () => void;
}

const DeclineToSPopup: FC<DeclineToSPopupProps> = ({
    isOpen,
    onConfirm,
    onClose,
}) => {
    const message =
        'By declining the Terms of Service, you will not be able to use the Residentia app.';

    return (
        <PopModal
            modalTitle="Confirm Decline?"
            modalBody={<NormalText>{message}</NormalText>}
            visibility={isOpen}
            confirmText="Decline"
            onConfirm={onConfirm}
            onCancel={onClose}
            cancelText="Close"
        />
    );
};

export default DeclineToSPopup;
