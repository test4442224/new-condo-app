import ImageSwiper from '../ui/swiper/ImageSwiper';
import React from 'react';
import styles from './AttachmentPopModalStyle';
import { Platform, Pressable, View } from 'react-native';
import Colors from '../../Constants/Colors';
import { DefectAttachment } from '../../models/defect/DefectAttachment';
import { Modal } from 'react-native-ui-lib';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faClose } from '@fortawesome/free-solid-svg-icons';

interface IAttachmentPopModal {
    defectAttachment: DefectAttachment[];
    visibility: boolean;
    onCancel?: () => void;
}

const AttachmentPopModal = ({
    defectAttachment,
    visibility,
    onCancel,
}: IAttachmentPopModal) => {
    let attachments: string[] = [];
    for (let paths of defectAttachment) {
        attachments.push(paths.attachmentPath);
    }

    return (
        <>
            <View>
                <Modal
                    visible={visibility}
                    accessibilityLabel="attachment-pop-modal"
                    testID="attachment-pop-modal"
                    overlayBackgroundColor={Colors.$backgroundDefaultDark}>
                    <View style={styles.imageContainer}>
                        <View
                            style={
                                Platform.OS === 'android'
                                    ? styles.androidCloseButtonContainer
                                    : styles.iosCloseButtonContainer
                            }
                            accessibilityLabel="image-popup-close-button"
                            testID="image-popup-close-button">
                            <Pressable onPress={onCancel}>
                                <FontAwesomeIcon
                                    icon={faClose}
                                    size={26}
                                    style={styles.closeButton}
                                />
                            </Pressable>
                        </View>
                        <ImageSwiper
                            imageURL={attachments}
                            autoplay={false}
                            isDefectPage={true}
                        />
                    </View>
                </Modal>
            </View>
        </>
    );
};

export default AttachmentPopModal;
