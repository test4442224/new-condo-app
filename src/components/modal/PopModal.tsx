import React from 'react';
import { StyleProp, ViewStyle, Modal, View, SafeAreaView } from 'react-native';
import styles from './PopModalStyle';
import SubHeaderText from '../ui/titles/SubHeaderText';
import NormalText from '../ui/titles/NormalText';
import SecondaryButton from '../ui/buttons/SecondaryButton';
import PrimaryButton from '../ui/buttons/PrimaryButton';
import { Spacings } from 'react-native-ui-lib';

interface IPopModal {
    style?: StyleProp<ViewStyle>;
    modalTitle: string;
    modalData?: string;
    modalHeaderBody?: string;
    modalBody: React.ReactNode;
    visibility: boolean;
    confirmText: string;
    cancelText?: string;
    testID?: string;
    onConfirm: () => void;
    onCancel?: () => void;
}

const PopModal = ({
    style,
    modalTitle,
    modalData,
    modalHeaderBody,
    modalBody,
    visibility,
    confirmText,
    testID,
    cancelText = 'Cancel',
    onConfirm,
    onCancel,
}: IPopModal) => {
    const renderModalDataFormat = (data: string) => {
        const lines = data.split('\n');

        return (
            <View style={{ marginVertical: Spacings.s2 }}>
                {lines.map((line, index) => {
                    const [key, value] = line.split(': ');

                    return (
                        <View key={index} style={styles.tableContainer}>
                            <NormalText style={styles.cellLeftContainer}>
                                {key}:
                            </NormalText>
                            <NormalText
                                style={[styles.cellRightContainer, style]}>
                                {value}
                            </NormalText>
                        </View>
                    );
                })}
            </View>
        );
    };

    return (
        <>
            <Modal testID={testID} transparent={true} visible={visibility}>
                {visibility ? <View style={[styles.overlay]} /> : null}
                <SafeAreaView
                    testID={testID}
                    accessibilityLabel={testID}
                    style={styles.generalContainer}>
                    <SafeAreaView style={styles.modalContainer}>
                        <SafeAreaView style={styles.textContainer}>
                            <SubHeaderText
                                center={true}
                                style={styles.titleText}>
                                {modalTitle}
                            </SubHeaderText>
                        </SafeAreaView>
                        {modalHeaderBody && (
                            <SafeAreaView style={styles.textContainer}>
                                <SubHeaderText
                                    center={true}
                                    underline={false}
                                    style={styles.headerBodyText}>
                                    {modalHeaderBody}
                                </SubHeaderText>
                            </SafeAreaView>
                        )}
                        <SafeAreaView style={styles.textContainer}>
                            <NormalText
                                center={true}
                                style={styles.text}
                                testID="modal-body">
                                {modalBody}
                            </NormalText>
                        </SafeAreaView>

                        <SafeAreaView style={styles.dataContainer}>
                            {modalData
                                ? renderModalDataFormat(modalData)
                                : null}
                        </SafeAreaView>

                        <SafeAreaView style={styles.buttonOuterContainer}>
                            {onCancel && (
                                <SafeAreaView style={styles.buttonContainer}>
                                    <SecondaryButton
                                        label={cancelText}
                                        testID="PopModal-Cancel"
                                        accessibilityLabel="PopModal-Cancel"
                                        onPress={onCancel}
                                    />
                                </SafeAreaView>
                            )}

                            <SafeAreaView style={styles.buttonContainer}>
                                <PrimaryButton
                                    label={confirmText}
                                    testID="PopModal-Confirm"
                                    accessibilityLabel="PopModal-Confirm"
                                    onPress={onConfirm}
                                />
                            </SafeAreaView>
                        </SafeAreaView>
                    </SafeAreaView>
                </SafeAreaView>
            </Modal>
        </>
    );
};

export default PopModal;
