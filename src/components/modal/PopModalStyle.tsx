import { StyleSheet } from 'react-native';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import Colors from '../../Constants/Colors';
import { BorderRadiuses, Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    generalContainer: {
        flex: 1,
    },
    overlay: {
        backgroundColor: Colors.$backgroundDefaultDark,
        opacity: 0.5,
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
    },
    modalContainer: {
        width: '90%',
        borderRadius: BorderRadiuses.br60,
        paddingHorizontal: Spacings.s2,
        paddingVertical: Spacings.s5,
        margin: Spacings.s5,
        backgroundColor: Colors.$backgroundDefault,
        top: '32%',
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    textContainer: {
        textAlign: 'center',
        marginVertical: Spacings.s2,
    },
    dataContainer: {
        textAlign: 'center',
        marginVertical: Spacings.s2,
        marginHorizontal: Spacings.s6,
    },
    buttonOuterContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: Spacings.s2,
    },
    buttonContainer: {
        flex: 1,
        marginHorizontal: Spacings.s1,
    },
    text: {
        textAlign: 'center',
        // fontStyle: 'italic',
    },
    tableContainer: {
        flexDirection: 'row',
    },
    cellLeftContainer: {
        flex: 1,
        fontWeight: WEIGHT_TYPES.MEDIUM,
    },
    cellRightContainer: {
        flex: 1,
    },
    titleText: {
        color: Colors.$textPrimary,
    },
    headerBodyText: {
        color: Colors.$textDefault,
    },
});
