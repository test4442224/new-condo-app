import { StyleSheet } from 'react-native';
import { BorderRadiuses, Spacings } from 'react-native-ui-lib';
import Colors from '../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        borderRadius: BorderRadiuses.br30,
        paddingVertical: Spacings.s1,
        paddingHorizontal: Spacings.s1,
        backgroundColor: Colors.$backgroundPrimaryLight,
    },
});
