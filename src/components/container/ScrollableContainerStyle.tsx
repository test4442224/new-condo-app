import { StyleSheet } from 'react-native';
import Colors from '../../Constants/Colors';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
    },
    scrollViewStyle: {
        flexGrow: 1,
        justifyContent: 'center',
    },
    viewContainer: {
        // paddingBottom: Spacings.s8,
        flex: 1,
    },
});
