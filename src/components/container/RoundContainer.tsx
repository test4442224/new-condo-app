import React from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import styles from './RoundContainerStyle';
import { View } from 'react-native-ui-lib';

interface IRoundContainer {
    children: React.ReactNode;
    style?: StyleProp<ViewStyle>;
}

const RoundContainer = ({ children, style }: IRoundContainer) => {
    return <View style={[styles.rootContainer, style]}>{children}</View>;
};

export default RoundContainer;
