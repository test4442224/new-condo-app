import { FC, useCallback, useState } from 'react';
import {
    RefreshControl,
    SafeAreaView,
    ScrollView,
    StyleProp,
    View,
    ViewStyle,
} from 'react-native';
import styles from './ScrollableContainerStyle';
import React from 'react';

interface IScrollableOuterContainer {
    children: React.ReactNode;
    refresh?: boolean;
    onRefresh?: () => void;
    style?: StyleProp<ViewStyle>;
}
const ScrollableContainer: FC<IScrollableOuterContainer> = ({
    children,
    refresh,
    onRefresh,
    style,
}) => {
    const [refreshing, setRefreshing] = useState<boolean>(false);
    const refreshCallBack = useCallback(() => {
        if (onRefresh) {
            setRefreshing(true);
            onRefresh();
            setTimeout(() => {
                setRefreshing(false);
            }, 500);
        }
    }, [onRefresh]);
    return (
        <SafeAreaView style={[styles.rootContainer, style]}>
            <ScrollView
                invertStickyHeaders
                stickyHeaderIndices={[5]}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={styles.scrollViewStyle}
                refreshControl={
                    refresh ? (
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={refreshCallBack}
                        />
                    ) : undefined
                }>
                <View style={styles.viewContainer}>{children}</View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default ScrollableContainer;
