import { cleanup, render } from '@testing-library/react-native';
import PaymentMethodSelect from '.';
import React from 'react';
import { PaymentMethod } from '../../store/payment/slice';
import styles from './styles';

describe('Payment Method Select component', () => {
    beforeEach(() => {
        cleanup();
    });

    it('should render buttons correctly if PayNow via UEN', () => {
        const { getByRole } = render(
            <PaymentMethodSelect
                onPressCashHandler={() => {}}
                onPressPayNowHandler={() => {}}
                isViaUen={false}
            />,
        );

        expect(getByRole('button', { name: 'PayNow' })).toBeOnTheScreen();
        expect(getByRole('button', { name: 'Cash' })).toBeOnTheScreen();
    });

    it('should render buttons correctly if PayNow not via UEN', () => {
        const { getByRole } = render(
            <PaymentMethodSelect
                onPressCashHandler={() => {}}
                onPressPayNowHandler={() => {}}
                isViaUen={true}
            />,
        );

        expect(
            getByRole('button', { name: 'PayNow via UEN' }),
        ).toBeOnTheScreen();
        expect(getByRole('button', { name: 'Cash' })).toBeOnTheScreen();
    });

    it('should render buttons with no additional styles if no payment method selected', () => {
        const { getByRole } = render(
            <PaymentMethodSelect
                onPressCashHandler={() => {}}
                onPressPayNowHandler={() => {}}
                isViaUen={false}
            />,
        );

        expect(getByRole('button', { name: 'PayNow' })).not.toHaveStyle(
            styles.paymentOptionSelected,
        );
        expect(getByRole('button', { name: 'Cash' })).not.toHaveStyle(
            styles.paymentOptionSelected,
        );
    });

    it('should render PayNow button with correct style if PayNow selected', () => {
        const { getByRole } = render(
            <PaymentMethodSelect
                onPressCashHandler={() => {}}
                onPressPayNowHandler={() => {}}
                isViaUen={false}
                selectedPaymentMethod={PaymentMethod.PAYNOW}
            />,
        );

        expect(getByRole('button', { name: 'PayNow' })).toHaveStyle(
            styles.paymentOptionSelected,
        );
        expect(getByRole('button', { name: 'Cash' })).not.toHaveStyle(
            styles.paymentOptionSelected,
        );
    });

    it('should render Cash button with correct style if Cash selected', () => {
        const { getByRole } = render(
            <PaymentMethodSelect
                onPressCashHandler={() => {}}
                onPressPayNowHandler={() => {}}
                isViaUen={false}
                selectedPaymentMethod={PaymentMethod.CASH}
            />,
        );

        expect(getByRole('button', { name: 'PayNow' })).not.toHaveStyle(
            styles.paymentOptionSelected,
        );
        expect(getByRole('button', { name: 'Cash' })).toHaveStyle(
            styles.paymentOptionSelected,
        );
    });
});
