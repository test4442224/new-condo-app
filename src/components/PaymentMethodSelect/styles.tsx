import { StyleSheet } from 'react-native';
import Colors from '../../Constants/Colors';
import { Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    buttonContainer: {
        flex: 1,
    },
    paymentOptionSelected: {
        backgroundColor: Colors.$backgroundPrimaryLight,
    },
    buttonsContainer: {
        gap: Spacings.s3,
        flexDirection: 'row',
        marginTop: Spacings.s3,
    },
});
