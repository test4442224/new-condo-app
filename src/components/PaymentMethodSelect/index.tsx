import React, { FC } from 'react';
import { View } from 'react-native';
import SecondaryButton from '../ui/buttons/SecondaryButton';
import SubHeaderText from '../ui/titles/SubHeaderText';
import styles from './styles';
import { PaymentMethod } from '../../store/manageBooking/slice';

type Props = {
    onPressPayNowHandler: () => void;
    onPressCashHandler: () => void;
    isViaUen: boolean;
    selectedPaymentMethod?: PaymentMethod | null;
    testID?: string;
};

const PaymentMethodSelect: FC<Props> = ({
    onPressPayNowHandler,
    onPressCashHandler,
    isViaUen,
    selectedPaymentMethod,
    testID,
}) => {
    return (
        <View testID={testID}>
            <View>
                <SubHeaderText>{'Select Payment Method:'}</SubHeaderText>
            </View>
            <View style={styles.buttonsContainer}>
                <View style={styles.buttonContainer}>
                    <SecondaryButton
                        style={
                            selectedPaymentMethod &&
                            selectedPaymentMethod === PaymentMethod.PAYNOW &&
                            styles.paymentOptionSelected
                        }
                        label={isViaUen ? 'PayNow via UEN' : 'PayNow'}
                        testID="payment-method-select.paynow"
                        onPress={onPressPayNowHandler}
                    />
                </View>

                <View style={styles.buttonContainer}>
                    <SecondaryButton
                        style={
                            selectedPaymentMethod &&
                            selectedPaymentMethod === PaymentMethod.CASH &&
                            styles.paymentOptionSelected
                        }
                        label="Cash"
                        testID="payment-method-select.cash"
                        onPress={onPressCashHandler}
                    />
                </View>
            </View>
        </View>
    );
};

export default PaymentMethodSelect;
