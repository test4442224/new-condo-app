import React, { FC } from 'react';
import Delta from 'quill-delta';
import { QuillDeltaToHtmlConverter } from 'quill-delta-to-html';
import sanitizeHtml from 'sanitize-html';
import RenderHTML from 'react-native-render-html';
import { View, useWindowDimensions } from 'react-native';

interface ITextDocumentProps {
    content: string;
}

const TextDocument: FC<ITextDocumentProps> = ({ content }) => {
    const descriptionDelta = JSON.parse(content) as Delta;
    const descriptionHtml = new QuillDeltaToHtmlConverter(
        descriptionDelta.ops,
        {},
    ).convert();
    const sanitizedDescriptionHtml = sanitizeHtml(descriptionHtml);

    const { width } = useWindowDimensions();

    return (
        <View>
            <RenderHTML
                source={{ html: sanitizedDescriptionHtml }}
                contentWidth={width}
            />
        </View>
    );
};

export default TextDocument;
