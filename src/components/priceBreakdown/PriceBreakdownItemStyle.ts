import { StyleSheet } from 'react-native';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';
import { Spacings } from 'react-native-ui-lib';

export default StyleSheet.create({
    rootContainer: {
        flex: 1,
    },
    rowContainer: {
        flexDirection: 'row',
    },
    boldText: {
        fontWeight: WEIGHT_TYPES.BOLD,
    },
    cellLeftContainer: {
        flex: 1,
    },
    paymentCellRightContainer: {
        flex: 2,
        textAlign: 'right',
        marginRight: Spacings.s2,
    },
});
