import React from 'react';
import { View } from 'react-native';
import styles from './PriceBreakdownListStyle';
import NormalText from '../ui/titles/NormalText';
import { PaymentMethod, PaymentStatus } from '../../store/manageBooking/slice';
import SubHeaderText from '../ui/titles/SubHeaderText';
import PriceBreakdownItem from './PriceBreakdownItem';
import PageName from '../../navigation/PageNameEnum';
import { IPriceSummary } from '../../models/booking/PriceBreakdown';
import { toDateFormat } from '../../utils/dateTimeUtil';
import {
    calculateSubtotalPriceAmount,
    calculateTotalPriceAmount,
} from '../../utils/CalculatePriceAmount';

interface IPriceBreakdownList {
    pageName: string;
    breakdownList: IPriceSummary[];
    paymentStatus?: PaymentStatus;
    totalPrice?: number;
    paidAt?: Date | null;
    bookingHours: number;
    paymentMethod?: PaymentMethod;
}
const PriceBreakdownList = ({
    pageName,
    breakdownList,
    paymentStatus,
    totalPrice,
    paidAt,
    bookingHours,
    paymentMethod,
}: IPriceBreakdownList) => {
    let sortedBreakdownList = breakdownList!.sort((a, b) =>
        a.description.localeCompare(b.description),
    );
    let priceBreakdownComponents = sortedBreakdownList!.map(item => (
        <View key={item.id} style={styles.paymentRowContainer}>
            <PriceBreakdownItem
                description={item.description}
                unitPriceAmount={calculateSubtotalPriceAmount(
                    item.unitPriceAmount,
                    item.priceType,
                    bookingHours,
                )}
            />
        </View>
    ));

    return (
        <>
            {breakdownList.length !== 0 ? (
                <>
                    <View style={styles.rowContainer}>
                        <SubHeaderText>
                            {pageName === PageName.REVIEW_BOOKING_DETAILS_SCREEN
                                ? 'Price Breakdown:'
                                : 'Payment Summary: '}
                        </SubHeaderText>
                    </View>

                    <View>
                        <View style={styles.paymentRowContainer}>
                            <NormalText
                                style={[
                                    styles.cellLeftContainer,
                                    styles.italicText,
                                ]}>
                                {'Item Description'}
                            </NormalText>
                            <NormalText
                                style={[
                                    styles.paymentCellRightContainer,
                                    styles.italicText,
                                ]}>
                                {'Item Total'}
                            </NormalText>
                        </View>

                        {priceBreakdownComponents}

                        <View style={styles.horizontalLine} />

                        <View style={styles.paymentRowContainer}>
                            <NormalText style={styles.cellLeftContainer}>
                                {paidAt
                                    ? `Total Paid on ${toDateFormat(paidAt)}`
                                    : paymentStatus ===
                                        PaymentStatus.PENDING_PAYMENT
                                      ? `Total Amount Payable`
                                      : `Total Amount`}
                            </NormalText>
                            <NormalText
                                style={[
                                    styles.paymentCellRightContainer,
                                    styles.boldText,
                                ]}>
                                {pageName ===
                                PageName.REVIEW_BOOKING_DETAILS_SCREEN
                                    ? `$${calculateTotalPriceAmount(
                                          breakdownList,
                                          bookingHours,
                                      ).toFixed(2)}`
                                    : `$${(+totalPrice!).toFixed(2)}`}
                            </NormalText>
                        </View>
                        {paymentMethod && (
                            <View style={styles.paymentRowContainer}>
                                <NormalText style={styles.cellLeftContainer}>
                                    {'Payment Method'}
                                </NormalText>
                                <NormalText
                                    style={[
                                        styles.paymentCellRightContainer,
                                        styles.boldText,
                                    ]}>
                                    {paymentMethod === PaymentMethod.PAYNOW
                                        ? 'PayNow via UEN'
                                        : 'Cash'}
                                </NormalText>
                            </View>
                        )}
                    </View>
                </>
            ) : null}
        </>
    );
};

export default PriceBreakdownList;
