import { View } from 'react-native-ui-lib';
import styles from './PriceBreakdownItemStyle';
import React from 'react';
import NormalText from '../ui/titles/NormalText';

interface IPriceBreakdownItem {
    description: string;
    unitPriceAmount: number;
}

const PriceBreakdownItem = ({
    description,
    unitPriceAmount,
}: IPriceBreakdownItem) => {
    const decimalSubTotal = +unitPriceAmount;

    return (
        <>
            <View style={styles.rootContainer}>
                <View style={styles.rowContainer}>
                    <NormalText style={[styles.cellLeftContainer]}>
                        {description}
                    </NormalText>

                    <NormalText
                        style={[
                            styles.paymentCellRightContainer,
                            styles.boldText,
                        ]}>
                        {`$${decimalSubTotal.toFixed(2)}`}
                    </NormalText>
                </View>
            </View>
        </>
    );
};

export default PriceBreakdownItem;
