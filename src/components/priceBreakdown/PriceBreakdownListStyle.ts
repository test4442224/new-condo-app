import { StyleSheet } from 'react-native';
import { Spacings } from 'react-native-ui-lib';
import Colors from '../../Constants/Colors';
import { WEIGHT_TYPES } from 'react-native-ui-lib/src/style/typographyPresets';

export default StyleSheet.create({
    rowContainer: {
        flexDirection: 'row',
        paddingVertical: Spacings.s1,
        alignItems: 'center',
    },
    generalContainer: {
        paddingVertical: Spacings.s2,
        paddingHorizontal: Spacings.s4,
    },
    labelDotText: {
        color: Colors.$textDisabled,
    },
    italicText: {
        fontStyle: 'italic',
    },
    paymentRowContainer: {
        flexDirection: 'row',
        paddingVertical: Spacings.s1,
        alignItems: 'stretch',
    },
    cellLeftContainer: {
        flex: 2,
    },
    paymentCellRightContainer: {
        flex: 1,
        textAlign: 'right',
        marginRight: Spacings.s2,
    },
    horizontalLine: {
        borderWidth: 0.7,
        borderColor: Colors.$outlinePrimary,
        marginVertical: Spacings.s3,
    },
    boldText: {
        fontWeight: WEIGHT_TYPES.BOLD,
    },
});
