// MainNavigation.js
import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ReviewBookingDetails from '../screens/FacilityBooking/ReviewBookingDetails/ReviewBookingDetails';
import BookingSuccess from '../screens/FacilityBooking/BookingComplete/BookingCompleteSuccess';
import BookingPending from '../screens/FacilityBooking/BookingComplete/BookingCompletePending';
import NavParamList from './NavParmList';
import PageName from './PageNameEnum';
import ManageBookings from '../screens/FacilityBooking/ManageBookings/ManageBookings';
import HomePage from '../screens/HomePage/HomePage';
import SelectFacilityTypeScreen from '../screens/FacilityBooking/NewBooking/SelectFacilityTypeScreen';
import SelectFacilityAndTimeScreen from '../screens/FacilityBooking/NewBooking/SelectFacilityAndTimeScreen';
import SelectDateScreen from '../screens/FacilityBooking/NewBooking/SelectDateScreen';
import BookingDetails from '../screens/FacilityBooking/ManageBookings/BookingDetails';
import ProfilePage from '../screens/ProfilePage/ProfilePage';
import NoticePage from '../screens/Notice/NoticePage';
import NoticeDetail from '../screens/Notice/NoticeDetail';
import ChangePassword from '../screens/Password/ChangePassword/ChangePassword';
import SettingsPage from '../screens/SettingsPage/SettingsPage';
import PrivacyPolicyPage from '../screens/LegalDocument/PrivacyPolicy/PrivacyPolicyPage';
import TermsOfServicePage from '../screens/LegalDocument/TermOfService/TermOfServicePage';
import AboutUsPage from '../screens/AboutUs/AboutUs';
import DefectDetails from '../screens/Defect/DefectDetails/DefectDetails';
import ManageDefects from '../screens/Defect/ManageDefects/ManageDefects';
import CreateDefectPage from '../screens/Defect/CreateDefect/CreateDefect';
import EditDefectPage from '../screens/Defect/EditDefect/EditDefect';
import ManagePaymentsPage from '../screens/Payment/ManagePayments/ManagePaymentPage';
import PaymentDetails from '../screens/Payment/PaymentDetails/PaymentDetails';
import PendingCashPayment from '../screens/Payment/PaymentStatus/PendingCashPayment/PendingCashPayment';
import PendingPayNowPayment from '../screens/Payment/PaymentStatus/PendingPayNowPayment';
import PendingVerification from '../screens/Payment/PaymentStatus/PendingVerification';
import StatementOfAccountPage from '../screens/StatementOfAccount/StatementOfAccountPage';

const MainNavigation = () => {
    const Stack = createNativeStackNavigator<NavParamList>();

    return (
        <Stack.Navigator initialRouteName={PageName.HOME_PAGE_SCREEN}>
            <Stack.Screen
                name={PageName.REVIEW_BOOKING_DETAILS_SCREEN}
                component={ReviewBookingDetails}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.BOOKING_DETAILS_SCREEN}
                component={BookingDetails}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.DEFECT_DETAILS_SCREEN}
                component={DefectDetails}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.BOOKING_SUCCESS_SCREEN}
                component={BookingSuccess}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.BOOKING_PENDING_SCREEN}
                component={BookingPending}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.MANAGE_BOOKINGS_SCREEN}
                component={ManageBookings}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.HOME_PAGE_SCREEN}
                component={HomePage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.SELECT_FACILITY_TYPE_SCREEN}
                component={SelectFacilityTypeScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.SELECT_TIME_SCREEN}
                component={SelectFacilityAndTimeScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.SELECT_DATE_SCREEN}
                component={SelectDateScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.PROFILE_PAGE_SCREEN}
                component={ProfilePage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.NOTICE_SCREEN}
                component={NoticePage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.NOTICE_DETAILS_SCREEN}
                component={NoticeDetail}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.STATEMENT_OF_ACCOUNT}
                component={StatementOfAccountPage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.CHANGE_PASSWORD_SCREEN}
                component={ChangePassword}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.SETTINGS_SCREEN}
                component={SettingsPage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.PRIVACY_POLICY_SCREEN}
                component={PrivacyPolicyPage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.TERMS_OF_SERVICE_SCREEN}
                component={TermsOfServicePage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.ABOUT_US_SCREEN}
                component={AboutUsPage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.MANAGE_DEFECTS_SCREEN}
                component={ManageDefects}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.CREATE_DEFECT_SCREEN}
                component={CreateDefectPage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.EDIT_DEFECT_SCREEN}
                component={EditDefectPage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.MANAGE_PAYMENTS_SCREEN}
                component={ManagePaymentsPage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.PAYMENT_DETAILS_SCREEN}
                component={PaymentDetails}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.PENDING_CASH_PAYMENT_SCREEN}
                component={PendingCashPayment}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.PENDING_PAYNOW_PAYMENT_SCREEN}
                component={PendingPayNowPayment}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.PENDING_VERIFICATION_SCREEN}
                component={PendingVerification}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    );
};

export default MainNavigation;
