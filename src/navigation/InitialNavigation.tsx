import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NavParamList from './NavParmList';
import PageName from './PageNameEnum';
import AcceptTermsOfService from '../screens/LegalDocument/AcceptTermsOfService/AcceptTermsOfServiceScreen';

const InitialNavigation = () => {
    const Stack = createNativeStackNavigator<NavParamList>();

    return (
        <Stack.Navigator
            initialRouteName={PageName.ACCEPT_TERMS_OF_SERVICE_SCREEN}>
            <Stack.Screen
                name={PageName.ACCEPT_TERMS_OF_SERVICE_SCREEN}
                component={AcceptTermsOfService}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    );
};

export default InitialNavigation;
