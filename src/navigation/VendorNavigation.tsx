import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NavParamList from './NavParmList';
import PageName from './PageNameEnum';
import VendorHomePage from '../screens/Vendor/VendorHomePage';
import ProfilePage from '../screens/ProfilePage/ProfilePage';
import ChangePassword from '../screens/Password/ChangePassword/ChangePassword';
import SettingsPage from '../screens/SettingsPage/SettingsPage';
import PrivacyPolicyPage from '../screens/LegalDocument/PrivacyPolicy/PrivacyPolicyPage';
import TermsOfServicePage from '../screens/LegalDocument/TermOfService/TermOfServicePage';
import AboutUsPage from '../screens/AboutUs/AboutUs';
import DefectDetails from '../screens/Defect/DefectDetails/DefectDetails';

const VendorNavigation = () => {
    const Stack = createNativeStackNavigator<NavParamList>();

    return (
        <Stack.Navigator initialRouteName={PageName.VENDOR_HOME_PAGE}>
            <Stack.Screen
                name={PageName.VENDOR_HOME_PAGE}
                component={VendorHomePage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.PROFILE_PAGE_SCREEN}
                component={ProfilePage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.CHANGE_PASSWORD_SCREEN}
                component={ChangePassword}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.SETTINGS_SCREEN}
                component={SettingsPage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.PRIVACY_POLICY_SCREEN}
                component={PrivacyPolicyPage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.TERMS_OF_SERVICE_SCREEN}
                component={TermsOfServicePage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.ABOUT_US_SCREEN}
                component={AboutUsPage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.DEFECT_DETAILS_SCREEN}
                component={DefectDetails}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    );
};

export default VendorNavigation;
