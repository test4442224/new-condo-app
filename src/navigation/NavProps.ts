import { NativeStackScreenProps } from '@react-navigation/native-stack';
import NavParamList from './NavParmList';
import PageName from './PageNameEnum';

export interface LoginNavProps
    extends NativeStackScreenProps<NavParamList, PageName.LOGIN_SCREEN> {}

export interface ReviewBookingDetailsNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.REVIEW_BOOKING_DETAILS_SCREEN
    > {}

export interface BookingDetailsNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.BOOKING_DETAILS_SCREEN
    > {}

export interface BookingSuccessNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.BOOKING_SUCCESS_SCREEN
    > {}

export interface BookingPendingNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.BOOKING_PENDING_SCREEN
    > {}

export interface SelectFacilityTypeNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.SELECT_FACILITY_TYPE_SCREEN
    > {}

export interface SelectFacilityAndTimeNavProps
    extends NativeStackScreenProps<NavParamList, PageName.SELECT_TIME_SCREEN> {}

export interface SelectDateNavProps
    extends NativeStackScreenProps<NavParamList, PageName.SELECT_DATE_SCREEN> {}

export interface ManageBookingsNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.MANAGE_BOOKINGS_SCREEN
    > {}

export interface HomePageNavProps
    extends NativeStackScreenProps<NavParamList, PageName.HOME_PAGE_SCREEN> {}

export interface ForgetPasswordProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.FORGET_PASSWORD_SCREEN
    > {}

export interface ResetPasswordProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.RESET_PASSWORD_SCREEN
    > {}

export interface ProfilePageScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.PROFILE_PAGE_SCREEN
    > {}

export interface NoticeScreenNavProps
    extends NativeStackScreenProps<NavParamList, PageName.NOTICE_SCREEN> {}

export interface NoticeDetailScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.NOTICE_DETAILS_SCREEN
    > {}
export interface DefectDetailScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.DEFECT_DETAILS_SCREEN
    > {}

export interface ChangePasswordScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.CHANGE_PASSWORD_SCREEN
    > {}

export interface SettingsScreenNavProps
    extends NativeStackScreenProps<NavParamList, PageName.SETTINGS_SCREEN> {}

export interface PrivacyPolicyScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.PRIVACY_POLICY_SCREEN
    > {}

export interface TermsOfServiceScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.TERMS_OF_SERVICE_SCREEN
    > {}
export interface AcceptTermsofServiceScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.ACCEPT_TERMS_OF_SERVICE_SCREEN
    > {}

export interface AboutUsScreenNavProps
    extends NativeStackScreenProps<NavParamList, PageName.ABOUT_US_SCREEN> {}
export interface ManageDefectssNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.MANAGE_DEFECTS_SCREEN
    > {}

export interface CreateDefectScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.CREATE_DEFECT_SCREEN
    > {}

export interface EditDefectScreenNavProps
    extends NativeStackScreenProps<NavParamList, PageName.EDIT_DEFECT_SCREEN> {}

export interface ManagePaymentsScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.MANAGE_PAYMENTS_SCREEN
    > {}

export interface PaymentDetailsScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.PAYMENT_DETAILS_SCREEN
    > {}
export interface PendingCashPaymentScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.PENDING_CASH_PAYMENT_SCREEN
    > {}

export interface PendingPayNowPaymentScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.PENDING_PAYNOW_PAYMENT_SCREEN
    > {}

export interface PendingVerificationScreenNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.PENDING_VERIFICATION_SCREEN
    > {}
export interface VendorHomePageScreenNavProps
    extends NativeStackScreenProps<NavParamList, PageName.VENDOR_HOME_PAGE> {}

export interface StatementOfAccountNavProps
    extends NativeStackScreenProps<
        NavParamList,
        PageName.STATEMENT_OF_ACCOUNT
    > {}
