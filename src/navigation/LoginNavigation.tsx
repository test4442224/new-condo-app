import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginPage from '../screens/LoginPage/LoginPage';
import ForgetPassword from '../screens/Password/ForgetPassword/ForgetPassword';
import NavParamList from './NavParmList';
import PageName from './PageNameEnum';
import ResetPassword from '../screens/Password/ResetPassword/ResetPassword';

const LoginNavigation = () => {
    const Stack = createNativeStackNavigator<NavParamList>();

    return (
        <Stack.Navigator initialRouteName={PageName.LOGIN_SCREEN}>
            <Stack.Screen
                name={PageName.LOGIN_SCREEN}
                component={LoginPage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.FORGET_PASSWORD_SCREEN}
                component={ForgetPassword}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={PageName.RESET_PASSWORD_SCREEN}
                component={ResetPassword}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    );
};

export default LoginNavigation;
