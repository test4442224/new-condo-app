import { ParamListBase } from '@react-navigation/native';
import PageName from './PageNameEnum';
import { IBookingSuccessProps } from '../screens/FacilityBooking/BookingComplete/BookingCompleteSuccess';
import { NoticeType } from '../store/notice/slice';
import { IBookingPendingProps } from '../screens/FacilityBooking/BookingComplete/BookingCompletePending';
import { DefectAttributes } from '../store/defect/slice';
import { IPaymentPendingProps } from '../screens/Payment/PaymentStatus/PendingCashPayment/PendingCashPayment';

interface NavParamList extends ParamListBase {
    [PageName.LOGIN_SCREEN]: undefined;
    [PageName.REVIEW_BOOKING_DETAILS_SCREEN]: undefined;
    [PageName.BOOKING_SUCCESS_SCREEN]: {
        bookingDetails: IBookingSuccessProps;
    };
    [PageName.BOOKING_PENDING_SCREEN]: {
        bookingDetails: IBookingPendingProps;
    };
    [PageName.SELECT_FACILITY_TYPE_SCREEN]: undefined;
    [PageName.MANAGE_BOOKINGS_SCREEN]: { tabStatus: string } | undefined;
    [PageName.SELECT_TIME_SCREEN]: undefined;
    [PageName.SELECT_DATE_SCREEN]: undefined;
    [PageName.HOME_PAGE_SCREEN]: undefined;
    [PageName.FORGET_PASSWORD_SCREEN]: undefined;
    [PageName.RESET_PASSWORD_SCREEN]: { encryptedUserId: string } | undefined;
    [PageName.BOOKING_DETAILS_SCREEN]: { bookingTitle: string };
    [PageName.NOTICE_DETAILS_SCREEN]: { noticeTitle: NoticeType };
    [PageName.DEFECT_DETAILS_SCREEN]: { id: number; statusType: string };
    [PageName.PROFILE_PAGE_SCREEN]: undefined;
    [PageName.NOTICE_SCREEN]: undefined;
    [PageName.CHANGE_PASSWORD_SCREEN]: undefined;
    [PageName.SETTINGS_SCREEN]: undefined;
    [PageName.PRIVACY_POLICY_SCREEN]: undefined;
    [PageName.TERMS_OF_SERVICE_SCREEN]: undefined;
    [PageName.ACCEPT_TERMS_OF_SERVICE_SCREEN]: undefined;
    [PageName.ABOUT_US_SCREEN]: undefined;
    [PageName.MANAGE_DEFECTS_SCREEN]: { tabStatus: string } | undefined;
    [PageName.CREATE_DEFECT_SCREEN]: undefined;
    [PageName.MANAGE_PAYMENTS_SCREEN]: undefined;
    [PageName.PAYMENT_DETAILS_SCREEN]: undefined;
    [PageName.PENDING_CASH_PAYMENT_SCREEN]: {
        paymentDetails: IPaymentPendingProps;
    };
    [PageName.PENDING_PAYNOW_PAYMENT_SCREEN]: {
        paymentDetails: IPaymentPendingProps;
    };
    [PageName.PENDING_VERIFICATION_SCREEN]: undefined;
    [PageName.EDIT_DEFECT_SCREEN]: {
        defectData: DefectAttributes | undefined;
        onEditSuccess: () => void;
    };
    [PageName.STATEMENT_OF_ACCOUNT]: undefined;
    [PageName.VENDOR_HOME_PAGE]: { tabStatus: string } | undefined;
}

export default NavParamList;
