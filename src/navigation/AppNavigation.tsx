import React, { useCallback, useEffect, useState } from 'react';
import LoginNavigation from './LoginNavigation';
import MainNavigation from './MainNavigation';
import { NavigationContainer } from '@react-navigation/native';
import { BackHandler } from 'react-native';
import { useAppDispatch, useAppSelector } from '../store/hook';
import Toast from 'react-native-simple-toast';
import { userActions } from '../store/user/UserActions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useGetUserRolesQuery } from '../store/userRole/userRoleApi';
import NormalText from '../components/ui/titles/NormalText';
import InitialNavigation from './InitialNavigation';
import { AsyncStorageKey } from '../models/asyncStorageKey/asyncStorageKey.enum';
import { useAgreeToSMutation, useLoginMutation } from '../store/user/userApi';
import { QueryStatus } from '@reduxjs/toolkit/dist/query';
import { useGetDocumentQuery } from '../store/legalDocument/legalDocumentApi';
import { LegalDocumentName } from '../models/legalDocuments/legalDocumentName.enum';
import { isAfter } from 'date-fns';
import useRtkResponse from '../utils/hooks/useRtkResponse';
import { RoleType } from '../models/userRole/RoleType';
import VendorNavigation from './VendorNavigation';

const AppNavigation = () => {
    const [isInitialised, setIsInitialised] = useState<boolean>(false);
    const [isVendorEmployee, setIsVendorEmployee] = useState<boolean>();
    const [isAgreedToSExpired, setIsAgreedToSExpired] =
        useState<boolean>(false);
    const dispatch = useAppDispatch();
    const { token, loggedOutReason, isAgreedToS, agreedToSAt } = useAppSelector(
        state => state.user,
    );
    const { isLoading, error } = useGetUserRolesQuery(
        { token: token! },
        {
            skip: !token,
            refetchOnMountOrArgChange: true,
        },
    );

    const userRoles = useAppSelector(
        state => state.userRole.allAvailableUseRoles,
    );
    const { setToSAgreement } = userActions;

    useRtkResponse(isLoading, error, false);
    const { data: toSData, isSuccess: isGetToSDataSuccess } =
        useGetDocumentQuery(
            {
                documentName: LegalDocumentName.TERMS_OF_SERVICE,
            },
            {
                skip: !token,
            },
        );

    const [_, { data: loginData, status: loginStatus }] = useLoginMutation({
        fixedCacheKey: 'loginResult',
    });

    const [
        _agreeToS,
        {
            data: agreeToSData,
            isSuccess: isAgreeToSSuccess,
            reset: resetAgreeToS,
        },
    ] = useAgreeToSMutation({
        fixedCacheKey: 'acceptToSResult',
    });

    const retrieveToken = useCallback(async () => {
        const storageToken = await AsyncStorage.getItem('token');
        if (storageToken) {
            dispatch(userActions.setToken(storageToken));
        }
    }, [dispatch]);

    const retrieveToSAgreement = useCallback(async () => {
        const dateString = await AsyncStorage.getItem(
            AsyncStorageKey.AGREED_TOS_AT,
        );

        dispatch(
            setToSAgreement({
                agreedToSAt: dateString,
                isAgreedToS:
                    dateString !== null &&
                    !Number.isNaN(Date.parse(dateString)),
            }),
        );
    }, [dispatch, setToSAgreement]);

    const storeDataToAsyncStorage = useCallback(async () => {
        if (loginStatus === QueryStatus.fulfilled && loginData) {
            await AsyncStorage.setItem(AsyncStorageKey.TOKEN, loginData.token);
            await AsyncStorage.setItem(
                AsyncStorageKey.CONDO_NAME,
                loginData.condo.name,
            );
            await AsyncStorage.setItem(
                AsyncStorageKey.CONDO_UEN,
                loginData.condo.uen,
            );
            if (loginData.agreedToSAt) {
                await AsyncStorage.setItem(
                    AsyncStorageKey.AGREED_TOS_AT,
                    loginData.agreedToSAt,
                );
            }
        }

        if (
            isAgreeToSSuccess &&
            agreeToSData &&
            !isNaN(Date.parse(agreeToSData.agreedToSAt))
        ) {
            await AsyncStorage.setItem(
                AsyncStorageKey.AGREED_TOS_AT,
                agreeToSData.agreedToSAt,
            );

            resetAgreeToS();
        }
    }, [
        loginStatus,
        loginData,
        isAgreeToSSuccess,
        agreeToSData,
        resetAgreeToS,
    ]);

    const initialiseApp = useCallback(async () => {
        if (!isInitialised) {
            await Promise.all([retrieveToken(), retrieveToSAgreement()]);
            setIsInitialised(true);
        }
        if (userRoles.length !== 0) {
            if (
                userRoles.find(
                    role => role.roleType === RoleType.VENDOR_EMPLOYEE,
                )
            ) {
                setIsVendorEmployee(true);
            } else {
                setIsVendorEmployee(false);
            }
        }
    }, [isInitialised, userRoles, retrieveToken, retrieveToSAgreement]);

    // Initialise app: load data from AsyncStorage
    useEffect(() => {
        initialiseApp();
    }, [initialiseApp]);
    //This is to prevent for android users from seeing a blank page upon tapping on back button
    useEffect(() => {
        const backAction = () => false;
        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            backAction,
        );
        return () => backHandler.remove();
    }, [token]);

    useEffect(() => {
        if (loggedOutReason) {
            Toast.show(loggedOutReason, Toast.LONG);
            dispatch(userActions.removeLoggedOutReason());
        }
    }, [loggedOutReason, dispatch]);

    // Store data to Async Thunk if data changed
    useEffect(() => {
        storeDataToAsyncStorage();
    }, [storeDataToAsyncStorage]);
    // Handle the case if ToS has updated version
    useEffect(() => {
        if (isGetToSDataSuccess && toSData && agreedToSAt) {
            const agreedToSDate = new Date(agreedToSAt);
            const currentToSUpdateDate = new Date(toSData.updatedAt);

            const isAgreedToSValid = isAfter(
                agreedToSDate,
                currentToSUpdateDate,
            );

            setIsAgreedToSExpired(!isAgreedToSValid);
        }
    }, [isGetToSDataSuccess, toSData, agreedToSAt]);
    const config = {
        screens: {
            ResetPasswordScreen: {
                path: 'resetpassword/:encryptedUserId?',
            },
        },
    };
    const linking = {
        prefixes: ['condoapp://'],
        config,
    };

    const isLoggedIn = token !== null;
    const isAgreedToSValid = isAgreedToS && !isAgreedToSExpired;
    return (
        <>
            {isInitialised && !isLoading && (
                <NavigationContainer
                    linking={linking}
                    fallback={<NormalText>Loading...</NormalText>}>
                    {!isLoggedIn && <LoginNavigation />}
                    {isLoggedIn && !isAgreedToSValid && <InitialNavigation />}
                    {isLoggedIn && isAgreedToSValid && isVendorEmployee && (
                        <VendorNavigation />
                    )}
                    {isLoggedIn && isAgreedToSValid && !isVendorEmployee && (
                        <MainNavigation />
                    )}
                </NavigationContainer>
            )}
        </>
    );
};

export default AppNavigation;
