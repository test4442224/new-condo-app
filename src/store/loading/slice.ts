import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';

interface LoadingState {
    isLoading: boolean;
}

const initialState: LoadingState = {
    isLoading: false,
};

const setLoading = (state: LoadingState, action: PayloadAction<boolean>) => {
    state.isLoading = action.payload;
};

const loadingSlice = createSlice({
    name: 'loadingSlice',
    initialState,
    reducers: { setLoading },
});

export const loadingActions = loadingSlice.actions;
export default loadingSlice;
export const loadingState = (state: RootState) => state.loading;
