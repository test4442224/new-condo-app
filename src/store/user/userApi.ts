import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import { getServiceApiUrl } from '../../Constants/api';
import { RootState } from '../store';
import {
    IGetResidentDetailResponse,
    IResident,
} from '../../models/resident/resident.model';
import { RoleType } from '../../models/userRole/RoleType';

export interface changePasswordResponse {
    message: string;
}
export interface changePasswordRequest {
    token: string;
    oldPassword: string;
    newPassword: string;
}

export interface IUpdateUserDetailsRequest {
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: number;
}

export interface IUpdateUserDetailsResponse {
    id: number;
    email: string;
    phoneNumber: number;
    firstName: string;
    lastName: string;
    isPasswordReset: boolean;
    status: string;
    createdAt: string;
    updatedAt: string;
}

export interface ILoginRequest {
    email: string;
    password: string;
    deviceToken: string;
}

export interface ILoginResponse {
    firstName: string;
    lastName: string;
    userId: number;
    phoneNumber: number;
    email: string;
    token: string;
    roleTypes: RoleType[];
    isPasswordReset: boolean;
    condo: {
        id: number;
        name: string;
        uen: string;
        aboutUs: string;
        aboutUsImagePath: string | null;
        paynowQrCodeImagePath: string;
    };
    isAgreedToS: boolean;
    agreedToSAt: string | null;
}

export const userApi = createApi({
    reducerPath: 'userApi',
    baseQuery: fetchBaseQuery({
        baseUrl: getServiceApiUrl() + '/user',
        prepareHeaders: (headers, { getState }) => {
            const { token } = (getState() as RootState).user;

            if (token) {
                headers.set('Authorization', `Bearer ${token}`);
            }
        },
    }),
    endpoints: builder => ({
        login: builder.mutation<ILoginResponse, ILoginRequest>({
            query: params => ({
                url: '/auth',
                method: 'POST',
                body: params,
            }),
        }),
        getUserEmailApi: builder.query<string, string>({
            query: (encryptedUserId: string) => {
                const url = `/decrypt-userid/${encryptedUserId}`;
                return {
                    url,
                    method: 'GET',
                };
            },
        }),
        changePasswordApi: builder.mutation<
            changePasswordResponse,
            changePasswordRequest
        >({
            query: ({ token, oldPassword, newPassword }) => {
                const url = '/change-password';
                return {
                    url,
                    method: 'PUT',
                    body: { oldPassword, newPassword },
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                };
            },
        }),
        // IMPLEMENTATION FOR EDIT PROFILE. COMMENTED FOR FUTURE USE
        // updateUserDetails: builder.mutation<
        //     IUpdateUserDetailsResponse,
        //     IUpdateUserDetailsRequest
        // >({
        //     query: params => ({
        //         url: 'details',
        //         body: params,
        //         method: 'PATCH',
        //     }),
        // }),
        agreeToS: builder.mutation<IResident, undefined>({
            query: () => ({
                url: '/agree-to-tos',
                method: 'PATCH',
            }),
        }),
        getUserDetails: builder.query<IGetResidentDetailResponse, undefined>({
            query: () => ({
                url: '/details',
                method: 'GET',
            }),
        }),
        logout: builder.mutation<undefined, { deviceToken: string }>({
            query: params => ({
                url: '/logout',
                method: 'POST',
                body: params,
            }),
        }),
    }),
});

export const {
    useLazyGetUserEmailApiQuery,
    useChangePasswordApiMutation,
    // useUpdateUserDetailsMutation,
    useAgreeToSMutation,
    useLoginMutation,
    useGetUserDetailsQuery,
    useLazyGetUserDetailsQuery,
    useLogoutMutation,
} = userApi;
