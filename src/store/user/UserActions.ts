import { PayloadAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import ActionTypes from '../ActionTypes';
import { isAxiosError } from 'axios';
import client from '../../utils/client';
import { userApi } from './userApi';
import { ICondo } from '../../models/condo/condo.model';

export const forgetPassword = createAsyncThunk(
    ActionTypes.FORGET_PASSWORD,
    async ({ email }: { email: string }) => {
        try {
            const response = await client.post('/user/forget-password', {
                email,
            });
            return {
                response: response.data,
                statusCode: response.status,
            };
        } catch (error) {
            if (isAxiosError(error)) {
                return {
                    response: error.response?.data.errorMessage,
                    statusCode: error.response?.status,
                };
            } else {
                throw error;
            }
        }
    },
);

export const resetPassword = createAsyncThunk(
    ActionTypes.RESET_PASSWORD,
    async ({ email, newPassword }: { email: string; newPassword: string }) => {
        try {
            const response = await client.post('/user/reset-password', {
                email,
                newPassword,
            });

            return {
                response: response.data.message,
                statusCode: response.status,
            };
        } catch (error) {
            if (isAxiosError(error)) {
                return {
                    response: error.response?.data.errorMessage,
                    statusCode: error.response?.status,
                };
            } else {
                throw error;
            }
        }
    },
);

interface UserState {
    condo: ICondo | null;
    token: string | null;
    id: number | null;
    isPasswordReset: boolean;
    loggedOutReason: string | null;
    profileDetails: ProfileDetails;
    isPrimaryUser: boolean;
    isAgreedToS: boolean;
    agreedToSAt: string | null;
}

interface UserUnitDetails {
    blockNumber: string | null;
    unitNumber: string | null;
}

interface ProfileDetails {
    firstName: string | null;
    lastName: string | null;
    email: string | null;
    phoneNumber: number | null;
    units: UserUnitDetails[];
}

const initialUserState: UserState = {
    condo: null,
    token: null,
    id: null,
    isPasswordReset: false,
    loggedOutReason: null,
    profileDetails: {
        firstName: null,
        lastName: null,
        email: null,
        phoneNumber: null,
        units: [{ blockNumber: null, unitNumber: null }],
    },
    isPrimaryUser: false,
    isAgreedToS: false,
    agreedToSAt: null,
};

const setToken = (state: UserState, action: PayloadAction<string | null>) => {
    state.token = action.payload;
};

const setEmail = (state: UserState, action: PayloadAction<string>) => {
    state.profileDetails.email = action.payload;
};

const setToSAgreement = (
    state: UserState,
    action: PayloadAction<{ agreedToSAt: string | null; isAgreedToS: boolean }>,
) => {
    const { agreedToSAt, isAgreedToS } = action.payload;

    state.agreedToSAt = agreedToSAt;
    state.isAgreedToS = isAgreedToS;
};

const userSlice = createSlice({
    name: 'users',
    initialState: initialUserState,
    reducers: {
        setToken,
        logout: state => {
            state.token = null;
            state.profileDetails = initialUserState.profileDetails;
            state.isPasswordReset = false;
            state.id = null;
            state.condo = null;
            state.agreedToSAt = null;
            state.isAgreedToS = false;
        },
        forceLogout: (state: UserState, action: PayloadAction<string>) => {
            state.token = null;
            state.profileDetails = initialUserState.profileDetails;
            state.isPasswordReset = false;
            state.loggedOutReason = action.payload;
            state.id = null;
            state.condo = null;
            state.agreedToSAt = null;
            state.isAgreedToS = false;
        },
        removeLoggedOutReason: state => {
            state.loggedOutReason = '';
        },
        setEmail,
        setToSAgreement,
    },
    extraReducers: builder => {
        const {
            getUserEmailApi,
            // updateUserDetails, // EDIT PROFILE REDUCER. COMMENTED FOR FUTURE USE
            agreeToS,
            login: loginMutation,
            getUserDetails,
        } = userApi.endpoints;

        builder
            .addMatcher(getUserDetails.matchRejected, state => {
                state.profileDetails = initialUserState.profileDetails;
            })
            .addMatcher(getUserDetails.matchFulfilled, (state, action) => {
                if (action.payload.units.length > 0) {
                    const blockNumber = action.payload.units[0].blockNumber;
                    const unitNumber = action.payload.units[0].unitNumber;
                    state.profileDetails.units = [{ blockNumber, unitNumber }];
                }
                state.profileDetails.email = action.payload.email;
                state.profileDetails.firstName = action.payload.firstName;
                state.profileDetails.lastName = action.payload.lastName;
                state.profileDetails.phoneNumber = action.payload.phoneNumber;
                state.isPrimaryUser = action.payload.isPrimaryUser;
            })
            // IMPLEMENTATION FOR EDIT PROFILE. COMMENTED FOR FUTURE USE
            // .addMatcher(updateUserDetails.matchFulfilled, (state, action) => {
            //     const units = state.profileDetails.units;
            //     const { firstName, lastName, email, phoneNumber } =
            //         action.payload;

            //     state.profileDetails = {
            //         units,
            //         firstName,
            //         lastName,
            //         email,
            //         phoneNumber,
            //     };
            // })
            .addMatcher(getUserEmailApi.matchFulfilled, (state, action) => {
                state.profileDetails.email = action.payload;
            })
            .addMatcher(agreeToS.matchFulfilled, (state, action) => {
                if (
                    action.payload.isAgreedToS &&
                    action.payload.agreedToSAt !== null
                ) {
                    state.isAgreedToS = action.payload.isAgreedToS;
                    state.agreedToSAt = action.payload.agreedToSAt;
                }
            })
            .addMatcher(loginMutation.matchFulfilled, (state, action) => {
                state.token = action.payload.token;
                state.isPasswordReset = action.payload.isPasswordReset;
                state.id = action.payload.userId;
                state.profileDetails.firstName = action.payload.firstName;
                state.profileDetails.lastName = action.payload.lastName;
                state.condo = action.payload.condo;
                state.agreedToSAt = action.payload.agreedToSAt;
                state.isAgreedToS = action.payload.isAgreedToS;
            });
    },
});

export const userActions = userSlice.actions;
export default userSlice.reducer;
