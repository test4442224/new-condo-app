import { PaymentMethod } from '../manageBooking/slice';

export const DEFAULT_PAYMENT_METHOD: PaymentMethod | null = null;
