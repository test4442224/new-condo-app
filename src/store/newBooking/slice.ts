import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { IFacilityType } from '../../models/facility/FacilityTypeModel';
import { ITimeSlot } from '../../models/booking/TimeSlotModel';
import { RootState } from '../store';
import { IFacility } from '../../models/facility/FacilityModel';
import IFacilityAvailableTimeSlot from '../../models/facility/FacilityAvailableTimeSlot';
import { PaymentMethod } from '../manageBooking/slice';
import { facilityTypeApi } from '../api/facilityTypeApi';
import { facilityDateTimeApi } from '../api/facilityDateTimeApi';

export interface NewBookingState {
    availableFacilityTypes: IFacilityType[];
    chosenFacilityType: IFacilityType | null;
    chosenDate: string | null;
    facilityAvailableTimeSlots: IFacilityAvailableTimeSlot[];
    chosenFacility: IFacility | null;
    chosenTimeSlots: ITimeSlot[] | null;
    paymentMethod: PaymentMethod | null;
}

const initialState: NewBookingState = {
    availableFacilityTypes: [],
    chosenFacilityType: null,
    chosenDate: null,
    facilityAvailableTimeSlots: [],
    chosenFacility: null,
    chosenTimeSlots: null,
    paymentMethod: null,
};

const setChosenFacilityType = (
    state: NewBookingState,
    action: PayloadAction<IFacilityType | null>,
) => {
    state.chosenFacilityType = action.payload;
};

const setChosenFacility = (
    state: NewBookingState,
    action: PayloadAction<IFacility | null>,
) => {
    state.chosenFacility = action.payload;
};

const setPaymentMethod = (
    state: NewBookingState,
    action: PayloadAction<PaymentMethod | null>,
) => {
    state.paymentMethod = action.payload;
};

const setChosenTimeSlot = (
    state: NewBookingState,
    action: PayloadAction<ITimeSlot[] | null>,
) => {
    state.chosenTimeSlots = action.payload;
};

const setChosenDate = (
    state: NewBookingState,
    action: PayloadAction<string | null>,
) => {
    if (action.payload === null) {
        state.chosenDate = null;
        return;
    }

    const date = new Date(action.payload);

    if (Number.isNaN(date.getTime())) {
        return;
    }

    state.chosenDate = action.payload;
};

const resetFacilityAvailableTimeSlots = (state: NewBookingState) => {
    state.facilityAvailableTimeSlots = [];
};

const resetAvailableFacilityTypes = (state: NewBookingState) => {
    state.availableFacilityTypes = [];
};

const newBookingSlice = createSlice({
    name: 'newBookingSlice',
    initialState,
    reducers: {
        setChosenFacilityType,
        setChosenFacility,
        setChosenTimeSlot,
        setChosenDate,
        resetFacilityAvailableTimeSlots,
        resetAvailableFacilityTypes,
        setPaymentMethod,
    },
    extraReducers: builder => {
        const { getAllFacilityTypes } = facilityTypeApi.endpoints;
        const { getFacilityAvailableTime } = facilityDateTimeApi.endpoints;

        builder.addMatcher(
            getFacilityAvailableTime.matchFulfilled,
            (state, action) => {
                state.facilityAvailableTimeSlots = action.payload;
            },
        );
        builder.addMatcher(getFacilityAvailableTime.matchRejected, state => {
            state.facilityAvailableTimeSlots = [];
        });
        builder.addMatcher(
            getAllFacilityTypes.matchFulfilled,
            (state, action) => {
                state.availableFacilityTypes = action.payload.filter(
                    facilityType =>
                        facilityType.facilities &&
                        facilityType.facilities.length > 0,
                );
            },
        );
        builder.addMatcher(getAllFacilityTypes.matchRejected, state => {
            state.availableFacilityTypes = [];
        });
    },
});

export const newBookingActions = newBookingSlice.actions;
export default newBookingSlice;
export const newBookingState = (state: RootState) => state.newBooking;
