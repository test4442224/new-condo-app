import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import { getServiceApiUrl } from '../../Constants/api';
import { NoticeAttributes, NoticeType } from './slice';
import { RootState } from '../store';

export const noticeApi = createApi({
    reducerPath: 'noticeApi',
    baseQuery: fetchBaseQuery({
        baseUrl: getServiceApiUrl() + '/notice',
        prepareHeaders: (headers, { getState }) => {
            const { token } = (getState() as RootState).user;

            if (token) {
                headers.set('Authorization', `Bearer ${token}`);
            }
        },
    }),
    endpoints: builder => ({
        getAllCarouselNoticeApi: builder.query<NoticeAttributes[], string>({
            query: token => {
                return {
                    url: '/carousel',
                    method: 'GET',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                };
            },
        }),
        getAllNoticesByNoticeType: builder.query<
            NoticeAttributes[],
            { noticeType: NoticeType }
        >({
            query: ({ noticeType }) => ({
                url: `?noticeType=${noticeType}`,
                method: 'GET',
            }),
        }),
    }),
});

export const {
    useGetAllCarouselNoticeApiQuery,
    useLazyGetAllCarouselNoticeApiQuery,
    useGetAllNoticesByNoticeTypeQuery,
    useLazyGetAllNoticesByNoticeTypeQuery,
} = noticeApi;
