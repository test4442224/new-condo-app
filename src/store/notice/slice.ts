import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { noticeApi } from './api';

export enum NoticeType {
    ANNOUNCEMENT = 'ANNOUNCEMENT',
    EVENT = 'EVENT',
}

export interface NoticeAttributes {
    id: number;
    noticeType: NoticeType;
    condoId: number;
    description: string;
    imageUrl: string | null;
    title: string;
    createdBy: number;
    isArchived: boolean;
    orderNumber: number | null;
    isOnBanner: boolean;
    startDateTime: Date | null;
    endDateTime: Date | null;
    createdAt: Date;
}

export interface NoticeState {
    noticesArray: NoticeAttributes[];
    isLoading: boolean;
    selectedNotice: NoticeAttributes | null;
    carouselArray: NoticeAttributes[];
}

const initialState: NoticeState = {
    noticesArray: [],
    isLoading: false,
    selectedNotice: null,
    carouselArray: [],
};

const selectNoticeForDetails = (
    state: NoticeState,
    action: PayloadAction<NoticeAttributes>,
) => {
    state.selectedNotice = action.payload;
};

export const noticeSlice = createSlice({
    name: 'notice',
    initialState,
    reducers: { selectNoticeForDetails },
    extraReducers: builders => {
        const { getAllCarouselNoticeApi, getAllNoticesByNoticeType } =
            noticeApi.endpoints;
        builders
            .addMatcher(getAllNoticesByNoticeType.matchPending, state => {
                state.isLoading = true;
            })
            .addMatcher(
                getAllNoticesByNoticeType.matchFulfilled,
                (state, action) => {
                    state.isLoading = false;
                    state.noticesArray = action.payload;
                },
            )
            .addMatcher(getAllNoticesByNoticeType.matchRejected, state => {
                state.isLoading = false;
            });

        builders.addMatcher(
            getAllCarouselNoticeApi.matchFulfilled,
            (state, action) => {
                state.isLoading = false;
                state.carouselArray = action.payload;
            },
        );
    },
});

export const noticeAction = noticeSlice.actions;
export default noticeSlice.reducer;
export const noticeState = (state: RootState) => state.notice;
