import { createSlice } from '@reduxjs/toolkit';
import { ILegalDocument } from '../../models/legalDocuments/legalDocument.model';
import { legalDocumentApi } from './legalDocumentApi';
import { LegalDocumentName } from '../../models/legalDocuments/legalDocumentName.enum';

interface LegalDocumentState {
    termOfService: ILegalDocument | null;
    privacyPolicy: ILegalDocument | null;
}

const initialState: LegalDocumentState = {
    termOfService: null,
    privacyPolicy: null,
};

export const legalDocumentSlice = createSlice({
    name: 'legalDocumentSlice',
    initialState,
    reducers: {},
    extraReducers: builder => {
        const { getDocument } = legalDocumentApi.endpoints;

        builder.addMatcher(getDocument.matchFulfilled, (state, action) => {
            const { name, content, id } = action.payload;

            switch (name) {
                case LegalDocumentName.PRIVACY_POLICY:
                    state.privacyPolicy = {
                        id,
                        name,
                        content,
                    };
                    break;
                case LegalDocumentName.TERMS_OF_SERVICE:
                    state.termOfService = {
                        id,
                        name,
                        content,
                    };
                    break;
                default:
                    break;
            }
        });
    },
});
