import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { IGetLegalDocumentResponse } from '../../models/legalDocuments/legalDocument.model';
import { getServiceApiUrl } from '../../Constants/api';

export const legalDocumentApi = createApi({
    reducerPath: 'legalDocumentApi',
    baseQuery: fetchBaseQuery({
        baseUrl: `${getServiceApiUrl()}/legal-documents`,
    }),
    endpoints: builder => ({
        getDocument: builder.query<
            IGetLegalDocumentResponse,
            { documentName: string }
        >({
            query: ({ documentName }) => ({
                url: `/${documentName}`,
                method: 'GET',
            }),
        }),
    }),
});

export const { useLazyGetDocumentQuery, useGetDocumentQuery } =
    legalDocumentApi;
