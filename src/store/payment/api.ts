import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { getServiceApiUrl } from '../../Constants/api';
import { RootState } from '../store';
import { IConservancyFeesAttribute, IPayment } from './slice';
import { CreatePaymentRequest } from './dto';

export const paymentApi = createApi({
    reducerPath: 'paymentApi',
    baseQuery: fetchBaseQuery({
        baseUrl: getServiceApiUrl(),
        prepareHeaders: (headers, { getState }) => {
            const { token } = (getState() as RootState).user;

            if (token) {
                headers.set('Authorization', `Bearer ${token}`);
            }
        },
    }),
    endpoints: builder => ({
        getAllConservancyPayments: builder.query<
            { data: IConservancyFeesAttribute[] },
            void
        >({
            query: token => {
                return {
                    url: '/conservancy-fees',
                    method: 'GET',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                };
            },
        }),
        createPayment: builder.mutation<IPayment, CreatePaymentRequest>({
            query: data => {
                const reqBody = new FormData();
                reqBody.append('paymentPurpose', data.paymentPurpose);
                reqBody.append('paymentMethod', data.paymentMethod);
                reqBody.append('referenceId', data.referenceId);
                reqBody.append('image', data.image);

                return {
                    url: '/payment',
                    method: 'POST',
                    body: reqBody,
                };
            },
        }),
    }),
});

export const {
    useLazyGetAllConservancyPaymentsQuery,
    useCreatePaymentMutation,
} = paymentApi;
