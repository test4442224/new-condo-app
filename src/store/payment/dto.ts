import { ImageData } from '../manageBooking/api';
import { PaymentMethod, PaymentPurpose } from './slice';

export interface CreatePaymentRequest {
    paymentPurpose: PaymentPurpose;
    paymentMethod: PaymentMethod;
    referenceId: string;
    image: ImageData;
}
