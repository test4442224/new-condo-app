import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { paymentApi } from './api';
import { PaymentStatus } from '../manageBooking/slice';

export enum PaymentPurpose {
    CONSERVANCY = 'CONSERVANCY',
    FACILITY = 'FACILITY',
}

export enum PaymentMethod {
    PAYNOW = 'PAYNOW',
    CASH = 'CASH',
}

export interface IConservancyFeesAttribute {
    id: number;
    referenceId: string;
    conservancyPaymentId: number;
    totalAmount: number;
    unitId: number;
    createdAt: string;
    updatedAt: string;
    payments: IPayment[];
    conservancyPayment: IConservancyPayment;
    conservancyPaymentBreakdowns: IConservancyPaymentBreakdown[];
}

export interface IConservancyPayment {
    id: number;
    name: string;
    billingPeriod: string;
    dueDate: string;
    condoId: number;
    createdAt: string;
    updatedAt: string;
}

export interface IConservancyPaymentBreakdown {
    id: number;
    conservancyUnitPaymentId: number;
    feeName: string;
    amount: string;
    createdAt: string;
    updatedAt: string;
}

export interface IPayment {
    id: number;
    paymentPurpose: PaymentPurpose;
    paymentMethod: PaymentMethod;
    bankTransactionNo: string | null;
    referenceId: string;
    status: PaymentStatus;
    userId: number;
    paynowReceiptImageUrl: string | null;
    amount: string;
    createdAt: string;
    updatedAt: string;
}

export interface ManagePaymentState {
    paymentsArray: IConservancyFeesAttribute[];
    isLoading: boolean;
    selectedPayment: IConservancyFeesAttribute | null;
    upcomingPayments: IConservancyFeesAttribute[];
}

const initialState: ManagePaymentState = {
    paymentsArray: [],
    isLoading: false,
    selectedPayment: null,
    upcomingPayments: [],
};

const selectPaymentForDetail = (
    state: ManagePaymentState,
    action: PayloadAction<IConservancyFeesAttribute>,
) => {
    state.selectedPayment = action.payload;
};

export const paymentSlice = createSlice({
    name: 'payment',
    initialState,
    reducers: {
        selectPaymentForDetail,
    },
    extraReducers: builders => {
        const { getAllConservancyPayments, createPayment } =
            paymentApi.endpoints;

        builders
            .addMatcher(getAllConservancyPayments.matchPending, state => {
                state.isLoading = true;
            })
            .addMatcher(
                getAllConservancyPayments.matchFulfilled,
                (state, action) => {
                    state.isLoading = false;
                    state.paymentsArray = action.payload.data;
                },
            )
            .addMatcher(getAllConservancyPayments.matchRejected, state => {
                state.isLoading = false;
            })
            .addMatcher(createPayment.matchFulfilled, (state, action) => {
                const referenceId = action.payload.referenceId;
                const conservancyUnitPayment = state.paymentsArray.find(
                    payment => payment.referenceId === referenceId,
                );
                conservancyUnitPayment?.payments.push(action.payload);
            });
    },
});

export const paymentAction = paymentSlice.actions;
export default paymentSlice.reducer;
// export const paymentState = (state: RootState) => state.payment;
