import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';

interface PageHeaderState {
    title: string;
}

const initialState: PageHeaderState = {
    title: '',
};

const setTitle = (state: PageHeaderState, aciton: PayloadAction<string>) => {
    state.title = aciton.payload;
};

const pageHeaderSlice = createSlice({
    name: 'pageHeaderSlice',
    initialState,
    reducers: { setTitle },
});

export const pageHeaderActions = pageHeaderSlice.actions;
export default pageHeaderSlice;
export const pageHeaderState = (state: RootState) => state.pageHeader;
