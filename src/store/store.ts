import {
    PayloadAction,
    PreloadedState,
    combineReducers,
    configureStore,
} from '@reduxjs/toolkit';

import userReducer from './user/UserActions';
import bookingReducer from './manageBooking/slice';
import userRoleSlice from './userRole/slice';
import newBookingSlice from './newBooking/slice';
import loadingSlice from './loading/slice';
import pageHeaderSlice from './pageHeader/slice';
import { facilityImageSlice } from './facilityImage/slice';
import { userRoleApi } from './userRole/userRoleApi';
import { noticeSlice } from './notice/slice';
import { noticeApi } from './notice/api';
import { userApi } from './user/userApi';
import { bookingApi } from './manageBooking/api';
import { legalDocumentApi } from './legalDocument/legalDocumentApi';
import { legalDocumentSlice } from './legalDocument/legalDocumentSlice';
import { facilityImageApi } from './facilityImage/api';
import { facilityTypeApi } from './api/facilityTypeApi';
import { facilityDateTimeApi } from './api/facilityDateTimeApi';
import { condoSlice } from './condo/slice';
import { condoApi } from './condo/api';
import { defectSlice } from './defect/slice';
import { defectApi } from './defect/api';
import { attachmentSlice } from './defect/attachment/slice';
import { attachmentApi } from './defect/attachment/api';
import { paymentSlice } from './payment/slice';
import { paymentApi } from './payment/api';
import { defectPriorityApi } from './defect/defectPriority/api';
import { statementOfAccountApi } from './statementOfAccount/api';
import { statementOfAccountSlice } from './statementOfAccount/slice';

const appReducer = combineReducers({
    booking: bookingReducer,
    user: userReducer,
    userRole: userRoleSlice.reducer,
    newBooking: newBookingSlice.reducer,
    loading: loadingSlice.reducer,
    pageHeader: pageHeaderSlice.reducer,
    facilityImage: facilityImageSlice.reducer,
    legalDocument: legalDocumentSlice.reducer,
    condo: condoSlice.reducer,
    notice: noticeSlice.reducer,
    defect: defectSlice.reducer,
    attachment: attachmentSlice.reducer,
    payment: paymentSlice.reducer,
    statementOfAccount: statementOfAccountSlice.reducer,
    [defectApi.reducerPath]: defectApi.reducer,
    [attachmentApi.reducerPath]: attachmentApi.reducer,
    [userRoleApi.reducerPath]: userRoleApi.reducer,
    [noticeApi.reducerPath]: noticeApi.reducer,
    [userApi.reducerPath]: userApi.reducer,
    [bookingApi.reducerPath]: bookingApi.reducer,
    [legalDocumentApi.reducerPath]: legalDocumentApi.reducer,
    [facilityImageApi.reducerPath]: facilityImageApi.reducer,
    [facilityTypeApi.reducerPath]: facilityTypeApi.reducer,
    [facilityDateTimeApi.reducerPath]: facilityDateTimeApi.reducer,
    [condoApi.reducerPath]: condoApi.reducer,
    [paymentApi.reducerPath]: paymentApi.reducer,
    [defectPriorityApi.reducerPath]: defectPriorityApi.reducer,
    [statementOfAccountApi.reducerPath]: statementOfAccountApi.reducer,
});

const rootReducer = (
    state: ReturnType<typeof appReducer> | undefined,
    action: PayloadAction<string | null>,
) => {
    return appReducer(state, action);
};

export const appStore = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware =>
        getDefaultMiddleware()
            .concat(userRoleApi.middleware)
            .concat(noticeApi.middleware)
            .concat(userApi.middleware)
            .concat(bookingApi.middleware)
            .concat(legalDocumentApi.middleware)
            .concat(facilityImageApi.middleware)
            .concat(facilityTypeApi.middleware)
            .concat(facilityDateTimeApi.middleware)
            .concat(condoApi.middleware)
            .concat(defectApi.middleware)
            .concat(attachmentApi.middleware)
            .concat(paymentApi.middleware)
            .concat(defectPriorityApi.middleware)
            .concat(statementOfAccountApi.middleware),
});

export const setupStore = (preloadedState?: PreloadedState<RootState>) => {
    return configureStore({
        reducer: rootReducer,
        middleware: getDefaultMiddleware =>
            getDefaultMiddleware({ serializableCheck: false })
                .concat(userRoleApi.middleware)
                .concat(noticeApi.middleware)
                .concat(userApi.middleware)
                .concat(bookingApi.middleware)
                .concat(legalDocumentApi.middleware)
                .concat(facilityImageApi.middleware)
                .concat(facilityTypeApi.middleware)
                .concat(facilityDateTimeApi.middleware)
                .concat(condoApi.middleware)
                .concat(defectApi.middleware)
                .concat(attachmentApi.middleware)
                .concat(paymentApi.middleware)
                .concat(defectPriorityApi.middleware)
                .concat(statementOfAccountApi.middleware),
        preloadedState,
    });
};

const store = setupStore();

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = typeof store.dispatch;

export default store;
