import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import { getServiceApiUrl } from '../../Constants/api';
import { RootState } from '../store';
import { IFacilityType } from '../../models/facility/FacilityTypeModel';
import { IGetAllFacilityTypeRequest } from '../../models/facility/facilityType/facilityTypeApiRequest';

export const facilityTypeApi = createApi({
    reducerPath: 'facilityTypeApi',
    baseQuery: fetchBaseQuery({
        baseUrl: `${getServiceApiUrl()}/facility-type`,
        prepareHeaders: (headers, { getState }) => {
            const { token } = (getState() as RootState).user;

            if (token) {
                headers.set('Authorization', `Bearer ${token}`);
            }
        },
    }),
    endpoints: builder => ({
        getAllFacilityTypes: builder.query<
            IFacilityType[],
            IGetAllFacilityTypeRequest
        >({
            query: ({ condoId }) => ({
                url: `/${condoId}/?onlyHasFacility=true`,
                method: 'GET',
            }),
        }),
    }),
});

export const { useGetAllFacilityTypesQuery, useLazyGetAllFacilityTypesQuery } =
    facilityTypeApi;
