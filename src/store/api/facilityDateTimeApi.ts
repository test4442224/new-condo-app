import {
    FetchArgs,
    createApi,
    fetchBaseQuery,
} from '@reduxjs/toolkit/dist/query/react';
import { getServiceApiUrl } from '../../Constants/api';
import { RootState } from '../store';
import {
    IUnavailableDatesRequest,
    IUnavailableDatesResponse,
} from '../../models/booking/UnavailableDates';
import { DateTime } from 'luxon';
import IFacilityAvailableTimeSlot from '../../models/facility/FacilityAvailableTimeSlot';
import { IGetFacilityAvailableTimeRequest } from '../../models/facility/FacilityDateTimeApiRequest';

export const facilityDateTimeApi = createApi({
    reducerPath: 'facilityDateTimeApi',
    baseQuery: fetchBaseQuery({
        baseUrl: `${getServiceApiUrl()}/facility-date-time`,
        prepareHeaders: (headers, { getState }) => {
            const { token } = (getState() as RootState).user;

            if (token) {
                headers.set('Authorization', `Bearer ${token}`);
            }
        },
    }),
    endpoints: builder => ({
        getUnavailableDates: builder.query<
            IUnavailableDatesResponse,
            IUnavailableDatesRequest
        >({
            query: ({ facilityTypeId, totalDays }) => {
                const startDate = DateTime.local().toISODate();
                const endDate = DateTime.local()
                    .plus({ days: totalDays })
                    .toISODate();

                const request: FetchArgs = {
                    url: `unavailable-date/${facilityTypeId}?startDate=${startDate}&endDate=${endDate}`,
                    method: 'GET',
                };

                return request;
            },
        }),
        getFacilityAvailableTime: builder.query<
            IFacilityAvailableTimeSlot[],
            IGetFacilityAvailableTimeRequest
        >({
            query: ({ dateString, facilityTypeId }) => {
                const date = new Date(dateString);

                const requestDateString = DateTime.fromJSDate(date).toISODate();

                return {
                    url: `/${facilityTypeId}?date=${requestDateString}`,
                    method: 'GET',
                };
            },
        }),
    }),
});

export const {
    useGetUnavailableDatesQuery,
    useLazyGetUnavailableDatesQuery,
    useGetFacilityAvailableTimeQuery,
    useLazyGetFacilityAvailableTimeQuery,
} = facilityDateTimeApi;
