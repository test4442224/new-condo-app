import { createAsyncThunk } from '@reduxjs/toolkit';
import { clientWithAuth } from '../../utils/client';
import { isAxiosError } from 'axios';
export interface CancelBookingResponse {
    statusCode: number;
    error: string | null;
}

export const cancelBookingThunk = createAsyncThunk(
    'booking/cancelBooking',
    async (
        { bookingId }: { bookingId: number; token: string },
        { rejectWithValue },
    ) => {
        try {
            await clientWithAuth.patch(`/booking/${bookingId}/cancel`, {
                cancelReason: 'Booking cancelled by USER',
            });
        } catch (error) {
            if (isAxiosError(error)) {
                if (error.response) {
                    return rejectWithValue(error.response.data.errorMessage);
                }
            }

            return rejectWithValue('Something went wrong!');
        }
    },
);
