import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { createBookingThunk } from './thunk';
import { cancelBookingThunk } from './cancelBookingThunk';
import { IPriceBreakdown } from '../../models/booking/PriceBreakdown';
import { bookingApi } from './api';

export interface ManageBookingAttributes {
    id: number;
    bookingReferenceId: string;
    facilityId: number;
    facility: Facility;
    startDateTime: string;
    endDateTime: string;
    user: User;
    createdAt: string | null;
    updatedAt: string | null;
    facilityImages: FacilityImage;
    isCancellable: boolean;
    isCancelledByUser: boolean | null;
    cancelReason: string | null;
    paymentMethod: PaymentMethod | null;
    paymentDueAt: Date;
    priceBreakdowns: IPriceBreakdown[];

    totalPrice: number;
    paymentStatus: PaymentStatus;
    cancelledAt: Date | null;
    paidAt: Date | null;
    refundedAt: Date | null;
}

export interface User {
    lastName: string;
    firstName: string;
    phoneNumber: string;
    // facilityImages: FacilityImage;
}

export interface Facility {
    name: string;
    location: string;
    description: string;
    type: string;
    pax: number;
}

export interface FacilityImage {
    images: string[];
}

export enum PaymentMethod {
    CASH = 'CASH',
    PAYNOW = 'PAYNOW',
}

export enum PaymentStatus {
    FREE = 'FREE',
    PAID = 'PAID',
    PENDING_PAYMENT = 'PENDING_PAYMENT',
    PENDING_REFUND = 'PENDING_REFUND',
    REFUNDED = 'REFUNDED',
    CANCELLED = 'CANCELLED',
    PENDING_VERIFICATION = 'PENDING_VERIFICATION',
}

export interface ManageBookingState {
    bookingsArray: ManageBookingAttributes[];
    isLoading: boolean;
    selectedBooking: ManageBookingAttributes | null;
    upcomingBookings: ManageBookingAttributes[];
}

const initialState: ManageBookingState = {
    bookingsArray: [],
    isLoading: false,
    selectedBooking: null,
    upcomingBookings: [],
};

const selectBookingForDetail = (
    state: ManageBookingState,
    action: PayloadAction<ManageBookingAttributes>,
) => {
    state.selectedBooking = action.payload;
};

export const bookingSlice = createSlice({
    name: 'booking',
    initialState,
    reducers: {
        selectBookingForDetail,
    },
    extraReducers: builders => {
        const { makePaynowPayment, getUpcomingBookings, getAllBookings } =
            bookingApi.endpoints;

        builders
            .addCase(createBookingThunk.pending, state => {
                state.isLoading = true;
            })
            .addCase(createBookingThunk.fulfilled, state => {
                state.isLoading = false;
            })
            .addCase(createBookingThunk.rejected, state => {
                state.isLoading = false;
            })
            .addCase(cancelBookingThunk.pending, _state => {
                // TODO: display cancelbooking loading
            })
            .addCase(cancelBookingThunk.fulfilled, state => {
                const bookingIndex = state.bookingsArray.findIndex(
                    (booking: ManageBookingAttributes) =>
                        booking.id === state.selectedBooking?.id,
                );
                const booking = state.bookingsArray[bookingIndex];
                let paymentStatus: PaymentStatus = booking.paymentStatus;
                let isCancelledByUser: boolean | null =
                    booking.isCancelledByUser;
                switch (booking.paymentStatus) {
                    case PaymentStatus.PAID:
                        paymentStatus = PaymentStatus.PENDING_REFUND;
                        isCancelledByUser = true;
                        break;
                    case PaymentStatus.PENDING_PAYMENT:
                        paymentStatus = PaymentStatus.CANCELLED;
                        isCancelledByUser = true;
                        break;
                    case PaymentStatus.FREE:
                        paymentStatus = PaymentStatus.CANCELLED;
                        isCancelledByUser = true;
                        break;
                    case PaymentStatus.PENDING_VERIFICATION:
                        paymentStatus = PaymentStatus.PENDING_REFUND;
                }

                if (bookingIndex !== -1) {
                    const updatedBooking: ManageBookingAttributes = {
                        ...state.bookingsArray[bookingIndex],
                        isCancellable: false,
                        createdAt: state.bookingsArray[bookingIndex].createdAt,
                        startDateTime:
                            state.bookingsArray[bookingIndex].startDateTime,
                        endDateTime:
                            state.bookingsArray[bookingIndex].endDateTime,
                        updatedAt: null,
                        paymentStatus,
                        isCancelledByUser,
                    };
                    state.bookingsArray[bookingIndex] = updatedBooking;
                    state.selectedBooking = updatedBooking;
                }
            })
            .addCase(cancelBookingThunk.rejected, state => {
                console.log(state);
            })
            .addMatcher(getUpcomingBookings.matchPending, state => {
                state.isLoading = true;
            })
            .addMatcher(getUpcomingBookings.matchFulfilled, (state, action) => {
                state.isLoading = false;
                state.upcomingBookings = action.payload;
            })
            .addMatcher(getUpcomingBookings.matchRejected, state => {
                state.isLoading = false;
            })
            .addMatcher(getAllBookings.matchPending, state => {
                state.isLoading = true;
            })
            .addMatcher(getAllBookings.matchFulfilled, (state, action) => {
                state.isLoading = false;
                state.bookingsArray = action.payload;
            })
            .addMatcher(getAllBookings.matchRejected, state => {
                state.isLoading = false;
            })
            .addMatcher(makePaynowPayment.matchFulfilled, (state, action) => {
                const updatedBooking = action.payload;
                const bookingToUpdateIdx = state.bookingsArray.findIndex(
                    booking => {
                        booking.id === updatedBooking.id;
                    },
                );
                state.bookingsArray[bookingToUpdateIdx] = updatedBooking;
            });
    },
});

export const bookingAction = bookingSlice.actions;
export default bookingSlice.reducer;
export const bookingState = (state: RootState) => state.booking;
