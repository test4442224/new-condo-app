import { createAsyncThunk } from '@reduxjs/toolkit';
import { isAxiosError } from 'axios';
import { clientWithAuth } from '../../utils/client';
import { PaymentMethod } from './slice';
import { IBookingLockResponse } from '../../models/booking/BookingLockResponse';

export const createBookingThunk = createAsyncThunk(
    'booking/createBooking',
    async ({
        facilityId,
        bookingDate,
        startTime,
        endTime,
        paymentMethod,
    }: {
        facilityId: number;
        bookingDate: string;
        startTime: string;
        endTime: string;
        token: string;
        paymentMethod: PaymentMethod | null;
    }) => {
        try {
            const response = await clientWithAuth.post('/booking', {
                facilityId,
                bookingDate,
                startTime,
                endTime,
                paymentMethod,
            });
            return {
                response: response.data,
                statusCode: response.status,
            };
        } catch (error) {
            if (isAxiosError(error)) {
                if (error.response) {
                    return {
                        response: null,
                        statusCode: error.response.status,
                        error: error.response.data.errorMessage,
                    };
                }
            }

            return {
                response: null,
                statusCode: 500,
                error: error,
            };
        }
    },
);

export const createBookingLockThunk = createAsyncThunk(
    'booking/lockBooking',
    async ({
        facilityId,
        bookingDate,
        startTime,
        endTime,
    }: {
        facilityId: number;
        bookingDate: string;
        startTime: string;
        endTime: string;
    }) => {
        try {
            const response = await clientWithAuth.post<IBookingLockResponse>(
                '/booking-lock',
                {
                    facilityId,
                    bookingDate,
                    startTime,
                    endTime,
                },
            );
            return {
                response: response.data,
                statusCode: response.status,
            };
        } catch (error) {
            if (isAxiosError(error)) {
                if (error.response) {
                    return {
                        response: null,
                        statusCode: error.response.status,
                        error: error.response.data.errorMessage,
                    };
                }
            }

            return {
                response: null,
                statusCode: 500,
                error: error,
            };
        }
    },
);

export const deleteBookingLockThunk = createAsyncThunk(
    'booking/lockBooking',
    async (bookingLockId: number) => {
        try {
            const response = await clientWithAuth.delete(
                `/booking-lock/${bookingLockId}`,
            );
            return {
                response: response.data,
                statusCode: response.status,
            };
        } catch (error) {
            if (isAxiosError(error)) {
                if (error.response) {
                    return {
                        response: null,
                        statusCode: error.response.status,
                        error: error.response.data.errorMessage,
                    };
                }
            }

            return {
                response: null,
                statusCode: 500,
                error: error,
            };
        }
    },
);
