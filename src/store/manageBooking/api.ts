import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import { getServiceApiUrl } from '../../Constants/api';
import { ManageBookingAttributes } from './slice';
import { RootState } from '../store';
import { ManageBookingsResponse } from '../../models/booking/ManageBookingResponse';

export type ImageData = {
    name: string;
    type: string;
    uri: string;
};

export const bookingApi = createApi({
    reducerPath: 'bookingApi',
    baseQuery: fetchBaseQuery({
        baseUrl: getServiceApiUrl() + '/booking',
        prepareHeaders: (headers, { getState }) => {
            const { token } = (getState() as RootState).user;

            if (token) {
                headers.set('Authorization', `Bearer ${token}`);
            }
        },
    }),
    endpoints: builder => ({
        makePaynowPayment: builder.mutation<
            ManageBookingAttributes,
            { token: string; bookingId: number; image: ImageData }
        >({
            query: ({ token, bookingId, image }) => {
                const form = new FormData();
                form.append('image', image);

                return {
                    url: `/${bookingId}/make-paynow-payment`,
                    method: 'PATCH',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                    body: form,
                };
            },
        }),
        getUpcomingBookings: builder.query<
            ManageBookingAttributes[],
            undefined
        >({
            query: () => ({
                url: '/upcoming',
                method: 'GET',
            }),
        }),
        getAllBookings: builder.query<ManageBookingsResponse[], undefined>({
            query: () => ({
                url: '/',
                method: 'GET',
            }),
        }),
        getBooking: builder.query<ManageBookingAttributes, number>({
            query: id => ({
                url: `/detail/${id}`,
                method: 'GET',
            }),
        }),
    }),
});

export const {
    useMakePaynowPaymentMutation,
    useLazyGetUpcomingBookingsQuery,
    useGetUpcomingBookingsQuery,
    useGetAllBookingsQuery,
    useLazyGetAllBookingsQuery,
    useLazyGetBookingQuery,
} = bookingApi;
