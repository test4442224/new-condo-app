import { PayloadAction } from '@reduxjs/toolkit';
import { ManageBookingAttributes, ManageBookingState } from './slice';

interface UpdateBookingAttributes {
    bookingId: number;
    propertyName: keyof ManageBookingAttributes;
    propertyValue: ManageBookingAttributes[keyof ManageBookingAttributes];
}

const findBookingByID = (
    state: ManageBookingState,
    action: PayloadAction<UpdateBookingAttributes>,
) => {
    const bookingIndex = state.bookingsArray.findIndex(
        (booking: ManageBookingAttributes) =>
            booking.id === action.payload.bookingId,
    );
    if (bookingIndex !== -1) {
        const updatedBooking: ManageBookingAttributes = {
            ...state.bookingsArray[bookingIndex],
            [action.payload.propertyName]: action.payload.propertyValue,
        };
        state.bookingsArray[bookingIndex] = updatedBooking;
    }
    return state;
};

export default findBookingByID;
