import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { getServiceApiUrl } from '../../Constants/api';
import { IGetCondoResponse } from '../../models/condo/condo.model';
import { RootState } from '../store';

export const condoApi = createApi({
    reducerPath: 'condoApi',
    baseQuery: fetchBaseQuery({
        baseUrl: `${getServiceApiUrl()}/condo`,
        prepareHeaders: (headers, { getState }) => {
            const { token } = (getState() as RootState).user;

            if (token) {
                headers.set('Authorization', `Bearer ${token}`);
            }
        },
    }),
    endpoints: builder => ({
        getCondoDetails: builder.query<IGetCondoResponse, void>({
            query: () => ({
                url: `/`,
                method: 'GET',
            }),
        }),
    }),
});

export const { useGetCondoDetailsQuery } = condoApi;
