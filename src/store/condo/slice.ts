import { createSlice } from '@reduxjs/toolkit';
import { condoApi } from './api';
import { ICondo } from '../../models/condo/condo.model';

interface CondoState {
    condo: ICondo | null;
}

const initialState: CondoState = {
    condo: null,
};

export const condoSlice = createSlice({
    name: 'condoSlice',
    initialState,
    reducers: {},
    extraReducers: builder => {
        const { getCondoDetails } = condoApi.endpoints;

        builder.addMatcher(getCondoDetails.matchFulfilled, (state, action) => {
            const { name, uen, aboutUs, aboutUsImagePath } = action.payload;
            state.condo = { name, uen, aboutUs, aboutUsImagePath };
        });
    },
});
