import { createSlice } from '@reduxjs/toolkit';
import { facilityImageApi } from './api';

interface FacilityImagesState {
    images: string[];
    isLoading: boolean;
}

const initialState: FacilityImagesState = {
    images: [],
    isLoading: false,
};

export const facilityImageSlice = createSlice({
    name: 'facilityImage',
    initialState,
    reducers: {},
    extraReducers: builders => {
        const { getAllFacilityImageUrls } = facilityImageApi.endpoints;

        builders
            .addMatcher(getAllFacilityImageUrls.matchPending, state => {
                state.isLoading = true;
            })
            .addMatcher(
                getAllFacilityImageUrls.matchFulfilled,
                (state, action) => {
                    state.isLoading = false;
                    state.images = action.payload;
                },
            )
            .addMatcher(getAllFacilityImageUrls.matchRejected, state => {
                state.images = initialState.images;
                state.isLoading = false;
            });
    },
});

export default facilityImageSlice.reducer;
