import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import { getServiceApiUrl } from '../../Constants/api';
import { RootState } from '../store';

export const facilityImageApi = createApi({
    reducerPath: 'facilityImageApi',
    baseQuery: fetchBaseQuery({
        baseUrl: `${getServiceApiUrl()}/facility-image`,
        prepareHeaders: (headers, { getState }) => {
            const { token } = (getState() as RootState).user;

            if (token) {
                headers.set('Authorization', `Bearer ${token}`);
            }
        },
    }),
    endpoints: builder => ({
        getAllFacilityImageUrls: builder.query<
            string[],
            { facilityId: number }
        >({
            query: ({ facilityId }) => ({
                url: `/${facilityId}`,
                method: 'GET',
            }),
        }),
    }),
});

export const {
    useGetAllFacilityImageUrlsQuery,
    useLazyGetAllFacilityImageUrlsQuery,
} = facilityImageApi;
