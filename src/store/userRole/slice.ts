import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import UserRole from '../../models/userRole/UserRole';
import { userRoleApi } from './userRoleApi';
import { RoleType } from '../../models/userRole/RoleType';

interface UserRoleState {
    currentUserRole: UserRole | null;
    allAvailableUseRoles: UserRole[];
    isDefectReporter: boolean;
}

const initialState: UserRoleState = {
    currentUserRole: null,
    allAvailableUseRoles: [],
    isDefectReporter: false,
};

const setAllAvailableRoles = (
    state: UserRoleState,
    action: PayloadAction<UserRole[]>,
) => {
    state.allAvailableUseRoles = action.payload;
};

const setCurrentUserRole = (
    state: UserRoleState,
    action: PayloadAction<UserRole | null>,
) => {
    state.currentUserRole = action.payload;
};

const setIsDefectReporter = (
    state: UserRoleState,
    action: PayloadAction<boolean>,
) => {
    state.isDefectReporter = action.payload;
};

const userRoleSlice = createSlice({
    name: 'userRole',
    initialState,
    reducers: {
        setCurrentUserRole,
        setAllAvailableRoles,
        setIsDefectReporter,
    },
    extraReducers: builder => {
        const { getUserRoles } = userRoleApi.endpoints;
        builder.addMatcher(getUserRoles.matchFulfilled, (state, action) => {
            state.currentUserRole = action.payload[0];
            state.allAvailableUseRoles = action.payload;
            const allUserRoles = action.payload;
            const checkIfDefectReporter = allUserRoles.filter(
                role => role.roleType === RoleType.DEFECT_REPORTER,
            );
            state.isDefectReporter = checkIfDefectReporter.length > 0;
        });
    },
});

export const userRoleActions = userRoleSlice.actions;
export default userRoleSlice;
