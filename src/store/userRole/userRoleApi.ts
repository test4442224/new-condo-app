import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { getServiceApiUrl } from '../../Constants/api';
import UserRole from '../../models/userRole/UserRole';

interface IGetUserRoleRequest {
    token: string;
}

export const userRoleApi = createApi({
    reducerPath: 'userRoleAPi',
    baseQuery: fetchBaseQuery({
        baseUrl: `${getServiceApiUrl()}/user/roles`,
    }),
    endpoints: builder => ({
        getUserRoles: builder.query<UserRole[], IGetUserRoleRequest>({
            query: ({ token }) => ({
                url: `/`,
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }),
        }),
    }),
});

export const { useGetUserRolesQuery, useLazyGetUserRolesQuery } = userRoleApi;
