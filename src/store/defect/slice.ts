import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';
import {
    DefectStatus,
    IDefectStatusHistory,
} from '../../models/defect/DefectStatusHistory';
import { DefectAttachment } from '../../models/defect/DefectAttachment';
import { defectApi } from './api';
import { IDefectPriority } from '../../models/defect/DefectPriority';
import { defectPriorityApi } from './defectPriority/api';

export interface DefectAttributes {
    id: number;
    defectReferenceNo: string;
    condoId: number;
    name: string;
    description: string;
    latestStatus: DefectStatus;
    reporterId: number;
    createdAt: Date;
    attachments: DefectAttachment[];
    statusHistories: IDefectStatusHistory[];
    priorityId: number | null;
    dueDate: string | null;
}

export interface DefectState {
    defectArray: DefectAttributes[];
    selectedDefect: DefectAttributes | null;
    condoDefectPriorities: IDefectPriority[];
}

const initialState: DefectState = {
    defectArray: [],
    selectedDefect: null,
    condoDefectPriorities: [],
};

const selectedDefectForDetails = (
    state: DefectState,
    action: PayloadAction<DefectAttributes>,
) => {
    state.selectedDefect = action.payload;
};

export const defectSlice = createSlice({
    name: 'defect',
    initialState,
    reducers: { selectedDefectForDetails },
    extraReducers: builders => {
        const { getAllDefectsByStatus, cancelDefect, getDefectbyId } =
            defectApi.endpoints;

        const { getCondoDefectPriorityLevels } = defectPriorityApi.endpoints;

        builders
            .addMatcher(
                getAllDefectsByStatus.matchFulfilled,
                (state, action) => {
                    state.defectArray = action.payload.data;
                },
            )
            .addMatcher(cancelDefect.matchFulfilled, (state, action) => {
                state.selectedDefect = action.payload;
            })
            .addMatcher(getDefectbyId.matchFulfilled, (state, action) => {
                state.selectedDefect = action.payload.data;
            })
            .addMatcher(
                getCondoDefectPriorityLevels.matchFulfilled,
                (state, action) => {
                    state.condoDefectPriorities = action.payload;
                },
            );
    },
});

export const defectAction = defectSlice.actions;
export default defectSlice.reducer;
export const DefectState = (state: RootState) => state.defect;
