import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../store';
import { attachmentApi } from './api';

export interface AttachmentAttributes {
    id: number;
    name: string;
    mimetype: string;
    type: string;
    uri: string;
    thumbnail: string;
}

export interface AttachmentState {
    attachmentArray: AttachmentAttributes[];
    selectedAttachment: AttachmentAttributes | null;
}

const initialState: AttachmentState = {
    attachmentArray: [],
    selectedAttachment: null,
};

export const attachmentSlice = createSlice({
    name: 'attachment',
    initialState,
    reducers: {},
    extraReducers: builders => {
        const { createDefectAttachment } = attachmentApi.endpoints;
        builders.addMatcher(
            createDefectAttachment.matchFulfilled,
            (state, action) => {
                state.selectedAttachment = action.payload;
            },
        );
    },
});

export const defectAction = attachmentSlice.actions;
export default attachmentSlice.reducer;
export const DefectState = (state: RootState) => state.defect;
