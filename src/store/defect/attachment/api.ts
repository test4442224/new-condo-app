import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import { getServiceApiUrl } from '../../../Constants/api';
import { RootState } from '../../store';
import { AttachmentAttributes } from './slice';

export type ImageData = {
    name: string;
    type: string;
    uri: string;
};

export const attachmentApi = createApi({
    reducerPath: 'attachmentApi',
    baseQuery: fetchBaseQuery({
        baseUrl: getServiceApiUrl() + '/defect',
        prepareHeaders: (headers, { getState }) => {
            const { token } = (getState() as RootState).user;

            if (token) {
                headers.set('Authorization', `Bearer ${token}`);
            }
        },
    }),
    endpoints: builder => ({
        createDefectAttachment: builder.mutation<
            AttachmentAttributes,
            { defectId: number; attachment: ImageData }
        >({
            query: ({ defectId, attachment }) => {
                const form = new FormData();
                form.append('attachment', attachment);
                return {
                    url: `${defectId}/attachment`,
                    method: 'POST',
                    body: form,
                };
            },
        }),
    }),
});

export const { useCreateDefectAttachmentMutation } = attachmentApi;
