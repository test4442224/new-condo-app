import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { getServiceApiUrl } from '../../Constants/api';
import { RootState } from '../store';
import { DefectAttributes } from './slice';
import { DefectStatus } from '../../models/defect/DefectStatusHistory';
import { IPaginationResponse } from '../../models/common/PaginationResponse.type';

export const defectApi = createApi({
    reducerPath: 'defectApi',
    baseQuery: fetchBaseQuery({
        baseUrl: `${getServiceApiUrl()}/defect`,
        prepareHeaders: (headers, { getState }) => {
            const { token } = (getState() as RootState).user;

            if (token) {
                headers.set('Authorization', `Bearer ${token}`);
            }
        },
    }),
    tagTypes: ['getDefectDetails'],
    endpoints: builder => ({
        getDefectbyId: builder.query<
            IPaginationResponse<DefectAttributes>,
            { defectId: number }
        >({
            query: ({ defectId }) => ({
                url: `/${defectId}`,
                params: {
                    details: true,
                },
            }),
            providesTags: ['getDefectDetails'],
        }),
        createDefect: builder.mutation<
            DefectAttributes,
            Partial<DefectAttributes>
        >({
            query: defect => ({
                url: '',
                method: 'POST',
                body: defect,
            }),
            invalidatesTags: ['getDefectDetails'],
        }),
        getAllDefectsByStatus: builder.query<
            IPaginationResponse<DefectAttributes[]>,
            { status: DefectStatus[]; sortBy: string }
        >({
            query: ({ status, sortBy }) => ({
                url: `?latestStatus=${status.join(',')}&sortBy=${sortBy}`,
                method: 'GET',
            }),
            providesTags: ['getDefectDetails'],
        }),
        cancelDefect: builder.mutation<DefectAttributes, { defectId: number }>({
            query: ({ defectId }) => {
                return {
                    url: `/${defectId}/cancel`,
                    method: 'PATCH',
                };
            },
            invalidatesTags: ['getDefectDetails'],
        }),
        updateDefect: builder.mutation<
            DefectAttributes,
            {
                defectId: number;
                updatedDefectBody: Pick<
                    DefectAttributes,
                    'name' | 'description'
                >;
            }
        >({
            query: ({ defectId, updatedDefectBody }) => {
                return {
                    url: `/${defectId}`,
                    method: 'PATCH',
                    body: updatedDefectBody,
                };
            },
            invalidatesTags: ['getDefectDetails'],
        }),
        removeAttachment: builder.mutation<
            DefectAttributes,
            {
                defectId: number;
                attachmentId: number;
            }
        >({
            query: ({ defectId, attachmentId }) => {
                return {
                    url: `/${defectId}/attachment/${attachmentId}`,
                    method: 'DELETE',
                };
            },
            invalidatesTags: ['getDefectDetails'],
        }),
    }),
});

export const {
    useGetDefectbyIdQuery,
    useLazyGetDefectbyIdQuery,
    useGetAllDefectsByStatusQuery,
    useLazyGetAllDefectsByStatusQuery,
    useCreateDefectMutation,
    useCancelDefectMutation,
    useUpdateDefectMutation,
    useRemoveAttachmentMutation,
} = defectApi;
