import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import { RootState } from '../../store';
import { IDefectPriority } from '../../../models/defect/DefectPriority';
import { getServiceApiUrl } from '../../../Constants/api';

export const defectPriorityApi = createApi({
    reducerPath: 'defectPriorityApi',
    baseQuery: fetchBaseQuery({
        baseUrl: `${getServiceApiUrl()}/condo`,
        prepareHeaders: (headers, { getState }) => {
            const { token } = (getState() as RootState).user;

            if (token) {
                headers.set('Authorization', `Bearer ${token}`);
            }

            return headers;
        },
    }),
    endpoints: builder => ({
        getCondoDefectPriorityLevels: builder.query<
            IDefectPriority[],
            { condoId: number }
        >({
            query: ({ condoId }) => ({
                url: `/${condoId}/defect-priority-level`,
                method: 'GET',
            }),
        }),
    }),
});

export const {
    useGetCondoDefectPriorityLevelsQuery,
    useLazyGetCondoDefectPriorityLevelsQuery,
} = defectPriorityApi;
