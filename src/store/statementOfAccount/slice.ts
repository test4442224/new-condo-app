import { createSlice } from '@reduxjs/toolkit';
import { statementOfAccountApi } from './api';

export interface IStatementOfAccount {
    id: number;
    userId: number;
    balanceAmount: number;
    startDate: Date;
    endDate: Date;
    statementFileLink: string;
    referenceId: string;
    createdAt: Date;
    updatedAt: Date;
}

export interface StatementOfAccountState {
    statementOfAccountArray: IStatementOfAccount[];
    isLoading: boolean;
}

const initialState: StatementOfAccountState = {
    statementOfAccountArray: [],
    isLoading: false,
};

export const statementOfAccountSlice = createSlice({
    name: 'statementOfAccount',
    initialState,
    reducers: {},
    extraReducers: builders => {
        const { getAllStatementOfAccounts } = statementOfAccountApi.endpoints;

        builders
            .addMatcher(getAllStatementOfAccounts.matchPending, state => {
                state.isLoading = true;
            })
            .addMatcher(
                getAllStatementOfAccounts.matchFulfilled,
                (state, action) => {
                    state.isLoading = false;
                    state.statementOfAccountArray = action.payload;
                },
            )
            .addMatcher(getAllStatementOfAccounts.matchRejected, state => {
                state.isLoading = false;
            });
    },
});

export const statementOfAccountAction = statementOfAccountSlice.actions;
export default statementOfAccountSlice.reducer;
