import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { getServiceApiUrl } from '../../Constants/api';
import { RootState } from '../store';
import { IStatementOfAccount } from './slice';

export const statementOfAccountApi = createApi({
    reducerPath: 'statementOfAccountApi',
    baseQuery: fetchBaseQuery({
        baseUrl: getServiceApiUrl(),
        prepareHeaders: (headers, { getState }) => {
            const { token } = (getState() as RootState).user;

            if (token) {
                headers.set('Authorization', `Bearer ${token}`);
            }
        },
    }),
    endpoints: builder => ({
        getAllStatementOfAccounts: builder.query<IStatementOfAccount[], void>({
            query: token => {
                return {
                    url: '/statement-of-account',
                    method: 'GET',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                };
            },
        }),
    }),
});

export const { useLazyGetAllStatementOfAccountsQuery } = statementOfAccountApi;
