import React, { useEffect } from 'react';
import {
    SafeAreaView,
    Platform,
    StatusBar,
    StyleSheet,
    View,
    KeyboardAvoidingView,
} from 'react-native';
import { Provider } from 'react-redux';
import store from './store/store';
import AppNavigation from './navigation/AppNavigation';
import { Appearance } from 'react-native';
import DesignSystem from './utils/DesignSystem';
import Colors from './Constants/Colors';
import Config from 'react-native-config';
import { LogBox } from 'react-native';
import messaging, {
    FirebaseMessagingTypes,
} from '@react-native-firebase/messaging';
import notifee from '@notifee/react-native';

if (Config.ENV === 'test') {
    LogBox.ignoreAllLogs();
}

export default function App() {
    const requestFCMPermission = async () => {
        await messaging().requestPermission();
    };

    const onMessageHandler = (
        remoteMessage: FirebaseMessagingTypes.RemoteMessage,
    ) => {
        const { notification, data } = remoteMessage;
        notifee.displayNotification({
            title: notification!.title,
            body: notification!.body,
            data: data,
        });
    };

    useEffect(() => {
        requestFCMPermission();
        const unsubMessaging = messaging().onMessage(onMessageHandler);
        return () => {
            unsubMessaging();
        };
    }, []);

    DesignSystem.configure();
    useEffect(() => {
        Appearance.setColorScheme('light');
    }, []);

    return (
        <Provider store={store}>
            <SafeAreaView style={styles.safeAreaContainer}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                    style={styles.keyboardAvoidingViewContainer}>
                    <View style={styles.rootContainer}>
                        <AppNavigation />
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </Provider>
    );
}

const styles = StyleSheet.create({
    safeAreaContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
    },
    rootContainer: {
        flex: 1,
        backgroundColor: Colors.$backgroundDefault,
        paddingTop: Platform.OS === 'android' ? 0 : StatusBar.currentHeight,
    },
    keyboardAvoidingViewContainer: {
        flex: 1,
    },
});
