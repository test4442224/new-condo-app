//Uncomment out the URL for android or iOS respectively. Unable to run locally using same endpoints

import { Platform } from 'react-native';
import Config from 'react-native-config';

//ANDROID
export const ANDROID_BACKEND_URL = Config.ANDROID_URL ?? Config.API_URL;
// export const ANDROID_BACKEND_URL = 'http://10.0.2.2:3001';

//iOS
export const IOS_BACKEND_URL = Config.API_URL;
// export const IOS_BACKEND_URL = 'http://localhost:3001';

export const PROD_URL = Config.API_URL;

export const getServiceApiUrl = (): string => {
    if (Config.ENV === 'development' || Config.ENV === 'test') {
        if (Platform.OS === 'android') {
            return ANDROID_BACKEND_URL;
        } else if (Platform.OS === 'ios') {
            return IOS_BACKEND_URL;
        } else {
            return '';
        }
    }

    return PROD_URL;
};

export const getImageUrl = (url: string): string => {
    if (process.env.NODE_ENV === 'development' && url.includes('localhost')) {
        const stringArray = url.split('/');

        let localImageUrl = getServiceApiUrl();

        for (let i = 3; i < stringArray.length; i++) {
            localImageUrl += `/${stringArray[i]}`;
        }

        return localImageUrl;
    } else {
        return url;
    }
};
