const Colors = {
    $backgroundDefault: '#FFFFFF',
    $backgroundDefaultDark: '#0C110F',
    $outlineDefault: '#CBCDCC',
    $textInverted: '#FFFFFF',
    $textDefault: '#0C110F',
    $iconDefault: '#FFFFFF',
    $iconDark: '#0C110F',
    $iconDisabled: '#979090',

    $backgroundPrimaryLight: '#D8DDF7', // for managebooking tab button
    $backgroundPrimary: '#152880',
    $outlinePrimary: '#152880',
    $textPrimary: '#152880',
    $textPrimaryLight: '#243BA5',
    $iconPrimary: '#152880',
    $backgroundIconDefault: '#ED6C02',

    // SUCCESS
    $textSuccess: '#4E8606',
    $backgroundSuccess: '#4E8606',
    $iconSuccess: '#4E8606',
    $backgroundSuccessLight: '#d8eddb', // for password checklist background

    // ERROR
    $textError: '#D11616',
    $backgroundError: '#D11616',
    $iconError: '#D11616',
    $backgroundErrorLight: '#F1B6B2', // for password checklist background

    // PENDING
    $textWarning: '#ED6C02',
    $backgroundWarning: '#E37B00',

    // DISABLE
    $backgroundDisabled: '#CBCDCC',
    $textDisabled: '#979090',
};

export default Colors;
