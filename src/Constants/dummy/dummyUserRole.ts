import { RoleType } from '../../models/userRole/RoleType';
import UserRole from '../../models/userRole/UserRole';

export const DUMMY_USER_ROLE: UserRole = {
    id: 5,
    roleType: RoleType.USER,
    condoId: 1,
    userId: 5,
};
