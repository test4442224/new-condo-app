export const MAX_ATTACHMENT_SIZE_IN_MB = 60;
export const MAX_IMAGE_LIMIT = 6;
export const MAX_VIDEO_LIMIT = 1;
