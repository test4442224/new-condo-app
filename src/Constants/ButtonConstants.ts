import { ButtonSizeProp } from 'react-native-ui-lib/src/components/button/ButtonTypes';

export const ButtonPaddings: Record<ButtonSizeProp, number> = {
    xSmall: 2,
    small: 4,
    medium: 14,
    large: 16,
};

export const ButtonHorizontalPaddings: Record<ButtonSizeProp, number> = {
    xSmall: 11,
    small: 14,
    medium: 16,
    large: 20,
};

export const ButtonMinWidth: Record<ButtonSizeProp, number> = {
    xSmall: 66,
    small: 70,
    medium: 77,
    large: 90,
};
