import { SerializedError } from '@reduxjs/toolkit';
import { FetchBaseQueryError } from '@reduxjs/toolkit/query/react';

export default class ErrorTypeVerificationUtilService {
    static isFetchBaseQueryError(error: unknown): error is FetchBaseQueryError {
        return typeof error === 'object' && error != null && 'status' in error;
    }

    static isSerializedError(error: unknown): error is SerializedError {
        return (
            typeof error === 'object' &&
            error instanceof Error &&
            error.name === 'SerializedError'
        );
    }
}
