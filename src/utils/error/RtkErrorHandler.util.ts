import ErrorTypeVerificationUtilService from './ErrorTypeVerification.util';

interface IGeneralErrorResponse {
    errorMessage: string;
}

export class RtkErrorHandler {
    private static readonly GENERAL_ERROR_MESSAGE =
        'Oops! Something went wrong! Please try again later.';
    private static readonly TIMEOUT_ERROR =
        'Request timeout. Please try again later.';
    private static readonly CONTACT_ADMIN_MESSAGE =
        'Oops! Something went wrong! Please contact condo management for assistance.';
    public static readonly UNAUTHORIZED_MESSAGE =
        'You are not authorized for this resource.';

    static getMessage(error: unknown): string {
        if (ErrorTypeVerificationUtilService.isFetchBaseQueryError(error)) {
            switch (error.status) {
                case 'FETCH_ERROR':
                    return this.GENERAL_ERROR_MESSAGE;
                case 'PARSING_ERROR':
                case 'TIMEOUT_ERROR':
                    return this.TIMEOUT_ERROR;
                case 'CUSTOM_ERROR':
                    return error.error || this.GENERAL_ERROR_MESSAGE;
                case 401:
                case 403:
                    return this.UNAUTHORIZED_MESSAGE;
                case 400:
                case 404:
                    return JSON.stringify(error.data);
                case 500:
                    return this.CONTACT_ADMIN_MESSAGE;
                default:
                    return (
                        (error.data as IGeneralErrorResponse).errorMessage ||
                        this.GENERAL_ERROR_MESSAGE
                    );
            }
        } else if (ErrorTypeVerificationUtilService.isSerializedError(error)) {
            return error.message || this.GENERAL_ERROR_MESSAGE;
        }

        return this.GENERAL_ERROR_MESSAGE;
    }
}
