import { StyleProp, ViewStyle } from 'react-native';
import {
    ManageBookingAttributes,
    PaymentStatus,
} from '../store/manageBooking/slice';
import { isAfterCurrentTime } from './dateTimeUtil';
import Colors from '../Constants/Colors';

export interface IPaymentStatusBadge {
    badgeStatus: string;
    badgeStyle: StyleProp<ViewStyle>;
}

export const generatePaymentStatusBadge = (
    details: ManageBookingAttributes,
) => {
    let badge: IPaymentStatusBadge = { badgeStatus: '', badgeStyle: {} };
    const isNotCompleted = isAfterCurrentTime(details!.endDateTime);

    // PENDING PAYMENT
    if (details.paymentStatus === PaymentStatus.PENDING_PAYMENT) {
        return (badge = {
            badgeStatus: 'PENDING PAYMENT',
            badgeStyle: { backgroundColor: Colors.$backgroundWarning },
        });
    }

    // CANCELLED
    if (details.paymentStatus === PaymentStatus.CANCELLED) {
        return (badge = {
            badgeStatus: 'CANCELLED',
            badgeStyle: { backgroundColor: Colors.$backgroundError },
        });
    }

    // PENDING REFUND
    if (details.paymentStatus === PaymentStatus.PENDING_REFUND) {
        return (badge = {
            badgeStatus: 'PENDING REFUND',
            badgeStyle: { backgroundColor: Colors.$backgroundWarning },
        });
    }

    // REFUNDED
    if (details.paymentStatus === PaymentStatus.REFUNDED) {
        return (badge = {
            badgeStatus: 'REFUNDED',
            badgeStyle: { backgroundColor: Colors.$backgroundError },
        });
    }

    // PAST FREE OR PAID = COMPLETED
    if (!details.isCancellable && !isNotCompleted) {
        return (badge = {
            badgeStatus: 'COMPLETED',
            badgeStyle: { backgroundColor: Colors.$backgroundSuccess },
        });
    }

    // PAID
    if (details.paymentStatus === PaymentStatus.PAID) {
        return (badge = {
            badgeStatus: 'PAID',
            badgeStyle: { backgroundColor: Colors.$backgroundSuccess },
        });
    }

    // UPCOMING FREE = CONFIRMED
    if (details.paymentStatus === PaymentStatus.FREE && isNotCompleted) {
        return (badge = {
            badgeStatus: 'CONFIRMED',
            badgeStyle: { backgroundColor: Colors.$backgroundSuccess },
        });
    }

    // PENDING VERIFICATION
    if (details.paymentStatus === PaymentStatus.PENDING_VERIFICATION) {
        return (badge = {
            badgeStatus: 'PENDING VERIFICATION',
            badgeStyle: { backgroundColor: Colors.$backgroundWarning },
        });
    }

    return badge;
};
