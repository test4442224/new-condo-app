import moment from 'moment';
import { ConvertDateTimeService } from './ConvertDateTimeService';

describe('ConvertDateTimeService', () => {
    const service = new ConvertDateTimeService();
    describe('convertTo12HourClock', () => {
        it('should convert a valid datetime string to 12-hour clock format', () => {
            const input = '2023-09-19T15:30:00.000Z'; // A valid datetime string
            const expectedOutput = '3:30 PM'; // Expected 12-hour clock format
            const result = service.convertTo12HourClock(input);
            expect(result).toBe(expectedOutput);
        });

        it('should convert a valid time string (24-hour clock) to 12-hour clock format', () => {
            const input = '15:30:00'; // A valid time string in 24-hour clock format
            const expectedOutput = '3:30 PM'; // Expected 12-hour clock format
            const result = service.convertTo12HourClock(input);
            expect(result).toBe(expectedOutput);
        });

        it('should not change the input if it is already in 12-hour clock format', () => {
            const input = '3:30 PM'; // A valid time string in 12-hour clock format
            const result = service.convertTo12HourClock(input);
            expect(result).toBe(input);
        });

        it('should handle Date objects and convert them to 12-hour clock format', () => {
            const input = new Date('2023-09-19T15:30:00.000Z'); // A Date object
            const expectedOutput = '3:30 PM'; // Expected 12-hour clock format
            const result = service.convertTo12HourClock(input);
            expect(result).toBe(expectedOutput);
        });

        it('should handle invalid input gracefully and return an error message', () => {
            const input = 'invalid-input'; // An invalid input
            const expectedOutput = 'invalid date format provided: ' + input; // Expected error message
            const result = service.convertTo12HourClock(input);
            expect(result).toBe(expectedOutput);
        });
        it('should handle empty input and return an error message', () => {
            const input = ''; // An empty string
            const expectedOutput = 'invalid date format provided: ' + input; // Expected error message
            const result = service.convertTo12HourClock(input);
            expect(result).toBe(expectedOutput);
        });
    });
    describe('convertDateFormat', () => {
        it('should convert a valid date string to a formatted date string', () => {
            const input = '2023-09-19'; // A valid date string
            const expectedOutput = '19 Sep 2023'; // Expected formatted date string
            const result = service.convertDateFormat(input);
            expect(result).toBe(expectedOutput);
        });

        it('should convert a Date object to a formatted date string', () => {
            const input = new Date('2023-09-19'); // A Date object
            const expectedOutput = '19 Sep 2023'; // Expected formatted date string
            const result = service.convertDateFormat(input);
            expect(result).toBe(expectedOutput);
        });

        it('should handle invalid input gracefully and return an empty string', () => {
            const input = 'invalid-input'; // An invalid input
            const expectedOutput = 'invalid date format provided: Invalid Date'; // Expected error message
            const result = service.convertDateFormat(input);
            expect(result).toBe(expectedOutput);
        });
    });

    describe('calculateTimeDifference', () => {
        it('should calculate the time difference between two valid datetime strings', () => {
            const startDate = '2023-09-19T12:00:00.000Z';
            const endDate = '2023-09-19T13:30:00.000Z';
            const expectedDifference = 5400000; // Milliseconds (1 hour and 30 minutes)
            const result = service.calculateTimeDifference(startDate, endDate);
            expect(result).toBe(expectedDifference);
        });

        it('should handle invalid input gracefully and return NaN', () => {
            const startDate = 'invalid-start-date';
            const endDate = 'invalid-end-date';
            const result = service.calculateTimeDifference(startDate, endDate);
            expect(isNaN(result)).toBe(true);
        });
    });

    describe('addDays', () => {
        it('should add a specified number of days to a valid date string', () => {
            const inputDate = 'invalid-date';
            const daysToAdd = 7;
            const result = service.addDays(inputDate, daysToAdd);

            // Check if the result is null
            expect(result).toBeNull();
        });

        it('should handle invalid input gracefully and return null', () => {
            const inputDate = 'invalid-date';
            const daysToAdd = 7;
            const result = service.addDays(inputDate, daysToAdd);
            expect(result).toBeNull();
        });
    });

    describe('subtractHours', () => {
        it('should subtract a specified number of hours from a valid datetime string', () => {
            const inputDateTime = '2023-09-19T15:30:00.000Z';
            const hoursToSubtract = 3;
            const expectedDateTime = '2023-09-19T12:30:00.000Z';

            // Format the result and expectedDateTime as ISO strings for comparison
            const formattedResult = service
                .subtractHours(inputDateTime, hoursToSubtract)!
                .toISOString();
            const formattedExpectedDateTime =
                moment(expectedDateTime).toISOString();

            expect(formattedResult).toBe(formattedExpectedDateTime);
        });

        it('should handle invalid input gracefully and return null', () => {
            const inputDateTime = 'invalid-datetime';
            const hoursToSubtract = 3;
            const result = service.subtractHours(
                inputDateTime,
                hoursToSubtract,
            );
            expect(result).toBeNull();
        });
    });

    describe('isValidDate', () => {
        it('should return true for a valid date string', () => {
            const input = '2023-09-19'; // A valid date string
            const result = service.isValidDate(input);
            expect(result).toBe(true);
        });

        it('should return true for a valid Date object', () => {
            const input = new Date('2023-09-19'); // A Date object
            const result = service.isValidDate(input);
            expect(result).toBe(true);
        });

        it('should return false for an invalid date string', () => {
            const input = 'invalid-date'; // An invalid date string
            const result = service.isValidDate(input);
            expect(result).toBe(false);
        });
    });

    describe('isDateInRange', () => {
        it('should return true for a date within the specified range', () => {
            const date = '2023-09-19'; // A date within the range
            const startDate = '2023-09-01';
            const endDate = '2023-09-30';
            const result = service.isDateInRange(date, startDate, endDate);
            expect(result).toBe(true);
        });

        it('should return false for a date outside the specified range', () => {
            const date = '2023-10-01'; // A date outside the range
            const startDate = '2023-09-01';
            const endDate = '2023-09-30';
            const result = service.isDateInRange(date, startDate, endDate);
            expect(result).toBe(false);
        });

        it('should handle invalid input gracefully and return false', () => {
            const date = 'invalid-date';
            const startDate = '2023-09-01';
            const endDate = '2023-09-30';
            const result = service.isDateInRange(date, startDate, endDate);
            expect(result).toBe(false);
        });
    });
});
