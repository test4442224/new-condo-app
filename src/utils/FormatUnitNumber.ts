export const formatUnitNumber = (unitNumber: string): string => {
    let [floor, unit] = unitNumber.split('-');
    floor = floor.length === 1 ? '0' + floor : floor;
    unit = unit.length === 1 ? '0' + unit : unit;
    return floor + '-' + unit;
};
