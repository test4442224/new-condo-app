import { StyleProp, ViewStyle } from 'react-native';
import Colors from '../Constants/Colors';

export interface IAttachmentTypeBadge {
    badgeStatus: string;
    badgeStyle: StyleProp<ViewStyle>;
}

export const generateAttachmentTypeBadge = (attachmentType: string) => {
    let badge: IAttachmentTypeBadge = { badgeStatus: '', badgeStyle: {} };

    if (attachmentType === 'Image') {
        return (badge = {
            badgeStatus: 'Image',
            badgeStyle: { backgroundColor: Colors.$backgroundDefaultDark },
        });
    }

    if (attachmentType === 'Video') {
        return (badge = {
            badgeStatus: 'Video',
            badgeStyle: { backgroundColor: Colors.$backgroundDefaultDark },
        });
    }

    return badge;
};
