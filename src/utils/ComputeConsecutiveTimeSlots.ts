import { ITimeSlot } from '../models/booking/TimeSlotModel';

export default class ComputeConsecutiveTimeSlotUtil {
    public static isTimeSlotConsecutive(timeslots: ITimeSlot[]): boolean {
        // passed in the sorted timeslots

        let cloneTimeSlots = [...timeslots];

        cloneTimeSlots.sort((a, b) => {
            return a.slotNumber! - b.slotNumber!;
        });

        for (let i = 0; i < cloneTimeSlots.length - 1; i++) {
            const currentSlot = cloneTimeSlots[i];
            const nextSlot = cloneTimeSlots[i + 1];

            if (currentSlot.end !== nextSlot.start) {
                return false; // not consecutive
            }
        }
        return true;
    }

    public static sortAscendingTimeSlots(timeslots: ITimeSlot[]): ITimeSlot[] {
        const sortedTimestamps = timeslots.slice().sort((a, b) => {
            return a.slotNumber! - b.slotNumber!;
        });
        return sortedTimestamps;
    }

    public static getTimeSlots(timeslots: ITimeSlot[]): ITimeSlot | null {
        if (timeslots.length === 0) {
            return null;
        }

        const isConsecutive = this.isTimeSlotConsecutive(timeslots);

        if (!isConsecutive) {
            return null;
        }

        const firstStartTime = timeslots[0].start;
        const lastEndTime = timeslots[timeslots.length - 1].end;

        return { start: firstStartTime, end: lastEndTime };
    }
}
