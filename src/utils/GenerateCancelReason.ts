import { ManageBookingAttributes } from '../store/manageBooking/slice';
import { to12HourFormat, toDateFormat } from './dateTimeUtil';

export const generateCancelReason = (booking: ManageBookingAttributes) => {
    // NO BOOKING
    if (!booking) return 'Faced an issue retrieiving data.';

    // USER JUST CANCEL BOOKING
    if (!booking.updatedAt) return 'Booking has just been cancelled.';

    // USER CANCEL FREE BOOKING / PENDING PAYMENT
    if (booking.isCancelledByUser && !booking.paidAt && booking.cancelledAt) {
        return `Cancelled on ${toDateFormat(booking.cancelledAt)} by ${
            booking?.user.firstName
        } ${booking?.user.lastName}.`;
    }

    // USER CANCEL PAID BOOKING (PAYNOW & CASH) && NO REFUND YET
    if (
        booking.isCancelledByUser &&
        booking.paymentMethod &&
        booking.paidAt &&
        booking.cancelledAt &&
        !booking.refundedAt
    ) {
        return `Cancelled on ${toDateFormat(
            booking.cancelledAt,
        )} at ${to12HourFormat(
            booking.cancelledAt,
        )}. Please refer to your email inbox for your refund instructions.`;
    }

    // to check after the implementation of verification of paid (paynow) booking
    // USER CANCEL PAID BOOKING && REFUNDED
    if (
        booking.isCancelledByUser &&
        booking.paymentMethod &&
        booking.cancelledAt &&
        booking.refundedAt
    )
        return `Cancelled on ${toDateFormat(
            booking.cancelledAt,
        )} at ${to12HourFormat(
            booking.cancelledAt,
        )}. Refunded on ${toDateFormat(booking.refundedAt)} ${to12HourFormat(
            booking.refundedAt,
        )}.`;

    // ADMIN CANCEL FREE BOOKING && PENDING BOOKING
    if (
        !booking.isCancelledByUser &&
        !booking.paymentMethod &&
        booking.cancelledAt
    )
        return `Cancelled on ${toDateFormat(
            booking?.cancelledAt,
        )} at ${to12HourFormat(
            booking.cancelledAt,
        )} by Condo Management due to '${booking.cancelReason}'`;

    // to check after the implementation of verification of paid (paynow) booking
    // ADMIN CANCEL PAID BOOKING && NO REFUND YET
    if (
        !booking.isCancelledByUser &&
        booking.paymentMethod &&
        booking.cancelledAt &&
        !booking.refundedAt
    )
        return `Cancelled on ${toDateFormat(
            booking.cancelledAt,
        )} at ${to12HourFormat(
            booking.cancelledAt,
        )} by Condo Management due to '${booking.cancelReason}'`;

    // to check after the implementation of verification of paid (paynow) booking
    // ADMIN CANCEL PAID BOOKING && REFUNDED
    if (
        !booking.isCancelledByUser &&
        booking.paymentMethod &&
        booking.cancelledAt &&
        booking.refundedAt
    )
        return `Cancelled on ${toDateFormat(
            booking.cancelledAt,
        )} at ${to12HourFormat(
            booking.cancelledAt,
        )} by Condo Management due to '${
            booking.cancelReason
        }' Refunded on ${toDateFormat(booking.refundedAt)} ${to12HourFormat(
            booking.refundedAt,
        )}.`;
};
