import PageName from '../navigation/PageNameEnum';
import { ConvertDateTimeService } from './ConvertDateTimeService';

/**
 * calculate the booking hours based on start and end time
 * @param pageName: the page name
 * @param start the start datetime / time
 * @param end the end datetime / time
 * @returns the booking hours
 */
export const calculateBookingHours = (
    pageName: string,
    start: string,
    end: string,
): number => {
    let bookingHours: number = 0;
    let startDate: Date;
    let endDate: Date;
    const millisecondsInHour: number = 60 * 60 * 1000;

    if (pageName === PageName.BOOKING_DETAILS_SCREEN) {
        startDate = new Date(start);
        endDate = new Date(end);
        bookingHours =
            (endDate.getTime() - startDate.getTime()) / millisecondsInHour;
    } else if (pageName === PageName.REVIEW_BOOKING_DETAILS_SCREEN) {
        const service = new ConvertDateTimeService();
        startDate = service.convertStringTimeToTime(start);
        endDate = service.convertStringTimeToTime(end);
        bookingHours =
            (endDate.getTime() - startDate.getTime()) / millisecondsInHour;
    }
    return bookingHours;
};
