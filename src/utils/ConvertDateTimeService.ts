import moment from 'moment';
import { Platform } from 'react-native';

export class ConvertDateTimeService {
    /**
     * Converts a date or time string to the 12-hour clock format (e.g., "11:00 AM").
     * Accepts either a date string, Date object, or a valid time string.
     *
     * @param {string | Date} time - The input to convert, which can be a date string, Date object, or valid time string.
     * @returns {string} The time string in 12-hour clock format, or an error message if the input format is invalid.
     */
    public convertTo12HourClock = (time: string | Date): string => {
        const convertTo = 'h:mm A'; // Desired 12-hour clock format
        const AMPMTimeString = 'HH:mm:ss'; // Format for time strings (24-hour clock)
        const dateTimeObjectString = 'YYYY-MM-DDTHH:mm:ss.SSSZ'; // Format for date-time strings

        let momentTime; // Initialize a variable to store the Moment.js object

        // Check if the input is a Date object
        if (time instanceof Date) {
            momentTime = moment(time.toISOString()); // Convert Date to ISO 8601 string and create a Moment.js object
        }

        // Attempt to parse the input as a date-time string
        const isDateTimeObject = moment(time, dateTimeObjectString, true);
        if (isDateTimeObject.isValid()) {
            momentTime = moment(time, dateTimeObjectString); // Create a Moment.js object if the input is a valid date-time string
        }

        // Attempt to parse the input as a time string (24-hour clock)
        const isAMPMTime = moment(time, AMPMTimeString, true);
        if (isAMPMTime.isValid()) {
            momentTime = moment(time, AMPMTimeString); // Create a Moment.js object if the input is a valid time string
        }

        // Attempt to parse the input as the desired 12-hour clock format
        const isCorrectFormat = moment(time, convertTo, true);
        if (isCorrectFormat.isValid()) {
            momentTime = moment(time, convertTo); // Create a Moment.js object if the input is in the desired format
        }

        // Check if a valid Moment.js object was created
        if (momentTime) {
            return momentTime.format(convertTo); // Format the Moment.js object and return it
        } else {
            return 'invalid date format provided: ' + time; // Return an error message if the input format is invalid
        }
    };
    /**
     * Converts a given date or date string to a formatted date string.
     * The date must be in `${year}-${month}-${date}` format!
     * @param {Date | string} date - The input date or date string to convert.
     * @returns {string} The formatted date string (e.g., 'Sep 19, 2023'), or an empty string for invalid input.
     */
    public convertDateFormat = (date: Date | string): string => {
        if (typeof date === 'string') {
            if (Platform.OS === 'android') {
                const [year, month, day] = date.split('-');
                date = `${day} ${month} ${year}`;
            }
            date = new Date(date);
        }

        // Check if the date is valid before formatting
        if (!moment(date).isValid()) {
            return 'invalid date format provided: ' + date; // Return an error message if the input format is invalid
        }

        return moment(date).format('DD MMM YYYY');
    };

    /**
     * Calculates the time difference in milliseconds between two dates or datetime strings.
     *
     * @param {Date | string} startDate - The start date or datetime.
     * @param {Date | string} endDate - The end date or datetime.
     * @returns {number} The time difference in milliseconds.
     */
    public calculateTimeDifference = (
        startDate: Date | string,
        endDate: Date | string,
    ): number => {
        if (typeof startDate === 'string') startDate = new Date(startDate);
        if (typeof endDate === 'string') endDate = new Date(endDate);
        return moment(endDate).diff(moment(startDate), 'milliseconds');
    };

    /**
     * Adds a specified number of days to a given date or date string.
     *
     * @param {Date | string} date - The input date or date string.
     * @param {number} days - The number of days to add.
     * @returns {Date | null} The resulting date after adding the specified days, or null for invalid input.
     */
    public addDays = (date: Date | string, days: number): Date | null => {
        if (typeof date === 'string') date = new Date(date);

        // Check if the date is valid before performing the calculation
        if (!moment(date).isValid()) {
            return null; // Return null for invalid input
        }

        return moment(date).add(days, 'days').toDate();
    };

    /**
     * Subtracts a specified number of hours from a given datetime or datetime string.
     *
     * @param {Date | string} dateTime - The input datetime or datetime string.
     * @param {number} hours - The number of hours to subtract.
     * @returns {Date | null} The resulting datetime after subtracting the specified hours, or null for invalid input.
     */
    public subtractHours = (
        dateTime: Date | string,
        hours: number,
    ): Date | null => {
        if (typeof dateTime === 'string') dateTime = new Date(dateTime);

        // Check if the dateTime is valid before performing the calculation
        if (!moment(dateTime).isValid()) {
            return null; // Return null for invalid input
        }

        return moment(dateTime).subtract(hours, 'hours').toDate();
    };

    /**
     * Checks if a given date or date string is valid.
     *
     * @param {Date | string} date - The input date or date string to validate.
     * @returns {boolean} True if the input is a valid date, otherwise false.
     */
    public isValidDate = (date: Date | string): boolean => {
        if (typeof date === 'string') date = new Date(date);
        return moment(date).isValid();
    };

    /**
     * Checks if a given date or date string is within a specified date range.
     *
     * @param {Date | string} date - The date to check.
     * @param {Date | string} startDate - The start date of the range.
     * @param {Date | string} endDate - The end date of the range.
     * @returns {boolean} True if the date is within the specified range, otherwise false.
     */
    public isDateInRange = (
        date: Date | string,
        startDate: Date | string,
        endDate: Date | string,
    ): boolean => {
        if (typeof date === 'string') date = new Date(date);
        if (typeof startDate === 'string') startDate = new Date(startDate);
        if (typeof endDate === 'string') endDate = new Date(endDate);
        return moment(date).isBetween(startDate, endDate);
    };

    public convertStringDateToDate = (dateString: string) => {
        const [month, dayWithComma, year] = dateString.split(' ');
        const day = parseInt(dayWithComma.replace(',', ''), 10);
        const monthNames = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec',
        ];
        const monthIndex = monthNames.findIndex(
            monthName => monthName.toLowerCase() === month.toLowerCase(),
        );
        const newDateFormat = new Date(
            parseInt(year, 10),
            monthIndex,
            day,
        ).toLocaleDateString('en-US', { timeZone: 'Asia/Singapore' });
        const currentDate = new Date().toLocaleDateString('en-US', {
            timeZone: 'Asia/Singapore',
        });

        return currentDate <= newDateFormat;
    };

    public convertStringTimeToTime = (timeString: string) => {
        const is12HourFormat =
            timeString.includes('PM') || timeString.includes('AM');
        let targetTime;
        if (is12HourFormat) {
            const [time] = timeString.split(' ');
            const [hours, minutes] = time.split(':').map(Number);
            const isPM = timeString.includes('PM') && hours !== 12;
            targetTime = new Date();
            targetTime.setHours(isPM ? hours + 12 : hours, minutes, 0, 0);
        } else {
            const [hours, minutes, seconds] = timeString.split(':');
            targetTime = new Date();
            targetTime.setHours(
                parseInt(hours, 10),
                parseInt(minutes, 10),
                parseInt(seconds, 10),
                0,
            );
        }
        return targetTime;
    };

    public isAfterCurrentTime = (timesString: string) => {
        const targetTime = this.convertStringTimeToTime(timesString);
        const newTimeFormat = new Date(
            targetTime.getTime() + 8 * 60 * 60 * 1000,
        );
        const currentTime = new Date(new Date().getTime() + 8 * 60 * 60 * 1000);
        return currentTime < newTimeFormat;
    };

    public convertDateTimeFormat = (date: Date | string): string => {
        if (typeof date === 'string') {
            const dateObj = new Date(date);

            if (!moment(dateObj).isValid()) {
                return `Invalid date string: ${date}`;
            }

            date = dateObj;
        }
        const newDate = this.convertDateFormat(date);
        const newTime = this.convertTo12HourClock(date);
        const concatenatedDateTime = `${newDate}, ${newTime}`;
        return concatenatedDateTime;
    };
}
