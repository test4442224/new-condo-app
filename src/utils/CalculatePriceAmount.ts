import { IPriceSummary, PriceType } from '../models/booking/PriceBreakdown';

export const calculateSubtotalPriceAmount = (
    unitPriceAmount: number,
    priceType: PriceType,
    bookingHours: number,
): number => {
    if (priceType === PriceType.PER_HOUR) return bookingHours * unitPriceAmount;

    return unitPriceAmount;
};

export const calculateTotalPriceAmount = (
    priceBreakdown: IPriceSummary[],
    bookingHours: number,
): number => {
    let totalPaid = 0;
    priceBreakdown.forEach(item => {
        if (item.priceType === PriceType.PER_HOUR)
            totalPaid += bookingHours * item.unitPriceAmount;
        else totalPaid += +item.unitPriceAmount;
    });

    return +totalPaid;
};
