export const getMaxDate = (day: number) => {
    if (day < 0) {
        throw new Error(
            `Error from getMaxDate(): day cannot be less than 0. Day received "${day}".`,
        );
    }

    const date = new Date();
    date.setDate(date.getDate() + day);
    return date;
};

export const getDatesFromRange = (start: Date, end: Date): Date[] => {
    const dates = [] as Date[];

    for (
        let date = new Date(start);
        date.getTime() < end.getTime();
        date.setDate(date.getDate() + 1)
    ) {
        dates.push(new Date(date));
    }

    return dates;
};
/**
 * Converts a given date object to a formatted date string.
 * @param {Date } dateObj Date object
 * @returns {string} the respose string in `${year}-${month}-${date}` format.
 */
export const convertDateToDateString = (dateObj: Date): string => {
    const year = dateObj.getFullYear();
    const month = dateObj.getMonth() + 1;
    const date = dateObj.getDate();

    const dateString = date.toString().padStart(2, '0');
    const monthString = month.toString().padStart(2, '0');

    return `${year}-${monthString}-${dateString}`;
};
