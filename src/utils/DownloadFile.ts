import { Platform, PermissionsAndroid } from 'react-native';
import ReactNativeBlobUtil from 'react-native-blob-util';
import Share from 'react-native-share';
import Toast from 'react-native-simple-toast';

export default function downloadFile(fileName: string, fileUrl: string) {
    ReactNativeBlobUtil.config({
        fileCache: true,
        appendExt: 'pdf',
        addAndroidDownloads: {
            useDownloadManager: true,
            notification: true,
            title: fileName,
            description: 'File downloaded by download manager.',
            mime: 'application/pdf',
        },
    })
        .fetch('GET', fileUrl)
        .then(res => {
            if (Platform.OS === 'ios') {
                const filePath = res.path();
                ReactNativeBlobUtil.fs.exists(filePath).then(exists => {
                    if (exists) {
                        let options = {
                            type: 'application/pdf',
                            url: filePath,
                            saveToFiles: true,
                            failOnCancel: false,
                        };
                        Share.open(options)
                            .then(_resp => {
                                Toast.show(
                                    'Successfully downloaded file!',
                                    Toast.LONG,
                                );
                            })
                            .catch(err => console.log(err));
                    } else {
                        Toast.show('Failed to download file.', Toast.LONG);
                    }
                });
            } else if (Platform.OS === 'android') {
                PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                ).then(granted => {
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        Toast.show('Successfully downloaded file!', Toast.LONG);
                    } else {
                        Toast.show('Permission denied.', Toast.LONG);
                    }
                });
            }
        })
        .catch(err => Toast.show(err.message, Toast.LONG));
}
