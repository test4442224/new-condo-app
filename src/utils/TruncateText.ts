export const trimTruncatedText = (text: string, maxLength: number) => {
    const trimmedText = text.replace(/\s{2,}/g, ' ');

    if (trimmedText.length <= maxLength) return trimmedText;

    const lastSpaceIndex = trimmedText.lastIndexOf(' ', maxLength);
    if (lastSpaceIndex !== -1)
        return trimmedText.substring(0, lastSpaceIndex) + '...';
    else return trimmedText.substring(0, maxLength) + '...';
};
