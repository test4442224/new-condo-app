import { StyleProp, ViewStyle } from 'react-native';
import { DefectStatus } from '../models/defect/DefectStatusHistory';
import Colors from '../Constants/Colors';

export interface IDefectStatusBadge {
    badgeStatus: string;
    badgeStyle: StyleProp<ViewStyle>;
}

export const generateDefectStatusBadge = (status: string) => {
    let badge: IDefectStatusBadge = { badgeStatus: '', badgeStyle: {} };

    // IN PROGRESS
    if (status === DefectStatus.IN_PROGRESS) {
        return (badge = {
            badgeStatus: 'IN PROGRESS',
            badgeStyle: { backgroundColor: Colors.$backgroundWarning },
        });
    }

    // COMPLETED
    if (status === DefectStatus.COMPLETED) {
        return (badge = {
            badgeStatus: 'COMPLETED',
            badgeStyle: { backgroundColor: Colors.$backgroundSuccess },
        });
    }

    // CANCELLED
    if (status === DefectStatus.CANCELLED) {
        return (badge = {
            badgeStatus: 'CANCELLED',
            badgeStyle: { backgroundColor: Colors.$backgroundError },
        });
    }

    // UNASSIGNED
    if (status === DefectStatus.UNASSIGNED) {
        return (badge = {
            badgeStatus: 'SUBMITTED',
            badgeStyle: { backgroundColor: Colors.$backgroundPrimary },
        });
    }

    return badge;
};
