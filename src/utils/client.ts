import axios from 'axios';
import { store } from '../store/store';
import { userActions } from '../store/user/UserActions';
import { getServiceApiUrl } from '../Constants/api';
import AsyncStorage from '@react-native-async-storage/async-storage';

const clientWithAuth = axios.create({
    baseURL: getServiceApiUrl(),
    withCredentials: true,
});

const client = axios.create({
    baseURL: getServiceApiUrl(),
});

clientWithAuth.interceptors.request.use(async request => {
    const token = await AsyncStorage.getItem('token');
    request.headers.Authorization = `Bearer ${token}`;
    return request;
});

clientWithAuth.interceptors.response.use(
    response => response,
    error => {
        if (
            error.response &&
            (error.response.status === 401 || error.response.status === 403)
        ) {
            store.dispatch(
                userActions.forceLogout(error.response.data.errorMessage),
            );

            AsyncStorage.clear();
        }

        throw error;
    },
);

export { clientWithAuth };
export default client;
