import moment from 'moment';

type DateOrString = Date | string;

const isSameDay = (firstDate: Date, secondDate: Date): boolean => {
    return moment(firstDate).isSame(secondDate, 'day');
};

const parsedDate = (date: DateOrString): Date => {
    if (typeof date === 'string') {
        return moment(date).toDate();
    }

    return date;
};

const isToday = (date: DateOrString): boolean =>
    isSameDay(parsedDate(date), new Date());

const isAfter = (firstDate: DateOrString, secondDate: DateOrString): boolean =>
    moment(parsedDate(firstDate)).isAfter(parsedDate(secondDate));

const isAfterCurrentTime = (date: DateOrString): boolean =>
    isAfter(date, new Date());

const isBeforeCurrentTime = (date: DateOrString): boolean =>
    !isAfter(date, new Date());

const to12HourFormat = (date: DateOrString): string =>
    moment(parsedDate(date)).format('h:mm A');

const toDateFormat = (date: DateOrString, short: boolean = false): string =>
    moment(parsedDate(date)).format(short ? 'D MMM YY' : 'D MMM YYYY');

const toDateTimeFormat = (date: DateOrString): string =>
    moment(parsedDate(date)).format('D MMM YYYY, h:mm A');

const formatTimeRange = ({
    start,
    end,
    separator,
}: {
    start: DateOrString;
    end: DateOrString;
    separator: string;
}) => {
    const startRange = to12HourFormat(parsedDate(start));
    const endRange = to12HourFormat(parsedDate(end));
    return `${startRange}${separator}${endRange}`;
};

function concatDateAndTime(date: string, time: string): string;
function concatDateAndTime(date: Date, time: string): Date;
function concatDateAndTime(date: DateOrString, time: string): DateOrString {
    // to extract out time as its coming in as an date object from the backend due to change in moment to date-fns
    time =
        time.includes('T') && time.includes('.000Z')
            ? time.split('T')[1].split('.000Z')[0]
            : time;

    if (typeof date === 'string') {
        return `${date.split('T')[0]}T${time}`;
    }
    return new Date(`${date.toISOString().split('T')[0]}T${time}`);
}

const addTimeFromString = (date: Date, time: string): Date => {
    const timeRegex = /^([01]\d|2[0-3]):([0-5]\d)$/;
    if (!timeRegex.test(time)) {
        throw new Error('Invalid time format - must be HH:mm');
    }

    const times = time.split(':');
    return moment(date)
        .add(times[0], 'hours')
        .add(times[1], 'minutes')
        .toDate();
};

export {
    isSameDay,
    isToday,
    addTimeFromString,
    isAfter,
    isAfterCurrentTime,
    isBeforeCurrentTime,
    concatDateAndTime,
    to12HourFormat,
    toDateFormat,
    formatTimeRange,
    toDateTimeFormat,
};
