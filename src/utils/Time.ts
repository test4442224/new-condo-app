export const getTimeString = (dateObj: Date): string => {
    if (Number.isNaN(dateObj.getTime())) {
        return '';
    }

    let hours: number = dateObj.getHours();
    const minutes: number = dateObj.getMinutes();
    let isAm = true;

    if (hours >= 12) {
        isAm = false;
        hours = hours > 12 ? hours - 12 : hours;
    }

    if (hours === 0) {
        hours = 12;
    }

    const hoursInString = hours.toString().padStart(1, '0');
    const minutesInString = minutes.toString().padStart(2, '0');

    return `${hoursInString}:${minutesInString} ${isAm ? 'AM' : 'PM'}`;
};
