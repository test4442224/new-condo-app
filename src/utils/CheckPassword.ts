export const checkPassword = (password: string) => {
    if (
        password.length < 8 ||
        !/[A-Z]/.test(password) ||
        !/[a-z]/.test(password) ||
        !/\d/.test(password) ||
        !/[!@#$%^&*()_+{}[\]:;<>,.?~\\-]/.test(password)
    ) {
        return 'Password does not meet requirements, please try again.';
    }
    return 'strong';
};
