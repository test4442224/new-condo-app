import { useCallback, useEffect } from 'react';
import { useAppDispatch } from '../../store/hook';
import { loadingActions } from '../../store/loading/slice';
import { RtkErrorHandler } from '../error/RtkErrorHandler.util';
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query';
import { SerializedError } from '@reduxjs/toolkit';
import Toast from 'react-native-simple-toast';
import ErrorTypeVerificationUtilService from '../error/ErrorTypeVerification.util';
import { userActions } from '../../store/user/UserActions';
import AsyncStorage from '@react-native-async-storage/async-storage';

const { setLoading } = loadingActions;

const useRtkResponse = (
    isFetching: boolean,
    error: FetchBaseQueryError | SerializedError | undefined,
    showToast: boolean = true,
) => {
    const dispatch = useAppDispatch();

    const logout = useCallback(async () => {
        dispatch(userActions.logout());

        await AsyncStorage.clear();
    }, [dispatch]);

    useEffect(() => {
        dispatch(setLoading(isFetching));
        if (!error) {
            return;
        }
        if (error) {
            const isFetchBaseQueryError =
                ErrorTypeVerificationUtilService.isFetchBaseQueryError(error);

            if (isFetchBaseQueryError && error.status === 401) {
                Toast.show(
                    'Your session has ended. Please login again.',
                    Toast.LONG,
                );

                logout();

                return;
            }
            if (showToast) {
                const errorMessage = RtkErrorHandler.getMessage(error);

                Toast.show(errorMessage, Toast.LONG);
            }
        }
    }, [isFetching, error, dispatch, logout, showToast]);
};

export default useRtkResponse;
