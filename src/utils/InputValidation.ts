export class InputValidation {
    public validateNameInputs(name: string) {
        let nameError = '';
        if (name.trim().length === 0) {
            nameError = 'Please enter a valid name.';
        }
        return nameError;
    }
    public validateEmailInput(email: string) {
        let emailError = '';
        if (!email.match(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)) {
            emailError = 'Please enter a valid email address.';
        }
        return emailError;
    }
    public validatePhoneNumberInput(phoneNumber: string) {
        let phoneNumberError = '';
        if (
            Number.isNaN(Number(phoneNumber)) ||
            Number(phoneNumber) < 10000000 ||
            Number(phoneNumber) > 99999999
        ) {
            phoneNumberError = 'Please enter a valid phone number.';
        }
        return phoneNumberError;
    }
    public static containsAlphanumeric(inputString: string): boolean {
        return /\w/.test(inputString);
    }
    public static checkInputsNotEmpty(inputs: string[]): boolean {
        for (const input of inputs) {
            if (!this.containsAlphanumeric(input)) {
                return false;
            }
        }
        return true;
    }
}
