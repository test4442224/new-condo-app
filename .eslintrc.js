module.exports = {
    root: true,
    extends: ['@react-native', 'plugin:prettier/recommended', 'prettier'],
    plugins: ['prettier'],
    rules: {
        'prettier/prettier': 'error',
        'react/react-in-jsx-scope': 'off',
        'react/jsx-uses-react': 'off',
    },
};
